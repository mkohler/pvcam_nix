{
  pkgs ? import (
    # b3083bc6933e is nixos-21.05 as of 2021-09-14.
    fetchTarball https://github.com/NixOS/nixpkgs/archive/b3083bc6933eb7fa4ee7bd4802e9f72b56f3e654.tar.gz)
    {
      overlays = [ (import ./overlays.nix) ];
    }
}:
with pkgs;
let
  pvcam = callPackage ./pvcam {
  inherit 
  stdenv
  dpkg
  gcc-unwrapped
  glibc
  libtiff
  libusb1
  patchelf
  wxGTK30
  ;
};
in
mkShell {
  buildInputs = [
    pvcam
    imagej
  ];
}

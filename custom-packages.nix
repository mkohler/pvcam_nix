# REMINDER: I'm not actually using this pinned version of nixpkgs because
# it doesn't have support for patchelf --clear-symbol-version.
{ system ? builtins.currentSystem,
  pkgs ? import (
    fetchTarball https://github.com/NixOS/nixpkgs/archive/6a3f5bcb061e1822f50e299f5616a0731636e4e7.tar.gz)
  {}
}:
with pkgs;
rec {

  pvcam = callPackage ./pvcam {
    inherit 
    stdenv
    dpkg
    gcc-unwrapped
    glibc
    libtiff
    libusb1
    patchelf
    wxGTK30
    ;
   };
}



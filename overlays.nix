self: super:
  let
    unstable_pkgs =
    import (
      fetchTarball
      # bcd607489d76 is nixos-unstable as of 2021-09-14.
      https://github.com/NixOS/nixpkgs/archive/bcd607489d76795508c48261e1ad05f5d4b7672f.tar.gz
      )
    {};
  in
  {
    imagej = unstable_pkgs.imagej;
  }


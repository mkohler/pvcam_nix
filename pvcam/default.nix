{
  stdenv,
  dpkg,
  gcc-unwrapped,
  glibc,
  libtiff,
  libusb1,
  patchelf,
  wxGTK30,
}:
stdenv.mkDerivation rec {
  version = "3.8.4.5-1";
  name = "pvcam-${version}";
  system = "x86_64-linux";
  src = ./.;

  nativeBuildInputs = [
    dpkg
    patchelf
  ];

  buildCommand = ''
    set -o xtrace
    pvcam=$out/pvcam
    pvcam_lib=$pvcam/opt/pvcam/library/x86_64
    pvcam_umd=$pvcam/opt/pvcam/drivers/user-mode/pvcam_usb.x86_64.umd
    mkdir -p $pvcam $pvcam_lib

    # Unpack libpvcam and libpvcamDDI into the Nix store.
    dpkg-deb -x ${src}/pvcam_3.8.4.5-1_amd64.deb $pvcam

    # Patch libpvcam. It needs glibc and std C++ libs, but it also needs its own
    # directory because it uses dlopen to load libpvcamDDI.so.
    patchelf --set-rpath $pvcam_lib:${glibc}/lib:${stdenv.cc.cc.lib}/lib $pvcam_lib/libpvcam.so

    # Patch libpvcamDDI. It needs libusb.
    patchelf --set-rpath $pvcam_lib:${glibc}/lib:${stdenv.cc.cc.lib}/lib:${libusb1}/lib $pvcam_lib/libpvcamDDI.so

    # Patch pvcam_usb.x86_64.umd
    patchelf --set-rpath ${libusb1}/lib:${glibc}/lib:${stdenv.cc.cc.lib}/lib $pvcam_umd

    # Unpack pvcam SDK, which has the header files and example apps.
    dpkg-deb -x ${src}/pvcam-sdk_3.8.4.5-1_amd64.deb $pvcam

    # Patch PVCamTestCli.
    #
    pvcam_bin=$pvcam/opt/pvcam/sdk/examples/PVCamTest/bin/linux-x86_64/release
    patchelf \
      --set-interpreter ${stdenv.cc.libc}/lib/ld-linux-x86-64.so.2 \
      $pvcam_bin/PVCamTestCli

    patchelf \
      --set-rpath \
      $pvcam_lib:${glibc}/lib:${stdenv.cc.cc.lib}/lib:${libtiff.out}/lib:${libusb1}/lib \
      $pvcam_bin/PVCamTestCli

    # PVCamTestCli wants versioned symbols, but the NixOS libtiff doesn't have
    # versioned symbols. We can patch PVCamTestCli so it doesn't need the
    # versions of the symbols.
    patchelf --clear-symbol-version LIBTIFF $pvcam_bin/PVCamTestCli
  '';

  meta = with stdenv.lib; {
    description = "Photometrics PVCAM libraries";
    platforms = [ "x86_64-linux" ];
  };
}
#PVCamTestCLi
#  libpvcam (via dlopen)
#    libpthread (part of glibc)
#    libstdc++
#    libpvcamDDI
#  libtiff
#  libusb
#
#  So
#
#  libpvcamDDI
#     no patching
#
#  libpvcam
#     the current directory needs to be added to RPATH so that it can find libpvcamddi
#
#
#
#

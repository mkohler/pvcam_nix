#!/bin/bash

echo "Cannot uninstall package installed from .deb file." >&2
echo "Use 'dpkg --purge pvcam-sdk' to uninstall it first." >&2
exit 1

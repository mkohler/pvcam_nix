/******************************************************************************/
/* Copyright (C) Teledyne Photometrics. All rights reserved.                  */
/******************************************************************************/
#pragma once
#ifndef PM_REGION_SHAPE_CTRL_H
#define PM_REGION_SHAPE_CTRL_H

// WxWidgets headers have to be included first
#include <wx/dataview.h>
#include <wx/scrolwin.h>

/* Local */
#include "backend/Frame.h"
#include "IRegionMouseMoveListener.h"
#include "ModelNotifier.h"
#include "Region.h"
#include "RegionModel.h"

/* System */
#include <mutex>
#include <vector>

// Holds no data
wxDECLARE_EVENT(PM_EVT_REFRESH_IMAGE, wxThreadEvent);

namespace pm {

class RegionShapeCtrl : public wxWindow, private IModelNotifier
{
private:
    enum class HandleType : int
    {
        None,
        Body,
        Top,
        Bottom,
        Left,
        Right,
        TopLeft,
        TopRight,
        BottomLeft,
        BottomRight,
    };

public:
    enum class OutlineShape : int
    {
        Rectangle,
        Ellipse,
    };

public:
    RegionShapeCtrl(wxWindow* parent);
    virtual ~RegionShapeCtrl();

public:
    bool AssociateModel(RegionModel* model);
    RegionModel* GetModel();
    const RegionModel* GetModel() const;

    std::shared_ptr<Region> GetActiveRegion() const;

    void EnableEditor(bool enabled = true);

    void SetRegionMouseMoveListener(IRegionMouseMoveListener* listener);

    void SetNonBinnedCanvasSize(const wxSize& size);
    void SetImageSize(const wxSize& size); // For SetImage
    void SetImagesSize(const std::vector<wxSize>& sizes); // For SetImages
    void SetOutlineShape(OutlineShape shape);
    void SetZoom(double zoom);

    // Can be called from other than GUI thread
    void SetImage(const Bitmap* image, const Frame::Point& offset,
            const Frame::Point& canvasOffset, uint16_t sbin, uint16_t pbin);
    // Can be called from other than GUI thread
    void SetImages(const std::vector<std::unique_ptr<Bitmap>>& images,
            const std::vector<Frame::Point>& offsets, size_t validCount,
            const Frame::Point& canvasOffset, uint16_t sbin, uint16_t pbin,
            const wxColour& fillColor,
            const std::vector<wxRect>& outlines = std::vector<wxRect>(),
            const Frame::Trajectories& trajectories = Frame::Trajectories());

private: // UI event handlers
    void OnSetCursor(wxSetCursorEvent& event);
    void OnPaint(wxPaintEvent& event);
    void OnMouseLeftDown(wxMouseEvent& event);
    void OnMouseLeftUp(wxMouseEvent& event);
    void OnMouseMove(wxMouseEvent& event);
    void OnMouseLeave(wxMouseEvent& event);
    void OnMouseCaptureLost(wxMouseCaptureLostEvent& event);

private: // Any thread to GUI thread "forwarders"
    void FireEventRefreshImage();
    void DoRefreshImage();

private:
    void DrawBackground(wxDC& dc, const wxRect& rect);
    void DrawImage(wxDC& dc, const wxRect& rect); // For SetImage
    void DrawImages(wxDC& dc, const wxRect& rect); // For SetImages
    void DrawRegions(wxDC& dc, const wxRect& rect);
    void DrawOutlines(wxDC& dc, const wxRect& rect);
    void DrawTrajectories(wxDC& dc, const wxRect& rect);
    void DrawOverlappingAreas(wxDC& dc, const wxRect& rect);
    void DrawRegionHandles(wxDC& dc, const wxRect& rect);

    HandleType GetHandleType(const wxPoint& point) const;
    std::shared_ptr<Region> FindRegion(const wxPoint& point) const;
    void SetActiveRegion(std::shared_ptr<Region> region);
    void UpdateRegionHandlesRect();

    wxRect GetViewportRect() const; // Return zoomed area
    wxRect GetNonZoomedRect(const wxRect& rect) const;

    void UpdateRegionMouseMoveListener();

private: // IModelNotifier
    // Set friendship so overridden methods can be private
    friend class ModelNotifier;
    virtual bool ItemAdded(const wxDataViewItem& parent,
            const wxDataViewItem& item) override;
    virtual bool ItemDeleted(const wxDataViewItem& parent,
            const wxDataViewItem& item) override;
    virtual bool ItemChanged(const wxDataViewItem& item) override;
    virtual bool ItemsAdded(const wxDataViewItem& parent,
            const wxDataViewItemArray& items) override;
    virtual bool ItemsDeleted(const wxDataViewItem& parent,
            const wxDataViewItemArray& items) override;
    virtual bool ItemsChanged(const wxDataViewItemArray& items) override;
    virtual bool Cleared() override;
    virtual void Resort() override;
    virtual bool ValueChanged(const wxDataViewItem& item, unsigned int col)
        override;

private:
    wxScrolledWindow* m_scrollParent{ nullptr };

    RegionModel* m_model{ nullptr };
    ModelNotifier* m_notifier{ nullptr };

    bool m_isEditorEnabled{ false };

    IRegionMouseMoveListener* m_regionMouseMoveListener{ nullptr };
    int m_regionMousePosX{ -1 };
    int m_regionMousePosY{ -1 };

    const std::vector<HandleType> m_handleTypes{
        //HandleType::None, // Just for completeness, not a handle
        HandleType::Body,
        HandleType::Top,
        HandleType::Bottom,
        HandleType::Left,
        HandleType::Right,
        HandleType::TopLeft,
        HandleType::TopRight,
        HandleType::BottomLeft,
        HandleType::BottomRight,
    };
    // Includes HandleType::BODY
    const std::map<HandleType, wxCursor> m_handleCursors{
        { HandleType::None, wxCursor(wxCURSOR_ARROW) },
        { HandleType::Body, wxCursor(wxCURSOR_SIZING) },
        { HandleType::Top, wxCursor(wxCURSOR_SIZENS) },
        { HandleType::Bottom, wxCursor(wxCURSOR_SIZENS) },
        { HandleType::Left, wxCursor(wxCURSOR_SIZEWE) },
        { HandleType::Right, wxCursor(wxCURSOR_SIZEWE) },
        { HandleType::TopLeft, wxCursor(wxCURSOR_SIZENWSE) },
        { HandleType::TopRight, wxCursor(wxCURSOR_SIZENESW) },
        { HandleType::BottomLeft, wxCursor(wxCURSOR_SIZENESW) },
        { HandleType::BottomRight, wxCursor(wxCURSOR_SIZENWSE) },
    };
    // Excludes HandleType::BODY
    std::map<HandleType, wxRect> m_handleRects{};

    wxRect m_canvasRect{};
    OutlineShape m_outlineShape{ OutlineShape::Rectangle };

    std::shared_ptr<Region> m_activeRegion{ nullptr };
    bool m_activeRegionEditing{ false }; // Mouse is captured and left button done
    wxRect m_activeRegionRect{}; // Bounding rectangle of region and all handles
    wxPoint m_startPoint{}; // Where left mouse button went down
    wxRect m_startRect{}; // Original region rectangle
    HandleType m_startHandleType{ HandleType::None };
    bool m_showHandles{ false };

    double m_zoom{ 1.0 };

    std::mutex m_mutex{};

    bool m_doRecompose{ false }; // Relies on SetImage if false, SetImages otherwise
    bool m_imagesChanged{ true };

    // Used when m_doRecompose == false
    wxImage m_image{};
    Frame::Point m_offset{ 0, 0 };
    wxBitmap m_bmp{};

    // Used when m_doRecompose == true
    std::vector<wxImage> m_images{};
    std::vector<Frame::Point> m_offsets{};
    std::vector<wxBitmap> m_bmps{};

    size_t m_validCount{ 0 };
    Frame::Point m_canvasOffset{ 0, 0 };
    uint16_t m_sbin{ 0 }; // Serial binning
    uint16_t m_pbin{ 0 }; // Parallel binning
    wxColour m_fillColor{ *wxBLACK };
    std::vector<wxRect> m_outlines{};
    Frame::Trajectories m_trajectories{};
};

} // namespace pm

#endif // PM_REGION_SHAPE_CTRL_H

/******************************************************************************/
/* Copyright (C) Teledyne Photometrics. All rights reserved.                  */
/******************************************************************************/
#pragma once
#ifndef PM_WXAPPLICATION_H
#define PM_WXAPPLICATION_H

// WxWidgets headers have to be included first
#include <wx/app.h>

/* Local */
#include "MainDlg.h"

namespace pm {

class PVCamTestApp final : public wxApp
{
public:
    PVCamTestApp();
    virtual ~PVCamTestApp();

public: // wxApp
    virtual bool OnInit() override;

protected: // UI event handlers
    virtual void OnActivateApp(wxActivateEvent& event);

private:
    MainDlg* m_mainDlg{ nullptr };
};

} // namespace pm

#endif // PM_WXAPPLICATION_H

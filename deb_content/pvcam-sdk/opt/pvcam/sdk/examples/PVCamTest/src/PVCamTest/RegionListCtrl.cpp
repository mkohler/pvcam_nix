/******************************************************************************/
/* Copyright (C) Teledyne Photometrics. All rights reserved.                  */
/******************************************************************************/
#include "RegionListCtrl.h"

// WxWidgets headers have to be included first
#include <wx/menu.h>

/* Local */
#include "Region.h"
#include "RegionModel.h"

/* System */
#include <vector>

// pm::RegionListColumn

pm::RegionListColumn::RegionListColumn(const wxString& title,
        wxDataViewRenderer* renderer, unsigned int modelColumn, int width,
        wxAlignment align, int flags)
    : wxDataViewColumn(title, renderer, modelColumn, width, align,
            flags & ~(wxDATAVIEW_COL_REORDERABLE | wxDATAVIEW_COL_HIDDEN
                | ((modelColumn != 0) ? wxDATAVIEW_COL_SORTABLE : 0)))
{
}

pm::RegionListColumn::RegionListColumn(const wxBitmap& bitmap,
        wxDataViewRenderer* renderer, unsigned int modelColumn, int width,
        wxAlignment align, int flags)
    : wxDataViewColumn(bitmap, renderer, modelColumn, width, align,
            flags & ~(wxDATAVIEW_COL_REORDERABLE | wxDATAVIEW_COL_HIDDEN
                | ((modelColumn != 0) ? wxDATAVIEW_COL_SORTABLE : 0)))
{
}

void pm::RegionListColumn::SetFlags(int flags)
{
    flags &= ~(wxDATAVIEW_COL_REORDERABLE | wxDATAVIEW_COL_HIDDEN);
    if (GetModelColumn() != 0)
    {
        flags &= ~(wxDATAVIEW_COL_SORTABLE);
    }
    wxDataViewColumn::SetFlags(flags);
}

void pm::RegionListColumn::SetSortable(bool sortable)
{
    wxDataViewColumn::SetSortable(sortable && GetModelColumn() == 0);
}

void pm::RegionListColumn::SetReorderable(bool WXUNUSED(reorderable))
{
    wxDataViewColumn::SetReorderable(false);
}

void pm::RegionListColumn::SetHidden(bool WXUNUSED(hidden))
{
    wxDataViewColumn::SetHidden(false);
}

int pm::RegionListColumn::GetFlags() const
{
    int flags = wxDataViewColumn::GetFlags();
    flags &= ~(wxDATAVIEW_COL_REORDERABLE | wxDATAVIEW_COL_HIDDEN);
    if (GetModelColumn() != 0)
    {
        flags &= ~(wxDATAVIEW_COL_SORTABLE);
    }
    return flags;
}

bool pm::RegionListColumn::IsSortable() const
{
    return (wxDataViewColumn::IsSortable() && GetModelColumn() == 0);
}

bool pm::RegionListColumn::IsReorderable() const
{
    return false;
}

bool pm::RegionListColumn::IsHidden() const
{
    return false;
}

// pm::RegionListCtrl

pm::RegionListCtrl::RegionListCtrl()
    //: wxDataViewListCtrl(),
{
}

pm::RegionListCtrl::RegionListCtrl(wxWindow* parent, wxWindowID id,
        const wxPoint& pos, const wxSize& size, long style)
    //: wxDataViewListCtrl(),
{
    Create(parent, id, pos, size, style);
}

pm::RegionListCtrl::~RegionListCtrl()
{
    if (GetModel() && m_notifier)
    {
        GetModel()->RemoveNotifier(m_notifier);
    }
}

bool pm::RegionListCtrl::Initialize(const wxSize& sensorSize, int maxRegions,
        wxDataViewCellMode columnsMode)
{
    m_updatingSelection = false;

    if (!sensorSize.IsFullySpecified())
        return false;

    if (maxRegions < 1)
        return false;

    if (!wxDataViewListCtrl::ClearColumns())
        return false;

    // Capacity 5 columns, data view list takes ownership of it
    std::vector<wxDataViewRenderer*> renderers(5);
    renderers[0] = new(std::nothrow) wxDataViewSpinRenderer(
            0, maxRegions - 1, wxDATAVIEW_CELL_INERT,
            wxALIGN_CENTER_VERTICAL | wxALIGN_CENTER_HORIZONTAL);
    renderers[1] = new(std::nothrow) wxDataViewSpinRenderer(
            0, sensorSize.GetWidth(), columnsMode,
            wxALIGN_CENTER_VERTICAL | wxALIGN_RIGHT);
    renderers[2] = new(std::nothrow) wxDataViewSpinRenderer(
            0, sensorSize.GetHeight(), columnsMode,
            wxALIGN_CENTER_VERTICAL | wxALIGN_RIGHT);
    renderers[3] = new(std::nothrow) wxDataViewSpinRenderer(
            1, sensorSize.GetWidth(), columnsMode,
            wxALIGN_CENTER_VERTICAL | wxALIGN_RIGHT);
    renderers[4] = new(std::nothrow) wxDataViewSpinRenderer(
            1, sensorSize.GetHeight(), columnsMode,
            wxALIGN_CENTER_VERTICAL | wxALIGN_RIGHT);

    for (auto renderer : renderers)
    {
        if (!renderer)
            return false;
    }

    // TODO: Does not work, why?
    //auto attr = renderers[0]->GetAttr();
    //attr.SetBold(true);
    //attr.SetBackgroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_BTNFACE));
    //renderers[0]->SetAttr(attr);

    // Capacity 5 columns, data view list takes ownership of it
    std::vector<RegionListColumn*> columns(5);
    columns[0] = new(std::nothrow) RegionListColumn(_("Nr."),
            renderers[0], 0, 40, wxALIGN_CENTER);
    columns[1] = new(std::nothrow) RegionListColumn(_("Left"),
            renderers[1], 1, 50, wxALIGN_RIGHT);
    columns[2] = new(std::nothrow) RegionListColumn(_("Top"),
            renderers[2], 2, 50, wxALIGN_RIGHT);
    columns[3] = new(std::nothrow) RegionListColumn(_("Width"),
            renderers[3], 3, 50, wxALIGN_RIGHT);
    columns[4] = new(std::nothrow) RegionListColumn(_("Height"),
            renderers[4], 4, 50, wxALIGN_RIGHT);

    for (auto column : columns)
    {
        if (!column)
            return false;
    }

    for (auto column : columns)
    {
        // Call base class method, this class does not allow column manipulation
        if (!wxDataViewListCtrl::AppendColumn(column))
            return false;
    }

    Unbind(wxEVT_DATAVIEW_SELECTION_CHANGED,
            &RegionListCtrl::OnSelectionChanged, this);
    Bind(wxEVT_DATAVIEW_SELECTION_CHANGED,
            &RegionListCtrl::OnSelectionChanged, this);

    return true;
}

bool pm::RegionListCtrl::AssociateModel(wxDataViewModel* model)
{
    if (GetModel() && m_notifier)
    {
        GetModel()->RemoveNotifier(m_notifier);
        m_notifier = nullptr;
    }

    if (!wxDataViewListCtrl::AssociateModel(model))
        return false;

    if (GetModel())
    {
        m_notifier = new(std::nothrow) ModelNotifier(this);
        if (!m_notifier)
        {
            return false;
        }
        GetModel()->AddNotifier(m_notifier);
    }

    Refresh();

    return true;
}

void pm::RegionListCtrl::UpdateSelection(bool fromModel)
{
    RegionModel* model = dynamic_cast<RegionModel*>(GetModel());
    if (!model)
        return;

    if (m_updatingSelection)
        return;

    m_updatingSelection = true;

    for (auto region : model->GetRegions())
    {
        const wxDataViewItem item = model->GetItem(region);
        if (!item.IsOk())
            continue;

        const bool isItemSelected = IsSelected(item);
        const bool isRegionSelected = region->IsSelected();
        if (isItemSelected == isRegionSelected)
            continue;

        if (fromModel)
        {
            // Update only one item - from model to view
            if (isRegionSelected)
                Select(item);
            else
                Unselect(item);
        }
        else
        {
            // Update all items - from view to model
            region->SetSelected(isItemSelected);
            model->RegionChanged(region);
        }
    }

    m_updatingSelection = false;
}

void pm::RegionListCtrl::OnSelectionChanged(wxDataViewEvent& WXUNUSED(event))
{
    UpdateSelection(false);
}

bool pm::RegionListCtrl::ItemAdded(const wxDataViewItem& WXUNUSED(parent),
        const wxDataViewItem& WXUNUSED(item))
{
    Refresh();
    return true;
}

bool pm::RegionListCtrl::ItemDeleted(const wxDataViewItem& WXUNUSED(parent),
        const wxDataViewItem& WXUNUSED(item))
{
    UpdateSelection(true);

    Refresh();
    return true;
}

bool pm::RegionListCtrl::ItemChanged(const wxDataViewItem& WXUNUSED(item))
{
    UpdateSelection(true);

    Refresh();
    return true;
}

bool pm::RegionListCtrl::ItemsAdded(const wxDataViewItem& WXUNUSED(parent),
        const wxDataViewItemArray& WXUNUSED(items))
{
    Refresh();
    return true;
}

bool pm::RegionListCtrl::ItemsDeleted(const wxDataViewItem& WXUNUSED(parent),
        const wxDataViewItemArray& WXUNUSED(items))
{
    UpdateSelection(true);

    Refresh();
    return true;
}

bool pm::RegionListCtrl::ItemsChanged(const wxDataViewItemArray& WXUNUSED(items))
{
    UpdateSelection(true);

    Refresh();
    return true;
}

bool pm::RegionListCtrl::Cleared()
{
    Refresh();
    return true;
}

void pm::RegionListCtrl::Resort()
{
}

bool pm::RegionListCtrl::ValueChanged(const wxDataViewItem& WXUNUSED(item),
        unsigned int WXUNUSED(col))
{
    Refresh();
    return true;
}

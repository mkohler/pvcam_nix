/******************************************************************************/
/* Copyright (C) Teledyne Photometrics. All rights reserved.                  */
/******************************************************************************/
#include "backend/TiffFileSave.h"

/* Local */
#include "backend/Log.h"
#include "backend/PvcamRuntimeLoader.h"

/* PVCAM */
#include "master.h"
#include "pvcam.h"

/* libtiff */
#include <tiffio.h>

/* System */
#include <cstring>
#include <limits>
#include <map>
#include <sstream>

pm::TiffFileSave::TiffFileSave(const std::string& fileName, PrdHeader& header,
        bool useBigTiff)
    : FileSave(fileName, header),
    m_isBigTiff(useBigTiff),
    m_frameRecomposedBytes(sizeof(uint16_t) * m_width * m_height)
{
}

pm::TiffFileSave::~TiffFileSave()
{
    if (IsOpen())
        Close();

    delete [] (uint8_t*)m_frameRecomposed;
    if (m_frameMeta)
    {
        PVCAM->pl_md_release_frame_struct(m_frameMeta);
    }
}

bool pm::TiffFileSave::Open()
{
    if (IsOpen())
        return true;

#if SIZE_MAX > UINT32_MAX
    if (!m_isBigTiff && m_rawDataBytes > std::numeric_limits<uint32_t>::max())
    {
        Log::LogE("TIFF format is unable to store more than 4GB raw data");
        return false;
    }
#endif

    // Image description without metadata has 200-220 bytes,
    // but including metadata it could be 1-105 kB!
    constexpr size_t estimatedOverheadBytes = 1500;
    const size_t estimatedFileBytes =
        m_header.frameCount * (estimatedOverheadBytes + m_rawDataBytes);
    if (!m_isBigTiff && estimatedFileBytes > std::numeric_limits<uint32_t>::max())
    {
        Log::LogE("The libtiff is unable to store Classic TIFF files bigger than 4GB,"
                " use Big TIFF instead");
        return false;
    }

    if (m_header.frameCount > std::numeric_limits<uint16_t>::max())
    {
        Log::LogE("TIFF format is unable to store more than 65536 pages");
        return false;
    }

    const char* mode = (m_isBigTiff) ? "w8" : "w";
    m_file = TIFFOpen(m_fileName.c_str(), mode);
    if (!m_file)
        return false;

    m_frameIndex = 0;

    return IsOpen();
}

bool pm::TiffFileSave::IsOpen() const
{
    return !!m_file;
}

void pm::TiffFileSave::Close()
{
    if (!IsOpen())
        return;

    if (m_header.frameCount != m_frameIndex)
    {
        Log::LogW("File does not contain declared number of frame,"
                " trying to fix...");

        m_header.frameCount = m_frameIndex;

        const auto tiffFrameCount = (uint16_t)m_frameIndex;
        for (uint16_t n = 0; n < tiffFrameCount; ++n)
        {
            if (!TIFFSetDirectory(m_file, n)
                    || !TIFFSetField(m_file, TIFFTAG_PAGENUMBER, n, tiffFrameCount)
                    || !TIFFWriteDirectory(m_file))
            {
                Log::LogE("Failed to fix multi-page tiff");
                break;
            }
        }
    }

    TIFFFlush(m_file);
    TIFFClose(m_file);
    m_file = nullptr;

    FileSave::Close();
}

bool pm::TiffFileSave::WriteFrame(const void* metaData,
        const void* extDynMetaData, const void* rawData)
{
    if (!FileSave::WriteFrame(metaData, extDynMetaData, rawData))
        return false;

    pm::BitmapFormat bmpFormat;
    bmpFormat.SetBitDepth(m_header.bitDepth);
    bmpFormat.SetColorMask(static_cast<pm::BayerPattern>(m_header.colorMask));
    try
    {
        bmpFormat.SetImageFormat(static_cast<pm::ImageFormat>(m_header.imageFormat));
    }
    catch (...)
    {
        return false;
    }

    if (m_frameIndex >= m_header.frameCount)
    {
        Log::LogE("Cannot write more pages to TIFF file than declared during open");
        return false;
    }

    TIFFSetField(m_file, TIFFTAG_IMAGEWIDTH, m_width);
    TIFFSetField(m_file, TIFFTAG_IMAGELENGTH, m_height);
    TIFFSetField(m_file, TIFFTAG_SAMPLEFORMAT, SAMPLEFORMAT_UINT);
    TIFFSetField(m_file, TIFFTAG_BITSPERSAMPLE, 8 * bmpFormat.GetBytesPerSample());
    TIFFSetField(m_file, TIFFTAG_ORIENTATION, ORIENTATION_TOPLEFT);
    TIFFSetField(m_file, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
    TIFFSetField(m_file, TIFFTAG_ROWSPERSTRIP, 8);
    TIFFSetField(m_file, TIFFTAG_SAMPLESPERPIXEL, bmpFormat.GetSamplesPerPixel());
    TIFFSetField(m_file, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_MINISBLACK);
    if (bmpFormat.GetBitDepth() <= 16)
    {
        TIFFSetField(m_file, TIFFTAG_MAXSAMPLEVALUE, (1u << bmpFormat.GetBitDepth()) - 1);
    }

    if (m_header.frameCount > 1)
    {
        // We are writing single page of the multi-page file
        TIFFSetField(m_file, TIFFTAG_SUBFILETYPE, FILETYPE_PAGE);
        // Set the page number
        TIFFSetField(m_file, TIFFTAG_PAGENUMBER, m_frameIndex, m_header.frameCount);
    }

    // Recompose the black-filled frame with metadata if any
    void* tiffData;
    size_t tiffDataBytes;
    if (m_header.version >= PRD_VERSION_0_3
            && (m_header.flags & PRD_FLAG_HAS_METADATA))
    {
        if (!PvcamRuntimeLoader::Get()->hasMetadataFunctions())
        {
            Log::LogE("Unable to store given data to TIFF file."
                    " Loaded PVCAM does not support metadata");
            return false;
        }

        // Allocate md_frame only once
        if (!m_frameMeta)
        {
            if (PV_OK != PVCAM->pl_md_create_frame_struct(&m_frameMeta,
                        (void*)rawData, (uns32)m_rawDataBytes))
                return false;
            // Allocate buffer for recomposed frame only once
            if (!m_frameRecomposed)
            {
                m_frameRecomposed = new(std::nothrow) uint8_t[m_frameRecomposedBytes];
                if (!m_frameRecomposed)
                {
                    PVCAM->pl_md_release_frame_struct(m_frameMeta);
                    m_frameMeta = nullptr;
                    return false;
                }
            }
        }

        // Fill the frame with black pixels
        memset(m_frameRecomposed, 0, m_frameRecomposedBytes);

        // Decode metadata and recompose the frame
        if (PV_OK != PVCAM->pl_md_frame_decode(m_frameMeta, (void*)rawData,
                    (uns32)m_rawDataBytes))
            return false;

        // While streaming we recompose implied ROI only which starts at [0, 0]
        const uint16_t xOff = 0;
        const uint16_t yOff = 0;

        if (PV_OK != PVCAM->pl_md_frame_recompose(m_frameRecomposed, xOff, yOff,
                    (uns16)m_width, (uns16)m_height, m_frameMeta))
            return false;

        tiffDataBytes = m_frameRecomposedBytes;
        tiffData = m_frameRecomposed;
    }
    else
    {
        tiffData = const_cast<void*>(rawData);
        tiffDataBytes = m_rawDataBytes;
    }

    // Build up the metadata description
    const std::string imageDesc = GetImageDesc(m_header, metaData, m_frameMeta);
    // Put the PVCAM metadata into the image description
    TIFFSetField(m_file, TIFFTAG_IMAGEDESCRIPTION, imageDesc.c_str());

    for (uint32_t row = 0; row < m_height; ++row)
    {
        const size_t rowOff = row * m_width * bmpFormat.GetBytesPerPixel();
        if (-1 == TIFFWriteScanline(m_file, (uint8_t*)tiffData + rowOff, row, 0))
            return false;
    }

    if (m_header.frameCount > 1)
    {
        TIFFWriteDirectory(m_file);
    }

    m_frameIndex++;
    return true;
}

bool pm::TiffFileSave::WriteFrame(const Frame& frame)
{
    if (!FileSave::WriteFrame(frame))
        return false;

    // TODO: Utilize possibly decoded, recomposed and "colorized" Frame
    //       to improve performance
    return WriteFrame(m_framePrdMetaData, m_framePrdExtDynMetaData, frame.GetData());
}

std::string pm::TiffFileSave::GetImageDesc(const PrdHeader& prdHeader,
        const void* prdMeta, const md_frame* pvcamMeta)
{
    if (!prdMeta)
        return "";

    std::ostringstream imageDesc;

    auto prdMetaData = static_cast<const PrdMetaData*>(prdMeta);

    imageDesc << "version=" << prdHeader.version;

    if (prdHeader.version >= PRD_VERSION_0_1)
    {
        static std::map<uint32_t, const char*> expResUnit =
        {
            { PRD_EXP_RES_US, "us" },
            { PRD_EXP_RES_MS, "ms" },
            { PRD_EXP_RES_S, "s" }
        };
        const PrdRegion& rgn = prdHeader.region;
        imageDesc
            << "\nbitDepth=" << prdHeader.bitDepth
            << "\nregion=["
                << rgn.s1 << "," << rgn.s2 << "," << rgn.sbin << ","
                << rgn.p1 << "," << rgn.p2 << "," << rgn.pbin << "]"
            << "\nframeNr=" << prdMetaData->frameNumber
            << "\nreadoutTime=" << prdMetaData->readoutTime << "us"
            << "\nexpTime=" << prdMetaData->exposureTime
                << ((expResUnit.count(prdHeader.exposureResolution) > 0)
                        ? expResUnit[prdHeader.exposureResolution]
                        : "<unknown unit>");
    }

    if (prdHeader.version >= PRD_VERSION_0_2)
    {
        uint64_t bofTime = prdMetaData->bofTime;
        uint64_t eofTime = prdMetaData->eofTime;
        if (prdHeader.version >= PRD_VERSION_0_4)
        {
            bofTime |= (uint64_t)prdMetaData->bofTimeHigh << 32;
            eofTime |= (uint64_t)prdMetaData->eofTimeHigh << 32;
        }
        imageDesc
            << "\nbofTime=" << bofTime << "us"
            << "\neofTime=" << eofTime << "us";
    }

    if (prdHeader.version >= PRD_VERSION_0_3)
    {
        // Cast 8bit values to 16bit, otherwise ostream processes it as char
        imageDesc
            << "\nroiCount=" << prdMetaData->roiCount
            << "\ncolorMask=" << (uint16_t)prdHeader.colorMask
            << "\nflags=0x" << std::hex << (uint16_t)prdHeader.flags << std::dec;
    }

    if (prdHeader.version >= PRD_VERSION_0_7)
    {
        imageDesc
            << "\ncolorWbScaleRed=" << prdMetaData->colorWbScaleRed
            << "\ncolorWbScaleGreen=" << prdMetaData->colorWbScaleGreen
            << "\ncolorWbScaleBlue" << prdMetaData->colorWbScaleBlue;
    }

    if (pvcamMeta && prdHeader.version >= PRD_VERSION_0_3
            && (prdHeader.flags & PRD_FLAG_HAS_METADATA))
    {
        const uint64_t rdfTimeNs = (uint64_t)pvcamMeta->header->timestampResNs
            * (pvcamMeta->header->timestampEOF - pvcamMeta->header->timestampBOF);
        const uint64_t expTimeNs = (uint64_t)pvcamMeta->header->exposureTimeResNs
            * pvcamMeta->header->exposureTime;
        const rgn_type& irgn = pvcamMeta->impliedRoi;
        // uns8 type is handled as underlying char by stream, cast it to uint16_t
        imageDesc
            << "\nmeta.header.version=" << (uint16_t)pvcamMeta->header->version
            << "\nmeta.header.frameNr=" << pvcamMeta->header->frameNr
            << "\nmeta.header.roiCount=" << pvcamMeta->header->roiCount
            << "\nmeta.header.timeBof=" << pvcamMeta->header->timestampBOF
            << "\nmeta.header.timeEof=" << pvcamMeta->header->timestampEOF
            << "\n  (diff=" << rdfTimeNs << "ns)"
            << "\nmeta.header.timeResNs=" << pvcamMeta->header->timestampResNs
            << "\nmeta.header.expTime=" << pvcamMeta->header->exposureTime
            << "\n  (" << expTimeNs << "ns)"
            << "\nmeta.header.expTimeResNs=" << pvcamMeta->header->exposureTimeResNs
            << "\nmeta.header.roiTimeResNs=" << pvcamMeta->header->roiTimestampResNs
            << "\nmeta.header.bitDepth=" << (uint16_t)pvcamMeta->header->bitDepth
            << "\nmeta.header.colorMask=" << (uint16_t)pvcamMeta->header->colorMask
            << "\nmeta.header.flags=0x"
                << std::hex << (uint16_t)pvcamMeta->header->flags << std::dec
            << "\nmeta.header.extMdSize=" << pvcamMeta->header->extendedMdSize
            << "\nmeta.extMdSize=" << pvcamMeta->extMdDataSize
            << "\nmeta.impliedRoi=["
                << irgn.s1 << "," << irgn.s2 << "," << irgn.sbin << ","
                << irgn.p1 << "," << irgn.p2 << "," << irgn.pbin << "]"
            << "\nmeta.roiCapacity=" << pvcamMeta->roiCapacity
            << "\nmeta.roiCount=" << pvcamMeta->roiCount;
        for (int n = 0; n < pvcamMeta->roiCount; ++n)
        {
            const md_frame_roi& roi = pvcamMeta->roiArray[n];
            const auto roiHdr = roi.header;
            if (roiHdr->flags & PL_MD_ROI_FLAG_INVALID)
                continue; // Skip invalid regions
            const rgn_type& rgn = roiHdr->roi;
            imageDesc
                << "\nmeta.roi[" << n << "].header.roiNr=" << roiHdr->roiNr;
            if (pvcamMeta->header->flags & PL_MD_FRAME_FLAG_ROI_TS_SUPPORTED)
            {
                const uint64_t rdrTimeNs =
                    (uint64_t)pvcamMeta->header->roiTimestampResNs
                    * (roiHdr->timestampEOR - roiHdr->timestampBOR);
                imageDesc
                    << "\nmeta.roi[" << n << "].header.timeBor=" << roiHdr->timestampBOR
                    << "\nmeta.roi[" << n << "].header.timeEor=" << roiHdr->timestampEOR
                    << "\n  (diff=" << rdrTimeNs << "ns)";
            }
            imageDesc
                << "\nmeta.roi[" << n << "].header.roi=["
                    << rgn.s1 << "," << rgn.s2 << "," << rgn.sbin << ","
                    << rgn.p1 << "," << rgn.p2 << "," << rgn.pbin << "]";
            imageDesc
                << "\nmeta.roi[" << n << "].header.flags=0x"
                    << std::hex << (uint16_t)roiHdr->flags << std::dec
                << "\nmeta.roi[" << n << "].header.extMdSize=" << roiHdr->extendedMdSize
                << "\nmeta.roi[" << n << "].dataSize=" << roi.dataSize
                << "\nmeta.roi[" << n << "].extMdSize=" << roi.extMdDataSize;
        }
    }

    return imageDesc.str();
}

/******************************************************************************/
/* Copyright (C) Teledyne Photometrics. All rights reserved.                  */
/******************************************************************************/
#pragma once
#ifndef PM_PARAM_BROWSER_DLG_H
#define PM_PARAM_BROWSER_DLG_H

// WxWidgets headers have to be included first
#include <wx/event.h>
#include "PVCamTest_Ui.h"

/* Local */
#include "backend/Param.h"

/* System */
#include <memory>
#include <map>

namespace pm {

/* Forward declarations */
class Camera;

class ParamBrowserDlg final : public ui::ParamBrowserDlg
{
public:
    ParamBrowserDlg(std::shared_ptr<Camera> camera);
    ParamBrowserDlg(const ParamBrowserDlg&) = delete;
    ParamBrowserDlg& operator=(const ParamBrowserDlg&) = delete;
    virtual ~ParamBrowserDlg();

public:
    void ActivateWindow(bool active = true);
    void EnableEditor(bool enabled = true);

private: // UI event handlers
    void OnInfoToggled(wxCommandEvent& event);
    void OnUnavailableToggled(wxCommandEvent& event);
    void OnReadOnlyToggled(wxCommandEvent& event);
    void OnReloadAllClicked(wxCommandEvent& event);
    void OnSearchFilterChanged(wxCommandEvent& event);
    void OnClearSearchClicked(wxCommandEvent& event);
    void OnPropertySelected(wxPropertyGridEvent& event);
    void OnPropertyChanged(wxPropertyGridEvent& event);
    void OnPropertyButtonClicked(wxCommandEvent& event);

    void HandlePropertyChange();

private:
    void ApplyFilter(const wxString& filter, bool showUnavailable,
            bool showReadOnly);
    void ReloadAll(bool init);
    void ReloadAttrs();

    wxPGProperty* CreateProperty(uint32_t paramId, bool& errorOccurred);

    void OnParamChanged(ParamBase& param, bool allAttrsChanged);
    void UnregisterParamChangeHandlers();

private: // Without Freeze/Thaw
    void DoApplyFilter(const wxString& filter, bool showUnavailable,
            bool showReadOnly);
    void DoReloadAll(bool init);
    void DoReloadAttrs();

private:
    static bool MatchesFilter(const wxString& filter, const wxString& text);

private:
    std::shared_ptr<Camera> m_camera;

    wxButton* m_btnReload{ nullptr };
    wxToolBarToolBase* m_btnInfo{ nullptr };
    wxToolBarToolBase* m_btnUnavail{ nullptr };
    wxToolBarToolBase* m_btnReadOnly{ nullptr };
    wxTextCtrl* m_search{ nullptr };
    wxToolBarToolBase* m_btnClearSearch{ nullptr };
    wxString m_filter{};
    bool m_showUnavailable{ false };
    bool m_showReadOnly{ true };
    int m_sashPos{ 0 };

    std::map<wxPGProperty*, wxPGProperty*> m_propParentMap{};
    std::map<ParamBase*, wxPGProperty*> m_paramPropMap{};
    std::map<ParamBase*, uint64_t> m_paramChangeHandleMap{};

    std::map<int16_t, wxPGProperty*> m_attrPropMap{};
    wxPGProperty* m_enumItemsProp{ nullptr };
};

} // namespace pm

#endif // PM_PARAM_BROWSER_DLG_H

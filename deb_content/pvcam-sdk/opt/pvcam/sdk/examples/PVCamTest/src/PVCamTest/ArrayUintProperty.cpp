/******************************************************************************/
/* Copyright (C) Teledyne Photometrics. All rights reserved.                  */
/******************************************************************************/
#include "ArrayUintProperty.h"

// WxWidgets headers have to be included first
#include <wx/propgrid/propgrid.h>
#include <wx/propgrid/editors.h>

// wxArrayUintEditorDialog

class wxArrayUintEditorDialog : public wxPGArrayEditorDialog
{
public:
    wxArrayUintEditorDialog();
    wxArrayUintEditorDialog(wxWindow *parent, const wxString& message,
            const wxString& caption, wxArrayUint& array,
            long style = wxAEDIALOG_STYLE,
            const wxPoint& pos = wxDefaultPosition,
            const wxSize& sz = wxDefaultSize);

public: // Non-virtual methods
    void Init();
    bool Create(wxWindow *parent, const wxString& message,
            const wxString& caption, wxArrayUint& array,
            long style = wxAEDIALOG_STYLE,
            const wxPoint& pos = wxDefaultPosition,
            const wxSize& sz = wxDefaultSize);

    const wxArrayUint& GetArray() const
    { return m_array; }

protected:
    // Mandatory array of type
    wxArrayUint m_array;

protected: // Mandatory overridden methods
    virtual wxString ArrayGet(size_t index) override;
    virtual size_t ArrayGetCount() override;
    virtual bool ArrayInsert(const wxString& str, int index) override;
    virtual bool ArraySet(size_t index, const wxString& str) override;
    virtual void ArrayRemoveAt(int index) override;
    virtual void ArraySwap(size_t first, size_t second) override;

private:
    wxDECLARE_DYNAMIC_CLASS_NO_COPY(wxArrayUintEditorDialog);
};

IMPLEMENT_DYNAMIC_CLASS(wxArrayUintEditorDialog, wxPGArrayEditorDialog)

wxString wxArrayUintEditorDialog::ArrayGet(size_t index)
{
    return std::to_string(m_array[index]);
}

size_t wxArrayUintEditorDialog::ArrayGetCount()
{
    return m_array.GetCount();
}

bool wxArrayUintEditorDialog::ArrayInsert(const wxString& str, int index)
{
    //wxArrayUint::value_type val;
    unsigned long val;
    if (!str.ToULong(&val))
        return false;
    if (index < 0)
        m_array.Add(val);
    else
        m_array.Insert(val, index);
    return true;
}

bool wxArrayUintEditorDialog::ArraySet(size_t index, const wxString& str)
{
    //wxArrayUint::value_type val;
    unsigned long val;
    if (!str.ToULong(&val))
        return false;
    m_array[index] = val;
    return true;
}

void wxArrayUintEditorDialog::ArrayRemoveAt(int index)
{
    m_array.RemoveAt(index);
}

void wxArrayUintEditorDialog::ArraySwap(size_t first, size_t second)
{
    const auto a = m_array[first];
    const auto b = m_array[second];
    m_array[first] = b;
    m_array[second] = a;
}

wxArrayUintEditorDialog::wxArrayUintEditorDialog()
    : wxPGArrayEditorDialog()
{
    Init();
}

wxArrayUintEditorDialog::wxArrayUintEditorDialog(wxWindow *parent,
        const wxString& message, const wxString& caption, wxArrayUint& array,
        long style, const wxPoint& pos, const wxSize& sz)
    : wxPGArrayEditorDialog()
{
    Init();
    Create(parent, message, caption, array, style, pos, sz);
}

void wxArrayUintEditorDialog::Init()
{
    wxPGArrayEditorDialog::Init();
}

bool wxArrayUintEditorDialog::Create(wxWindow* parent, const wxString& message,
        const wxString& caption, wxArrayUint& array, long style,
        const wxPoint& pos, const wxSize& sz)
{
    m_array = array;
    return wxPGArrayEditorDialog::Create(parent, message, caption, style, pos, sz);
}

// wxArrayUintProperty

// Comparison required by value type implementation
bool operator==(const wxArrayUint& a, const wxArrayUint& b)
{
    if (a.GetCount() != b.GetCount())
        return false;
    for (size_t n = 0; n < a.GetCount(); ++n)
        if (a[n] != b[n])
            return false;
    return true;
}

WX_PG_IMPLEMENT_VARIANT_DATA_DUMMY_EQ(wxArrayUint)

WX_PG_IMPLEMENT_PROPERTY_CLASS(wxArrayUintProperty, wxPGProperty, wxArrayUint,
                               const wxArrayUint&, TextCtrlAndButton)

wxArrayUintProperty::wxArrayUintProperty(
        const wxString& label, const wxString& name, const wxArrayUint& array)
    : wxPGProperty(label, name)
{
    SetValue(WXVARIANT(array));
}

void wxArrayUintProperty::OnSetValue()
{
    // Generate cached display string, to optimize grid drawing
    GenerateValueAsString(m_display);
}

wxString wxArrayUintProperty::ValueToString(wxVariant& value, int argFlags) const
{
    wxString str;
    if (argFlags & wxPG_FULL_VALUE)
    {
        GenerateValueAsString(str);
    }
    else
    {
        // Display cached string only if value truly matches m_value
        if (value.GetData() == m_value.GetData())
            return m_display;
        else
            GenerateValueAsString(str);
    }
    return str;
}

bool wxArrayUintProperty::StringToValue(wxVariant& variant,
        const wxString& text, int WXUNUSED(argFlags)) const
{
    //wxArrayUint::value_type val;
    unsigned long val;
    wxString str;
    wxArrayUint newArray;

    bool ok = true;

    WX_PG_TOKENIZER1_BEGIN(text, m_delimiter)
    {
        if (!token.empty())
        {
            if (!token.ToULong(&val))
            {
                str.Printf(_("'%s' is not an uint32_t number"), token.c_str());
                ok = false;
                break;
            }

            // TODO: Put validator code here

            newArray.Add(val);
        }
    }
    WX_PG_TOKENIZER1_END()

    // When invalid token found, show error message and don't change anything
    if (!ok)
    {
        //ShowError(str);
        return false;
    }

    if (!(wxArrayUintRefFromVariant(m_value) == newArray))
    {
        variant = WXVARIANT(newArray);
        return true;
    }
    return false;
}

bool wxArrayUintProperty::OnEvent(wxPropertyGrid* propgrid,
        wxWindow* WXUNUSED(primary), wxEvent& event)
{
    if (!propgrid->IsMainButtonEvent(event))
        return false;

    // Update the value in case of last minute changes
    wxVariant useValue = propgrid->GetUncommittedPropertyValue();
    wxArrayUint& value = wxArrayUintRefFromVariant(useValue);

    // Create editor dialog
    wxArrayUintEditorDialog dlg;
    dlg.Create(propgrid, wxEmptyString, m_label, value);
    dlg.Move(propgrid->GetGoodEditorDialogPosition(this, dlg.GetSize()));

    // Execute editor dialog
    int res = dlg.ShowModal();
    if (res == wxID_OK && dlg.IsModified())
    {
        SetValueInEvent(WXVARIANT(dlg.GetArray()));
        return true;
    }
    return false;
}

bool wxArrayUintProperty::DoSetAttribute(const wxString& name, wxVariant& value)
{
    if (name == wxPG_ARRAY_DELIMITER)
    {
        m_delimiter = value.GetChar();
        GenerateValueAsString(m_display);
        return false;
    }
    return true;
}

void wxArrayUintProperty::GenerateValueAsString(wxString& target) const
{
    const wxArrayUint& value = wxArrayUintRefFromVariant(m_value);

    target.Empty();
    for (size_t n = 0; n < value.GetCount(); n++)
    {
        target += std::to_string(value[n]);
        if (n < (value.GetCount() - 1))
            target += m_delimiter;
    }
}

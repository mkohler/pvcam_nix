///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Nov  6 2017)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "RegionListCtrl.h"

#include "PVCamTest_Ui.h"

#include "resources/add.png.h"
#include "resources/delete.png.h"
#include "resources/folder.png.h"

///////////////////////////////////////////////////////////////////////////
using namespace pm::ui;

MainDlg::MainDlg( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxFrame( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxSize( 410,-1 ), wxDefaultSize );
	this->SetBackgroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_BTNFACE ) );
	
	wxBoxSizer* bSizerMain;
	bSizerMain = new wxBoxSizer( wxVERTICAL );
	
	wxStaticBoxSizer* sbSizerSettings;
	sbSizerSettings = new wxStaticBoxSizer( new wxStaticBox( this, wxID_ANY, _("Settings") ), wxVERTICAL );
	
	m_tabsSettings = new wxNotebook( sbSizerSettings->GetStaticBox(), wxID_ANY, wxDefaultPosition, wxDefaultSize, 0 );
	m_tabSettingsBasic = new wxPanel( m_tabsSettings, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizerTabSettingsBasic;
	bSizerTabSettingsBasic = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizerPortSpeed;
	bSizerPortSpeed = new wxBoxSizer( wxHORIZONTAL );
	
	wxString m_cbPortSpeedChoices[] = { _("<ports&speeds>") };
	int m_cbPortSpeedNChoices = sizeof( m_cbPortSpeedChoices ) / sizeof( wxString );
	m_cbPortSpeed = new wxChoice( m_tabSettingsBasic, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_cbPortSpeedNChoices, m_cbPortSpeedChoices, 0 );
	m_cbPortSpeed->SetSelection( 0 );
	bSizerPortSpeed->Add( m_cbPortSpeed, 1, wxALIGN_CENTER_VERTICAL|wxBOTTOM, 1 );
	
	
	bSizerTabSettingsBasic->Add( bSizerPortSpeed, 0, wxEXPAND|wxLEFT|wxRIGHT|wxTOP, 5 );
	
	wxBoxSizer* bSizerGain;
	bSizerGain = new wxBoxSizer( wxHORIZONTAL );
	
	m_lblGain = new wxStaticText( m_tabSettingsBasic, wxID_ANY, _("Gain"), wxDefaultPosition, wxDefaultSize, 0 );
	m_lblGain->Wrap( -1 );
	bSizerGain->Add( m_lblGain, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT, 5 );
	
	wxString m_cbGainChoices[] = { _("<gains>") };
	int m_cbGainNChoices = sizeof( m_cbGainChoices ) / sizeof( wxString );
	m_cbGain = new wxChoice( m_tabSettingsBasic, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_cbGainNChoices, m_cbGainChoices, 0 );
	m_cbGain->SetSelection( 0 );
	bSizerGain->Add( m_cbGain, 1, wxALIGN_CENTER_VERTICAL|wxBOTTOM|wxTOP, 1 );
	
	m_lblEmGain = new wxStaticText( m_tabSettingsBasic, wxID_ANY, _("EM"), wxDefaultPosition, wxDefaultSize, 0 );
	m_lblEmGain->Wrap( -1 );
	bSizerGain->Add( m_lblEmGain, 0, wxALIGN_CENTER_VERTICAL|wxLEFT|wxRIGHT, 5 );
	
	m_editEmGain = new wxSpinCtrl( m_tabSettingsBasic, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 60,-1 ), wxSP_ARROW_KEYS, 1, 65535, 1 );
	bSizerGain->Add( m_editEmGain, 0, wxALIGN_CENTER_VERTICAL|wxBOTTOM|wxTOP, 1 );
	
	
	bSizerTabSettingsBasic->Add( bSizerGain, 0, wxEXPAND|wxLEFT|wxRIGHT, 5 );
	
	wxBoxSizer* bSizerExposure;
	bSizerExposure = new wxBoxSizer( wxHORIZONTAL );
	
	m_lblExpTime = new wxStaticText( m_tabSettingsBasic, wxID_ANY, _("Exposure time"), wxDefaultPosition, wxDefaultSize, wxALIGN_RIGHT );
	m_lblExpTime->Wrap( -1 );
	bSizerExposure->Add( m_lblExpTime, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT, 5 );
	
	m_editExpTimes = new wxTextCtrl( m_tabSettingsBasic, wxID_ANY, _("0"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizerExposure->Add( m_editExpTimes, 1, wxALIGN_CENTER_VERTICAL|wxBOTTOM|wxEXPAND|wxTOP, 1 );
	
	wxString m_cbExpTimeResChoices[] = { _("<res>") };
	int m_cbExpTimeResNChoices = sizeof( m_cbExpTimeResChoices ) / sizeof( wxString );
	m_cbExpTimeRes = new wxChoice( m_tabSettingsBasic, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_cbExpTimeResNChoices, m_cbExpTimeResChoices, 0 );
	m_cbExpTimeRes->SetSelection( 0 );
	bSizerExposure->Add( m_cbExpTimeRes, 0, wxALIGN_CENTER_VERTICAL|wxBOTTOM|wxLEFT|wxTOP, 1 );
	
	
	bSizerTabSettingsBasic->Add( bSizerExposure, 0, wxEXPAND|wxLEFT|wxRIGHT, 5 );
	
	wxBoxSizer* bSizerTimeLapseDelay;
	bSizerTimeLapseDelay = new wxBoxSizer( wxHORIZONTAL );
	
	m_lblTimeLapseDelay = new wxStaticText( m_tabSettingsBasic, wxID_ANY, _("Time-lapse delay"), wxDefaultPosition, wxDefaultSize, 0 );
	m_lblTimeLapseDelay->Wrap( -1 );
	bSizerTimeLapseDelay->Add( m_lblTimeLapseDelay, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT, 5 );
	
	m_editTimeLapseDelay = new wxSpinCtrl( m_tabSettingsBasic, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 70,-1 ), wxSP_ARROW_KEYS, 0, 3600000, 1 );
	bSizerTimeLapseDelay->Add( m_editTimeLapseDelay, 0, wxALIGN_CENTER_VERTICAL|wxBOTTOM|wxTOP, 1 );
	
	m_lblTimeLapseDelayMs = new wxStaticText( m_tabSettingsBasic, wxID_ANY, _("ms"), wxDefaultPosition, wxDefaultSize, 0 );
	m_lblTimeLapseDelayMs->Wrap( -1 );
	bSizerTimeLapseDelay->Add( m_lblTimeLapseDelayMs, 0, wxALIGN_CENTER_VERTICAL|wxLEFT, 5 );
	
	
	bSizerTabSettingsBasic->Add( bSizerTimeLapseDelay, 0, wxEXPAND|wxLEFT|wxRIGHT, 5 );
	
	wxBoxSizer* bSizerTrigger;
	bSizerTrigger = new wxBoxSizer( wxHORIZONTAL );
	
	m_lblTrigger = new wxStaticText( m_tabSettingsBasic, wxID_ANY, _("Trigger"), wxDefaultPosition, wxDefaultSize, 0 );
	m_lblTrigger->Wrap( -1 );
	bSizerTrigger->Add( m_lblTrigger, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT, 5 );
	
	wxString m_cbTriggerModeChoices[] = { _("<trigger-modes>") };
	int m_cbTriggerModeNChoices = sizeof( m_cbTriggerModeChoices ) / sizeof( wxString );
	m_cbTriggerMode = new wxChoice( m_tabSettingsBasic, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_cbTriggerModeNChoices, m_cbTriggerModeChoices, 0 );
	m_cbTriggerMode->SetSelection( 0 );
	bSizerTrigger->Add( m_cbTriggerMode, 1, wxALIGN_CENTER_VERTICAL|wxBOTTOM|wxTOP, 1 );
	
	
	bSizerTabSettingsBasic->Add( bSizerTrigger, 0, wxEXPAND|wxLEFT|wxRIGHT, 5 );
	
	wxBoxSizer* bSizerExpOut;
	bSizerExpOut = new wxBoxSizer( wxHORIZONTAL );
	
	m_lblExpOut = new wxStaticText( m_tabSettingsBasic, wxID_ANY, _("Exp. out"), wxDefaultPosition, wxDefaultSize, 0 );
	m_lblExpOut->Wrap( -1 );
	bSizerExpOut->Add( m_lblExpOut, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT, 5 );
	
	wxString m_cbExpOutModeChoices[] = { _("<exp-out-modes>") };
	int m_cbExpOutModeNChoices = sizeof( m_cbExpOutModeChoices ) / sizeof( wxString );
	m_cbExpOutMode = new wxChoice( m_tabSettingsBasic, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_cbExpOutModeNChoices, m_cbExpOutModeChoices, 0 );
	m_cbExpOutMode->SetSelection( 0 );
	bSizerExpOut->Add( m_cbExpOutMode, 1, wxALIGN_CENTER_VERTICAL|wxBOTTOM|wxTOP, 1 );
	
	
	bSizerTabSettingsBasic->Add( bSizerExpOut, 0, wxEXPAND|wxLEFT|wxRIGHT, 5 );
	
	wxBoxSizer* bSizerClear;
	bSizerClear = new wxBoxSizer( wxHORIZONTAL );
	
	m_lblClear = new wxStaticText( m_tabSettingsBasic, wxID_ANY, _("Clear"), wxDefaultPosition, wxDefaultSize, 0 );
	m_lblClear->Wrap( -1 );
	bSizerClear->Add( m_lblClear, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT, 5 );
	
	wxString m_cbClearModeChoices[] = { _("<clear-modes>") };
	int m_cbClearModeNChoices = sizeof( m_cbClearModeChoices ) / sizeof( wxString );
	m_cbClearMode = new wxChoice( m_tabSettingsBasic, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_cbClearModeNChoices, m_cbClearModeChoices, 0 );
	m_cbClearMode->SetSelection( 0 );
	bSizerClear->Add( m_cbClearMode, 1, wxALIGN_CENTER_VERTICAL|wxBOTTOM|wxTOP, 1 );
	
	m_lblClearCycles = new wxStaticText( m_tabSettingsBasic, wxID_ANY, _("Cycles"), wxDefaultPosition, wxDefaultSize, 0 );
	m_lblClearCycles->Wrap( -1 );
	bSizerClear->Add( m_lblClearCycles, 0, wxALIGN_CENTER_VERTICAL|wxLEFT|wxRIGHT, 5 );
	
	m_editClearCycles = new wxSpinCtrl( m_tabSettingsBasic, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 50,-1 ), wxSP_ARROW_KEYS, 0, 65535, 2 );
	bSizerClear->Add( m_editClearCycles, 0, wxALIGN_CENTER_VERTICAL|wxBOTTOM|wxTOP, 1 );
	
	
	bSizerTabSettingsBasic->Add( bSizerClear, 0, wxEXPAND|wxLEFT|wxRIGHT, 5 );
	
	wxBoxSizer* bSizerP;
	bSizerP = new wxBoxSizer( wxHORIZONTAL );
	
	m_lblP = new wxStaticText( m_tabSettingsBasic, wxID_ANY, _("Par. clocking"), wxDefaultPosition, wxDefaultSize, 0 );
	m_lblP->Wrap( -1 );
	bSizerP->Add( m_lblP, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT, 5 );
	
	wxString m_cbPModeChoices[] = { _("<par.-clk.-modes>") };
	int m_cbPModeNChoices = sizeof( m_cbPModeChoices ) / sizeof( wxString );
	m_cbPMode = new wxChoice( m_tabSettingsBasic, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_cbPModeNChoices, m_cbPModeChoices, 0 );
	m_cbPMode->SetSelection( 0 );
	bSizerP->Add( m_cbPMode, 1, wxALIGN_CENTER_VERTICAL|wxBOTTOM|wxTOP, 1 );
	
	
	bSizerTabSettingsBasic->Add( bSizerP, 0, wxBOTTOM|wxEXPAND|wxLEFT|wxRIGHT, 5 );
	
	
	m_tabSettingsBasic->SetSizer( bSizerTabSettingsBasic );
	m_tabSettingsBasic->Layout();
	bSizerTabSettingsBasic->Fit( m_tabSettingsBasic );
	m_tabsSettings->AddPage( m_tabSettingsBasic, _("Basic"), true );
	m_tabSettingsRegions = new wxPanel( m_tabsSettings, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizerTabSettingsRegions;
	bSizerTabSettingsRegions = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizerRoiControls;
	bSizerRoiControls = new wxBoxSizer( wxHORIZONTAL );
	
	wxBoxSizer* bSizerRoiButtons;
	bSizerRoiButtons = new wxBoxSizer( wxVERTICAL );
	
	m_btnRoiAdd = new wxBitmapButton( m_tabSettingsRegions, wxID_ANY, add_png_to_wx_bitmap(), wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW );
	bSizerRoiButtons->Add( m_btnRoiAdd, 0, wxALIGN_CENTER_VERTICAL, 5 );
	
	
	bSizerRoiButtons->Add( 0, 0, 1, wxEXPAND, 5 );
	
	m_btnRoiRemove = new wxBitmapButton( m_tabSettingsRegions, wxID_ANY, delete_png_to_wx_bitmap(), wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW );
	bSizerRoiButtons->Add( m_btnRoiRemove, 0, wxALIGN_CENTER_VERTICAL, 5 );
	
	
	bSizerRoiControls->Add( bSizerRoiButtons, 0, wxEXPAND, 5 );
	
	wxStaticBoxSizer* bSizerRoiBin;
	bSizerRoiBin = new wxStaticBoxSizer( new wxStaticBox( m_tabSettingsRegions, wxID_ANY, _("Binning") ), wxHORIZONTAL );
	
	wxString m_cbRoiBinChoices[] = { _("<factors>") };
	int m_cbRoiBinNChoices = sizeof( m_cbRoiBinChoices ) / sizeof( wxString );
	m_cbRoiBin = new wxChoice( bSizerRoiBin->GetStaticBox(), wxID_ANY, wxDefaultPosition, wxDefaultSize, m_cbRoiBinNChoices, m_cbRoiBinChoices, 0 );
	m_cbRoiBin->SetSelection( 0 );
	bSizerRoiBin->Add( m_cbRoiBin, 3, wxALIGN_CENTER_VERTICAL|wxBOTTOM|wxLEFT|wxRIGHT, 2 );
	
	m_lblRoiBinSer = new wxStaticText( bSizerRoiBin->GetStaticBox(), wxID_ANY, _("X"), wxDefaultPosition, wxDefaultSize, wxALIGN_RIGHT );
	m_lblRoiBinSer->Wrap( -1 );
	bSizerRoiBin->Add( m_lblRoiBinSer, 0, wxALIGN_CENTER_VERTICAL|wxLEFT|wxRIGHT, 5 );
	
	m_editRoiBinSer = new wxSpinCtrl( bSizerRoiBin->GetStaticBox(), wxID_ANY, wxT("1"), wxDefaultPosition, wxSize( 50,-1 ), wxSP_ARROW_KEYS, 1, 99, 31 );
	bSizerRoiBin->Add( m_editRoiBinSer, 2, wxALIGN_CENTER_VERTICAL|wxBOTTOM|wxRIGHT, 2 );
	
	m_lblRoiBinPar = new wxStaticText( bSizerRoiBin->GetStaticBox(), wxID_ANY, _("Y"), wxDefaultPosition, wxDefaultSize, wxALIGN_RIGHT );
	m_lblRoiBinPar->Wrap( -1 );
	bSizerRoiBin->Add( m_lblRoiBinPar, 0, wxALIGN_CENTER_VERTICAL|wxLEFT|wxRIGHT, 5 );
	
	m_editRoiBinPar = new wxSpinCtrl( bSizerRoiBin->GetStaticBox(), wxID_ANY, wxT("1"), wxDefaultPosition, wxSize( 50,-1 ), wxSP_ARROW_KEYS, 1, 99, 0 );
	bSizerRoiBin->Add( m_editRoiBinPar, 2, wxALIGN_CENTER_VERTICAL|wxBOTTOM|wxRIGHT, 2 );
	
	
	bSizerRoiControls->Add( bSizerRoiBin, 1, wxEXPAND|wxLEFT, 5 );
	
	
	bSizerTabSettingsRegions->Add( bSizerRoiControls, 0, wxEXPAND|wxLEFT|wxRIGHT|wxTOP, 5 );
	
	m_dvlRois = new RegionListCtrl( m_tabSettingsRegions, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxDV_HORIZ_RULES|wxDV_ROW_LINES|wxDV_VERT_RULES );
	m_dvlRois->SetMinSize( wxSize( -1,100 ) );
	
	bSizerTabSettingsRegions->Add( m_dvlRois, 1, wxEXPAND|wxLEFT|wxRIGHT|wxTOP, 5 );
	
	wxBoxSizer* bSizerRoiMultiWindow;
	bSizerRoiMultiWindow = new wxBoxSizer( wxHORIZONTAL );
	
	m_chbRoiMultiWin = new wxCheckBox( m_tabSettingsRegions, wxID_ANY, _("Open each region in its own window"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizerRoiMultiWindow->Add( m_chbRoiMultiWin, 0, wxALIGN_CENTER_VERTICAL, 5 );
	
	
	bSizerTabSettingsRegions->Add( bSizerRoiMultiWindow, 0, wxBOTTOM|wxEXPAND|wxLEFT|wxRIGHT|wxTOP, 5 );
	
	
	m_tabSettingsRegions->SetSizer( bSizerTabSettingsRegions );
	m_tabSettingsRegions->Layout();
	bSizerTabSettingsRegions->Fit( m_tabSettingsRegions );
	m_tabsSettings->AddPage( m_tabSettingsRegions, _("Regions"), false );
	m_tabSettingsCentroids = new wxPanel( m_tabsSettings, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizerTabSettingsCentroids;
	bSizerTabSettingsCentroids = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizerCentroidsEnable;
	bSizerCentroidsEnable = new wxBoxSizer( wxHORIZONTAL );
	
	m_chbCentroidsEnabled = new wxCheckBox( m_tabSettingsCentroids, wxID_ANY, _("Enable"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizerCentroidsEnable->Add( m_chbCentroidsEnabled, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT, 5 );
	
	m_lblCentroidsMode = new wxStaticText( m_tabSettingsCentroids, wxID_ANY, _("Mode"), wxDefaultPosition, wxDefaultSize, 0 );
	m_lblCentroidsMode->Wrap( -1 );
	bSizerCentroidsEnable->Add( m_lblCentroidsMode, 0, wxALIGN_CENTER_VERTICAL|wxLEFT|wxRIGHT, 5 );
	
	wxString m_chCentroidsModeChoices[] = { _("<centroids-modes>") };
	int m_chCentroidsModeNChoices = sizeof( m_chCentroidsModeChoices ) / sizeof( wxString );
	m_chCentroidsMode = new wxChoice( m_tabSettingsCentroids, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_chCentroidsModeNChoices, m_chCentroidsModeChoices, 0 );
	m_chCentroidsMode->SetSelection( 0 );
	bSizerCentroidsEnable->Add( m_chCentroidsMode, 1, wxALIGN_CENTER_VERTICAL|wxBOTTOM, 1 );
	
	
	bSizerTabSettingsCentroids->Add( bSizerCentroidsEnable, 0, wxEXPAND|wxLEFT|wxRIGHT|wxTOP, 5 );
	
	wxBoxSizer* bSizerCentroidsControls;
	bSizerCentroidsControls = new wxBoxSizer( wxHORIZONTAL );
	
	m_lblCentroidsCount = new wxStaticText( m_tabSettingsCentroids, wxID_ANY, _("Count"), wxDefaultPosition, wxDefaultSize, 0 );
	m_lblCentroidsCount->Wrap( -1 );
	bSizerCentroidsControls->Add( m_lblCentroidsCount, 0, wxALIGN_CENTER_VERTICAL|wxLEFT|wxRIGHT, 5 );
	
	m_editCentroidsCount = new wxSpinCtrl( m_tabSettingsCentroids, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 60,-1 ), wxSP_ARROW_KEYS, 1, 500, 100 );
	bSizerCentroidsControls->Add( m_editCentroidsCount, 0, wxALIGN_CENTER_VERTICAL|wxBOTTOM|wxTOP, 1 );
	
	m_lblCentroidsRadius = new wxStaticText( m_tabSettingsCentroids, wxID_ANY, _("Radius"), wxDefaultPosition, wxDefaultSize, 0 );
	m_lblCentroidsRadius->Wrap( -1 );
	bSizerCentroidsControls->Add( m_lblCentroidsRadius, 0, wxALIGN_CENTER_VERTICAL|wxLEFT|wxRIGHT, 5 );
	
	m_editCentroidsRadius = new wxSpinCtrl( m_tabSettingsCentroids, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 50,-1 ), wxSP_ARROW_KEYS, 0, 10, 0 );
	bSizerCentroidsControls->Add( m_editCentroidsRadius, 0, wxALIGN_CENTER_VERTICAL|wxBOTTOM|wxTOP, 1 );
	
	
	bSizerTabSettingsCentroids->Add( bSizerCentroidsControls, 0, wxEXPAND|wxLEFT|wxRIGHT, 5 );
	
	wxBoxSizer* bSizerCentroidsBgCount;
	bSizerCentroidsBgCount = new wxBoxSizer( wxHORIZONTAL );
	
	m_lblCentroidsBgCount = new wxStaticText( m_tabSettingsCentroids, wxID_ANY, _("Background removal with"), wxDefaultPosition, wxDefaultSize, 0 );
	m_lblCentroidsBgCount->Wrap( -1 );
	bSizerCentroidsBgCount->Add( m_lblCentroidsBgCount, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT, 5 );
	
	wxString m_chCentroidsBgCountChoices[] = { _("0") };
	int m_chCentroidsBgCountNChoices = sizeof( m_chCentroidsBgCountChoices ) / sizeof( wxString );
	m_chCentroidsBgCount = new wxChoice( m_tabSettingsCentroids, wxID_ANY, wxDefaultPosition, wxSize( 70,-1 ), m_chCentroidsBgCountNChoices, m_chCentroidsBgCountChoices, 0 );
	m_chCentroidsBgCount->SetSelection( 0 );
	bSizerCentroidsBgCount->Add( m_chCentroidsBgCount, 0, wxALIGN_CENTER_VERTICAL|wxBOTTOM|wxTOP, 1 );
	
	m_lblCentroidsBgCount2 = new wxStaticText( m_tabSettingsCentroids, wxID_ANY, _("frames"), wxDefaultPosition, wxDefaultSize, 0 );
	m_lblCentroidsBgCount2->Wrap( -1 );
	bSizerCentroidsBgCount->Add( m_lblCentroidsBgCount2, 0, wxALIGN_CENTER_VERTICAL|wxLEFT, 5 );
	
	
	bSizerTabSettingsCentroids->Add( bSizerCentroidsBgCount, 0, wxEXPAND|wxLEFT|wxRIGHT, 5 );
	
	wxBoxSizer* bSizerCentroidsThreshold;
	bSizerCentroidsThreshold = new wxBoxSizer( wxHORIZONTAL );
	
	m_lblCentroidsThreshold = new wxStaticText( m_tabSettingsCentroids, wxID_ANY, _("Threshold multiplier"), wxDefaultPosition, wxDefaultSize, 0 );
	m_lblCentroidsThreshold->Wrap( -1 );
	bSizerCentroidsThreshold->Add( m_lblCentroidsThreshold, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT, 5 );
	
	m_editDblCentroidsThreshold = new wxSpinCtrlDouble( m_tabSettingsCentroids, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 85,-1 ), wxSP_ARROW_KEYS, 0, 256, 1, 0.0625 );
	bSizerCentroidsThreshold->Add( m_editDblCentroidsThreshold, 0, wxALIGN_CENTER_VERTICAL|wxBOTTOM|wxTOP, 1 );
	
	
	bSizerTabSettingsCentroids->Add( bSizerCentroidsThreshold, 0, wxEXPAND|wxLEFT|wxRIGHT, 5 );
	
	wxBoxSizer* bSizerTrackLinkFrames;
	bSizerTrackLinkFrames = new wxBoxSizer( wxHORIZONTAL );
	
	m_lblTrackLinkFrames = new wxStaticText( m_tabSettingsCentroids, wxID_ANY, _("Link"), wxDefaultPosition, wxDefaultSize, 0 );
	m_lblTrackLinkFrames->Wrap( -1 );
	bSizerTrackLinkFrames->Add( m_lblTrackLinkFrames, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT, 5 );
	
	m_editTrackLinkFrames = new wxSpinCtrl( m_tabSettingsCentroids, wxID_ANY, wxT("2"), wxDefaultPosition, wxSize( 50,-1 ), wxSP_ARROW_KEYS, 2, 10, 0 );
	bSizerTrackLinkFrames->Add( m_editTrackLinkFrames, 0, wxALIGN_CENTER_VERTICAL|wxBOTTOM|wxTOP, 1 );
	
	m_lblTrackLinkFrames2 = new wxStaticText( m_tabSettingsCentroids, wxID_ANY, _("frames"), wxDefaultPosition, wxDefaultSize, 0 );
	m_lblTrackLinkFrames2->Wrap( -1 );
	bSizerTrackLinkFrames->Add( m_lblTrackLinkFrames2, 0, wxALIGN_CENTER_VERTICAL|wxLEFT|wxRIGHT, 5 );
	
	m_chbTrackCpuOnly = new wxCheckBox( m_tabSettingsCentroids, wxID_ANY, _("Processing on CPU only"), wxDefaultPosition, wxDefaultSize, 0 );
	m_chbTrackCpuOnly->SetToolTip( _("Does not use GPU for processing even if available") );
	
	bSizerTrackLinkFrames->Add( m_chbTrackCpuOnly, 0, wxALIGN_CENTER_VERTICAL|wxLEFT, 5 );
	
	
	bSizerTabSettingsCentroids->Add( bSizerTrackLinkFrames, 0, wxEXPAND|wxLEFT|wxRIGHT, 5 );
	
	wxBoxSizer* bSizerTrackMaxDist;
	bSizerTrackMaxDist = new wxBoxSizer( wxHORIZONTAL );
	
	m_lblTrackMaxDist = new wxStaticText( m_tabSettingsCentroids, wxID_ANY, _("Maximum distance"), wxDefaultPosition, wxDefaultSize, 0 );
	m_lblTrackMaxDist->Wrap( -1 );
	bSizerTrackMaxDist->Add( m_lblTrackMaxDist, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT, 5 );
	
	m_editTrackMaxDist = new wxSpinCtrl( m_tabSettingsCentroids, wxID_ANY, wxT("25"), wxDefaultPosition, wxSize( 70,-1 ), wxSP_ARROW_KEYS, 1, 65535, 0 );
	bSizerTrackMaxDist->Add( m_editTrackMaxDist, 0, wxALIGN_CENTER_VERTICAL|wxBOTTOM|wxTOP, 1 );
	
	m_lblTrackMaxDist2 = new wxStaticText( m_tabSettingsCentroids, wxID_ANY, _("pixels"), wxDefaultPosition, wxDefaultSize, 0 );
	m_lblTrackMaxDist2->Wrap( -1 );
	bSizerTrackMaxDist->Add( m_lblTrackMaxDist2, 0, wxALIGN_CENTER_VERTICAL|wxLEFT, 5 );
	
	
	bSizerTabSettingsCentroids->Add( bSizerTrackMaxDist, 0, wxEXPAND|wxLEFT|wxRIGHT, 5 );
	
	wxBoxSizer* bSizerTrackTraj;
	bSizerTrackTraj = new wxBoxSizer( wxHORIZONTAL );
	
	m_chbTrackTraj = new wxCheckBox( m_tabSettingsCentroids, wxID_ANY, _("Show trajectory for"), wxDefaultPosition, wxDefaultSize, 0 );
	m_chbTrackTraj->SetValue(true); 
	bSizerTrackTraj->Add( m_chbTrackTraj, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT, 5 );
	
	m_editTrackTraj = new wxSpinCtrl( m_tabSettingsCentroids, wxID_ANY, wxT("10"), wxDefaultPosition, wxSize( 55,-1 ), wxSP_ARROW_KEYS, 0, 100, 0 );
	bSizerTrackTraj->Add( m_editTrackTraj, 0, wxALIGN_CENTER_VERTICAL|wxBOTTOM|wxTOP, 1 );
	
	m_lblTrackTraj = new wxStaticText( m_tabSettingsCentroids, wxID_ANY, _("frames"), wxDefaultPosition, wxDefaultSize, 0 );
	m_lblTrackTraj->Wrap( -1 );
	bSizerTrackTraj->Add( m_lblTrackTraj, 0, wxALIGN_CENTER_VERTICAL|wxLEFT, 5 );
	
	
	bSizerTabSettingsCentroids->Add( bSizerTrackTraj, 0, wxEXPAND|wxLEFT|wxRIGHT, 5 );
	
	wxBoxSizer* bSizerMetaRoiOutline;
	bSizerMetaRoiOutline = new wxBoxSizer( wxHORIZONTAL );
	
	m_chbMetaRoiOutline = new wxCheckBox( m_tabSettingsCentroids, wxID_ANY, _("Outline detected objects"), wxDefaultPosition, wxDefaultSize, 0 );
	m_chbMetaRoiOutline->SetValue(true); 
	bSizerMetaRoiOutline->Add( m_chbMetaRoiOutline, 0, wxALIGN_CENTER_VERTICAL, 5 );
	
	
	bSizerTabSettingsCentroids->Add( bSizerMetaRoiOutline, 0, wxBOTTOM|wxEXPAND|wxLEFT|wxRIGHT|wxTOP, 5 );
	
	
	m_tabSettingsCentroids->SetSizer( bSizerTabSettingsCentroids );
	m_tabSettingsCentroids->Layout();
	bSizerTabSettingsCentroids->Fit( m_tabSettingsCentroids );
	m_tabsSettings->AddPage( m_tabSettingsCentroids, _("Centroids"), false );
	m_tabSettingsTracking = new wxPanel( m_tabsSettings, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizerTabSettingsTrack;
	bSizerTabSettingsTrack = new wxBoxSizer( wxVERTICAL );
	
	m_dvlCentroids = new RegionListCtrl( m_tabSettingsTracking, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxDV_HORIZ_RULES|wxDV_ROW_LINES|wxDV_VERT_RULES );
	m_dvlCentroids->SetMinSize( wxSize( -1,100 ) );
	
	bSizerTabSettingsTrack->Add( m_dvlCentroids, 1, wxBOTTOM|wxEXPAND|wxLEFT|wxRIGHT|wxTOP, 5 );
	
	
	m_tabSettingsTracking->SetSizer( bSizerTabSettingsTrack );
	m_tabSettingsTracking->Layout();
	bSizerTabSettingsTrack->Fit( m_tabSettingsTracking );
	m_tabsSettings->AddPage( m_tabSettingsTracking, _("Tracking"), false );
	m_tabSettingsColor = new wxPanel( m_tabsSettings, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizerTabSettingsColor;
	bSizerTabSettingsColor = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizerColorEnable;
	bSizerColorEnable = new wxBoxSizer( wxHORIZONTAL );
	
	m_chbColorEnable = new wxCheckBox( m_tabSettingsColor, wxID_ANY, _("Enable color processing"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizerColorEnable->Add( m_chbColorEnable, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT, 5 );
	
	m_chbColorOverrideMask = new wxCheckBox( m_tabSettingsColor, wxID_ANY, _("Override mask"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizerColorEnable->Add( m_chbColorOverrideMask, 0, wxALIGN_CENTER_VERTICAL|wxLEFT, 5 );
	
	wxArrayString m_cbColorCustomMaskChoices;
	m_cbColorCustomMask = new wxChoice( m_tabSettingsColor, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_cbColorCustomMaskChoices, 0 );
	m_cbColorCustomMask->SetSelection( 0 );
	bSizerColorEnable->Add( m_cbColorCustomMask, 0, wxALIGN_CENTER_VERTICAL|wxBOTTOM|wxEXPAND, 1 );
	
	
	bSizerTabSettingsColor->Add( bSizerColorEnable, 0, wxEXPAND|wxLEFT|wxRIGHT|wxTOP, 5 );
	
	wxBoxSizer* bSizerAutoWhiteBalance;
	bSizerAutoWhiteBalance = new wxBoxSizer( wxHORIZONTAL );
	
	m_btnAutoWhiteBalance = new wxButton( m_tabSettingsColor, wxID_ANY, _("Initialize RGB scale factors"), wxDefaultPosition, wxDefaultSize, 0 );
	m_btnAutoWhiteBalance->SetToolTip( _("During this process multiple exposures will be taken") );
	
	bSizerAutoWhiteBalance->Add( m_btnAutoWhiteBalance, 1, wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );
	
	m_btnDefaultWhiteBalance = new wxButton( m_tabSettingsColor, wxID_ANY, _("Default"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizerAutoWhiteBalance->Add( m_btnDefaultWhiteBalance, 0, wxALIGN_CENTER_VERTICAL|wxLEFT, 5 );
	
	
	bSizerTabSettingsColor->Add( bSizerAutoWhiteBalance, 0, wxEXPAND|wxLEFT|wxRIGHT, 5 );
	
	wxFlexGridSizer* fgSizerScales;
	fgSizerScales = new wxFlexGridSizer( 0, 2, 0, 2 );
	fgSizerScales->AddGrowableCol( 0 );
	fgSizerScales->SetFlexibleDirection( wxHORIZONTAL );
	fgSizerScales->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	
	m_panelRed = new wxPanel( m_tabSettingsColor, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	m_panelRed->SetBackgroundColour( wxColour( 255, 170, 170 ) );
	
	wxBoxSizer* bSizerRed;
	bSizerRed = new wxBoxSizer( wxHORIZONTAL );
	
	m_lblRed = new wxStaticText( m_panelRed, wxID_ANY, _(" R "), wxDefaultPosition, wxSize( 20,-1 ), 0 );
	m_lblRed->Wrap( -1 );
	m_lblRed->SetBackgroundColour( wxColour( 255, 170, 170 ) );
	
	bSizerRed->Add( m_lblRed, 0, wxALIGN_CENTER, 5 );
	
	m_sliderRed = new wxSlider( m_panelRed, wxID_ANY, 50, 0, 100, wxDefaultPosition, wxDefaultSize, wxSL_HORIZONTAL|wxNO_BORDER );
	m_sliderRed->SetBackgroundColour( wxColour( 255, 170, 170 ) );
	
	bSizerRed->Add( m_sliderRed, 1, wxALIGN_CENTER_VERTICAL|wxBOTTOM|wxEXPAND, 3 );
	
	
	m_panelRed->SetSizer( bSizerRed );
	m_panelRed->Layout();
	bSizerRed->Fit( m_panelRed );
	fgSizerScales->Add( m_panelRed, 1, wxEXPAND, 5 );
	
	m_editDblRed = new wxSpinCtrlDouble( m_tabSettingsColor, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 65,-1 ), wxSP_ARROW_KEYS, 0.01, 100, 1, 1 );
	fgSizerScales->Add( m_editDblRed, 0, wxALIGN_CENTER_VERTICAL|wxBOTTOM|wxLEFT|wxTOP, 1 );
	
	m_panelGreen = new wxPanel( m_tabSettingsColor, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	m_panelGreen->SetBackgroundColour( wxColour( 170, 255, 170 ) );
	
	wxBoxSizer* bSizerGreen;
	bSizerGreen = new wxBoxSizer( wxHORIZONTAL );
	
	m_lblGreen = new wxStaticText( m_panelGreen, wxID_ANY, _(" G "), wxDefaultPosition, wxSize( 20,-1 ), 0 );
	m_lblGreen->Wrap( -1 );
	m_lblGreen->SetBackgroundColour( wxColour( 170, 255, 170 ) );
	
	bSizerGreen->Add( m_lblGreen, 0, wxALIGN_CENTER, 5 );
	
	m_sliderGreen = new wxSlider( m_panelGreen, wxID_ANY, 50, 0, 100, wxDefaultPosition, wxDefaultSize, wxSL_HORIZONTAL|wxNO_BORDER );
	m_sliderGreen->SetBackgroundColour( wxColour( 170, 255, 170 ) );
	
	bSizerGreen->Add( m_sliderGreen, 1, wxALIGN_CENTER_VERTICAL|wxBOTTOM|wxEXPAND, 3 );
	
	
	m_panelGreen->SetSizer( bSizerGreen );
	m_panelGreen->Layout();
	bSizerGreen->Fit( m_panelGreen );
	fgSizerScales->Add( m_panelGreen, 1, wxEXPAND, 5 );
	
	m_editDblGreen = new wxSpinCtrlDouble( m_tabSettingsColor, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 65,-1 ), wxSP_ARROW_KEYS, 0.01, 100, 1, 1 );
	fgSizerScales->Add( m_editDblGreen, 0, wxALIGN_CENTER_VERTICAL|wxBOTTOM|wxLEFT|wxTOP, 1 );
	
	m_panelBlue = new wxPanel( m_tabSettingsColor, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	m_panelBlue->SetBackgroundColour( wxColour( 170, 255, 255 ) );
	
	wxBoxSizer* bSizerBlue;
	bSizerBlue = new wxBoxSizer( wxHORIZONTAL );
	
	m_lblBlue = new wxStaticText( m_panelBlue, wxID_ANY, _(" B "), wxDefaultPosition, wxSize( 20,-1 ), 0 );
	m_lblBlue->Wrap( -1 );
	m_lblBlue->SetBackgroundColour( wxColour( 170, 255, 255 ) );
	
	bSizerBlue->Add( m_lblBlue, 0, wxALIGN_CENTER, 5 );
	
	m_sliderBlue = new wxSlider( m_panelBlue, wxID_ANY, 50, 0, 100, wxDefaultPosition, wxDefaultSize, wxSL_HORIZONTAL|wxNO_BORDER );
	m_sliderBlue->SetBackgroundColour( wxColour( 170, 255, 255 ) );
	
	bSizerBlue->Add( m_sliderBlue, 1, wxALIGN_CENTER_VERTICAL|wxBOTTOM|wxEXPAND, 3 );
	
	
	m_panelBlue->SetSizer( bSizerBlue );
	m_panelBlue->Layout();
	bSizerBlue->Fit( m_panelBlue );
	fgSizerScales->Add( m_panelBlue, 1, wxEXPAND, 5 );
	
	m_editDblBlue = new wxSpinCtrlDouble( m_tabSettingsColor, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 65,-1 ), wxSP_ARROW_KEYS, 0.01, 100, 1, 1 );
	fgSizerScales->Add( m_editDblBlue, 0, wxALIGN_CENTER_VERTICAL|wxBOTTOM|wxLEFT|wxTOP, 1 );
	
	
	bSizerTabSettingsColor->Add( fgSizerScales, 0, wxEXPAND|wxLEFT|wxRIGHT|wxTOP, 5 );
	
	wxBoxSizer* bSizerScaleLabels;
	bSizerScaleLabels = new wxBoxSizer( wxHORIZONTAL );
	
	m_lblScaleMin = new wxStaticText( m_tabSettingsColor, wxID_ANY, _("0.01"), wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT );
	m_lblScaleMin->Wrap( -1 );
	bSizerScaleLabels->Add( m_lblScaleMin, 0, wxLEFT, 20 );
	
	
	bSizerScaleLabels->Add( 0, 0, 1, wxEXPAND, 5 );
	
	m_lblScaleDefault = new wxStaticText( m_tabSettingsColor, wxID_ANY, _("1"), wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE );
	m_lblScaleDefault->Wrap( -1 );
	bSizerScaleLabels->Add( m_lblScaleDefault, 0, wxLEFT|wxRIGHT, 5 );
	
	
	bSizerScaleLabels->Add( 0, 0, 1, wxEXPAND, 5 );
	
	m_lblScaleMax = new wxStaticText( m_tabSettingsColor, wxID_ANY, _("100"), wxDefaultPosition, wxDefaultSize, wxALIGN_RIGHT );
	m_lblScaleMax->Wrap( -1 );
	bSizerScaleLabels->Add( m_lblScaleMax, 0, wxRIGHT, 67 );
	
	
	bSizerTabSettingsColor->Add( bSizerScaleLabels, 0, wxBOTTOM|wxEXPAND|wxLEFT|wxRIGHT, 5 );
	
	wxBoxSizer* bSizerDebayerAlgorithm;
	bSizerDebayerAlgorithm = new wxBoxSizer( wxHORIZONTAL );
	
	m_lblDebayerAlgorithm = new wxStaticText( m_tabSettingsColor, wxID_ANY, _("Debayer algorithm"), wxDefaultPosition, wxDefaultSize, 0 );
	m_lblDebayerAlgorithm->Wrap( -1 );
	bSizerDebayerAlgorithm->Add( m_lblDebayerAlgorithm, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT, 5 );
	
	wxString m_cbDebayerAlgorithmChoices[] = { _("<debayer-algorithms>") };
	int m_cbDebayerAlgorithmNChoices = sizeof( m_cbDebayerAlgorithmChoices ) / sizeof( wxString );
	m_cbDebayerAlgorithm = new wxChoice( m_tabSettingsColor, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_cbDebayerAlgorithmNChoices, m_cbDebayerAlgorithmChoices, 0 );
	m_cbDebayerAlgorithm->SetSelection( 0 );
	bSizerDebayerAlgorithm->Add( m_cbDebayerAlgorithm, 1, wxALIGN_CENTER_VERTICAL|wxBOTTOM|wxTOP, 1 );
	
	
	bSizerTabSettingsColor->Add( bSizerDebayerAlgorithm, 0, wxEXPAND|wxLEFT|wxRIGHT, 5 );
	
	wxBoxSizer* bSizerAutoExposureAlgorithm;
	bSizerAutoExposureAlgorithm = new wxBoxSizer( wxHORIZONTAL );
	
	m_lblAutoExposureAlgorithm = new wxStaticText( m_tabSettingsColor, wxID_ANY, _("Auto exposure algorithm"), wxDefaultPosition, wxDefaultSize, 0 );
	m_lblAutoExposureAlgorithm->Wrap( -1 );
	bSizerAutoExposureAlgorithm->Add( m_lblAutoExposureAlgorithm, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT, 5 );
	
	wxString m_cbAutoExposureAlgorithmChoices[] = { _("<autoexp-algorithms>") };
	int m_cbAutoExposureAlgorithmNChoices = sizeof( m_cbAutoExposureAlgorithmChoices ) / sizeof( wxString );
	m_cbAutoExposureAlgorithm = new wxChoice( m_tabSettingsColor, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_cbAutoExposureAlgorithmNChoices, m_cbAutoExposureAlgorithmChoices, 0 );
	m_cbAutoExposureAlgorithm->SetSelection( 0 );
	bSizerAutoExposureAlgorithm->Add( m_cbAutoExposureAlgorithm, 1, wxALIGN_CENTER_VERTICAL|wxBOTTOM|wxTOP, 1 );
	
	
	bSizerTabSettingsColor->Add( bSizerAutoExposureAlgorithm, 0, wxEXPAND|wxLEFT|wxRIGHT, 5 );
	
	wxBoxSizer* bSizerColorCpuOnly;
	bSizerColorCpuOnly = new wxBoxSizer( wxHORIZONTAL );
	
	m_chbColorCpuOnly = new wxCheckBox( m_tabSettingsColor, wxID_ANY, _("Processing on CPU only"), wxDefaultPosition, wxDefaultSize, 0 );
	m_chbColorCpuOnly->SetValue(true); 
	m_chbColorCpuOnly->SetToolTip( _("Does not use GPU for processing even if available") );
	
	bSizerColorCpuOnly->Add( m_chbColorCpuOnly, 0, wxALIGN_CENTER_VERTICAL, 5 );
	
	
	bSizerTabSettingsColor->Add( bSizerColorCpuOnly, 1, wxBOTTOM|wxEXPAND|wxLEFT|wxRIGHT|wxTOP, 5 );
	
	
	m_tabSettingsColor->SetSizer( bSizerTabSettingsColor );
	m_tabSettingsColor->Layout();
	bSizerTabSettingsColor->Fit( m_tabSettingsColor );
	m_tabsSettings->AddPage( m_tabSettingsColor, _("Color"), false );
	m_tabSettingsSaving = new wxPanel( m_tabsSettings, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizerTabSettingsSaving;
	bSizerTabSettingsSaving = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizerSaveAs;
	bSizerSaveAs = new wxBoxSizer( wxVERTICAL );
	
	wxString m_cbSaveAsChoices[] = { _("<storage-types>") };
	int m_cbSaveAsNChoices = sizeof( m_cbSaveAsChoices ) / sizeof( wxString );
	m_cbSaveAs = new wxChoice( m_tabSettingsSaving, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_cbSaveAsNChoices, m_cbSaveAsChoices, 0 );
	m_cbSaveAs->SetSelection( 0 );
	bSizerSaveAs->Add( m_cbSaveAs, 0, wxALIGN_CENTER_VERTICAL|wxBOTTOM|wxEXPAND, 1 );
	
	
	bSizerTabSettingsSaving->Add( bSizerSaveAs, 0, wxEXPAND|wxLEFT|wxRIGHT|wxTOP, 5 );
	
	wxBoxSizer* bSizerSaveDir;
	bSizerSaveDir = new wxBoxSizer( wxHORIZONTAL );
	
	m_editSaveDir = new wxTextCtrl( m_tabSettingsSaving, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizerSaveDir->Add( m_editSaveDir, 1, wxALIGN_CENTER_VERTICAL|wxBOTTOM|wxEXPAND|wxTOP, 1 );
	
	m_btnSaveDir = new wxBitmapButton( m_tabSettingsSaving, wxID_ANY, folder_png_to_wx_bitmap(), wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW );
	bSizerSaveDir->Add( m_btnSaveDir, 0, wxALIGN_CENTER_VERTICAL|wxBOTTOM|wxLEFT|wxTOP, 1 );
	
	
	bSizerTabSettingsSaving->Add( bSizerSaveDir, 0, wxEXPAND|wxLEFT|wxRIGHT, 5 );
	
	wxBoxSizer* bSizerSaveDigits;
	bSizerSaveDigits = new wxBoxSizer( wxHORIZONTAL );
	
	m_lblSaveDigits = new wxStaticText( m_tabSettingsSaving, wxID_ANY, _("Use file name counter with"), wxDefaultPosition, wxDefaultSize, 0 );
	m_lblSaveDigits->Wrap( -1 );
	bSizerSaveDigits->Add( m_lblSaveDigits, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT, 5 );
	
	m_editSaveDigits = new wxSpinCtrl( m_tabSettingsSaving, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 60,-1 ), wxSP_ARROW_KEYS, 0, 9, 0 );
	bSizerSaveDigits->Add( m_editSaveDigits, 0, wxALIGN_CENTER_VERTICAL|wxBOTTOM|wxTOP, 1 );
	
	m_lblSaveDigits2 = new wxStaticText( m_tabSettingsSaving, wxID_ANY, _("fixed digits"), wxDefaultPosition, wxDefaultSize, 0 );
	m_lblSaveDigits2->Wrap( -1 );
	bSizerSaveDigits->Add( m_lblSaveDigits2, 0, wxALIGN_CENTER_VERTICAL|wxLEFT, 5 );
	
	
	bSizerTabSettingsSaving->Add( bSizerSaveDigits, 0, wxEXPAND|wxLEFT|wxRIGHT, 5 );
	
	wxBoxSizer* bSizerSaveFirst;
	bSizerSaveFirst = new wxBoxSizer( wxHORIZONTAL );
	
	m_chbSaveFirst = new wxCheckBox( m_tabSettingsSaving, wxID_ANY, _("Only first"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizerSaveFirst->Add( m_chbSaveFirst, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT, 5 );
	
	m_editSaveFirst = new wxSpinCtrl( m_tabSettingsSaving, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 80,-1 ), wxSP_ARROW_KEYS, 1, 2147483647, 1 );
	bSizerSaveFirst->Add( m_editSaveFirst, 0, wxALIGN_CENTER_VERTICAL|wxBOTTOM|wxTOP, 1 );
	
	m_lblSaveFirst = new wxStaticText( m_tabSettingsSaving, wxID_ANY, _("frames"), wxDefaultPosition, wxDefaultSize, 0 );
	m_lblSaveFirst->Wrap( -1 );
	bSizerSaveFirst->Add( m_lblSaveFirst, 0, wxALIGN_CENTER_VERTICAL|wxLEFT, 5 );
	
	
	bSizerTabSettingsSaving->Add( bSizerSaveFirst, 0, wxEXPAND|wxLEFT|wxRIGHT, 5 );
	
	wxBoxSizer* bSizerSaveLast;
	bSizerSaveLast = new wxBoxSizer( wxHORIZONTAL );
	
	m_chbSaveLast = new wxCheckBox( m_tabSettingsSaving, wxID_ANY, _("Only last"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizerSaveLast->Add( m_chbSaveLast, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT, 5 );
	
	m_editSaveLast = new wxSpinCtrl( m_tabSettingsSaving, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 80,-1 ), wxSP_ARROW_KEYS, 1, 2147483647, 1 );
	bSizerSaveLast->Add( m_editSaveLast, 0, wxALIGN_CENTER_VERTICAL|wxBOTTOM|wxTOP, 1 );
	
	m_lblSaveLast = new wxStaticText( m_tabSettingsSaving, wxID_ANY, _("frames"), wxDefaultPosition, wxDefaultSize, 0 );
	m_lblSaveLast->Wrap( -1 );
	bSizerSaveLast->Add( m_lblSaveLast, 0, wxALIGN_CENTER_VERTICAL|wxLEFT, 5 );
	
	
	bSizerTabSettingsSaving->Add( bSizerSaveLast, 0, wxEXPAND|wxLEFT|wxRIGHT, 5 );
	
	wxBoxSizer* bSizerSaveStack;
	bSizerSaveStack = new wxBoxSizer( wxHORIZONTAL );
	
	m_chbSaveStack = new wxCheckBox( m_tabSettingsSaving, wxID_ANY, _("Image stack"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizerSaveStack->Add( m_chbSaveStack, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT, 5 );
	
	m_editSaveStack = new wxSpinCtrl( m_tabSettingsSaving, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 80,-1 ), wxSP_ARROW_KEYS, 0, 2147483647, 10 );
	bSizerSaveStack->Add( m_editSaveStack, 0, wxALIGN_CENTER_VERTICAL|wxBOTTOM|wxTOP, 1 );
	
	m_lblSaveStack = new wxStaticText( m_tabSettingsSaving, wxID_ANY, _("MiB (1 MiB = 1024*1024 B)"), wxDefaultPosition, wxDefaultSize, 0 );
	m_lblSaveStack->Wrap( -1 );
	bSizerSaveStack->Add( m_lblSaveStack, 0, wxALIGN_CENTER_VERTICAL|wxLEFT, 5 );
	
	
	bSizerTabSettingsSaving->Add( bSizerSaveStack, 0, wxBOTTOM|wxEXPAND|wxLEFT|wxRIGHT, 5 );
	
	
	m_tabSettingsSaving->SetSizer( bSizerTabSettingsSaving );
	m_tabSettingsSaving->Layout();
	bSizerTabSettingsSaving->Fit( m_tabSettingsSaving );
	m_tabsSettings->AddPage( m_tabSettingsSaving, _("Saving"), false );
	m_tabSettingsDisplay = new wxPanel( m_tabsSettings, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizerTabSettingsDisplay;
	bSizerTabSettingsDisplay = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizerParamBrowser;
	bSizerParamBrowser = new wxBoxSizer( wxHORIZONTAL );
	
	m_btnOpenParameterBrowser = new wxButton( m_tabSettingsDisplay, wxID_ANY, _("Open Parameter Browser"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizerParamBrowser->Add( m_btnOpenParameterBrowser, 1, wxALIGN_CENTER_VERTICAL, 1 );
	
	
	bSizerTabSettingsDisplay->Add( bSizerParamBrowser, 0, wxEXPAND|wxLEFT|wxRIGHT|wxTOP, 5 );
	
	wxBoxSizer* bSizerFillMethod;
	bSizerFillMethod = new wxBoxSizer( wxHORIZONTAL );
	
	m_lblFillMethod = new wxStaticText( m_tabSettingsDisplay, wxID_ANY, _("Fill image with"), wxDefaultPosition, wxDefaultSize, 0 );
	m_lblFillMethod->Wrap( -1 );
	bSizerFillMethod->Add( m_lblFillMethod, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT, 5 );
	
	wxString m_cbFillMethodChoices[] = { _("<fill-methods>") };
	int m_cbFillMethodNChoices = sizeof( m_cbFillMethodChoices ) / sizeof( wxString );
	m_cbFillMethod = new wxChoice( m_tabSettingsDisplay, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_cbFillMethodNChoices, m_cbFillMethodChoices, 0 );
	m_cbFillMethod->SetSelection( 0 );
	bSizerFillMethod->Add( m_cbFillMethod, 1, wxALIGN_CENTER_VERTICAL|wxBOTTOM|wxTOP, 1 );
	
	
	bSizerTabSettingsDisplay->Add( bSizerFillMethod, 0, wxEXPAND|wxLEFT|wxRIGHT, 5 );
	
	wxBoxSizer* bSizerAutoContrast;
	bSizerAutoContrast = new wxBoxSizer( wxHORIZONTAL );
	
	m_chbAutoConbright = new wxCheckBox( m_tabSettingsDisplay, wxID_ANY, _("Automatically adjust image contrast and brightness"), wxDefaultPosition, wxDefaultSize, 0 );
	m_chbAutoConbright->SetValue(true); 
	bSizerAutoContrast->Add( m_chbAutoConbright, 0, wxALIGN_CENTER_VERTICAL, 5 );
	
	
	bSizerTabSettingsDisplay->Add( bSizerAutoContrast, 0, wxBOTTOM|wxEXPAND|wxLEFT|wxRIGHT|wxTOP, 5 );
	
	wxFlexGridSizer* fgSizerAutoConbright;
	fgSizerAutoConbright = new wxFlexGridSizer( 2, 3, 0, 0 );
	fgSizerAutoConbright->AddGrowableCol( 1 );
	fgSizerAutoConbright->SetFlexibleDirection( wxHORIZONTAL );
	fgSizerAutoConbright->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );
	
	m_lblContrast = new wxStaticText( m_tabSettingsDisplay, wxID_ANY, _("Contrast"), wxDefaultPosition, wxDefaultSize, 0 );
	m_lblContrast->Wrap( -1 );
	fgSizerAutoConbright->Add( m_lblContrast, 0, wxALIGN_CENTER_VERTICAL|wxALIGN_RIGHT|wxLEFT, 5 );
	
	m_sliderContrast = new wxSlider( m_tabSettingsDisplay, wxID_ANY, 0, -255, 255, wxDefaultPosition, wxDefaultSize, wxSL_HORIZONTAL );
	m_sliderContrast->Enable( false );
	
	fgSizerAutoConbright->Add( m_sliderContrast, 1, wxALIGN_CENTER_VERTICAL|wxEXPAND|wxLEFT|wxRIGHT|wxTOP, 2 );
	
	m_editContrast = new wxSpinCtrl( m_tabSettingsDisplay, wxID_ANY, wxT("0"), wxDefaultPosition, wxSize( 55,-1 ), wxSP_ARROW_KEYS, -255, 255, 0 );
	m_editContrast->Enable( false );
	
	fgSizerAutoConbright->Add( m_editContrast, 0, wxALIGN_CENTER_VERTICAL|wxBOTTOM|wxTOP, 1 );
	
	m_lblBrightness = new wxStaticText( m_tabSettingsDisplay, wxID_ANY, _("Brightness"), wxDefaultPosition, wxDefaultSize, 0 );
	m_lblBrightness->Wrap( -1 );
	fgSizerAutoConbright->Add( m_lblBrightness, 0, wxALIGN_CENTER_VERTICAL|wxALIGN_RIGHT|wxLEFT, 5 );
	
	m_sliderBrightness = new wxSlider( m_tabSettingsDisplay, wxID_ANY, 0, -255, 255, wxDefaultPosition, wxDefaultSize, wxSL_HORIZONTAL );
	m_sliderBrightness->Enable( false );
	
	fgSizerAutoConbright->Add( m_sliderBrightness, 1, wxALIGN_CENTER_VERTICAL|wxEXPAND|wxLEFT|wxRIGHT|wxTOP, 1 );
	
	m_editBrightness = new wxSpinCtrl( m_tabSettingsDisplay, wxID_ANY, wxT("0"), wxDefaultPosition, wxSize( 55,-1 ), wxSP_ARROW_KEYS, -255, 255, 0 );
	m_editBrightness->Enable( false );
	
	fgSizerAutoConbright->Add( m_editBrightness, 0, wxALIGN_CENTER_VERTICAL|wxBOTTOM|wxTOP, 1 );
	
	
	bSizerTabSettingsDisplay->Add( fgSizerAutoConbright, 0, wxEXPAND|wxLEFT|wxRIGHT, 5 );
	
	wxBoxSizer* bSizerZoomFactor;
	bSizerZoomFactor = new wxBoxSizer( wxHORIZONTAL );
	
	m_lblZoomFactor = new wxStaticText( m_tabSettingsDisplay, wxID_ANY, _("Zoom factor"), wxDefaultPosition, wxDefaultSize, 0 );
	m_lblZoomFactor->Wrap( -1 );
	bSizerZoomFactor->Add( m_lblZoomFactor, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT, 5 );
	
	wxString m_cbZoomFactorChoices[] = { _("<zoom-factors>") };
	int m_cbZoomFactorNChoices = sizeof( m_cbZoomFactorChoices ) / sizeof( wxString );
	m_cbZoomFactor = new wxChoice( m_tabSettingsDisplay, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_cbZoomFactorNChoices, m_cbZoomFactorChoices, 0 );
	m_cbZoomFactor->SetSelection( 0 );
	bSizerZoomFactor->Add( m_cbZoomFactor, 1, wxALIGN_CENTER_VERTICAL|wxBOTTOM|wxTOP, 1 );
	
	
	bSizerTabSettingsDisplay->Add( bSizerZoomFactor, 0, wxEXPAND|wxLEFT|wxRIGHT, 5 );
	
	wxBoxSizer* bSizerRoiOutline;
	bSizerRoiOutline = new wxBoxSizer( wxHORIZONTAL );
	
	m_chbRoiOutline = new wxCheckBox( m_tabSettingsDisplay, wxID_ANY, _("Keep regions outlined during acquisition"), wxDefaultPosition, wxDefaultSize, 0 );
	m_chbRoiOutline->SetValue(true); 
	bSizerRoiOutline->Add( m_chbRoiOutline, 0, wxALIGN_CENTER_VERTICAL, 5 );
	
	
	bSizerTabSettingsDisplay->Add( bSizerRoiOutline, 0, wxEXPAND|wxLEFT|wxRIGHT|wxTOP, 5 );
	
	wxBoxSizer* bSizerLiveImage;
	bSizerLiveImage = new wxBoxSizer( wxHORIZONTAL );
	
	m_chbLiveImage = new wxCheckBox( m_tabSettingsDisplay, wxID_ANY, _("Show image during acquisition"), wxDefaultPosition, wxDefaultSize, 0 );
	m_chbLiveImage->SetValue(true); 
	bSizerLiveImage->Add( m_chbLiveImage, 0, wxALIGN_CENTER_VERTICAL, 5 );
	
	
	bSizerTabSettingsDisplay->Add( bSizerLiveImage, 0, wxBOTTOM|wxEXPAND|wxLEFT|wxRIGHT|wxTOP, 5 );
	
	
	m_tabSettingsDisplay->SetSizer( bSizerTabSettingsDisplay );
	m_tabSettingsDisplay->Layout();
	bSizerTabSettingsDisplay->Fit( m_tabSettingsDisplay );
	m_tabsSettings->AddPage( m_tabSettingsDisplay, _("Display"), false );
	
	sbSizerSettings->Add( m_tabsSettings, 1, wxEXPAND, 5 );
	
	
	bSizerMain->Add( sbSizerSettings, 0, wxEXPAND|wxLEFT|wxRIGHT|wxTOP, 5 );
	
	wxStaticBoxSizer* sbSizerBuffers;
	sbSizerBuffers = new wxStaticBoxSizer( new wxStaticBox( this, wxID_ANY, _("Buffers") ), wxVERTICAL );
	
	wxBoxSizer* bSizerBuffer;
	bSizerBuffer = new wxBoxSizer( wxHORIZONTAL );
	
	m_lblCircFrameCount = new wxStaticText( sbSizerBuffers->GetStaticBox(), wxID_ANY, _("Frames"), wxDefaultPosition, wxSize( -1,-1 ), wxALIGN_RIGHT );
	m_lblCircFrameCount->Wrap( -1 );
	bSizerBuffer->Add( m_lblCircFrameCount, 0, wxALIGN_CENTER_VERTICAL|wxBOTTOM|wxRIGHT|wxTOP, 5 );
	
	m_editCircFrameCount = new wxSpinCtrl( sbSizerBuffers->GetStaticBox(), wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 60,-1 ), wxSP_ARROW_KEYS, 2, 65535, 10 );
	bSizerBuffer->Add( m_editCircFrameCount, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT, 5 );
	
	m_lblCircFrameNrLabel = new wxStaticText( sbSizerBuffers->GetStaticBox(), wxID_ANY, _("Cur. frame nr.:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_lblCircFrameNrLabel->Wrap( -1 );
	bSizerBuffer->Add( m_lblCircFrameNrLabel, 0, wxALIGN_CENTER_VERTICAL|wxBOTTOM|wxLEFT|wxRIGHT|wxTOP, 5 );
	
	m_lblCircFrameNr = new wxStaticText( sbSizerBuffers->GetStaticBox(), wxID_ANY, _("0"), wxDefaultPosition, wxDefaultSize, 0 );
	m_lblCircFrameNr->Wrap( -1 );
	m_lblCircFrameNr->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD, false, wxEmptyString ) );
	
	bSizerBuffer->Add( m_lblCircFrameNr, 3, wxALIGN_CENTER_VERTICAL|wxBOTTOM|wxEXPAND|wxRIGHT|wxTOP, 5 );
	
	m_lblAcqFpsLabel = new wxStaticText( sbSizerBuffers->GetStaticBox(), wxID_ANY, _("FPS:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_lblAcqFpsLabel->Wrap( -1 );
	bSizerBuffer->Add( m_lblAcqFpsLabel, 0, wxALIGN_CENTER_VERTICAL|wxBOTTOM|wxLEFT|wxRIGHT|wxTOP, 5 );
	
	m_lblAcqFps = new wxStaticText( sbSizerBuffers->GetStaticBox(), wxID_ANY, _("0"), wxDefaultPosition, wxDefaultSize, 0 );
	m_lblAcqFps->Wrap( -1 );
	m_lblAcqFps->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD, false, wxEmptyString ) );
	
	bSizerBuffer->Add( m_lblAcqFps, 2, wxBOTTOM|wxEXPAND|wxTOP, 5 );
	
	
	sbSizerBuffers->Add( bSizerBuffer, 0, wxALIGN_CENTER_HORIZONTAL|wxEXPAND|wxLEFT|wxRIGHT, 5 );
	
	m_sliderFrame = new wxSlider( sbSizerBuffers->GetStaticBox(), wxID_ANY, 50, 0, 100, wxDefaultPosition, wxDefaultSize, wxSL_HORIZONTAL );
	sbSizerBuffers->Add( m_sliderFrame, 0, wxALIGN_BOTTOM|wxBOTTOM|wxEXPAND, 5 );
	
	wxBoxSizer* bSizerBufferAcq;
	bSizerBufferAcq = new wxBoxSizer( wxHORIZONTAL );
	
	m_lblBufferAcq = new wxStaticText( sbSizerBuffers->GetStaticBox(), wxID_ANY, _("Fast acquisition cache usage"), wxDefaultPosition, wxDefaultSize, 0 );
	m_lblBufferAcq->Wrap( -1 );
	bSizerBufferAcq->Add( m_lblBufferAcq, 0, wxBOTTOM|wxRIGHT, 2 );
	
	m_lblBufferAcqLost = new wxStaticText( sbSizerBuffers->GetStaticBox(), wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT );
	m_lblBufferAcqLost->Wrap( -1 );
	bSizerBufferAcq->Add( m_lblBufferAcqLost, 1, wxBOTTOM|wxEXPAND|wxLEFT|wxRIGHT, 2 );
	
	m_lblBufferAcqMax = new wxStaticText( sbSizerBuffers->GetStaticBox(), wxID_ANY, _("0"), wxDefaultPosition, wxDefaultSize, 0 );
	m_lblBufferAcqMax->Wrap( -1 );
	m_lblBufferAcqMax->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD, false, wxEmptyString ) );
	m_lblBufferAcqMax->SetToolTip( _("Acquisition queue is roughly a half of the circular buffer.\nIts size does not change during acquisition.") );
	
	bSizerBufferAcq->Add( m_lblBufferAcqMax, 0, wxBOTTOM|wxEXPAND|wxLEFT, 2 );
	
	
	sbSizerBuffers->Add( bSizerBufferAcq, 0, wxEXPAND|wxLEFT|wxRIGHT, 5 );
	
	m_gaugeBufferAcq = new wxGauge( sbSizerBuffers->GetStaticBox(), wxID_ANY, 100, wxDefaultPosition, wxSize( -1,10 ), wxGA_HORIZONTAL|wxGA_SMOOTH );
	m_gaugeBufferAcq->SetValue( 0 ); 
	sbSizerBuffers->Add( m_gaugeBufferAcq, 0, wxBOTTOM|wxEXPAND|wxLEFT|wxRIGHT, 5 );
	
	wxBoxSizer* bSizerBufferDisk;
	bSizerBufferDisk = new wxBoxSizer( wxHORIZONTAL );
	
	m_lblBufferDisk = new wxStaticText( sbSizerBuffers->GetStaticBox(), wxID_ANY, _("Processing cache usage"), wxDefaultPosition, wxDefaultSize, 0 );
	m_lblBufferDisk->Wrap( -1 );
	bSizerBufferDisk->Add( m_lblBufferDisk, 0, wxBOTTOM|wxRIGHT, 2 );
	
	m_lblBufferDiskLost = new wxStaticText( sbSizerBuffers->GetStaticBox(), wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxALIGN_LEFT );
	m_lblBufferDiskLost->Wrap( -1 );
	bSizerBufferDisk->Add( m_lblBufferDiskLost, 1, wxBOTTOM|wxEXPAND|wxLEFT|wxRIGHT, 2 );
	
	m_lblBufferDiskMax = new wxStaticText( sbSizerBuffers->GetStaticBox(), wxID_ANY, _("0"), wxDefaultPosition, wxDefaultSize, 0 );
	m_lblBufferDiskMax->Wrap( -1 );
	m_lblBufferDiskMax->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD, false, wxEmptyString ) );
	m_lblBufferDiskMax->SetToolTip( _("Processing queue is used e.g. for saving frames or linking particles.\nIts capacity depends on available free RAM thus can change every few seconds during acquisition.") );
	
	bSizerBufferDisk->Add( m_lblBufferDiskMax, 0, wxBOTTOM|wxEXPAND|wxLEFT, 2 );
	
	
	sbSizerBuffers->Add( bSizerBufferDisk, 0, wxEXPAND|wxLEFT|wxRIGHT, 5 );
	
	m_gaugeBufferDisk = new wxGauge( sbSizerBuffers->GetStaticBox(), wxID_ANY, 100, wxDefaultPosition, wxSize( -1,10 ), wxGA_HORIZONTAL|wxGA_SMOOTH );
	m_gaugeBufferDisk->SetValue( 0 ); 
	sbSizerBuffers->Add( m_gaugeBufferDisk, 0, wxBOTTOM|wxEXPAND|wxLEFT|wxRIGHT, 5 );
	
	
	bSizerMain->Add( sbSizerBuffers, 0, wxEXPAND|wxLEFT|wxRIGHT|wxTOP, 5 );
	
	wxStaticBoxSizer* sbSizerAcquisition;
	sbSizerAcquisition = new wxStaticBoxSizer( new wxStaticBox( this, wxID_ANY, _("Acquisition") ), wxVERTICAL );
	
	wxBoxSizer* bSizerAcq;
	bSizerAcq = new wxBoxSizer( wxHORIZONTAL );
	
	wxString m_cbAcqModeChoices[] = { _("<acq-modes>") };
	int m_cbAcqModeNChoices = sizeof( m_cbAcqModeChoices ) / sizeof( wxString );
	m_cbAcqMode = new wxChoice( sbSizerAcquisition->GetStaticBox(), wxID_ANY, wxDefaultPosition, wxDefaultSize, m_cbAcqModeNChoices, m_cbAcqModeChoices, 0 );
	m_cbAcqMode->SetSelection( 0 );
	bSizerAcq->Add( m_cbAcqMode, 1, wxALIGN_CENTER_VERTICAL|wxEXPAND|wxLEFT|wxRIGHT, 5 );
	
	m_lblSnapFrameCount = new wxStaticText( sbSizerAcquisition->GetStaticBox(), wxID_ANY, _("Snap frames"), wxDefaultPosition, wxSize( -1,-1 ), wxALIGN_RIGHT );
	m_lblSnapFrameCount->Wrap( -1 );
	bSizerAcq->Add( m_lblSnapFrameCount, 0, wxALIGN_CENTER_VERTICAL|wxLEFT|wxRIGHT, 5 );
	
	m_editSnapFrameCount = new wxSpinCtrl( sbSizerAcquisition->GetStaticBox(), wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 80,-1 ), wxSP_ARROW_KEYS, 1, 65535, 1 );
	bSizerAcq->Add( m_editSnapFrameCount, 0, wxALIGN_CENTER_VERTICAL, 5 );
	
	
	sbSizerAcquisition->Add( bSizerAcq, 0, wxEXPAND|wxLEFT|wxRIGHT, 5 );
	
	wxBoxSizer* bSizerStartStop;
	bSizerStartStop = new wxBoxSizer( wxHORIZONTAL );
	
	m_btnStart = new wxButton( sbSizerAcquisition->GetStaticBox(), wxID_ANY, _("Start"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizerStartStop->Add( m_btnStart, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT, 5 );
	
	m_btnStop = new wxButton( sbSizerAcquisition->GetStaticBox(), wxID_ANY, _("Stop"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizerStartStop->Add( m_btnStop, 0, wxALIGN_CENTER_VERTICAL|wxLEFT, 5 );
	
	
	sbSizerAcquisition->Add( bSizerStartStop, 0, wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxBOTTOM|wxLEFT|wxRIGHT|wxTOP, 5 );
	
	
	bSizerMain->Add( sbSizerAcquisition, 0, wxEXPAND|wxLEFT|wxRIGHT|wxTOP, 5 );
	
	m_tabsInfos = new wxNotebook( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0 );
	m_tabLogs = new wxPanel( m_tabsInfos, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	m_tabLogs->SetBackgroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_WINDOW ) );
	
	wxBoxSizer* bSizerTabsLogs;
	bSizerTabsLogs = new wxBoxSizer( wxVERTICAL );
	
	m_lblLogs = new wxTextCtrl( m_tabLogs, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_MULTILINE|wxTE_NOHIDESEL|wxTE_READONLY|wxTE_WORDWRAP );
	m_lblLogs->SetMinSize( wxSize( -1,100 ) );
	
	bSizerTabsLogs->Add( m_lblLogs, 1, wxALL|wxEXPAND, 1 );
	
	
	m_tabLogs->SetSizer( bSizerTabsLogs );
	m_tabLogs->Layout();
	bSizerTabsLogs->Fit( m_tabLogs );
	m_tabsInfos->AddPage( m_tabLogs, _("Logs"), true );
	m_tabFrameDesc = new wxPanel( m_tabsInfos, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	m_tabFrameDesc->SetBackgroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_WINDOW ) );
	
	wxBoxSizer* bSizerTabsFrameDesc;
	bSizerTabsFrameDesc = new wxBoxSizer( wxVERTICAL );
	
	m_lblFrameDesc = new wxTextCtrl( m_tabFrameDesc, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_MULTILINE|wxTE_NOHIDESEL|wxTE_READONLY|wxTE_WORDWRAP );
	m_lblFrameDesc->SetMinSize( wxSize( -1,100 ) );
	
	bSizerTabsFrameDesc->Add( m_lblFrameDesc, 1, wxALL|wxEXPAND, 1 );
	
	
	m_tabFrameDesc->SetSizer( bSizerTabsFrameDesc );
	m_tabFrameDesc->Layout();
	bSizerTabsFrameDesc->Fit( m_tabFrameDesc );
	m_tabsInfos->AddPage( m_tabFrameDesc, _("Frame description"), false );
	
	bSizerMain->Add( m_tabsInfos, 1, wxEXPAND|wxLEFT|wxRIGHT|wxTOP, 5 );
	
	m_progressBar = new wxGauge( this, wxID_ANY, 100, wxDefaultPosition, wxSize( -1,15 ), wxGA_HORIZONTAL|wxGA_SMOOTH );
	m_progressBar->SetValue( 0 ); 
	bSizerMain->Add( m_progressBar, 0, wxBOTTOM|wxEXPAND|wxLEFT|wxRIGHT|wxTOP, 5 );
	
	
	this->SetSizer( bSizerMain );
	this->Layout();
	
	this->Centre( wxBOTH );
}

MainDlg::~MainDlg()
{
}

ImageDlg::ImageDlg( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxFrame( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );
	
	wxBoxSizer* bSizerImage;
	bSizerImage = new wxBoxSizer( wxVERTICAL );
	
	m_scrolledWindow = new wxScrolledWindow( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxHSCROLL|wxVSCROLL );
	m_scrolledWindow->SetScrollRate( 5, 5 );
	m_scrolledWindow->SetBackgroundColour( wxColour( 128, 128, 128 ) );
	
	wxBoxSizer* bSizerScrolledWindow;
	bSizerScrolledWindow = new wxBoxSizer( wxVERTICAL );
	
	m_canvas = new RegionShapeCtrl(m_scrolledWindow);
	bSizerScrolledWindow->Add( m_canvas, 1, wxEXPAND, 5 );
	
	
	m_scrolledWindow->SetSizer( bSizerScrolledWindow );
	m_scrolledWindow->Layout();
	bSizerScrolledWindow->Fit( m_scrolledWindow );
	bSizerImage->Add( m_scrolledWindow, 1, wxEXPAND, 5 );
	
	
	this->SetSizer( bSizerImage );
	this->Layout();
}

ImageDlg::~ImageDlg()
{
}

WhiteBalanceWizard::WhiteBalanceWizard( wxWindow* parent, wxWindowID id, const wxString& title, const wxBitmap& bitmap, const wxPoint& pos, long style ) 
{
	this->Create( parent, id, title, bitmap, pos, style );
	this->SetSizeHints( wxSize( -1,-1 ), wxSize( -1,-1 ) );
	
	wxWizardPageSimple* m_wizPageRegion = new wxWizardPageSimple( this );
	m_pages.Add( m_wizPageRegion );
	
	wxBoxSizer* bSizerWizPageRegion;
	bSizerWizPageRegion = new wxBoxSizer( wxVERTICAL );
	
	m_lblWizPageRegion = new wxStaticText( m_wizPageRegion, wxID_ANY, _("Please acquire at least one image and select an ROI of a region in which the scene is known to be uniformly white or grey.\n\nIn the next step the procedure will automatically adjust the exposure to find the best correction factors for color channels.\n\nThis can take couple of seconds.\n"), wxDefaultPosition, wxDefaultSize, 0 );
	m_lblWizPageRegion->Wrap( -1 );
	bSizerWizPageRegion->Add( m_lblWizPageRegion, 1, wxALL|wxEXPAND, 5 );
	
	m_lblWizPageRegionEnd = new wxStaticText( m_wizPageRegion, wxID_ANY, _("Please click the Finish button to start."), wxDefaultPosition, wxDefaultSize, 0 );
	m_lblWizPageRegionEnd->Wrap( -1 );
	bSizerWizPageRegion->Add( m_lblWizPageRegionEnd, 0, wxALL|wxEXPAND, 5 );
	
	
	m_wizPageRegion->SetSizer( bSizerWizPageRegion );
	m_wizPageRegion->Layout();
	bSizerWizPageRegion->Fit( m_wizPageRegion );
	
	this->Centre( wxBOTH );
	
	for ( unsigned int i = 1; i < m_pages.GetCount(); i++ )
	{
		m_pages.Item( i )->SetPrev( m_pages.Item( i - 1 ) );
		m_pages.Item( i - 1 )->SetNext( m_pages.Item( i ) );
	}
}

WhiteBalanceWizard::~WhiteBalanceWizard()
{
	m_pages.Clear();
}

ParamBrowserDlg::ParamBrowserDlg( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxSize( 410,-1 ), wxDefaultSize );
	
	wxBoxSizer* bSizerParamBrowser;
	bSizerParamBrowser = new wxBoxSizer( wxVERTICAL );
	
	m_splitter = new wxSplitterWindow( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxSP_3D|wxSP_LIVE_UPDATE|wxSP_NOBORDER );
	m_splitter->SetSashGravity( 1 );
	m_splitter->Connect( wxEVT_IDLE, wxIdleEventHandler( ParamBrowserDlg::m_splitterOnIdle ), NULL, this );
	m_splitter->SetMinimumPaneSize( 150 );
	
	m_panelParams = new wxPanel( m_splitter, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizerParams;
	bSizerParams = new wxBoxSizer( wxVERTICAL );
	
	m_pgManager = new wxPropertyGridManager(m_panelParams, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxPGMAN_DEFAULT_STYLE|wxPG_AUTO_SORT|wxPG_DESCRIPTION|wxPG_NO_INTERNAL_BORDER|wxPG_SPLITTER_AUTO_CENTER|wxPG_TOOLBAR|wxPG_TOOLTIPS);
	m_pgManager->SetExtraStyle( wxPG_EX_ENABLE_TLP_TRACKING|wxPG_EX_HIDE_PAGE_BUTTONS|wxPG_EX_INIT_NOCAT|wxPG_EX_MODE_BUTTONS|wxPG_EX_TOOLBAR_SEPARATOR ); 
	
	m_pgParams = m_pgManager->AddPage( _("Camera Parameters"), wxNullBitmap );
	bSizerParams->Add( m_pgManager, 1, wxEXPAND, 5 );
	
	
	m_panelParams->SetSizer( bSizerParams );
	m_panelParams->Layout();
	bSizerParams->Fit( m_panelParams );
	m_panelInfo = new wxPanel( m_splitter, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizerInfo;
	bSizerInfo = new wxBoxSizer( wxVERTICAL );
	
	m_pgInfo = new wxPropertyGrid(m_panelInfo, wxID_ANY, wxDefaultPosition, wxSize( -1,-1 ), wxPG_DEFAULT_STYLE|wxPG_SPLITTER_AUTO_CENTER|wxPG_TOOLTIPS);
	m_pgInfo->SetExtraStyle( wxPG_EX_ENABLE_TLP_TRACKING|wxPG_EX_HELP_AS_TOOLTIPS|wxPG_EX_MULTIPLE_SELECTION ); 
	bSizerInfo->Add( m_pgInfo, 1, wxEXPAND, 5 );
	
	
	m_panelInfo->SetSizer( bSizerInfo );
	m_panelInfo->Layout();
	bSizerInfo->Fit( m_panelInfo );
	m_splitter->SplitHorizontally( m_panelParams, m_panelInfo, -150 );
	bSizerParamBrowser->Add( m_splitter, 1, wxEXPAND, 5 );
	
	
	this->SetSizer( bSizerParamBrowser );
	this->Layout();
	
	this->Centre( wxBOTH );
}

ParamBrowserDlg::~ParamBrowserDlg()
{
}

/******************************************************************************/
/* Copyright (C) Teledyne Photometrics. All rights reserved.                  */
/******************************************************************************/
#include "backend/TaskSet_ComputeFrameStats.h"

/* Local */
#include "backend/Bitmap.h"
#include "backend/exceptions/Exception.h"

/* System */
#include <cassert>
#include <numeric>

// TaskSet_ComputeFrameStats::Task

pm::TaskSet_ComputeFrameStats::ATask::ATask(
        std::shared_ptr<Semaphore> semDone, size_t taskIndex, size_t taskCount)
    : pm::Task(semDone, taskIndex, taskCount),
    m_maxTasks(taskCount)
{
}

void pm::TaskSet_ComputeFrameStats::ATask::SetUp(const Bitmap* bmp,
        FrameStats* stats)
{
    const size_t dataBytes = bmp->GetDataBytes();

    m_maxTasks = (dataBytes < 256)
        ? (dataBytes == 0) ? 0 : 1
        : GetTaskCount();

    m_bmp = const_cast<Bitmap*>(bmp);
    m_stats = stats;
}

void pm::TaskSet_ComputeFrameStats::ATask::Execute()
{
    assert(m_bmp != nullptr);
    assert(m_stats != nullptr);

    m_stats->Clear();

    if (GetTaskIndex() >= m_maxTasks)
        return;

    switch (m_bmp->GetFormat().GetDataType())
    {
    case BitmapDataType::UInt8:
        ExecuteT<uint8_t>();
        break;
    case BitmapDataType::UInt16:
        Execute_UInt16_Mono();
        break;
    case BitmapDataType::UInt32:
        ExecuteT<uint32_t>();
        break;
    default:
        throw Exception("Unsupported bitmap data type");
    }
}

template<typename T>
void pm::TaskSet_ComputeFrameStats::ATask::ExecuteT()
{
    const size_t step = m_maxTasks;
    const size_t offset = GetTaskIndex();

    const size_t pixelCount = m_bmp->GetWidth() * m_bmp->GetHeight();

    // Prepare reversed illogical values as starting point
    T min = std::numeric_limits<T>::max();
    T max = std::numeric_limits<T>::lowest();
    double sum = 0;
    uint32_t count = 0;

    const T* dataStart = static_cast<T*>(m_bmp->GetData());
    const T* dataEnd = dataStart + pixelCount;
    const T* data = dataStart + offset;
    while (data < dataEnd)
    {
        if (min > *data)
            min = *data;
        else if (max < *data)
            max = *data;
        sum += *data;
        count++;
        data += step;
    }

    if (count > 0)
    {
        m_stats->Set(static_cast<double>(min), static_cast<double>(max), sum, count);
    }
}

void pm::TaskSet_ComputeFrameStats::ATask::Execute_UInt16_Mono()
{
    const size_t step = m_maxTasks;
    const size_t offset = GetTaskIndex();

    const size_t pixelCount = m_bmp->GetWidth() * m_bmp->GetHeight();

    // Prepare reversed illogical values as starting point
    uint16_t min = std::numeric_limits<uint16_t>::max();
    uint16_t max = std::numeric_limits<uint16_t>::lowest();
    uint64_t sum = 0;
    uint32_t count = 0;

    const uint16_t* dataStart = static_cast<uint16_t*>(m_bmp->GetData());
    const uint16_t* dataEnd = dataStart + pixelCount;
    const uint16_t* data = dataStart + offset;
    while (data < dataEnd)
    {
        if (min > *data)
            min = *data;
        else if (max < *data)
            max = *data;
        sum += *data;
        count++;
        data += step;
    }

    if (count > 0)
    {
        m_stats->Set(static_cast<double>(min), static_cast<double>(max),
                static_cast<double>(sum), count);
    }
}

// TaskSet_ComputeFrameStats

pm::TaskSet_ComputeFrameStats::TaskSet_ComputeFrameStats(
        std::shared_ptr<ThreadPool> pool)
    : TaskSet(pool)
{
    CreateTasks<ATask>();

    const size_t taskCount = GetTasks().size();
    m_taskStats.resize(taskCount);
}

void pm::TaskSet_ComputeFrameStats::SetUp(const Bitmap* bmp, FrameStats* stats)
{
    assert(bmp != nullptr);
    assert(stats != nullptr);

    // TODO: Implement
    if (bmp->GetFormat().GetPixelType() != BitmapPixelType::Mono)
        throw Exception("Unsupported bitmap pixel type");

    m_stats = stats;

    const auto tasks = GetTasks();
    const size_t taskCount = tasks.size();
    for (size_t n = 0; n < taskCount; ++n)
    {
        static_cast<ATask*>(tasks[n])->SetUp(bmp, &m_taskStats[n]);
    }
}

void pm::TaskSet_ComputeFrameStats::Wait()
{
    TaskSet::Wait();
    CollectResults();
}

template<typename Rep, typename Period>
bool pm::TaskSet_ComputeFrameStats::Wait(
        const std::chrono::duration<Rep, Period>& timeout)
{
    const bool retVal = TaskSet::Wait(timeout);
    CollectResults();
    return retVal;
}

void pm::TaskSet_ComputeFrameStats::CollectResults()
{
    m_stats->Clear();

    // Prepare reversed illogical values as starting point
    double min = std::numeric_limits<double>::max();
    double max = std::numeric_limits<double>::lowest();
    double sum = 0;
    uint32_t pixelCount = 0;

    const size_t taskCount = GetTasks().size();
    for (size_t n = 0; n < taskCount; ++n)
    {
        if (min > m_taskStats[n].GetMin())
            min = m_taskStats[n].GetMin();
        if (max < m_taskStats[n].GetMax())
            max = m_taskStats[n].GetMax();
        sum += m_taskStats[n].GetSum();
        pixelCount += m_taskStats[n].GetPixelCount();
    }

    // It could happen there is no ROI in metadata thus zero pixels
    if (pixelCount > 0)
    {
        m_stats->Set(min, max, sum, pixelCount);
    }
}

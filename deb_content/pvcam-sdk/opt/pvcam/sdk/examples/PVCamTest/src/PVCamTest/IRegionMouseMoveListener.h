/******************************************************************************/
/* Copyright (C) Teledyne Photometrics. All rights reserved.                  */
/******************************************************************************/
#pragma once
#ifndef PM_I_REGION_MOUSE_MOVE_LISTENER_H
#define PM_I_REGION_MOUSE_MOVE_LISTENER_H

namespace pm {

class IRegionMouseMoveListener
{
public:
    virtual void OnRegionMouseMoved(int x, int y) = 0;
};

} // namespace pm

#endif // PM_I_REGION_MOUSE_MOVE_LISTENER_H

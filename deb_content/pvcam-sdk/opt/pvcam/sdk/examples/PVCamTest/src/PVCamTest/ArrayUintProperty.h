/******************************************************************************/
/* Copyright (C) Teledyne Photometrics. All rights reserved.                  */
/******************************************************************************/
#pragma once
#ifndef PM_ARRAY_UINT_PROPERTY_H
#define PM_ARRAY_UINT_PROPERTY_H

// WxWidgets headers have to be included first
#include <wx/propgrid/propgrid.h>

/* System */
#include <cstdint>

// WX has pre-defined signed types only and size_t, no way to extend it.
// _WX_DEFINE_BASEARRAY macro is hidden in their src/common/dynarray.cpp
#define WX_DEFINE_ARRAY_UINT(T, name) \
    WX_DEFINE_TYPEARRAY_PTR(T, name, wxBaseArraySizeT)

WX_DEFINE_ARRAY_UINT(uint32_t, wxArrayUint);

WX_PG_DECLARE_VARIANT_DATA(wxArrayUint);

class wxArrayUintProperty : public wxPGProperty
{
    WX_PG_DECLARE_PROPERTY_CLASS(wxArrayUintProperty)

public:
    wxArrayUintProperty(const wxString& label = wxPG_LABEL,
            const wxString& name = wxPG_LABEL,
            const wxArrayUint& value = wxArrayUint());

public:
    virtual void OnSetValue() override;
    virtual wxString ValueToString(wxVariant& value, int argFlags = 0) const override;
    virtual bool StringToValue(wxVariant& variant, const wxString& text,
            int argFlags = 0) const override;
    virtual bool OnEvent(wxPropertyGrid* propgrid, wxWindow* primary,
            wxEvent& event) override;
    virtual bool DoSetAttribute(const wxString& name, wxVariant& value) override;

    // Generates cache for displayed text
    virtual void GenerateValueAsString(wxString& target) const;

protected:
    wxString m_display{}; // Cache for displayed text
    wxChar m_delimiter{ ',' };
};

#endif // PM_ARRAY_UINT_PROPERTY_H

/******************************************************************************/
/* Copyright (C) Teledyne Photometrics. All rights reserved.                  */
/******************************************************************************/
#include "backend/FrameStats.h"

pm::FrameStats::FrameStats()
{
    Clear();
}

void pm::FrameStats::Clear()
{
    m_min = 0.0;
    m_max = 0.0;
    m_mean = 0.0;
    m_sum = 0.0;
    m_pixelCount = 0;
}

void pm::FrameStats::Set(const FrameStats& stats)
{
    Set(stats.m_min, stats.m_max, stats.m_sum, stats.m_pixelCount);
}

void pm::FrameStats::Set(double min, double max, double sum, uint32_t pixelCount)
{
    m_min = min;
    m_max = max;
    // TODO: Cut the decimal part for integer types only
    m_mean = (pixelCount == 0) ? 0.0 : (double)(uint64_t)(sum / pixelCount);
    m_sum = sum;
    m_pixelCount = pixelCount;
}

double pm::FrameStats::GetMin() const
{
    return m_min;
}

void pm::FrameStats::SetMin(double min)
{
    m_min = min;
}

double pm::FrameStats::GetMax() const
{
    return m_max;
}

void pm::FrameStats::SetMax(double max)
{
    m_max = max;
}

double pm::FrameStats::GetMean() const
{
    return m_mean;
}

void pm::FrameStats::SetMean(double mean)
{
    m_mean = mean;
}

double pm::FrameStats::GetSum() const
{
    return m_sum;
}

void pm::FrameStats::SetSum(double sum)
{
    m_sum = sum;
}

uint32_t pm::FrameStats::GetPixelCount() const
{
    return m_pixelCount;
}

void pm::FrameStats::SetPixelCount(uint32_t pixelCount)
{
    m_pixelCount = pixelCount;
}

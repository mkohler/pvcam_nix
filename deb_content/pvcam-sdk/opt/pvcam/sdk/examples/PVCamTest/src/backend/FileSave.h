/******************************************************************************/
/* Copyright (C) Teledyne Photometrics. All rights reserved.                  */
/******************************************************************************/
#pragma once
#ifndef _FILE_SAVE_H
#define _FILE_SAVE_H

/* Local */
#include "backend/File.h"
#include "backend/Frame.h"
#include "backend/PrdFileFormat.h"

namespace pm {

class FileSave : public File
{
public:
    FileSave(const std::string& fileName, const PrdHeader& header);
    virtual ~FileSave();

    FileSave() = delete;
    FileSave(const FileSave&) = delete;
    FileSave(FileSave&&) = delete;
    FileSave& operator=(const FileSave&) = delete;
    FileSave& operator=(FileSave&&) = delete;

public:
    const PrdHeader& GetHeader() const;

public: // From File
    virtual void Close() override;

public:
    // New frame is added at end of the file

    virtual bool WriteFrame(const void* metaData, const void* extDynMetaData,
            const void* rawData);
    virtual bool WriteFrame(const Frame& frame);

private:
    bool UpdateFrameExtMetaData(const Frame& frame);
    bool UpdateFrameExtDynMetaData(const Frame& frame);

    uint32_t GetExtMetaDataSizeInBytes(const pm::Frame& frame);

protected:
    PrdHeader m_header{};

    const size_t m_width;
    const size_t m_height;
    const size_t m_rawDataBytes;

    void* m_framePrdMetaData{ nullptr };
    void* m_framePrdExtDynMetaData{ nullptr };
    uint32_t m_framePrdExtDynMetaDataBytes{ 0 };

private:
    // Zero until first frame comes, then set to orig. size from header
    uint32_t m_frameOrigSizeOfPrdMetaDataStruct{ 0 };

    uint32_t m_framePrdMetaDataExtFlags{ 0 };
    uint32_t m_trajectoriesBytes{ 0 };
};

} // namespace pm

#endif /* _FILE_SAVE_H */

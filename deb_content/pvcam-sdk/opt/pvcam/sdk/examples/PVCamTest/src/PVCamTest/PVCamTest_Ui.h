///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Nov  6 2017)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#ifndef __PVCAMTEST_UI_H__
#define __PVCAMTEST_UI_H__

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/intl.h>
class RegionListCtrl;

#include <wx/string.h>
#include <wx/choice.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/sizer.h>
#include <wx/stattext.h>
#include <wx/spinctrl.h>
#include <wx/textctrl.h>
#include <wx/panel.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/bmpbuttn.h>
#include <wx/button.h>
#include <wx/statbox.h>
#include <wx/dataview.h>
#include <wx/checkbox.h>
#include <wx/slider.h>
#include <wx/notebook.h>
#include <wx/gauge.h>
#include <wx/frame.h>
#include "RegionShapeCtrl.h"
#include <wx/scrolwin.h>
#include <wx/wizard.h>
#include <wx/dynarray.h>
WX_DEFINE_ARRAY_PTR( wxWizardPageSimple*, WizardPages );
#include <wx/propgrid/propgrid.h>
#include <wx/propgrid/manager.h>
#include <wx/propgrid/advprops.h>
#include <wx/splitter.h>
#include <wx/dialog.h>

///////////////////////////////////////////////////////////////////////////

namespace pm
{
	namespace ui
	{
		///////////////////////////////////////////////////////////////////////////////
		/// Class MainDlg
		///////////////////////////////////////////////////////////////////////////////
		class MainDlg : public wxFrame 
		{
			private:
			
			protected:
				wxNotebook* m_tabsSettings;
				wxPanel* m_tabSettingsBasic;
				wxChoice* m_cbPortSpeed;
				wxStaticText* m_lblGain;
				wxChoice* m_cbGain;
				wxStaticText* m_lblEmGain;
				wxSpinCtrl* m_editEmGain;
				wxStaticText* m_lblExpTime;
				wxTextCtrl* m_editExpTimes;
				wxChoice* m_cbExpTimeRes;
				wxStaticText* m_lblTimeLapseDelay;
				wxSpinCtrl* m_editTimeLapseDelay;
				wxStaticText* m_lblTimeLapseDelayMs;
				wxStaticText* m_lblTrigger;
				wxChoice* m_cbTriggerMode;
				wxStaticText* m_lblExpOut;
				wxChoice* m_cbExpOutMode;
				wxStaticText* m_lblClear;
				wxChoice* m_cbClearMode;
				wxStaticText* m_lblClearCycles;
				wxSpinCtrl* m_editClearCycles;
				wxStaticText* m_lblP;
				wxChoice* m_cbPMode;
				wxPanel* m_tabSettingsRegions;
				wxBitmapButton* m_btnRoiAdd;
				wxBitmapButton* m_btnRoiRemove;
				wxChoice* m_cbRoiBin;
				wxStaticText* m_lblRoiBinSer;
				wxSpinCtrl* m_editRoiBinSer;
				wxStaticText* m_lblRoiBinPar;
				wxSpinCtrl* m_editRoiBinPar;
				RegionListCtrl* m_dvlRois;
				wxCheckBox* m_chbRoiMultiWin;
				wxPanel* m_tabSettingsCentroids;
				wxCheckBox* m_chbCentroidsEnabled;
				wxStaticText* m_lblCentroidsMode;
				wxChoice* m_chCentroidsMode;
				wxStaticText* m_lblCentroidsCount;
				wxSpinCtrl* m_editCentroidsCount;
				wxStaticText* m_lblCentroidsRadius;
				wxSpinCtrl* m_editCentroidsRadius;
				wxStaticText* m_lblCentroidsBgCount;
				wxChoice* m_chCentroidsBgCount;
				wxStaticText* m_lblCentroidsBgCount2;
				wxStaticText* m_lblCentroidsThreshold;
				wxSpinCtrlDouble* m_editDblCentroidsThreshold;
				wxStaticText* m_lblTrackLinkFrames;
				wxSpinCtrl* m_editTrackLinkFrames;
				wxStaticText* m_lblTrackLinkFrames2;
				wxCheckBox* m_chbTrackCpuOnly;
				wxStaticText* m_lblTrackMaxDist;
				wxSpinCtrl* m_editTrackMaxDist;
				wxStaticText* m_lblTrackMaxDist2;
				wxCheckBox* m_chbTrackTraj;
				wxSpinCtrl* m_editTrackTraj;
				wxStaticText* m_lblTrackTraj;
				wxCheckBox* m_chbMetaRoiOutline;
				wxPanel* m_tabSettingsTracking;
				RegionListCtrl* m_dvlCentroids;
				wxPanel* m_tabSettingsColor;
				wxCheckBox* m_chbColorEnable;
				wxCheckBox* m_chbColorOverrideMask;
				wxChoice* m_cbColorCustomMask;
				wxButton* m_btnAutoWhiteBalance;
				wxButton* m_btnDefaultWhiteBalance;
				wxPanel* m_panelRed;
				wxStaticText* m_lblRed;
				wxSlider* m_sliderRed;
				wxSpinCtrlDouble* m_editDblRed;
				wxPanel* m_panelGreen;
				wxStaticText* m_lblGreen;
				wxSlider* m_sliderGreen;
				wxSpinCtrlDouble* m_editDblGreen;
				wxPanel* m_panelBlue;
				wxStaticText* m_lblBlue;
				wxSlider* m_sliderBlue;
				wxSpinCtrlDouble* m_editDblBlue;
				wxStaticText* m_lblScaleMin;
				wxStaticText* m_lblScaleDefault;
				wxStaticText* m_lblScaleMax;
				wxStaticText* m_lblDebayerAlgorithm;
				wxChoice* m_cbDebayerAlgorithm;
				wxStaticText* m_lblAutoExposureAlgorithm;
				wxChoice* m_cbAutoExposureAlgorithm;
				wxCheckBox* m_chbColorCpuOnly;
				wxPanel* m_tabSettingsSaving;
				wxChoice* m_cbSaveAs;
				wxTextCtrl* m_editSaveDir;
				wxBitmapButton* m_btnSaveDir;
				wxStaticText* m_lblSaveDigits;
				wxSpinCtrl* m_editSaveDigits;
				wxStaticText* m_lblSaveDigits2;
				wxCheckBox* m_chbSaveFirst;
				wxSpinCtrl* m_editSaveFirst;
				wxStaticText* m_lblSaveFirst;
				wxCheckBox* m_chbSaveLast;
				wxSpinCtrl* m_editSaveLast;
				wxStaticText* m_lblSaveLast;
				wxCheckBox* m_chbSaveStack;
				wxSpinCtrl* m_editSaveStack;
				wxStaticText* m_lblSaveStack;
				wxPanel* m_tabSettingsDisplay;
				wxButton* m_btnOpenParameterBrowser;
				wxStaticText* m_lblFillMethod;
				wxChoice* m_cbFillMethod;
				wxCheckBox* m_chbAutoConbright;
				wxStaticText* m_lblContrast;
				wxSlider* m_sliderContrast;
				wxSpinCtrl* m_editContrast;
				wxStaticText* m_lblBrightness;
				wxSlider* m_sliderBrightness;
				wxSpinCtrl* m_editBrightness;
				wxStaticText* m_lblZoomFactor;
				wxChoice* m_cbZoomFactor;
				wxCheckBox* m_chbRoiOutline;
				wxCheckBox* m_chbLiveImage;
				wxStaticText* m_lblCircFrameCount;
				wxSpinCtrl* m_editCircFrameCount;
				wxStaticText* m_lblCircFrameNrLabel;
				wxStaticText* m_lblCircFrameNr;
				wxStaticText* m_lblAcqFpsLabel;
				wxStaticText* m_lblAcqFps;
				wxSlider* m_sliderFrame;
				wxStaticText* m_lblBufferAcq;
				wxStaticText* m_lblBufferAcqLost;
				wxStaticText* m_lblBufferAcqMax;
				wxGauge* m_gaugeBufferAcq;
				wxStaticText* m_lblBufferDisk;
				wxStaticText* m_lblBufferDiskLost;
				wxStaticText* m_lblBufferDiskMax;
				wxGauge* m_gaugeBufferDisk;
				wxChoice* m_cbAcqMode;
				wxStaticText* m_lblSnapFrameCount;
				wxSpinCtrl* m_editSnapFrameCount;
				wxButton* m_btnStart;
				wxButton* m_btnStop;
				wxNotebook* m_tabsInfos;
				wxPanel* m_tabLogs;
				wxTextCtrl* m_lblLogs;
				wxPanel* m_tabFrameDesc;
				wxTextCtrl* m_lblFrameDesc;
				wxGauge* m_progressBar;
			
			public:
				
				MainDlg( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = _("PVCamTest"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 410,720 ), long style = wxCAPTION|wxCLOSE_BOX|wxICONIZE|wxMINIMIZE_BOX|wxRESIZE_BORDER|wxSYSTEM_MENU|wxTAB_TRAVERSAL );
				
				~MainDlg();
			
		};
		
		///////////////////////////////////////////////////////////////////////////////
		/// Class ImageDlg
		///////////////////////////////////////////////////////////////////////////////
		class ImageDlg : public wxFrame 
		{
			private:
			
			protected:
				wxScrolledWindow* m_scrolledWindow;
				RegionShapeCtrl *m_canvas;
			
			public:
				
				ImageDlg( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = _("Image Preview"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 539,458 ), long style = wxCAPTION|wxFRAME_TOOL_WINDOW|wxRESIZE_BORDER|wxTAB_TRAVERSAL );
				
				~ImageDlg();
			
		};
		
		///////////////////////////////////////////////////////////////////////////////
		/// Class WhiteBalanceWizard
		///////////////////////////////////////////////////////////////////////////////
		class WhiteBalanceWizard : public wxWizard 
		{
			private:
			
			protected:
				wxStaticText* m_lblWizPageRegion;
				wxStaticText* m_lblWizPageRegionEnd;
			
			public:
				
				WhiteBalanceWizard( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = _("White balance wizard"), const wxBitmap& bitmap = wxNullBitmap, const wxPoint& pos = wxDefaultPosition, long style = wxCAPTION|wxCLOSE_BOX|wxSTAY_ON_TOP|wxSYSTEM_MENU );
				WizardPages m_pages;
				~WhiteBalanceWizard();
			
		};
		
		///////////////////////////////////////////////////////////////////////////////
		/// Class ParamBrowserDlg
		///////////////////////////////////////////////////////////////////////////////
		class ParamBrowserDlg : public wxDialog 
		{
			private:
			
			protected:
				wxSplitterWindow* m_splitter;
				wxPanel* m_panelParams;
				wxPropertyGridManager* m_pgManager;
				wxPropertyGridPage* m_pgParams;
				wxPanel* m_panelInfo;
				wxPropertyGrid* m_pgInfo;
			
			public:
				
				ParamBrowserDlg( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = _("Parameter Browser"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 460,670 ), long style = wxCAPTION|wxCLOSE_BOX|wxRESIZE_BORDER|wxSYSTEM_MENU ); 
				~ParamBrowserDlg();
				
				void m_splitterOnIdle( wxIdleEvent& )
				{
					m_splitter->SetSashPosition( -150 );
					m_splitter->Disconnect( wxEVT_IDLE, wxIdleEventHandler( ParamBrowserDlg::m_splitterOnIdle ), NULL, this );
				}
			
		};
		
	} // namespace ui
} // namespace pm

#endif //__PVCAMTEST_UI_H__

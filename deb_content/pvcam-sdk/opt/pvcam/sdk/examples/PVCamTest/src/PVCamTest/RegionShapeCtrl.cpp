/******************************************************************************/
/* Copyright (C) Teledyne Photometrics. All rights reserved.                  */
/******************************************************************************/
#include "RegionShapeCtrl.h"

// WxWidgets headers have to be included first
#include <wx/dcbuffer.h>
#include <wx/log.h>
#include <wx/menu.h>

/* Local */
#include "backend/Bitmap.h"

/* System */
#include <cmath>

wxDEFINE_EVENT(PM_EVT_REFRESH_IMAGE, wxThreadEvent);

constexpr int cRegionHandleDiameter = 4; // Handle diameter (use even number)
constexpr int cMaxDelta = 60; // Max H/W in pixels for optimized bitmap repaint

pm::RegionShapeCtrl::RegionShapeCtrl(wxWindow* parent)
    : wxWindow(parent, wxID_ANY)
{
    // From DOC: Notice that in previous versions of wxWidgets a common way to
    // work around the above mentioned flickering problem was to define an empty
    // EVT_ERASE_BACKGROUND handler. Setting background style to wxBG_STYLE_PAINT
    // is a simpler and more efficient solution to the same problem.
    SetBackgroundStyle(wxBG_STYLE_PAINT);

    SetCursor(wxCursor(wxCURSOR_ARROW));

    m_scrollParent = dynamic_cast<wxScrolledWindow*>(GetParent());
    if (m_scrollParent)
    {
    }
    else
    {
        wxLogWarning("RegionShapeCtrl doesn't have wxScrolledWindow parent");
    }

    Bind(wxEVT_SET_CURSOR, &RegionShapeCtrl::OnSetCursor, this);
    Bind(wxEVT_PAINT, &RegionShapeCtrl::OnPaint, this);
    Bind(wxEVT_LEFT_DOWN, &RegionShapeCtrl::OnMouseLeftDown, this);
    Bind(wxEVT_LEFT_UP, &RegionShapeCtrl::OnMouseLeftUp, this);
    Bind(wxEVT_MOTION, &RegionShapeCtrl::OnMouseMove, this);
    Bind(wxEVT_LEAVE_WINDOW, &RegionShapeCtrl::OnMouseLeave, this);
    Bind(wxEVT_MOUSE_CAPTURE_LOST, &RegionShapeCtrl::OnMouseCaptureLost, this);

    Bind(PM_EVT_REFRESH_IMAGE, [&](wxThreadEvent& WXUNUSED(event)) {
                DoRefreshImage();
            });
}

pm::RegionShapeCtrl::~RegionShapeCtrl()
{
    if (m_model)
    {
        if (m_notifier)
        {
            m_model->RemoveNotifier(m_notifier);
        }
        m_model->DecRef(); // Discard old model, if any
    }
}

bool pm::RegionShapeCtrl::AssociateModel(RegionModel* model)
{
    if (m_model == model)
        return true;

    if (m_model)
    {
        if (m_notifier)
        {
            m_model->RemoveNotifier(m_notifier);
            m_notifier = nullptr;
        }
        m_model->DecRef(); // Discard old model, if any
    }

    m_model = model;

    if (m_model)
    {
        m_model->IncRef(); // Add our own reference to the new model
        m_notifier = new(std::nothrow) ModelNotifier(this);
        if (!m_notifier)
        {
            m_model->DecRef(); // Discard old model, if any
            return false;
        }
        m_model->AddNotifier(m_notifier);
    }

    FireEventRefreshImage();

    return true;
}

pm::RegionModel* pm::RegionShapeCtrl::GetModel()
{
    return m_model;
}

const pm::RegionModel* pm::RegionShapeCtrl::GetModel() const
{
    return m_model;
}

std::shared_ptr<pm::Region> pm::RegionShapeCtrl::GetActiveRegion() const
{
    if (m_activeRegionEditing || !m_activeRegion)
        return nullptr;

    return m_activeRegion;
}

void pm::RegionShapeCtrl::EnableEditor(bool enabled)
{
    m_isEditorEnabled = enabled;

    FireEventRefreshImage();
}

void pm::RegionShapeCtrl::SetRegionMouseMoveListener(
        IRegionMouseMoveListener* listener)
{
    m_regionMouseMoveListener = listener;
}

void pm::RegionShapeCtrl::SetNonBinnedCanvasSize(const wxSize& size)
{
    m_canvasRect = wxRect(size);
}

void pm::RegionShapeCtrl::SetImageSize(const wxSize& size)
{
    m_image = wxImage(size, false);

    m_bmp = wxBitmap();

    m_validCount = 0;
}

void pm::RegionShapeCtrl::SetImagesSize(const std::vector<wxSize>& sizes)
{
    const auto count = sizes.size();

    m_images.clear();
    m_images.resize(count);
    for (size_t n = 0; n < sizes.size(); ++n)
    {
        m_images[n] = wxImage(sizes[n], false);
    }

    m_bmps.clear();
    m_bmps.resize(count);

    m_offsets.clear();
    m_offsets.resize(count);

    m_validCount = 0;
}

void pm::RegionShapeCtrl::SetOutlineShape(OutlineShape shape)
{
    m_outlineShape = shape;

    FireEventRefreshImage();
}

void pm::RegionShapeCtrl::SetZoom(double zoom)
{
    m_zoom = zoom;

    UpdateRegionHandlesRect();

    FireEventRefreshImage();
}

void pm::RegionShapeCtrl::SetImage(const Bitmap* image,
        const Frame::Point& offset, const Frame::Point& canvasOffset,
        uint16_t sbin, uint16_t pbin)
{
    std::lock_guard<std::mutex> lock(m_mutex);

    m_doRecompose = false;
    m_imagesChanged = true;

    if (image)
    {
        memcpy(m_image.GetData(), image->GetData(), image->GetDataBytes());
    }
    m_offset = offset;

    // Cannot create m_bmp here, SetImage is called from multiple threads

    m_validCount = (image) ? 1 : 0;
    m_canvasOffset = canvasOffset;
    m_sbin = sbin;
    m_pbin = pbin;
    m_fillColor = wxTransparentColour;
    m_outlines.clear();
    m_trajectories = Frame::Trajectories();

    FireEventRefreshImage();
}

void pm::RegionShapeCtrl::SetImages(
        const std::vector<std::unique_ptr<Bitmap>>& images,
        const std::vector<Frame::Point>& offsets, size_t validCount,
        const Frame::Point& canvasOffset, uint16_t sbin, uint16_t pbin,
        const wxColour& fillColor, const std::vector<wxRect>& outlines,
        const Frame::Trajectories& trajectories)
{
    std::lock_guard<std::mutex> lock(m_mutex);

    m_doRecompose = true;
    m_imagesChanged = true;

    for (size_t n = 0; n < validCount; ++n)
    {
        auto& image = images[n];
        auto& localImage = m_images[n];
        assert(localImage.GetWidth() == (int)image->GetWidth());
        assert(localImage.GetHeight() == (int)image->GetHeight());
        memcpy(localImage.GetData(), image->GetData(), image->GetDataBytes());
    }
    m_offsets = offsets;

    // Cannot create m_bmps here, SetImages is called from multiple threads

    m_validCount = validCount;
    m_canvasOffset = canvasOffset;
    m_sbin = sbin;
    m_pbin = pbin;
    m_fillColor = fillColor;
    m_outlines = outlines;
    m_trajectories = trajectories;

    FireEventRefreshImage();
}

void pm::RegionShapeCtrl::OnSetCursor(wxSetCursorEvent& event)
{
    if (!m_isEditorEnabled || !m_model)
    {
        event.Skip();
        return;
    }

    const wxRealPoint scaledPos(event.GetX() / m_zoom, event.GetY() / m_zoom);
    const wxPoint pos(scaledPos); // Truncates floating point values

    const HandleType type = GetHandleType(wxPoint(pos.x, pos.y));
    if (m_handleCursors.count(type))
    {
        event.SetCursor(m_handleCursors.at(type));
    }
    else
    {
        event.Skip();
    }
}

void pm::RegionShapeCtrl::OnPaint(wxPaintEvent& WXUNUSED(event))
{
    std::lock_guard<std::mutex> lock(m_mutex);

    wxAutoBufferedPaintDC dc(this);

    dc.SetUserScale(m_zoom, m_zoom);

    for (wxRegionIterator regionIt(GetUpdateRegion()); regionIt; ++regionIt)
    {
        const auto zoomedRect(regionIt.GetRect());
        const auto rect(GetNonZoomedRect(zoomedRect));

        if (m_doRecompose)
        {
            DrawImages(dc, rect);
            DrawOutlines(dc, rect);
            DrawTrajectories(dc, rect);
            DrawRegions(dc, rect);
            DrawOverlappingAreas(dc, rect);
            DrawRegionHandles(dc, rect);
        }
        else
        {
            DrawImage(dc, rect);
        }
    }
}

void pm::RegionShapeCtrl::OnMouseLeftDown(wxMouseEvent& event)
{
    if (!m_isEditorEnabled || !m_model)
    {
        event.Skip();
        return;
    }

    const wxRealPoint scaledPos(event.GetX() / m_zoom, event.GetY() / m_zoom);
    const wxPoint pos(scaledPos); // Truncates floating point values

    m_startPoint = pos;

    if (m_activeRegion)
    {
        m_startHandleType = GetHandleType(pos);
        m_startRect = m_activeRegion->GetClientRect();
        m_showHandles = false;
    }

    m_activeRegionEditing = true;

    if (!HasCapture())
    {
        CaptureMouse();
    }

    Refresh();
}

void pm::RegionShapeCtrl::OnMouseLeftUp(wxMouseEvent& event)
{
    if (!m_isEditorEnabled || !m_model)
    {
        event.Skip();
        return;
    }

    if (HasCapture())
    {
        ReleaseMouse();
    }

    if (m_activeRegionEditing)
    {
        m_activeRegionEditing = false;

        if (m_activeRegion)
        {
            auto activeRegion = m_activeRegion;

            const wxRealPoint scaledPos(event.GetX() / m_zoom, event.GetY() / m_zoom);
            const wxPoint pos(scaledPos); // Truncates floating point values

            m_showHandles = (GetHandleType(pos) != HandleType::None);
            if (!m_showHandles)
            {
                m_activeRegion->SetSelected(false);
                m_model->RegionChanged(m_activeRegion);
                SetActiveRegion(nullptr);
            }

            const wxRect rect(activeRegion->GetClientRect());
            if (rect.IsEmpty())
            {
                m_model->DeleteRegion(activeRegion);
            }
        }

        Refresh();
    }
}

void pm::RegionShapeCtrl::OnMouseMove(wxMouseEvent& event)
{
    const wxRealPoint scaledPos(event.GetX() / m_zoom, event.GetY() / m_zoom);
    const wxPoint pos(scaledPos); // Truncates floating point values

    m_regionMousePosX = event.GetX();
    m_regionMousePosY = event.GetY();
    UpdateRegionMouseMoveListener();

    if (!m_isEditorEnabled || !m_model)
    {
        event.Skip();
        return;
    }

    if (!m_activeRegionEditing)
    {
        std::shared_ptr<Region> region = FindRegion(pos);
        if (m_activeRegion && m_activeRegion != region)
        {
            m_activeRegion->SetSelected(false);
            m_model->RegionChanged(m_activeRegion);
            SetActiveRegion(nullptr);
            Refresh();
        }
        if (region && region != m_activeRegion)
        {
            SetActiveRegion(region);
            m_activeRegion->SetSelected(true);
            m_model->RegionChanged(m_activeRegion);
            m_showHandles = true;
            Refresh();
        }
    }
    else
    {
        if (pos != m_startPoint && !m_activeRegion)
        {
            SetActiveRegion(std::make_shared<Region>(wxRect(m_startPoint, pos)));
            m_activeRegion->SetSelected(true);
            if (!m_model->AppendRegion(m_activeRegion))
            {
                SetActiveRegion(nullptr);
                wxLogDebug("RegionShapeCtrl::OnMouseMove: region adding refused");
                return;
            }
            // New region is always resized from bottom-right corner
            m_startHandleType = HandleType::BottomRight;
            m_startRect = m_activeRegion->GetClientRect();
            m_showHandles = false;
        }

        if (m_activeRegion)
        {
            const wxPoint delta = pos - m_startPoint;

            const auto oldX = m_startRect.x;
            const auto oldY = m_startRect.y;
            const auto oldW = m_startRect.width;
            const auto oldH = m_startRect.height;
            auto newX = oldX;
            auto newY = oldY;
            auto newW = oldW;
            auto newH = oldH;
            switch (m_startHandleType)
            {
            case HandleType::Body:
                newX += delta.x;
                newY += delta.y;
                break;
            case HandleType::Top:
                newY += delta.y;
                newH += -delta.y;
                break;
            case HandleType::Bottom:
                newH += delta.y;
                break;
            case HandleType::Left:
                newX += delta.x;
                newW += -delta.x;
                break;
            case HandleType::Right:
                newW += delta.x;
                break;
            case HandleType::TopLeft:
                newX += delta.x;
                newY += delta.y;
                newW += -delta.x;
                newH += -delta.y;
                break;
            case HandleType::TopRight:
                newY += delta.y;
                newW += delta.x;
                newH += -delta.y;
                break;
            case HandleType::BottomLeft:
                newX += delta.x;
                newW += -delta.x;
                newH += delta.y;
                break;
            case HandleType::BottomRight:
                newW += delta.x;
                newH += delta.y;
                break;
            default:
                break;
            }
            // Adjust X/Y/W/H in case of negative W/H
            if (newW < 0)
            {
                newW = -newW;
                newX -= newW;
            }
            if (newH < 0)
            {
                newH = -newH;
                newY -= newH;
            }
            // Adjust X/Y to keep region in valid sensor area
            if (newX < 0)
            {
                if (m_startHandleType != HandleType::Body)
                    newW += newX;
                newX = 0;
            }
            if (newX + newW > m_canvasRect.width)
            {
                if (m_startHandleType != HandleType::Body)
                    newW = m_canvasRect.width - newX;
                newX = m_canvasRect.width - newW;
            }
            if (newY < 0)
            {
                if (m_startHandleType != HandleType::Body)
                    newH += newY;
                newY = 0;
            }
            if (newY + newH > m_canvasRect.height)
            {
                if (m_startHandleType != HandleType::Body)
                    newH = m_canvasRect.height - newY;
                newY = m_canvasRect.height - newH;
            }

            m_activeRegion->SetClientRect(wxRect(newX, newY, newW, newH));
            if (oldX != newX)
            {
                m_model->RegionValueChanged(m_activeRegion, 1);
            }
            if (oldY != newY)
            {
                m_model->RegionValueChanged(m_activeRegion, 2);
            }
            if (oldW != newW)
            {
                m_model->RegionValueChanged(m_activeRegion, 3);
            }
            if (oldH != newH)
            {
                m_model->RegionValueChanged(m_activeRegion, 4);
            }

            UpdateRegionHandlesRect();
            Refresh();
        }
    }
}

void pm::RegionShapeCtrl::OnMouseLeave(wxMouseEvent& event)
{
    m_regionMousePosX = -1;
    m_regionMousePosY = -1;
    UpdateRegionMouseMoveListener();

    if (!m_isEditorEnabled || !m_model)
    {
        event.Skip();
        return;
    }

    if (!m_activeRegionEditing && m_activeRegion)
    {
        m_activeRegion->SetSelected(false);
        m_model->RegionChanged(m_activeRegion);
        SetActiveRegion(nullptr);
        Refresh();
    }
}

void pm::RegionShapeCtrl::OnMouseCaptureLost(
        wxMouseCaptureLostEvent& WXUNUSED(event))
{
    SetActiveRegion(nullptr);
}

void pm::RegionShapeCtrl::FireEventRefreshImage()
{
    // Direct call could cause a dead lock e.g. in IRegionMouseMoveListener-s
    //if (wxThread::GetCurrentId() == wxThread::GetMainId())
    //{
    //    DoRefreshImage();
    //}
    //else
    {
        wxThreadEvent* event = new(std::nothrow) wxThreadEvent(PM_EVT_REFRESH_IMAGE);
        if (event)
        {
            wxQueueEvent(GetEventHandler(), event);
        }
    }
}

void pm::RegionShapeCtrl::DoRefreshImage()
{
    Refresh();
    UpdateRegionMouseMoveListener();
}

void pm::RegionShapeCtrl::DrawBackground(wxDC& dc, const wxRect& rect)
{
    auto updRect(rect.Intersect(m_canvasRect));
    if (updRect.IsEmpty())
        return;

    if (m_fillColor == wxTransparentColour)
    {
        // There is no wxGREY color macro defined
        dc.SetPen(*wxThePenList->FindOrCreatePen(wxGREY_PEN->GetColour(), 0));
        dc.SetBrush(*wxGREY_BRUSH);
        dc.DrawRectangle(updRect);

        // With transparent pen the cross-diagonal pattern is not drawn on
        // rectangle edges, where would be usually drawn a border outline.
        // Without inflating the rectangle no pattern would be drawn during
        // scrolling or window resizing, because repainted area is only a few
        // pixels wide/high.
        wxDCClipper dcc(dc, updRect);
        updRect.Inflate(3);

        dc.SetPen(*wxTRANSPARENT_PEN);
        dc.SetBrush(*wxTheBrushList->FindOrCreateBrush(
                    wxMEDIUM_GREY_BRUSH->GetColour(),
                    wxBRUSHSTYLE_CROSSDIAG_HATCH));
        dc.DrawRectangle(updRect);
    }
    else
    {
        dc.SetPen(*wxThePenList->FindOrCreatePen(m_fillColor, 0));
        dc.SetBrush(*wxTheBrushList->FindOrCreateBrush(m_fillColor));
        dc.DrawRectangle(updRect);
    }
}

void pm::RegionShapeCtrl::DrawImage(wxDC& dc, const wxRect& rect)
{
    if (m_imagesChanged)
    {
        m_imagesChanged = false;
        m_bmp = (m_validCount > 0 && m_image.IsOk())
            ? wxBitmap(m_image, wxBITMAP_SCREEN_DEPTH)
            : wxBitmap();
    }

    auto& bmp = m_bmp;
    if (!bmp.IsOk())
    {
        DrawBackground(dc, rect);
        return;
    }

    const wxPoint bmpPos(m_offset.x - m_canvasOffset.x, m_offset.y - m_canvasOffset.y);
    const wxSize bmpSize(bmp.GetSize());
    const wxRect bmpRect(bmpPos, bmpSize);

    const wxPoint bmpNonBinnedPos(bmpRect.x * m_sbin, bmpRect.y * m_pbin);
    const wxSize bmpNonBinnedSize(bmpRect.width * m_sbin, bmpRect.height * m_pbin);
    const wxRect bmpNonBinnedRect(bmpNonBinnedPos, bmpNonBinnedSize);

    // With some binning the image size can be smaller than sensor.
    const bool fillNeeded = m_canvasRect != bmpNonBinnedRect;
    if (fillNeeded)
    {
        DrawBackground(dc, rect);
    }

    auto updRect(rect.Intersect(bmpNonBinnedRect));
    if (updRect.IsEmpty())
        return;

    dc.SetUserScale(m_sbin * m_zoom, m_pbin * m_zoom);

#if defined(_WIN32)
    const bool useBlit = rect.width <= cMaxDelta || rect.height <= cMaxDelta;
    if (!useBlit)
    {
        // Drawing whole bitmap is much faster on Windows if whole area
        // should be updated, e.g. during live acquisition.
        // On Linux it is super fast, but it spends 99% of time in wxBitmap
        // constructor.
        dc.DrawBitmap(bmp, bmpRect.GetTopLeft());
    }
    else
#endif
    {
        const wxPoint updBmpPos(updRect.x / m_sbin, updRect.y / m_pbin);
        const wxSize updBmpSize(updRect.width / m_sbin, updRect.height / m_pbin);
        wxRect updBmpRect(updBmpPos, updBmpSize);
        updBmpRect.Inflate(2);

        wxMemoryDC mdc;
        mdc.SelectObject(bmp);
        dc.Blit(updBmpRect.GetTopLeft(), updBmpRect.GetSize(), &mdc,
                updBmpRect.GetTopLeft() - bmpRect.GetTopLeft());
        mdc.SelectObject(wxNullBitmap);
    }

    dc.SetUserScale(m_zoom, m_zoom);
}

void pm::RegionShapeCtrl::DrawImages(wxDC& dc, const wxRect& rect)
{
    if (m_imagesChanged)
    {
        m_imagesChanged = false;
        for (size_t n = 0; n < m_validCount; ++n)
        {
            auto& image = m_images[n];
            auto& bmp = m_bmps[n];
            bmp = (image.IsOk())
                ? wxBitmap(image, wxBITMAP_SCREEN_DEPTH)
                : wxBitmap();
        }
    }

    if (m_validCount == 0 || !m_bmps[0].IsOk())
    {
        DrawBackground(dc, rect);
        return;
    }

    dc.SetUserScale(m_sbin * m_zoom, m_pbin * m_zoom);

    for (size_t n = 0; n < m_validCount; ++n)
    {
        auto& bmp = m_bmps[n];
        if (!bmp.IsOk()) // Doesn't occur for n==0 due to condition above
            continue;
        const auto& offset = m_offsets[n];

        const wxPoint bmpPos(offset.x - m_canvasOffset.x, offset.y - m_canvasOffset.y);
        const wxSize bmpSize(bmp.GetSize());
        const wxRect bmpRect(bmpPos, bmpSize);

        const wxPoint bmpNonBinnedPos(bmpRect.x * m_sbin, bmpRect.y * m_pbin);
        const wxSize bmpNonBinnedSize(bmpRect.width * m_sbin, bmpRect.height * m_pbin);
        const wxRect bmpNonBinnedRect(bmpNonBinnedPos, bmpNonBinnedSize);

        // With some binning the image size can be smaller than sensor.
        // Fill background in first iteration only, because it would be too
        // complicated to recognize in advance whether all bitmaps cover whole
        // area or not.
        const bool fillNeeded = n == 0 && m_canvasRect != bmpNonBinnedRect;
        if (fillNeeded)
        {
            dc.SetUserScale(m_zoom, m_zoom);
            DrawBackground(dc, rect);
            dc.SetUserScale(m_sbin * m_zoom, m_pbin * m_zoom);
        }

#if defined(_WIN32)
        const bool useBlit = rect.width <= cMaxDelta || rect.height <= cMaxDelta;
        if (!useBlit)
        {
            // Drawing whole bitmap is much faster on Windows if whole area
            // should be updated, e.g. during live acquisition.
            // On Linux it is super fast, but it spends 99% of time in wxBitmap
            // constructor.
            dc.DrawBitmap(bmp, bmpRect.GetTopLeft());
        }
        else
#endif
        {
            auto updRect(rect.Intersect(bmpNonBinnedRect));
            if (updRect.IsEmpty())
                continue;

            const wxPoint updBmpPos(updRect.x / m_sbin, updRect.y / m_pbin);
            const wxSize updBmpSize(updRect.width / m_sbin, updRect.height / m_pbin);
            wxRect updBmpRect(updBmpPos, updBmpSize);
            updBmpRect.Inflate(2);

            wxMemoryDC mdc;
            mdc.SelectObject(bmp);
            dc.Blit(updBmpRect.GetTopLeft(), updBmpRect.GetSize(), &mdc,
                    updBmpRect.GetTopLeft() - bmpRect.GetTopLeft());
            mdc.SelectObject(wxNullBitmap);
        }
    }

    dc.SetUserScale(m_zoom, m_zoom);
}

void pm::RegionShapeCtrl::DrawRegions(wxDC& dc, const wxRect& rect)
{
    if (!m_model)
        return;

    dc.SetBrush(*wxTRANSPARENT_BRUSH);

    for (auto region : m_model->GetRegions())
    {
        if (!region->IsVisible())
            continue;
        const auto regionRect = region->GetClientRect();
        if (!rect.Intersects(regionRect))
            continue;
        const wxColour& color = (region->IsSelected()) ? *wxGREEN : *wxBLUE;
        if (dc.GetPen().GetColour() != color)
        {
            dc.SetPen(*wxThePenList->FindOrCreatePen(color, 0));
        }
        dc.DrawRectangle(regionRect);
    }
}

void pm::RegionShapeCtrl::DrawOutlines(wxDC& dc, const wxRect& rect)
{
    if (m_outlines.empty())
        return;

    dc.SetBrush(*wxTRANSPARENT_BRUSH);
    dc.SetPen(*wxThePenList->FindOrCreatePen(*wxGREEN, 0));

    switch (m_outlineShape)
    {
    case OutlineShape::Rectangle:
        for (wxRect outline : m_outlines)
        {
            outline.x -= m_canvasOffset.x;
            outline.y -= m_canvasOffset.y;
            if (!rect.Intersects(outline))
                continue;
            dc.DrawRectangle(outline);
        }
        break;
    case OutlineShape::Ellipse:
        for (wxRect outline : m_outlines)
        {
            outline.x -= m_canvasOffset.x;
            outline.y -= m_canvasOffset.y;
            outline.width--;
            outline.height--;
            if (!rect.Intersects(outline))
                continue;
            dc.DrawEllipse(outline);
        }
        break;
    // default missing intentionally, compiler warns us if new enum is added
    };
}

void pm::RegionShapeCtrl::DrawTrajectories(wxDC& dc, const wxRect& WXUNUSED(rect))
{
    if (m_trajectories.data.empty())
        return;

    static std::vector<wxColour> colors;
    if (colors.empty())
    {
        for (int n = 0; n < 20; ++n)
        {
            const uint8_t ch = static_cast<uint8_t>((100 * n) & 0xFF);
            colors.emplace_back(255, ch, 255 - ch);
            colors.emplace_back(255, 255 - ch, ch);
            colors.emplace_back(255 - ch, ch, ch);
        }
    }

    dc.SetBrush(*wxTRANSPARENT_BRUSH);

    const auto visibleRect(GetNonZoomedRect(GetViewportRect()));

    for (const auto& trajectory : m_trajectories.data)
    {
        const size_t index = trajectory.header.particleId % colors.size();
        dc.SetPen(*wxThePenList->FindOrCreatePen(colors.at(index)));

        bool visible = false;
        std::vector<wxPoint> points;
        points.reserve(trajectory.data.size());
        for (const auto& point : trajectory.data)
        {
            if (!point.isValid)
                continue;
            const wxPoint p(point.x - m_canvasOffset.x, point.y - m_canvasOffset.y);
            points.push_back(p);
            if (!visible)
            {
                visible = visibleRect.Contains(p);
            }
        }

        if (visible)
        {
            dc.DrawLines(points.size(), points.data());
        }
    }
}

void pm::RegionShapeCtrl::DrawOverlappingAreas(wxDC& dc, const wxRect& rect)
{
    if (!m_model)
        return;

    const auto& regions = m_model->GetOverlappingRegions();
    if (regions.empty())
        return;

    dc.SetBrush(*wxRED_BRUSH);
    dc.SetPen(*wxThePenList->FindOrCreatePen(*wxRED, 0));

    for (auto it = regions.cbegin(); it != regions.cend(); ++it)
    {
        const auto rectA = it->first->GetClientRect();
        const auto rectB = it->second->GetClientRect();
        const auto area = rectA.Intersect(rectB);
        const auto updRect(rect.Intersect(area));
        if (updRect.IsEmpty())
            continue;
        dc.DrawRectangle(updRect);
    }
}

void pm::RegionShapeCtrl::DrawRegionHandles(wxDC& dc, const wxRect& rect)
{
    if (!m_activeRegion || !m_activeRegion->IsVisible() || !m_showHandles)
        return;

    dc.SetBrush(*wxTRANSPARENT_BRUSH);
    dc.SetPen(*wxThePenList->FindOrCreatePen(*wxCYAN, 0));

    for (auto handleType : m_handleTypes)
    {
        if (handleType == HandleType::Body)
            continue; // Body handle not drawn, DrawRegion draws the lines
        const auto& handleRect = m_handleRects.at(handleType);
        if (!rect.Intersects(handleRect))
            continue;
        dc.DrawRectangle(handleRect);
    }
}

pm::RegionShapeCtrl::HandleType pm::RegionShapeCtrl::GetHandleType(
        const wxPoint& point) const
{
    if (!m_activeRegion)
        return HandleType::None;

    HandleType type = HandleType::None;

    bool handled = false;
    if (m_activeRegionRect.Contains(point))
    {
        type = HandleType::Body;
        handled = true;
    }
    // OnSetCursor is obviously called before OnMouseMove sets active region.
    // So if the point is in client area, the shape is also active.
    if (m_showHandles || handled)
    {
        for (auto handleType : m_handleTypes)
        {
            if (handleType == HandleType::Body)
                continue; // Body is a bit larger and handled above
            const wxRect& rect = m_handleRects.at(handleType);
            if (!rect.IsEmpty() && rect.Contains(point))
            {
                type = handleType;
                break;
            }
        }
    }

    return type;
}

std::shared_ptr<pm::Region> pm::RegionShapeCtrl::FindRegion(
        const wxPoint& point) const
{
    if (!m_model)
        return nullptr;

    if (m_activeRegion && m_activeRegionRect.Contains(point))
        return m_activeRegion;

    for (auto region : m_model->GetRegions())
    {
        if (region->GetClientRect().Contains(point))
            return region;
    }

    return nullptr;
}

void pm::RegionShapeCtrl::SetActiveRegion(std::shared_ptr<Region> region)
{

    const auto oldActiveRegion = m_activeRegion;
    m_activeRegion = region;
    if (!m_activeRegion)
    {
        m_showHandles = false;
        m_activeRegionEditing = false;

        if (oldActiveRegion && oldActiveRegion->IsSelected())
        {
            oldActiveRegion->SetSelected(false);
            m_model->RegionChanged(oldActiveRegion);
            Refresh();
        }

        if (HasCapture())
        {
            ReleaseMouse();
        }
    }
    else
    {
        UpdateRegionHandlesRect();
    }
}

void pm::RegionShapeCtrl::UpdateRegionHandlesRect()
{
    if (!m_activeRegion)
        return;

    const int II = ceil(cRegionHandleDiameter / m_zoom);
    const int I2 = II * 2;
    const int DD = II * 2 + 1;

    const auto bodyRect(m_activeRegion->GetClientRect());
    m_activeRegionRect = bodyRect;
    m_activeRegionRect.Inflate(II, II);

    const int WW = m_activeRegionRect.width;
    const int W1 = WW / 2;
    const int HH = m_activeRegionRect.height;
    const int H1 = HH / 2;
    const int XX = m_activeRegionRect.x;
    const int YY = m_activeRegionRect.y;

    const bool widthEnough = (WW >= 3 * I2);
    const bool heightEnough = (HH >= 3 * I2);

    // No need to clear the map, it has always the same keys
    m_handleRects[HandleType::Body] = bodyRect;
    m_handleRects[HandleType::Top] = (widthEnough)
        ? wxRect(XX + W1 - II - 1, YY              , DD, DD)
        : wxRect();
    m_handleRects[HandleType::Bottom] = (widthEnough)
        ? wxRect(XX + W1 - II - 1, YY + HH - I2 - 1, DD, DD)
        : wxRect();
    m_handleRects[HandleType::Left] = (heightEnough)
        ? wxRect(XX              , YY + H1 - II - 1, DD, DD)
        : wxRect();
    m_handleRects[HandleType::Right] = (heightEnough)
        ? wxRect(XX + WW - I2 - 1, YY + H1 - II - 1, DD, DD)
        : wxRect();
    m_handleRects[HandleType::TopLeft] =
        wxRect(XX              , YY              , DD, DD);
    m_handleRects[HandleType::TopRight] =
        wxRect(XX + WW - I2 - 1, YY              , DD, DD);
    m_handleRects[HandleType::BottomLeft] =
        wxRect(XX              , YY + HH - I2 - 1, DD, DD);
    m_handleRects[HandleType::BottomRight] =
        wxRect(XX + WW - I2 - 1, YY + HH - I2 - 1, DD, DD);
}

wxRect pm::RegionShapeCtrl::GetViewportRect() const
{
    if (!m_scrollParent)
        return GetClientRect();

    wxPoint scrollPpu;
    m_scrollParent->GetScrollPixelsPerUnit(&scrollPpu.x, &scrollPpu.y);
    const auto viewStart = m_scrollParent->GetViewStart();
    const wxRect viewportRect(
            wxPoint(viewStart.x * scrollPpu.x, viewStart.y * scrollPpu.y),
            m_scrollParent->GetClientSize());
    return viewportRect;
}

wxRect pm::RegionShapeCtrl::GetNonZoomedRect(const wxRect& rect) const
{
    const wxRealPoint ltF(rect.GetLeft() / m_zoom, rect.GetTop() / m_zoom);
    const wxRealPoint rbF(rect.GetRight() / m_zoom, rect.GetBottom() / m_zoom);
    // Round down left-top floating point
    const wxPoint lt(std::floor(ltF.x), std::floor(ltF.y));
    // Round up right-bottom floating point
    const wxPoint rb(std::ceil(rbF.x), std::ceil(rbF.y));
    wxRect nonZoomedRect(lt, rb);
    // Extend W/H by rounding error
    const int safeBorder = std::ceil(1.0 / m_zoom);
    nonZoomedRect.width += safeBorder;
    nonZoomedRect.height += safeBorder;
    return nonZoomedRect;
}

void pm::RegionShapeCtrl::UpdateRegionMouseMoveListener()
{
    if (m_regionMouseMoveListener)
    {
        m_regionMouseMoveListener->OnRegionMouseMoved(
                m_regionMousePosX, m_regionMousePosY);
    }
}

bool pm::RegionShapeCtrl::ItemAdded(const wxDataViewItem& WXUNUSED(parent),
        const wxDataViewItem& WXUNUSED(item))
{
    Refresh();
    return true;
}

bool pm::RegionShapeCtrl::ItemDeleted(const wxDataViewItem& WXUNUSED(parent),
        const wxDataViewItem& item)
{
    if (!m_model || !item.IsOk())
        return false;

    // m_model->GetRegion returns null as model does not contain the region anymore
    bool hasSelection = false;
    for (auto region : m_model->GetRegions())
    {
        if (region->IsSelected())
        {
            hasSelection = true;
            break;
        }
    }
    if (!hasSelection)
    {
        SetActiveRegion(nullptr);
    }

    Refresh();
    return true;
}

bool pm::RegionShapeCtrl::ItemChanged(const wxDataViewItem& item)
{
    if (!m_model)
        return false;

    std::shared_ptr<Region> region = m_model->GetRegion(item);

    if (region && region->IsSelected())
    {
        SetActiveRegion(region);
    }
    else
    {
        bool hasSelection = false;
        for (auto rgn : m_model->GetRegions())
        {
            if (rgn->IsSelected())
            {
                hasSelection = true;
                break;
            }
        }
        if (!hasSelection)
        {
            SetActiveRegion(nullptr);
        }
    }

    Refresh();
    return true;
}

bool pm::RegionShapeCtrl::ItemsAdded(const wxDataViewItem& WXUNUSED(parent),
        const wxDataViewItemArray& WXUNUSED(items))
{
    Refresh();
    return true;
}

bool pm::RegionShapeCtrl::ItemsDeleted(const wxDataViewItem& WXUNUSED(parent),
        const wxDataViewItemArray& WXUNUSED(items))
{
    Refresh();
    return true;
}

bool pm::RegionShapeCtrl::ItemsChanged(const wxDataViewItemArray& WXUNUSED(items))
{
    Refresh();
    return true;
}

bool pm::RegionShapeCtrl::Cleared()
{
    Refresh();
    return true;
}

void pm::RegionShapeCtrl::Resort()
{
}

bool pm::RegionShapeCtrl::ValueChanged(const wxDataViewItem& WXUNUSED(item),
        unsigned int WXUNUSED(col))
{
    Refresh();
    return true;
}

/******************************************************************************/
/* Copyright (C) Teledyne Photometrics. All rights reserved.                  */
/******************************************************************************/
#pragma once
#ifndef PM_WHITEBALANCEWIZARD_H
#define PM_WHITEBALANCEWIZARD_H

// WxWidgets headers have to be included first
#include <wx/event.h>
#include "PVCamTest_Ui.h"

/* System */
#include <functional>

namespace pm {

class WhiteBalanceWizard final : public ui::WhiteBalanceWizard
{
public:
    using HandlerProto = void();
    using Handler = std::function<HandlerProto>;

public:
    WhiteBalanceWizard();
    WhiteBalanceWizard(const WhiteBalanceWizard&) = delete;
    WhiteBalanceWizard& operator=(const WhiteBalanceWizard&) = delete;
    virtual ~WhiteBalanceWizard();

public:
    bool ShowModelessWizard(const Handler& onCancel, const Handler& onFinish);

private: // UI event handlers
    void OnClose(wxCloseEvent& event);
    void OnCancel(wxWizardEvent& event);
    void OnFinish(wxWizardEvent& event);

private:
    Handler m_onCancelHandler;
    Handler m_onFinishHandler;
};

} // namespace pm

#endif // PM_WHITEBALANCEWIZARD_H

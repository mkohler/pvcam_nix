/******************************************************************************/
/* Copyright (C) Teledyne Photometrics. All rights reserved.                  */
/******************************************************************************/
#include "WhiteBalanceWizard.h"

pm::WhiteBalanceWizard::WhiteBalanceWizard()
    : pm::ui::WhiteBalanceWizard(nullptr),
    m_onCancelHandler([](){}),
    m_onFinishHandler([](){})
{
    Bind(wxEVT_CLOSE_WINDOW, &WhiteBalanceWizard::OnClose, this);
    Bind(wxEVT_WIZARD_CANCEL, &WhiteBalanceWizard::OnCancel, this);
    Bind(wxEVT_WIZARD_FINISHED, &WhiteBalanceWizard::OnFinish, this);
}

pm::WhiteBalanceWizard::~WhiteBalanceWizard()
{
}

bool pm::WhiteBalanceWizard::ShowModelessWizard(const Handler& onCancel,
        const Handler& onFinish)
{
    ShowPage(m_pages[0]);
    if (!Show())
        return false;

    m_onCancelHandler = onCancel;
    m_onFinishHandler = onFinish;

    return true;
}

void pm::WhiteBalanceWizard::OnClose(wxCloseEvent& WXUNUSED(event))
{
    m_onCancelHandler();

    m_onCancelHandler = [](){};
    m_onFinishHandler = [](){};
}

void pm::WhiteBalanceWizard::OnCancel(wxWizardEvent& WXUNUSED(event))
{
    m_onCancelHandler();

    m_onCancelHandler = [](){};
    m_onFinishHandler = [](){};
}

void pm::WhiteBalanceWizard::OnFinish(wxWizardEvent& WXUNUSED(event))
{
    m_onFinishHandler();

    m_onCancelHandler = [](){};
    m_onFinishHandler = [](){};
}

/******************************************************************************/
/* Copyright (C) Teledyne Photometrics. All rights reserved.                  */
/******************************************************************************/
#include "ImageDlg.h"

// WxWidgets headers have to be included first
#include <wx/dc.h>
#include <wx/display.h>
#include <wx/filedlg.h>
#include <wx/menu.h>

/* Local */
#include "backend/Bitmap.h"
#include "backend/Camera.h"
#include "backend/ColorRuntimeLoader.h"
#include "backend/ColorUtils.h"
#include "backend/Log.h"
#include "backend/PrdFileUtils.h"
#include "backend/PvcamRuntimeLoader.h"
#include "backend/TiffFileSave.h"
#include "backend/Timer.h"
#include "backend/UniqueThreadPool.h"
#include "backend/Utils.h"
#include "Region.h"
#include "RegionModel.h"

/* libtiff */
#include <tiffio.h>

/* System */
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iomanip>
#include <numeric>
#include <sstream>

wxDEFINE_EVENT(PM_EVT_UPDATE_CANVAS_SIZE, wxThreadEvent);

// Helper wrappers for PVCAM metadata functions
// ImageDlg cannot work without these functions

namespace pm {

rs_bool PVCAM_pl_md_create_frame_struct_cont(md_frame** pFrame, uint16_t roiCount)
{
    if (!PvcamRuntimeLoader::Get()->hasMetadataFunctions())
    {
        md_frame* pTmp;
        pTmp = (md_frame*)::calloc(1, sizeof(md_frame));
        if (!pTmp)
        {
            Log::LogE("Cannot allocate memory for md_frame");
            return PV_FAIL;
        }

        pTmp->roiArray = (md_frame_roi*)::calloc(roiCount, sizeof(md_frame_roi));
        if (!pTmp->roiArray)
        {
            ::free(pTmp);

            Log::LogE("Cannot allocate memory for %u md_frame_roi(s)", roiCount);
            return PV_FAIL;
        }

        pTmp->roiCapacity = roiCount;
        *pFrame = pTmp;

        return PV_OK;
    }

    return PVCAM->pl_md_create_frame_struct_cont(pFrame, roiCount);
}

rs_bool PVCAM_pl_md_release_frame_struct(md_frame* pFrame)
{
    if (!PvcamRuntimeLoader::Get()->hasMetadataFunctions())
    {
        if (!pFrame->roiArray)
        {
            Log::LogE("Null roiArray in given frame");
            return PV_FAIL;
        }

        ::free(pFrame->roiArray);
        ::free(pFrame);

        return PV_OK;
    }

    return PVCAM->pl_md_release_frame_struct(pFrame);
}

} // namespace pm

// pm::ImageDlg

pm::ImageDlg::ImageDlg()
    : pm::ui::ImageDlg(nullptr),
    m_tasksConvToRgb8(UniqueThreadPool::Get().GetPool())
{
    Bind(wxEVT_CLOSE_WINDOW, &ImageDlg::OnClose, this);
    Bind(wxEVT_CONTEXT_MENU, &ImageDlg::OnCanvasContextMenu, this);

    Bind(PM_EVT_UPDATE_CANVAS_SIZE, [&](wxThreadEvent& event) {
                DoUpdateCanvasSize(event.GetPayload<wxSize>());
            });

    m_canvas->SetRegionMouseMoveListener(this);

    m_workThread = std::make_unique<std::thread>(&ImageDlg::WorkThreadFunc, this);
}

pm::ImageDlg::~ImageDlg()
{
    m_workThreadAbortFlag = true;
    m_workCond.notify_one();
    m_workThread->join();

    FreeBuffers();
    m_frame = nullptr;

    ColorUtils::AssignContexts(&m_otfSetup.colorContext, nullptr);
    ColorUtils::AssignContexts(&m_otfSetupOld.colorContext, nullptr);
    ColorUtils::AssignContexts(&m_workOtfSetup.colorContext, nullptr);
}

void pm::ImageDlg::ActivateWindow(bool active)
{
    Show(active);
    if (active)
    {
        Raise(); // Move to the top in Z-order
    }
}

void pm::ImageDlg::EnableEditor(bool enabled)
{
    m_canvas->EnableEditor(enabled);
}

void pm::ImageDlg::SetFillMethod(FillMethod fillMethod)
{
    {
        std::lock_guard<std::mutex> lock(m_workMutex);

        m_workOtfSetup.fillMethod = fillMethod;

        m_workUpdatedFlag = true;
    }
    m_workCond.notify_one();
}

void pm::ImageDlg::SetAutoConbright(bool enabled)
{
    {
        std::lock_guard<std::mutex> lock(m_workMutex);

        m_workOtfSetup.autoConbright = enabled;

        m_workUpdatedFlag = true;
    }
    m_workCond.notify_one();
}

void pm::ImageDlg::SetContrast(int contrast)
{
    {
        std::lock_guard<std::mutex> lock(m_workMutex);

        m_workOtfSetup.contrast = contrast;

        m_workUpdatedFlag = true;
    }
    m_workCond.notify_one();
}

void pm::ImageDlg::SetBrightness(int brightness)
{
    {
        std::lock_guard<std::mutex> lock(m_workMutex);

        m_workOtfSetup.brightness = brightness;

        m_workUpdatedFlag = true;
    }
    m_workCond.notify_one();
}

void pm::ImageDlg::SetZoomFactor(ZoomFactor zoomFactor)
{
    {
        std::lock_guard<std::mutex> lock(m_workMutex);

        m_workOtfSetup.zoomFactor = zoomFactor;

        const int32_t factor = static_cast<int32_t>(zoomFactor);
        m_workOtfSetup.zoom = (factor < 0)
            ? 1.0 / (double)(1 << -factor)
            : (double)(1 << factor);

        m_workUpdatedFlag = true;
    }
    m_workCond.notify_one();
}

void pm::ImageDlg::SetColorEnabled(bool enabled)
{
    enabled = enabled && PH_COLOR;

    {
        std::lock_guard<std::mutex> lock(m_workMutex);

        m_workOtfSetup.colorEnabled = enabled;
        m_workOtfSetup.isColor = enabled && m_sbin == 1 && m_pbin == 1;

        m_workUpdatedFlag = true;
    }
    m_workCond.notify_one();
}

void pm::ImageDlg::SetColorContext(const ph_color_context* context)
{
    {
        std::lock_guard<std::mutex> lock(m_workMutex);

        ColorUtils::AssignContexts(&m_workOtfSetup.colorContext, context);
        m_workUpdatedFlag = true;
    }
    m_workCond.notify_one();
}

void pm::ImageDlg::SetTrackShowTrajectory(bool enabled)
{
    {
        std::lock_guard<std::mutex> lock(m_workMutex);

        m_workOtfSetup.trackShowTrajectory = enabled;

        m_workUpdatedFlag = true;
    }
    m_workCond.notify_one();
}

void pm::ImageDlg::SetMetaRoiShowOutline(bool enabled)
{
    {
        std::lock_guard<std::mutex> lock(m_workMutex);

        m_workOtfSetup.metaRoiShowOutline = enabled;

        m_workUpdatedFlag = true;
    }
    m_workCond.notify_one();
}

void pm::ImageDlg::SetRoiShowOutline(bool enabled)
{
    {
        std::lock_guard<std::mutex> lock(m_workMutex);

        m_workOtfSetup.roiShowOutline = enabled;

        m_workUpdatedFlag = true;
    }
    m_workCond.notify_one();
}

bool pm::ImageDlg::SetImageFormat(short roiIdx, Camera* camera,
        RegionModel* regionModel)
{
    std::lock_guard<std::mutex> lock(m_mutex);

    do
    {
        if (!camera || !regionModel)
            break;

        // New region number has to be valid
        if (roiIdx < -1)
            break;

        const bool firstSetup = (m_roiIdx < -1);
        if (firstSetup)
        {
            m_roiIdx = roiIdx;

            if (m_roiIdx == -1)
            {
                // Preview window with recomposed and zoomed full-frame image
                m_defaultTitle = "Image Preview";
            }
            else // m_roiIdx >= 0
            {
                m_defaultTitle = std::to_string(m_roiIdx + 1);
            }
            SetTitle(m_defaultTitle);
        }
        else
        {
            // Always the same region number has to be used after first setup
            if (m_roiIdx != roiIdx)
                break;
        }

        const bool acqCfgChanged = m_frameAcqCfg != camera->GetFrameAcqCfg();
        m_frameAcqCfg = camera->GetFrameAcqCfg();

        m_usesCentroids = m_frameAcqCfg.HasMetadata()
            && camera->GetParams().Get<PARAM_CENTROIDS_ENABLED>()->IsAvail()
            && camera->GetParams().Get<PARAM_CENTROIDS_ENABLED>()->GetCur();
        m_centroidsMode =
            camera->GetParams().Get<PARAM_CENTROIDS_MODE>()->IsAvail()
            ? camera->GetParams().Get<PARAM_CENTROIDS_MODE>()->GetCur()
            : PL_CENTROIDS_MODE_LOCATE;
        m_centroidsRadius =
            camera->GetParams().Get<PARAM_CENTROIDS_RADIUS>()->IsAvail()
            ? camera->GetParams().Get<PARAM_CENTROIDS_RADIUS>()->GetCur()
            : 1;
        m_expTimeRes =
            camera->GetParams().Get<PARAM_EXP_RES_INDEX>()->IsAvail()
            ? camera->GetParams().Get<PARAM_EXP_RES_INDEX>()->GetCur()
            : static_cast<uint16_t>(EXP_RES_ONE_MILLISEC);

        if (m_frame && m_frame->IsValid() && m_frame->GetAcqCfg() != m_frameAcqCfg)
        {
            m_frame->Invalidate();
        }

        const uint16_t sbin = camera->GetSettings().GetBinningSerial();
        const uint16_t pbin = camera->GetSettings().GetBinningParallel();
        if (sbin == 0 || pbin == 0)
            break;
        const bool binChanged = (m_sbin != sbin || m_pbin != pbin);
        if (binChanged)
        {
            m_sbin = sbin;
            m_pbin = pbin;
            m_workOtfSetup.isColor = m_workOtfSetup.colorEnabled
                && m_sbin == 1 && m_pbin == 1;
            m_otfSetup.isColor = m_workOtfSetup.isColor;
        }

        wxRect rect;
        wxRect rectFull = wxRect(0, 0,
                camera->GetParams().Get<PARAM_SER_SIZE>()->GetCur(),
                camera->GetParams().Get<PARAM_PAR_SIZE>()->GetCur());
        if (m_roiIdx == -1)
        {
            wxRect impliedRect;
            if (regionModel->GetRegions().empty())
            {
                impliedRect = rectFull;
            }
            else
            {
                impliedRect = regionModel->GetRegion(0)->GetClientRect();
                for (size_t n = 1; n < regionModel->GetRegionCount(); n++)
                {
                    impliedRect.Union(regionModel->GetRegion(n)->GetClientRect());
                }
            }
            rect = impliedRect;
        }
        else // m_roiIdx >= 0
        {
            if ((size_t)m_roiIdx >= regionModel->GetRegionCount())
                break;
            rectFull = regionModel->GetRegion(m_roiIdx)->GetClientRect();
            rect = rectFull;
        }

        // TODO: Recalculate m_rect & m_rectFull with respect to binning
        //       to align them properly on pixels
        const uint32_t width = rect.GetWidth() / m_sbin;
        const uint32_t height = rect.GetHeight() / m_pbin;
        const uint32_t widthFull = rectFull.GetWidth() / m_sbin;
        const uint32_t heightFull = rectFull.GetHeight() / m_pbin;

        if (m_frameAcqCfg.GetFrameBytes() == 0)
        {
            // We need to calculate the size before doing first PVCAM setup
            const size_t bpp = m_frameAcqCfg.GetBitmapFormat().GetBytesPerPixel();
            m_frameAcqCfg.SetFrameBytes(width * height * bpp);
        }

        //const bool rectChanged = (m_rect != rect);
        m_rect = rect;

        const bool rectFullChanged = (m_rectFull != rectFull);
        if (rectFullChanged || binChanged || acqCfgChanged)
        {
            m_rectFull = rectFull;

            FreeBuffers();

            try
            {
                m_nativeFrameFullBmp = new Bitmap(
                        widthFull, heightFull, m_frameAcqCfg.GetBitmapFormat());

                auto rgbFrameFullFormat = m_frameAcqCfg.GetBitmapFormat();
                rgbFrameFullFormat.SetPixelType(BitmapPixelType::RGB);
                m_rgbFrameFullBmp =
                    new Bitmap(widthFull, heightFull, rgbFrameFullFormat);

                m_frameProcessor.Invalidate();
            }
            catch (const std::exception& ex)
            {
                Log::LogE("%s", ex.what());
                break;
            }

            m_canvas->SetNonBinnedCanvasSize(rectFull.GetSize());
            m_canvas->SetImageSize(wxSize(widthFull, heightFull));

            const auto& outputBmpRois = m_frameAcqCfg.GetOutputBmpRois();
            std::vector<wxSize> imagesSize;
            if (outputBmpRois.empty())
            {
                imagesSize.push_back(wxSize(widthFull, heightFull));
            }
            else
            {
                imagesSize.reserve(outputBmpRois.size());
                for (auto& rgn : outputBmpRois)
                {
                    const uint16_t roiW = (rgn.s2 - rgn.s1 + 1) / rgn.sbin;
                    const uint16_t roiH = (rgn.p2 - rgn.p1 + 1) / rgn.pbin;
                    imagesSize.push_back(wxSize(roiW, roiH));
                }
            }
            m_canvas->SetImagesSize(imagesSize);

            m_outlines.clear();

            const uint16_t roiOffX = (uint16_t)m_rectFull.GetLeft() / m_sbin;
            const uint16_t roiOffY = (uint16_t)m_rectFull.GetTop() / m_pbin;
            const auto canvasOffset = Frame::Point(roiOffX, roiOffY);
            if (m_roiIdx == -1 || m_usesCentroids)
            {
                m_canvas->SetImages({}, {}, 0, canvasOffset, m_sbin, m_pbin,
                        wxTransparentColour);
            }
            else
            {
                m_canvas->SetImage(nullptr, canvasOffset, canvasOffset, m_sbin, m_pbin);
            }
        }

        if (m_centroidsMode == PL_CENTROIDS_MODE_BLOB)
        {
            m_canvas->SetOutlineShape(RegionShapeCtrl::OutlineShape::Ellipse);
        }
        else
        {
            m_canvas->SetOutlineShape(RegionShapeCtrl::OutlineShape::Rectangle);
        }

        const wxSize size = FireEventUpdateCanvasSize();

        if (rectFullChanged)
        {
            // Keep commented, with small zoom the window size would be small too
            //SetMaxClientSize(size);

            // Get default size based on primary display
            wxSize defSize(1024, 730); // cca 1024x768 minus panel height
            // Not searching for primary wxDisplay as it seems it does not always work.
            // For us the primary monitor is the one with point (0, 0).
            const int index = wxDisplay::GetFromPoint(GetRect().GetTopLeft());
            wxDisplay display((index == wxNOT_FOUND) ? 0 : (unsigned int)index);
            const wxRect dispRect = display.GetClientArea();
            const wxRect scrRect = GetRect();
            const wxRect cliRect = GetClientRect();
            const int wOver = scrRect.width - cliRect.width;
            const int hOver = scrRect.height - cliRect.height;
            // MainDlg with ImageDlg should fill up to whole display area.
            defSize.SetWidth(dispRect.width - scrRect.x - dispRect.x - wOver);
            defSize.SetHeight(dispRect.height - scrRect.y - dispRect.y - hOver);

            const wxSize maxInitSize = wxRect(wxPoint(0, 0), size).Intersect(
                    wxRect(wxPoint(0, 0), defSize))
                .GetSize();
            SetClientSize(maxInitSize);
        }

        if (firstSetup && m_roiIdx == -1)
        {
            // Allow region editing in preview window only
            m_canvas->AssociateModel(regionModel);
        }

        // Update virtual size, that also updates scrollbars
        m_scrolledWindow->FitInside();

        return true;
    }
    ONCE;

    FreeBuffers();
    m_frame = nullptr;

    return false;
}

void pm::ImageDlg::UpdateImage(std::shared_ptr<Frame> frame)
{
    {
        std::lock_guard<std::mutex> lock(m_workMutex);

        m_workFrame = frame;

        m_workUpdatedFlag = true;
    }
    m_workCond.notify_one();
}

void pm::ImageDlg::SaveImage(const std::string& fullFileName)
{
    std::lock_guard<std::mutex> lock(m_mutex);

    DoSaveImage(fullFileName);
}

void pm::ImageDlg::OnClose(wxCloseEvent& event)
{
    // Do not allow to close window by user directly
    if (event.CanVeto())
        event.Veto();
}

void pm::ImageDlg::OnCanvasContextMenu(wxContextMenuEvent& event)
{
    std::unique_lock<std::mutex> lock(m_mutex);

    if (!IsEnabled())
    {
        event.Skip(); // Let base class handle this event
        return;
    }

    // Have to remember it before showing menu because of mouse leave event
    std::shared_ptr<Region> region = m_canvas->GetActiveRegion();

    wxPoint pos = event.GetPosition();
    pos = ScreenToClient(pos);

    // Do not show popup menu when right-click on window frame
    if (!m_canvas->GetClientRect().Contains(pos))
    {
        event.Skip(); // Let base class handle this event
        return;
    }

    wxMenu menu;
    bool addSeparator = false;
    if (region)
    {
        menu.Append(wxID_DELETE, _("Delete region"));
        addSeparator = true;
    }
    if (m_frame && m_frame->IsValid())
    {
        if (addSeparator)
        {
            menu.AppendSeparator();
        }
        menu.Append(wxID_SAVEAS, _("Save image as..."));
    }

    // Nothing to show
    if (menu.GetMenuItemCount() == 0)
        return;

    lock.unlock();
    // Popup menu is shown without holding mutex locked so user can click
    // Save menu on the right frame.
    const int id = GetPopupMenuSelectionFromUser(menu, pos);
    // Save dialog is then shown with locked mutex so the saved image freezes
    // and user sees what gets stored.
    lock.lock();

    switch (id)
    {
    case wxID_DELETE:
    {
        auto model = m_canvas->GetModel();
        if (!model)
            break;
        model->DeleteRegion(region);
        break;
    }
    case wxID_SAVEAS:
    {
        wxFileDialog saveDialog(this, _("Save image as..."),
                wxEmptyString, wxEmptyString,
                _("TIFF files (*.tiff,*.tif)|*.tiff;*.tif"),
                wxFD_SAVE | wxFD_OVERWRITE_PROMPT);
        if (saveDialog.ShowModal() != wxID_OK)
            break;
        DoSaveImage(saveDialog.GetPath().ToStdString());
        break;
    }
    default:
        break;
    }
}

wxSize pm::ImageDlg::FireEventUpdateCanvasSize()
{
    const wxSize size(
            m_otfSetup.zoom * (double)m_rectFull.GetWidth(),
            m_otfSetup.zoom * (double)m_rectFull.GetHeight());

    if (wxThread::GetCurrentId() == wxThread::GetMainId())
    {
        DoUpdateCanvasSize(size);
    }
    else
    {
        wxThreadEvent* event = new(std::nothrow) wxThreadEvent(PM_EVT_UPDATE_CANVAS_SIZE);
        if (event)
        {
            event->SetPayload(size);
            wxQueueEvent(GetEventHandler(), event);
        }
    }

    return size;
}

void pm::ImageDlg::DoUpdateCanvasSize(wxSize size)
{
    m_canvas->SetZoom(m_otfSetup.zoom);

    m_canvas->SetMinClientSize(size);
    m_canvas->SetMaxClientSize(size);
    m_canvas->SetClientSize(size);

    // Update virtual size, that also updates scrollbars
    m_scrolledWindow->FitInside();
}

void pm::ImageDlg::OnRegionMouseMoved(int x, int y)
{
    std::lock_guard<std::mutex> lock(m_mutex);

    std::ostringstream title;
    title << m_defaultTitle;

    if (x >= 0 && y >= 0)
    {
        // Correlate view zoom factor
        x /= m_otfSetup.zoom;
        y /= m_otfSetup.zoom;

        // Correlate possible sub-pixel offset for ROIs not aligned to binning
        const uint16_t absX = (uint16_t)(x + (m_rectFull.x / m_sbin) * m_sbin);
        const uint16_t absY = (uint16_t)(y + (m_rectFull.y / m_pbin) * m_pbin);
        title << " - [x:" << absX << ", y:" << absY << "]";

        if (m_frame && m_frame->IsValid())
        {
            title << " = ";

            // Get coordinates in binned image at (0,0) offset
            const uint16_t X = x / m_sbin;
            const uint16_t Y = y / m_pbin;

            bool pixelFound = false;
            const auto& rgns = m_frameProcessor.GetBitmapRegions();
            const auto& rawBmps = m_frameProcessor.GetRawBitmaps();
            const auto& debBmps = m_frameProcessor.GetDebayeredBitmaps();
            const auto count = m_frameProcessor.GetValidBitmapCount();
            for (size_t roiIdx = 0; roiIdx < count; ++roiIdx)
            {
                const auto& rgn = rgns[roiIdx];
                if (absX >= rgn.s1 && absX <= rgn.s2 && absY >= rgn.p1 && absY <= rgn.p2)
                {
                    pixelFound = true;

                    // Calculate the coordinates in the target bitmap
                    const uint16_t roiX = (absX - rgn.s1) / rgn.sbin;
                    const uint16_t roiY = (absY - rgn.p1) / rgn.pbin;

                    const auto& rawBmp = rawBmps[roiIdx];
                    switch (m_frame->GetAcqCfg().GetBitmapFormat().GetPixelType())
                    {
                    case BitmapPixelType::Mono: {
                        const double value = rawBmp->GetSample(roiX, roiY);
                        title << value << " ADUs";
                        if (m_otfSetup.isColor)
                        {
                            const auto& debBmp = debBmps[roiIdx];
                            const double r = debBmp->GetSample(roiX, roiY, 0);
                            const double g = debBmp->GetSample(roiX, roiY, 1);
                            const double b = debBmp->GetSample(roiX, roiY, 2);
                            title << " (r:" << r << ", g:" << g << ", b:" << b << ")";
                        }
                        break; }
                    case BitmapPixelType::RGB: {
                        const double r = rawBmp->GetSample(roiX, roiY, 0);
                        const double g = rawBmp->GetSample(roiX, roiY, 1);
                        const double b = rawBmp->GetSample(roiX, roiY, 2);
                        title << " r:" << r << ", g:" << g << ", b:" << b << " ADUs";
                        break; }
                    }
                    break;
                }
            }
            if (!pixelFound)
            {
                if (m_otfSetup.fillMethod == FillMethod::NoFill)
                {
                    title << "[n/a]";
                }
                else
                {
                    title << m_fillValue << " ADUs (filled)";
                }
            }
        }
    }

    SetTitle(title.str());
}

void pm::ImageDlg::DoSaveImage(const std::string& fullFileName)
{
    if (!m_frameFullBmp || m_sbin == 0 || m_pbin == 0)
        return;

    if (!m_frame || !m_frame->IsValid())
        return;

    const md_frame* frameMeta = m_frame->GetMetadata(); //Can be null

    rgn_type impliedRoi;
    uint16_t roiCount;
    if (m_frame->GetAcqCfg().HasMetadata())
    {
        impliedRoi = frameMeta->impliedRoi;
        roiCount = frameMeta->roiCount;
    }
    else
    {
        impliedRoi.s1 = (uint16_t)m_rect.GetLeft();
        impliedRoi.s2 = (uint16_t)m_rect.GetRight();
        impliedRoi.sbin = m_sbin;
        impliedRoi.p1 = (uint16_t)m_rect.GetTop();
        impliedRoi.p2 = (uint16_t)m_rect.GetBottom();
        impliedRoi.pbin = m_pbin;
        roiCount = 1;
    }

    const uint32_t widthFull = m_rectFull.GetWidth() / m_sbin;
    const uint32_t heightFull = m_rectFull.GetHeight() / m_pbin;

    TIFF* file = TIFFOpen(fullFileName.c_str(), "w");
    if (!file)
        return;

    const auto& bmpFormat = m_frameFullBmp->GetFormat();
    TIFFSetField(file, TIFFTAG_IMAGEWIDTH, widthFull);
    TIFFSetField(file, TIFFTAG_IMAGELENGTH, heightFull);
    TIFFSetField(file, TIFFTAG_BITSPERSAMPLE, 8 * bmpFormat.GetBytesPerSample());
    TIFFSetField(file, TIFFTAG_ORIENTATION, ORIENTATION_TOPLEFT);
    TIFFSetField(file, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
    TIFFSetField(file, TIFFTAG_SAMPLESPERPIXEL, bmpFormat.GetSamplesPerPixel());
    TIFFSetField(file, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_MINISBLACK);
    if (bmpFormat.GetBitDepth() <= 16)
    {
        TIFFSetField(file, TIFFTAG_MAXSAMPLEVALUE, (1u << bmpFormat.GetBitDepth()) - 1);
    }

    PrdHeader prdHeader;
    ClearPrdHeaderStructure(prdHeader);
    // PRD v0.1 data
    prdHeader.version = PRD_VERSION_0_7;
    prdHeader.bitDepth = static_cast<uint8_t>(
            m_frame->GetAcqCfg().GetBitmapFormat().GetBitDepth());
    prdHeader.frameCount = 1;
    memcpy(&prdHeader.region, &impliedRoi, sizeof(PrdRegion));
    prdHeader.sizeOfPrdMetaDataStruct = sizeof(PrdMetaData);
    switch (m_expTimeRes)
    {
    case EXP_RES_ONE_MICROSEC:
        prdHeader.exposureResolution = PRD_EXP_RES_US;
        break;
    case EXP_RES_ONE_MILLISEC:
    default: // Just in case, should never happen
        prdHeader.exposureResolution = PRD_EXP_RES_MS;
        break;
    case EXP_RES_ONE_SEC:
        prdHeader.exposureResolution = PRD_EXP_RES_S;
        break;
    }
    // PRD v0.3 data
    prdHeader.colorMask = static_cast<uint8_t>(
            m_frame->GetAcqCfg().GetBitmapFormat().GetColorMask());
    prdHeader.flags = (m_frame->GetAcqCfg().HasMetadata())
        ? PRD_FLAG_HAS_METADATA : 0x00;
    prdHeader.frameSize = m_frame->GetAcqCfg().GetFrameBytes();
    // PRD v0.6 data
    prdHeader.imageFormat = static_cast<uint8_t>(
            m_frame->GetAcqCfg().GetBitmapFormat().GetImageFormat());

    const Frame::Info& fi = m_frame->GetInfo();
    const uint64_t bof = fi.GetTimestampBOF() * 100;
    const uint64_t eof = fi.GetTimestampEOF() * 100;

    PrdMetaData metaData;
    memset(&metaData, 0, sizeof(PrdMetaData));
    // PRD v0.1 data
    metaData.frameNumber = fi.GetFrameNr();
    metaData.readoutTime = fi.GetReadoutTime() * 100;
    metaData.exposureTime = fi.GetExpTime();
    // PRD v0.2 data
    metaData.bofTime = static_cast<uint32_t>(bof & 0xFFFFFFFF);
    metaData.eofTime = static_cast<uint32_t>(eof & 0xFFFFFFFF);
    // PRD v0.3 data
    metaData.roiCount = roiCount;
    // PRD v0.4 data
    metaData.bofTimeHigh = static_cast<uint32_t>((bof >> 32) & 0xFFFFFFFF);
    metaData.eofTimeHigh = static_cast<uint32_t>((eof >> 32) & 0xFFFFFFFF);
    // PRD v0.7 data
    metaData.colorWbScaleRed = fi.GetColorWbScaleRed();
    metaData.colorWbScaleGreen = fi.GetColorWbScaleGreen();
    metaData.colorWbScaleBlue = fi.GetColorWbScaleBlue();

    // Build up the metadata description
    const std::string imageDesc =
        TiffFileSave::GetImageDesc(prdHeader, &metaData, frameMeta);
    // Put the PVCAM metadata into the image description
    TIFFSetField(file, TIFFTAG_IMAGEDESCRIPTION, imageDesc.c_str());

    // Recompose image with original bit depth
    const auto frameFullBmpType = (m_otfSetup.isColor)
        ? FrameProcessor::UseBmp::Debayered
        : FrameProcessor::UseBmp::Raw;
    RecomposeFrame(m_frameFullBmp, frameFullBmpType, m_fillValue);

    // Nothing to do on error
    TIFFWriteRawStrip(file, 0, m_frameFullBmp->GetData(), m_frameFullBmp->GetDataBytes());

    TIFFFlush(file);
    TIFFClose(file);
}

void pm::ImageDlg::FreeBuffers()
{
    delete m_rgbFrameFullBmp;
    m_rgbFrameFullBmp = nullptr;

    delete m_nativeFrameFullBmp;
    m_nativeFrameFullBmp = nullptr;

    m_frameFullBmp = nullptr;
}

void pm::ImageDlg::WorkThreadFunc()
{
    std::unique_lock<std::mutex> fullLock(m_mutex, std::defer_lock);
    std::unique_lock<std::mutex> workLock(m_workMutex, std::defer_lock);

    while (!m_workThreadAbortFlag)
    {
        workLock.lock(); // Only m_workMutex is locked

        if (!m_workUpdatedFlag)
        {
            m_workCond.wait(workLock, [this] {
                return (m_workUpdatedFlag || m_workThreadAbortFlag);
            });
        }
        if (m_workThreadAbortFlag)
            break;
        m_workUpdatedFlag = false;

        workLock.unlock(); // Unlock m_workMutex before we lock both

        const bool notSetupYet = (m_roiIdx < -1);
        if (notSetupYet)
            continue;

        std::lock(fullLock, workLock); // Both mutexes are locked without deadlock

        // Backup old values and quickly move data out of m_work variables

        m_frameOld = m_frame;
        m_frame = m_workFrame;

        auto colorCtxOld = m_otfSetupOld.colorContext; // Backup color context
        m_otfSetupOld = m_otfSetup; // Deep copy overwrites color context
        m_otfSetupOld.colorContext = colorCtxOld; // Restore color context
        ColorUtils::AssignContexts(&m_otfSetupOld.colorContext, m_otfSetup.colorContext);

        auto colorCtx = m_otfSetup.colorContext; // Backup color context
        m_otfSetup = m_workOtfSetup; // Deep copy overwrites color context
        m_otfSetup.colorContext = colorCtx; // Restore color context
        ColorUtils::AssignContexts(&m_otfSetup.colorContext, m_workOtfSetup.colorContext);

        workLock.unlock(); // Only m_mutex stays locked

        ProcessImage();

        fullLock.unlock(); // Both mutexes are unlocked
    }
}

void pm::ImageDlg::ProcessImage()
{
    // m_mutex is already locked

    //Timer mainTimer;
    //Timer timer;

    bool isProcessingNeeded = false;

    if (m_frame != m_frameOld
            || m_otfSetup.fillMethod != m_otfSetupOld.fillMethod
            || m_otfSetup.autoConbright != m_otfSetupOld.autoConbright
            || m_otfSetup.contrast != m_otfSetupOld.contrast
            || m_otfSetup.brightness != m_otfSetupOld.brightness
            || m_otfSetup.colorEnabled != m_otfSetupOld.colorEnabled
            || m_otfSetup.trackShowTrajectory != m_otfSetupOld.trackShowTrajectory
            || m_otfSetup.metaRoiShowOutline != m_otfSetupOld.metaRoiShowOutline
            || m_otfSetup.roiShowOutline != m_otfSetupOld.roiShowOutline)
    {
        isProcessingNeeded = true;
    }

    if (PH_COLOR && m_otfSetup.colorContext && !ColorUtils::CompareContexts(
                m_otfSetup.colorContext, m_otfSetupOld.colorContext))
    {
        if (PH_COLOR_ERROR_NONE
                != PH_COLOR->context_apply_changes(m_otfSetup.colorContext))
        {
            ColorUtils::LogError("Failure applying color helper context changes");
            return;
        }
        isProcessingNeeded = true;
    }

    if (m_otfSetup.zoomFactor != m_otfSetupOld.zoomFactor)
    {
        FireEventUpdateCanvasSize();
        isProcessingNeeded = true;
    }

    if (!isProcessingNeeded)
        return;

    // TODO: Remove this restriction
    if (m_otfSetup.isColor)
    {
        switch (m_frameAcqCfg.GetBitmapFormat().GetDataType())
        {
        case BitmapDataType::UInt8:
        case BitmapDataType::UInt16:
            break; // Supported types for color helper library
        default:
            m_workOtfSetup.isColor = false;
            m_otfSetup.isColor = m_workOtfSetup.isColor;
            break;
        }
    }

    const auto frameFullBmpType = (m_otfSetup.isColor)
        ? FrameProcessor::UseBmp::Debayered
        : FrameProcessor::UseBmp::Raw;
    m_frameFullBmp = (m_otfSetup.isColor) ? m_rgbFrameFullBmp : m_nativeFrameFullBmp;
    if (m_otfSetup.colorContext)
    {
        m_rgbFrameFullBmp->ChangeColorMask(
                static_cast<BayerPattern>(m_otfSetup.colorContext->pattern));
    }

    do
    {
        if (!m_frame || !m_frame->IsValid())
            break;

        //Log::LogI("Processing frameNr=%u, roiIdx=%d",
        //        m_frame->GetInfo().GetFrameNr(), m_roiIdx);

        if (m_frameAcqCfg != m_frame->GetAcqCfg())
        {
            Log::LogE("Frame configuration differs from previously set");
            break;
        }

        //timer.Reset();
        if (!m_frame->DecodeMetadata())
            break;
        //Log::LogI("  Decode took %.1f us", timer.Microseconds());

        try
        {
            //timer.Reset();
            m_frameProcessor.SetFrame(m_frame);
            //Log::LogI("  SetFrame took %.1f us", timer.Microseconds());

            if (m_otfSetup.isColor)
            {
                //timer.Reset();
                if (m_roiIdx == -1 || m_usesCentroids)
                {
                    m_frameProcessor.Debayer(m_otfSetup.colorContext);
                }
                else // m_roiIdx >= 0
                {
                    m_frameProcessor.DebayerRoi(m_roiIdx, m_otfSetup.colorContext);
                }
                //Log::LogI("  Debayer took %.1f us", timer.Microseconds());
            }

            const double maxPixelValue =
                ::pow(2, m_frameAcqCfg.GetBitmapFormat().GetBitDepth()) - 1;
            double min = 0.0;
            double max = maxPixelValue;
            double mean = 0.0;
            // TODO: Optimize and do not calculate mean if the fillMethod is never used
            //const bool isFillNeeded = frameMeta->roiCount > 1
            //    || m_rectFull != rectImpl
            //    || (m_usesCentroids
            //        && m_centroidsMode != PL_CENTROIDS_MODE_TRACK
            //        && m_centroidsMode != PL_CENTROIDS_MODE_BLOB);
            //const bool isMeanNeeded = isFillNeeded
            //    && m_otfSetup.fillMethod == FillMethod::MeanValue;
            if (m_otfSetup.autoConbright || m_otfSetup.fillMethod == FillMethod::MeanValue)
            {
                //timer.Reset();
                m_frameProcessor.ComputeStats();
                const auto& stats = m_frameProcessor.GetStats();
                //Log::LogI("  FrameStats took %.1f us (min=%.0f,max=%.0f,mean=%.0f)",
                //        timer.Microseconds(), stats.GetMin(), stats.GetMax(),
                //        stats.GetMean());

                mean = stats.GetMean();

                if (m_otfSetup.autoConbright)
                {
                    min = stats.GetMin();
                    max = stats.GetMax();

                    if (max > maxPixelValue)
                    {
                        Log::LogW("Frame nr. %u has max. pixel value over the bit depth range (%.0f > %.0f)",
                                m_frame->GetInfo().GetFrameNr(), max, maxPixelValue);
                        // TODO: Do not limit the values now, let's scale actual values,
                        //       there could be wrong bit depth reported with
                        //       frame summing feature enabled.
                        //max = maxPixelValue;
                        //min = Clamp(min, 0.0, max);
                        //mean = Clamp(mean, 0.0, max);
                    }
                }
            }

            switch (m_otfSetup.fillMethod)
            {
            case FillMethod::MeanValue:
                m_fillValue = mean;
                break;
            case FillMethod::NoFill:
            case FillMethod::BlackColor:
                m_fillValue = 0.0;
                break;
            case FillMethod::CustomColor:
                // TODO: Allow any custom color, so far the UI supports black color only
                m_fillValue = 0.0;
                break;
            }

            //timer.Reset();
            if (m_roiIdx == -1 || m_usesCentroids)
            {
                m_frameProcessor.CovertToRgb8bit(frameFullBmpType,
                        min, max, m_otfSetup.autoConbright, m_otfSetup.brightness,
                        m_otfSetup.contrast);
            }
            else // m_roiIdx >= 0
            {
                m_frameProcessor.CovertRoiToRgb8bit(m_roiIdx, frameFullBmpType,
                        min, max, m_otfSetup.autoConbright, m_otfSetup.brightness,
                        m_otfSetup.contrast);
            }
            const auto fillValue8 = TaskSet_ConvertToRgb8::ConvertOnePixel(
                    m_fillValue, min, max, m_otfSetup.autoConbright,
                    m_otfSetup.brightness, m_otfSetup.contrast);
            //Log::LogI("  Conv. to 8bit RGB took %.1f us", timer.Microseconds());

            //timer.Reset();
            ConvertOutlinesToWxRects();
            //Log::LogI("  Conv. to outlines took %.1f us", timer.Microseconds());

            const auto& trajectories = (m_otfSetup.trackShowTrajectory)
                ? m_frame->GetTrajectories() : Frame::Trajectories();

            //timer.Reset();
            // Shift the origin of each ROI shown in separate window to [0, 0]
            const uint16_t roiOffX = (uint16_t)m_rectFull.GetLeft() / m_sbin;
            const uint16_t roiOffY = (uint16_t)m_rectFull.GetTop() / m_pbin;
            const auto canvasOffset = Frame::Point(roiOffX, roiOffY);
            const auto fillColor = (m_otfSetup.fillMethod == FillMethod::NoFill)
                ? wxTransparentColour
                : wxColour(fillValue8, fillValue8, fillValue8);
            if (m_roiIdx == -1 || m_usesCentroids)
            {
                m_canvas->SetImages(m_frameProcessor.GetRgb8bitBitmaps(),
                        m_frameProcessor.GetBitmapPositions(),
                        m_frameProcessor.GetValidBitmapCount(),
                        canvasOffset, m_sbin, m_pbin, fillColor,
                        m_outlines, trajectories);
            }
            else
            {
                const auto image =
                    m_frameProcessor.GetRgb8bitBitmaps()[m_roiIdx].get();
                const auto offset =
                    m_frameProcessor.GetBitmapPositions()[m_roiIdx];
                m_canvas->SetImage(image, offset, canvasOffset, m_sbin, m_pbin);
            }
            //Log::LogI("  SetImage took %.1f us", timer.Microseconds());
        }
        catch (const std::exception& ex)
        {
            Log::LogE("%s", ex.what());
            break;
        }

        //Log::LogI("ProcessImage took %.1f us\n", mainTimer.Microseconds());
        return;
    }
    ONCE;

    // Here we go on error or with no frame

    // Update background image on region canvas to be invalid
    const uint16_t roiOffX = (uint16_t)m_rectFull.GetLeft() / m_sbin;
    const uint16_t roiOffY = (uint16_t)m_rectFull.GetTop() / m_pbin;
    const auto canvasOffset = Frame::Point(roiOffX, roiOffY);
    if (m_roiIdx == -1 || m_usesCentroids)
    {
        m_canvas->SetImages({}, {}, 0, canvasOffset, m_sbin, m_pbin,
                wxTransparentColour);
    }
    else
    {
        m_canvas->SetImage(nullptr, canvasOffset, canvasOffset, m_sbin, m_pbin);
    }

    //Log::LogI("ProcessImage took %.1f us\n", mainTimer.Microseconds());
}

void pm::ImageDlg::ConvertOutlinesToWxRects()
{
    m_outlines.clear();

    if (!m_usesCentroids || !m_otfSetup.metaRoiShowOutline)
        return;

    auto AddOutline = [](std::vector<wxRect>& container, const rgn_type& rgn) {
        const uint16_t roiX = rgn.s1 / rgn.sbin;
        const uint16_t roiY = rgn.p1 / rgn.pbin;
        const uint32_t roiW = (rgn.s2 - rgn.s1 + 1) / rgn.sbin;
        const uint32_t roiH = (rgn.p2 - rgn.p1 + 1) / rgn.pbin;
        container.emplace_back(roiX, roiY, roiW, roiH);
    };

    const md_frame* frameMeta = m_frame->GetMetadata();

    switch (m_centroidsMode)
    {
    case PL_CENTROIDS_MODE_LOCATE:
        for (uint16_t i = 0; i < frameMeta->roiCount; ++i)
        {
            md_frame_roi_header* roiHdr = frameMeta->roiArray[i].header;
            AddOutline(m_outlines, roiHdr->roi);
        }
        break;
    case PL_CENTROIDS_MODE_TRACK:
        for (uint16_t i = 0; i < frameMeta->roiCount; ++i)
        {
            md_frame_roi_header* roiHdr = frameMeta->roiArray[i].header;
            if (!(roiHdr->flags & PL_MD_ROI_FLAG_HEADER_ONLY))
                continue;
            AddOutline(m_outlines, roiHdr->roi);
        }
        break;
    case PL_CENTROIDS_MODE_BLOB:
        for (uint16_t i = 0; i < frameMeta->roiCount; ++i)
        {
            md_frame_roi_header* roiHdr = frameMeta->roiArray[i].header;
            if (!(roiHdr->flags & PL_MD_ROI_FLAG_HEADER_ONLY))
                continue;
            rgn_type rgn = roiHdr->roi;
            if (rgn.s1 == rgn.s2 && rgn.p1 == rgn.p2)
            {
                // Fix crippled blob size
                rgn.s1 -= m_centroidsRadius;
                rgn.s2 += m_centroidsRadius;
                rgn.p1 -= m_centroidsRadius;
                rgn.p2 += m_centroidsRadius;
            }
            AddOutline(m_outlines, rgn);
        }
        break;
    // default missing intentionally, compiler warns us if new enum is added
    }
}

void pm::ImageDlg::RecomposeFrame(Bitmap* dstBmp, FrameProcessor::UseBmp srcBmpType,
        double fillValue)
{
    // Shift the origin of each ROI shown in separate window to [0, 0]
    const uint16_t dstOffX = (uint16_t)m_rectFull.GetLeft() / m_sbin;
    const uint16_t dstOffY = (uint16_t)m_rectFull.GetTop() / m_pbin;

    if (m_roiIdx == -1 || m_usesCentroids)
    {
        rgn_type rgn;
        uint16_t roiCount;
        if (m_frame->GetAcqCfg().HasMetadata())
        {
            const md_frame* frameMeta = m_frame->GetMetadata();
            rgn = frameMeta->impliedRoi;
            roiCount = frameMeta->roiCount;
        }
        else
        {
            rgn.s1 = (uint16_t)m_rect.GetLeft();
            rgn.s2 = (uint16_t)m_rect.GetRight();
            rgn.sbin = m_sbin;
            rgn.p1 = (uint16_t)m_rect.GetTop();
            rgn.p2 = (uint16_t)m_rect.GetBottom();
            rgn.pbin = m_pbin;
            roiCount = 1;
        }

        const bool hasNoData = roiCount == 0
            || rgn.s1 > rgn.s2 || rgn.s2 > m_rectFull.GetRight() || rgn.sbin == 0
            || rgn.p1 > rgn.p2 || rgn.p2 > m_rectFull.GetBottom() || rgn.pbin == 0;

        int rectW;
        int rectH;
        if (hasNoData)
        {
            // Correct size in case of invalid impliedRoi
            rectW = m_rectFull.GetWidth();
            rectH = m_rectFull.GetHeight();
        }
        else
        {
            rectW = rgn.s2 + 1 - rgn.s1;
            rectH = rgn.p2 + 1 - rgn.p1;
        }

        // Fill image with some value only if metadata has more regions,
        // centroids are used or implied ROI is not full sensor.
        const bool isFillNeeded = roiCount > 1
            || m_rectFull.GetWidth() != rectW || m_rectFull.GetHeight() != rectH
            || (m_usesCentroids
                && m_centroidsMode != PL_CENTROIDS_MODE_TRACK
                && m_centroidsMode != PL_CENTROIDS_MODE_BLOB);
        if (isFillNeeded)
        {
            dstBmp->Fill(fillValue);
        }

        // If there is no valid ROI in metadata there is no data to recompose,
        // buffer has been filled with black color value
        if (hasNoData)
            return;

        m_frameProcessor.Recompose(srcBmpType, dstBmp, dstOffX, dstOffY);
    }
    else
    {
        // User defined region (one of) in separate window
        m_frameProcessor.RecomposeRoi((uint16_t)m_roiIdx,
                srcBmpType, dstBmp, dstOffX, dstOffY);
    }
}

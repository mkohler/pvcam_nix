/******************************************************************************/
/* Copyright (C) Teledyne Photometrics. All rights reserved.                  */
/******************************************************************************/
#pragma once
#ifndef PM_REGION_LIST_CTRL_H
#define PM_REGION_LIST_CTRL_H

// WxWidgets headers have to be included first
#include <wx/dataview.h>

/* Local */
#include "ModelNotifier.h"

namespace pm {

class RegionListColumn : public wxDataViewColumn
{
public:
    RegionListColumn(const wxString& title, wxDataViewRenderer* renderer,
            unsigned int modelColumn, int width = wxDVC_DEFAULT_WIDTH,
            wxAlignment align = wxALIGN_CENTER,
            int flags = wxDATAVIEW_COL_RESIZABLE);
    RegionListColumn(const wxBitmap& bitmap, wxDataViewRenderer* renderer,
            unsigned int modelColumn, int width = wxDVC_DEFAULT_WIDTH,
            wxAlignment align = wxALIGN_CENTER,
            int flags = wxDATAVIEW_COL_RESIZABLE);

public: // wxSettableHeaderColumn
    virtual void SetFlags(int flags) override;
    virtual void SetSortable(bool sortable) override;
    virtual void SetReorderable(bool reorderable) override;
    virtual void SetHidden(bool hidden) override;

public: // wxHeaderColumn
    virtual int GetFlags() const override;
    virtual bool IsSortable() const override;
    virtual bool IsReorderable() const override;
    virtual bool IsHidden() const override;
};

class RegionListCtrl : public wxDataViewListCtrl, private IModelNotifier
{
public:
    RegionListCtrl();
    RegionListCtrl(wxWindow* parent, wxWindowID id,
            const wxPoint& pos = wxDefaultPosition,
            const wxSize& size = wxDefaultSize, long style = wxDV_ROW_LINES);
    virtual ~RegionListCtrl();

public:
    bool Initialize(const wxSize& sensorSize, int maxRegions,
            wxDataViewCellMode columnsMode);

public: // wxDataViewCtrl
    virtual bool AssociateModel(wxDataViewModel* model) override;

    virtual bool AppendColumn(wxDataViewColumn* WXUNUSED(column)) override
    { return false; }
    virtual bool PrependColumn(wxDataViewColumn* WXUNUSED(column)) override
    { return false; }
    virtual bool InsertColumn(unsigned int WXUNUSED(pos),
            wxDataViewColumn* WXUNUSED(column)) override
    { return false; }

    virtual bool DeleteColumn(wxDataViewColumn* WXUNUSED(column)) override
    { return false; }
    virtual bool ClearColumns() override
    { return false; }
    virtual bool EnableDragSource(const wxDataFormat& WXUNUSED(format)) override
    { return false; }
    virtual bool EnableDropTarget(const wxDataFormat& WXUNUSED(format)) override
    { return false; }

private:
    void UpdateSelection(bool fromModel);

private: // UI event handlers
    void OnSelectionChanged(wxDataViewEvent& event);

private: // IModelNotifier
    // Set friendship so overridden methods can be private
    friend class ModelNotifier;
    virtual bool ItemAdded(const wxDataViewItem& parent,
            const wxDataViewItem& item) override;
    virtual bool ItemDeleted(const wxDataViewItem& parent,
            const wxDataViewItem& item) override;
    virtual bool ItemChanged(const wxDataViewItem& item) override;
    virtual bool ItemsAdded(const wxDataViewItem& parent,
            const wxDataViewItemArray& items) override;
    virtual bool ItemsDeleted(const wxDataViewItem& parent,
            const wxDataViewItemArray& items) override;
    virtual bool ItemsChanged(const wxDataViewItemArray& items) override;
    virtual bool Cleared() override;
    virtual void Resort() override;
    virtual bool ValueChanged(const wxDataViewItem& item, unsigned int col)
        override;

private:
    ModelNotifier* m_notifier{ nullptr };

    bool m_updatingSelection{ false };
};

} // namespace pm

#endif // PM_REGION_LIST_CTRL_H

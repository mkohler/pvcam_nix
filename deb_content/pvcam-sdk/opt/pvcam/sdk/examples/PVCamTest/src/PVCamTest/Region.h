/******************************************************************************/
/* Copyright (C) Teledyne Photometrics. All rights reserved.                  */
/******************************************************************************/
#pragma once
#ifndef PM_REGION_H
#define PM_REGION_H

// WxWidgets headers have to be included first
#include <wx/gdicmn.h> // wxRect

namespace pm {

class Region
{
public:
    Region();
    Region(const wxRect& clientRect);

public:
    wxRect GetClientRect() const
    { return m_clientRect; }
    void SetClientRect(const wxRect& clientRect);

    bool IsVisible() const
    { return m_isVisible; }
    void SetVisible(bool visible);

    bool IsSelected() const
    { return m_isSelected; }
    void SetSelected(bool selected);

private:
    wxRect m_clientRect{ 0, 0, 50, 50 };
    bool m_isVisible{ true };
    bool m_isSelected{ false };
};

} // namespace pm

#endif // PM_REGION_H

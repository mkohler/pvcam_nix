/******************************************************************************/
/* Copyright (C) Teledyne Photometrics. All rights reserved.                  */
/******************************************************************************/
#include "PVCamTestApp.h"

// WxWidgets headers have to be included first
#include <wx/display.h>

/* Local */
#include "backend/Log.h"

pm::PVCamTestApp::PVCamTestApp()
{
    // Initiate the Log instance as the very first before any logging starts
    // No listener registered yet thus message not shown anywhere
    Log::LogI("APPLICATION STARTED");

    wxInitAllImageHandlers();

    // Connect Events
    Bind(wxEVT_ACTIVATE_APP, &PVCamTestApp::OnActivateApp, this);
}

pm::PVCamTestApp::~PVCamTestApp()
{
    // Disconnect Events
    Unbind(wxEVT_ACTIVATE_APP, &PVCamTestApp::OnActivateApp, this);

    Log::Flush();
}

bool pm::PVCamTestApp::OnInit()
{
    /* Taken from WxWidgets documentation:

        Notice that if you want to use the command line processing provided by
        wxWidgets you have to call the base class version in the derived class
        OnInit().

       We have our own parser and must not call it. */
    //if (!wxApp::OnInit())
    //    return false;

    m_mainDlg = new(std::nothrow) pm::MainDlg();
    if (m_mainDlg)
    {
        // Restore possibly minimized window when started
        if (m_mainDlg->IsIconized())
        {
            m_mainDlg->Iconize(false);
        }

        m_mainDlg->ActivateWindow();

        // Set default position to primary monitor and size to fit in width
        // maximize in height.
        // Not searching for primary wxDisplay as it seems it does not always work.
        // For us the primary monitor is the one with point (0, 0).
        const int index = wxDisplay::GetFromPoint(wxPoint(0, 0));
        wxDisplay display((index == wxNOT_FOUND) ? 0 : (unsigned int)index);
        const wxRect dispRect = display.GetClientArea();
        // Update main window size so all controls fit in
        m_mainDlg->Fit();
        m_mainDlg->SetSize(dispRect.GetX(), dispRect.GetY(), wxDefaultCoord,
                dispRect.GetHeight(), wxSIZE_USE_EXISTING);

        m_mainDlg->ShowHelp();
    }

    return true;
}

void pm::PVCamTestApp::OnActivateApp(wxActivateEvent& event)
{
    if (m_mainDlg && event.GetActive())
    {
        m_mainDlg->ActivateWindow();
    }
}

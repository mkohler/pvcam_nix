/******************************************************************************/
/* Copyright (C) Teledyne Photometrics. All rights reserved.                  */
/******************************************************************************/
#pragma once
#ifndef PM_MODEL_NOTIFIER_H
#define PM_MODEL_NOTIFIER_H

// WxWidgets headers have to be included first
#include <wx/dataview.h>

namespace pm {

// Our classes cannot directly derive from wxDataViewModelNotifier because its
// ownership is taken over by wxDataViewModel after AddNotifier call.
// But we can derive from custom interface.
class IModelNotifier
{
public:
    virtual bool ItemAdded(const wxDataViewItem& parent,
            const wxDataViewItem& item) = 0;
    virtual bool ItemDeleted(const wxDataViewItem& parent,
            const wxDataViewItem& item) = 0;
    virtual bool ItemChanged(const wxDataViewItem& item) = 0;
    virtual bool ItemsAdded(const wxDataViewItem& parent,
            const wxDataViewItemArray& items) = 0;
    virtual bool ItemsDeleted(const wxDataViewItem& parent,
            const wxDataViewItemArray& items) = 0;
    virtual bool ItemsChanged(const wxDataViewItemArray& items) = 0;
    virtual bool Cleared() = 0;
    virtual void Resort() = 0;
    virtual bool ValueChanged(const wxDataViewItem& item, unsigned int col) = 0;
};

class ModelNotifier : public wxDataViewModelNotifier
{
public:
    ModelNotifier(IModelNotifier* ctrl)
    { m_ctrl = ctrl; }

public: // wxDataViewModelNotifier
    virtual bool ItemAdded(const wxDataViewItem& parent,
            const wxDataViewItem& item) override
    { return m_ctrl->ItemAdded(parent, item); }
    virtual bool ItemDeleted(const wxDataViewItem& parent,
            const wxDataViewItem& item) override
    { return m_ctrl->ItemDeleted(parent, item); }
    virtual bool ItemChanged(const wxDataViewItem& item) override
    { return m_ctrl->ItemChanged(item); }
    virtual bool ItemsAdded(const wxDataViewItem& parent,
            const wxDataViewItemArray& items) override
    { return m_ctrl->ItemsAdded(parent, items); }
    virtual bool ItemsDeleted(const wxDataViewItem& parent,
            const wxDataViewItemArray& items) override
    { return m_ctrl->ItemsDeleted(parent, items); }
    virtual bool ItemsChanged(const wxDataViewItemArray& items) override
    { return m_ctrl->ItemsChanged(items); }
    virtual bool Cleared() override
    { return m_ctrl->Cleared(); }
    virtual void Resort() override
    { m_ctrl->Resort(); }
    virtual bool ValueChanged(const wxDataViewItem& item, unsigned int col) override
    { return m_ctrl->ValueChanged(item, col); }

private:
    IModelNotifier* m_ctrl;
};

} // namespace pm

#endif // PM_MODEL_NOTIFIER_H

/******************************************************************************/
/* Copyright (C) Teledyne Photometrics. All rights reserved.                  */
/******************************************************************************/
#pragma once
#ifndef PM_REGION_MODEL_H
#define PM_REGION_MODEL_H

// WxWidgets headers have to be included first
#include <wx/dataview.h>

/* Local */
#include "Region.h"

/* System */
#include <set>
#include <memory>
#include <mutex>
#include <utility>
#include <vector>

namespace pm {

class RegionModel : public wxDataViewModel
{
public:
    RegionModel();
    virtual ~RegionModel();

public: // Use these helpers instead of base class methods
    bool AppendRegion(std::shared_ptr<Region> region);
    bool DeleteRegion(std::shared_ptr<Region> region);
    bool AppendRegions(const std::vector<std::shared_ptr<Region>>& regions);
    void DeleteAllRegions();

    void RegionChanged(std::shared_ptr<Region> region);
    void RegionValueChanged(std::shared_ptr<Region> region, unsigned int col);

    std::shared_ptr<Region> GetRegion(const wxDataViewItem& item) const;
    wxDataViewItem GetItem(std::shared_ptr<Region> region) const;

    void SetMaxRegionCount(int count); // -1 to set no limit
    int GetMaxRegionCount() const; // -1 if no limit set

    unsigned int GetRegionCount() const;
    std::shared_ptr<Region> GetRegion(unsigned int index) const;

    std::vector<std::shared_ptr<Region>> GetRegions() const;
    std::set<std::pair<std::shared_ptr<Region>, std::shared_ptr<Region>>>
        GetOverlappingRegions() const;

public: // wxDataViewModel
    virtual unsigned int GetColumnCount() const override;
    virtual wxString GetColumnType(unsigned int col) const;

    virtual void GetValue(wxVariant& value, const wxDataViewItem& item,
            unsigned int col) const;
    virtual bool SetValue(const wxVariant& value, const wxDataViewItem& item,
            unsigned int col);

    virtual bool GetAttr(const wxDataViewItem& item, unsigned int col,
            wxDataViewItemAttr& attr) const;
    virtual bool IsEnabled(const wxDataViewItem& item, unsigned int col) const;

    virtual wxDataViewItem GetParent(const wxDataViewItem& item) const;
    virtual bool IsContainer(const wxDataViewItem& item) const;
    virtual unsigned int GetChildren(const wxDataViewItem& item,
            wxDataViewItemArray& children) const;

    virtual bool IsListModel() const
    { return true; }

private:
    std::shared_ptr<Region> DoGetRegion(const wxDataViewItem& item) const;
    wxDataViewItem DoGetItem(std::shared_ptr<Region> region) const;

private:
    std::vector<std::shared_ptr<Region>> m_regions{};
    mutable std::mutex m_regionsMutex{};
    int m_maxRegionCount{ -1 };
};

} // namespace pm

#endif // PM_REGION_MODEL_H

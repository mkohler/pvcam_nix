/******************************************************************************/
/* Copyright (C) Teledyne Photometrics. All rights reserved.                  */
/******************************************************************************/
#pragma once
#ifndef PM_MAINDLG_H
#define PM_MAINDLG_H

// WxWidgets headers have to be included first
#include <wx/event.h>
#include <wx/thread.h>
#include <wx/timer.h>
#include <wx/textentry.h>
#include <wx/valnum.h>
#include <wx/window.h>
#include <wx/windowptr.h>
#include "RegionListCtrl.h"
#include "PVCamTest_Ui.h"

/* Local */
#include "backend/Acquisition.h"
#include "backend/Camera.h"
#include "backend/ConsoleLogger.h"
#include "backend/FpsLimiter.h"
#include "backend/Log.h"
#include "backend/Settings.h"
#include "ImageDlg.h"
#include "ParamBrowserDlg.h"
#include "RegionModel.h"
//#include "RegionListCtrl.h" // Must be included before PVCamTest_Ui.h
#include "WhiteBalanceWizard.h"

/* pvcam_helper_color */
#include "pvcam_helper_color.h"

/* System */
#include <map>
#include <mutex>
#include <utility>
#include <vector>

// Holds no data
// Uses task ID to distinguish among tasks
wxDECLARE_EVENT(PM_EVT_START_TASK, wxThreadEvent);
// Holds no data
// Uses task ID to distinguish among tasks
wxDECLARE_EVENT(PM_EVT_TASK_DONE, wxThreadEvent);
// Holds string message as data
wxDECLARE_EVENT(PM_EVT_SHOW_HELP, wxCommandEvent);
// Holds no data
wxDECLARE_EVENT(PM_EVT_ACQ_STARTED, wxCommandEvent);
// Holds no data
wxDECLARE_EVENT(PM_EVT_ACQ_FINISHED, wxCommandEvent);

// Taken from pvcam.h
struct _TAG_FRAME_INFO;
typedef struct _TAG_FRAME_INFO FRAME_INFO;

namespace pm {

enum class TaskType : int
{
    FIND_CAMERA,
    SETUP_ACQUISITION,
    RUN_ACQUISITION,
    AUTO_EXP_AND_WHITE_BALANCE,
};

class MainDlg final : public ui::MainDlg, public wxThreadHelper,
        private IFpsLimiterListener, private Log::IListener,
        private IModelNotifier
{
public:
    MainDlg();
    virtual ~MainDlg();

public:
    void ActivateWindow(bool active = true);

    // Shows pop-up with help text if some is set
    void ShowHelp();

    void UpdateProgress(int min, int max, int current);

private: // wxThreadHelper
    virtual wxThread::ExitCode Entry() override;
    //virtual void OnDelete() override; // Is in doc, but does not exist, why?

private: // UI event handlers
    void OnClose(wxCloseEvent& event);
    void OnIconize(wxIconizeEvent& event);

    void OnPortSpeedChanged(wxCommandEvent& event);
    void OnGainChanged(wxCommandEvent& event);
    void OnEmGainChanged(wxSpinEvent& event);
    void OnExposuresChanged(wxCommandEvent& event);
    void OnExposuresFocusLost(wxFocusEvent& event);
    void OnExpTimeResChanged(wxCommandEvent& event);
    void OnTimeLapseDelayChanged(wxSpinEvent& event);
    void OnTriggerModeChanged(wxCommandEvent& event);
    void OnExpOutModeChanged(wxCommandEvent& event);
    void OnClearModeChanged(wxCommandEvent& event);
    void OnClearCyclesChanged(wxSpinEvent& event);
    void OnPModeChanged(wxCommandEvent& event);

    void OnCircBufferFrameCountChanged(wxSpinEvent& event);
    void OnCircBufferFrameChanged(wxScrollEvent& event);

    void OnSaveAsChanged(wxCommandEvent& event);
    void OnSaveDirClicked(wxCommandEvent& event);
    void OnSaveDirChanged(wxCommandEvent& event);
    void OnSaveDigitsChanged(wxSpinEvent& event);
    void OnSaveFirstClicked(wxCommandEvent& event);
    void OnSaveFirstChanged(wxSpinEvent& event);
    void OnSaveLastClicked(wxCommandEvent& event);
    void OnSaveLastChanged(wxSpinEvent& event);
    void OnSaveStackClicked(wxCommandEvent& event);
    void OnSaveStackChanged(wxSpinEvent& event);

    void OnAcqModeChanged(wxCommandEvent& event);
    void OnSnapFrameCountChanged(wxSpinEvent& event);
    void OnStartClicked(wxCommandEvent& event);
    void OnStopClicked(wxCommandEvent& event);

    void OnInfosPageChanged(wxBookCtrlEvent& event);

    void OnRoiAddClicked(wxCommandEvent& event);
    void OnRoiRemoveClicked(wxCommandEvent& event);
    void OnRoiBinChanged(wxCommandEvent& event);
    void OnRoiBinSerChanged(wxSpinEvent& event);
    void OnRoiBinParChanged(wxSpinEvent& event);
    void OnRoiTableSelectionChanged(wxDataViewEvent& event);
    void OnRoiTableItemContextMenu(wxDataViewEvent& event);
    void OnRoiMultiWindowClicked(wxCommandEvent& event);

    void OnCentroidsEnabledClicked(wxCommandEvent& event);
    void OnCentroidsModeChanged(wxCommandEvent& event);
    void OnCentroidsCountChanged(wxSpinEvent& event);
    void OnCentroidsRadiusChanged(wxSpinEvent& event);
    void OnCentroidsBgCountChanged(wxCommandEvent& event);
    void OnCentroidsThresholdChanged(wxSpinDoubleEvent& event);

    void OnTrackLinkFramesChanged(wxSpinEvent& event);
    void OnTrackMaxDistChanged(wxSpinEvent& event);
    void OnTrackCpuOnlyClicked(wxCommandEvent& event);
    void OnTrackTrajectoryShowClicked(wxCommandEvent& event);
    void OnTrackTrajectoryDurationChanged(wxSpinEvent& event);
    void OnMetaRoiOutlineClicked(wxCommandEvent& event);

    void OnOpenParameterBrowserClicked(wxCommandEvent& event);
    void OnFillMethodChanged(wxCommandEvent& event);
    void OnAutoConbrightClicked(wxCommandEvent& event);
    void OnContrastSliderChanged(wxCommandEvent& event);
    void OnContrastSpinChanged(wxSpinEvent& event);
    void OnBrightnessSliderChanged(wxCommandEvent& event);
    void OnBrightnessSpinChanged(wxSpinEvent& event);
    void OnZoomFactorChanged(wxCommandEvent& event);
    void OnRoiOutlineClicked(wxCommandEvent& event);
    void OnShowLiveImageClicked(wxCommandEvent& event);

    void OnColorEnableClicked(wxCommandEvent& event);
    void OnOverrideColorMaskClicked(wxCommandEvent& event);
    void OnCustomColorMaskChanged(wxCommandEvent& event);
    void OnWbAutoClicked(wxCommandEvent& event);
    void OnWbDefaultClicked(wxCommandEvent& event);
    void OnWbSliderRedChanged(wxCommandEvent& event);
    void OnWbSpinRedChanged(wxSpinDoubleEvent& event);
    void OnWbSliderGreenChanged(wxCommandEvent& event);
    void OnWbSpinGreenChanged(wxSpinDoubleEvent& event);
    void OnWbSliderBlueChanged(wxCommandEvent& event);
    void OnWbSpinBlueChanged(wxSpinDoubleEvent& event);
    void OnDebayerAlgorithmChanged(wxCommandEvent& event);
    void OnAutoExposureAlgorithmChanged(wxCommandEvent& event);
    void OnColorCpuOnlyClicked(wxCommandEvent& event);

private: // Param class on-change handlers
    void OnParamPortSpeedChanged(ParamBase& param, bool allAttrsChanged);
    void OnParamGainIndexChanged(ParamBase& param, bool allAttrsChanged);
    void OnParamColorModeChanged(ParamBase& param, bool allAttrsChanged);
    void OnParamBitDepthChanged(ParamBase& param, bool allAttrsChanged);
    void OnParamEmGainChanged(ParamBase& param, bool allAttrsChanged);
    void OnParamExpResIndexChanged(ParamBase& param, bool allAttrsChanged);
    void OnParamExposureModeChanged(ParamBase& param, bool allAttrsChanged);
    void OnParamExposeOutModeChanged(ParamBase& param, bool allAttrsChanged);
    void OnParamClearModeChanged(ParamBase& param, bool allAttrsChanged);
    void OnParamClearCyclesChanged(ParamBase& param, bool allAttrsChanged);
    void OnParamPModeChanged(ParamBase& param, bool allAttrsChanged);
    void OnParamSmartStreamModeEnabledChanged(ParamBase& param, bool allAttrsChanged);
    void OnParamSmartStreamExpParamsChanged(ParamBase& param, bool allAttrsChanged);
    void OnParamCentroidsEnabledChanged(ParamBase& param, bool allAttrsChanged);
    void OnParamCentroidsModeChanged(ParamBase& param, bool allAttrsChanged);
    void OnParamCentroidsCountChanged(ParamBase& param, bool allAttrsChanged);
    void OnParamCentroidsRadiusChanged(ParamBase& param, bool allAttrsChanged);
    void OnParamCentroidsBgCountChanged(ParamBase& param, bool allAttrsChanged);
    void OnParamCentroidsThresholdChanged(ParamBase& param, bool allAttrsChanged);

private: // Custom event handlers
    void OnUiUpdateTimerTimeout(wxTimerEvent& event);

    void OnEventStartTask(wxThreadEvent& event);
    void OnEventTaskDone(wxThreadEvent& event);
    void OnEventShowHelp(wxCommandEvent& event);
    void OnEventAcqStarted(wxCommandEvent& event);
    void OnEventAcqFinished(wxCommandEvent& event);

    void OnFindCameraTaskDone(wxThreadEvent& event);
    bool OnFindCameraTaskDone_InitData();
    bool OnFindCameraTaskDone_InitGui();
    void OnSetupAcquisitionTaskDone(wxThreadEvent& event);
    void OnRunAcquisitionTaskDone(wxThreadEvent& event);
    void OnAutoExpAndWhiteBalanceTaskDone(wxThreadEvent& event);

private:
    void StartTask(TaskType taskType);
    void StopTask();
    wxThread::ExitCode FindCameraTaskEntry();
    wxThread::ExitCode SetupAcquisitionTaskEntry();
    wxThread::ExitCode RunAcquisitionTaskEntry();
    wxThread::ExitCode AutoExpAndWhiteBalanceTaskEntry();

    // Logs error and returns false if given condition is false
    bool IsError(bool cond, const std::string& message = std::string());

    // Starts new acquisition
    void StartAcq();
    // Requests current acquisition to stop
    void StopAcq(bool abortBufferedFramesProcessing = true);

    // Prepares help text
    void SetHelpText(const std::vector<Option>& options);

    // Add all CLI options
    bool AddCliOptions();
    // Help option handler
    bool HandleHelpOption(const std::string& value);
    // Centroids outline checkbox option handler
    bool HandleMetaRoiShowOutlineOption(const std::string& value);
    // Region multi-window checkbox option handler
    bool HandleRoiMultiWindowOption(const std::string& value);
    // Region outline checkbox option handler
    bool HandleRoiShowOutlineOption(const std::string& value);
    // Image auto-conbright checkbox option handler
    bool HandleImageAutoConbrightOption(const std::string& value);
    // Image manual contrast editbox option handler
    bool HandleImageContrastOption(const std::string& value);
    // Image manual brightness editbox option handler
    bool HandleImageBrightnessOption(const std::string& value);

    void DoUpdateProgress(int min, int max, int current);
    void DoUpdateFrameSlider(int index, int frameNr);
    void DoUpdateFrameRate(double acqFps);
    void DoUpdateAcqStats(size_t valid, size_t lost, size_t max, size_t cached);
    void DoUpdateDiskStats(size_t valid, size_t lost, size_t max, size_t cached);
    void DoUpdateFrameDesc(std::shared_ptr<pm::Frame> frame);
    void DoUpdateCentroidModel();
    void DoUpdateColorCapable();
    void DoUpdateRoiOutlines(bool visible);

    void UpdateAcqModeTooltip();

private: // IFpsLimiterListener
    virtual void OnFpsLimiterEvent(FpsLimiter* sender,
            std::shared_ptr<pm::Frame> frame) override;
    virtual void OnFpsLimiterAcqFinished(FpsLimiter* sender) override;

private: // Log::IListener
    virtual void OnLogEntryAdded(const Log::Entry& entry) override;

private: // UI handling
    // Enable/disable all controls in the dialog, Stop button is always disabled
    void UiSetAllControlsEnabled(bool enabled);
    // Enable/disable ROI buttons in the dialog
    void UiSetRoiButtonsEnabled(bool enabled);
    // Enable/disable color controls in the dialog
    void UiSetColorControlsEnabled(bool enabled);
    // Enable/disable centroid controls in the dialog
    void UiSetCentroidControlsEnabled(bool enabled);
    // Update image windows to turn color processing on and off
    void UiUpdateColorEnabled();
    // Update image windows e.g. to new white balance scale factors
    void UiUpdateColorContext();
    // Update value on progress bar and percent counter
    void UiUpdateProgressValue(bool isInf, int range, int value);
    // Update value on circ. buffer slider
    void UiUpdateFrameSliderValue(int index, int frameNr);
    // Update single, or VTM or SS exposure(s)
    void UiUpdateExposures();
    // Update single exposure
    void UiUpdateExposure();
    // Update VTM exposures
    void UiUpdateVtmExposures();
    // Update smart streaming exposures
    void UiUpdateSmartExposures();
    // Update frame rate value(s)
    void UiUpdateFrameRate(double acqFps);
    // Update acquisition cache usage
    void UiUpdateAcqStats(size_t valid, size_t lost, size_t max, size_t cached);
    // Update disk cache usage
    void UiUpdateDiskStats(size_t valid, size_t lost, size_t max, size_t cached);
    // Update description on frame change
    void UiUpdateFrameDescText(const std::string& text);

private:
    static float ConvertWbSliderValueToScale(int value);
    static int ConvertWbScaleToSliderValue(float value);

private:
    static void PV_DECL CameraRemovalCallback(FRAME_INFO* frameInfo,
            void* MainDlg_pointer);

private: // IModelNotifier
    // Set friendship so overridden methods can be private
    friend class ModelNotifier;
    virtual bool ItemAdded(const wxDataViewItem& parent,
            const wxDataViewItem& item) override;
    virtual bool ItemDeleted(const wxDataViewItem& parent,
            const wxDataViewItem& item) override;
    virtual bool ItemChanged(const wxDataViewItem& item) override;
    virtual bool ItemsAdded(const wxDataViewItem& parent,
            const wxDataViewItemArray& items) override;
    virtual bool ItemsDeleted(const wxDataViewItem& parent,
            const wxDataViewItemArray& items) override;
    virtual bool ItemsChanged(const wxDataViewItemArray& items) override;
    virtual bool Cleared() override;
    virtual void Resort() override;
    virtual bool ValueChanged(const wxDataViewItem& item, unsigned int col)
        override;

private:
    bool m_hasConsole{ false };
    std::shared_ptr<ConsoleLogger> m_consoleLogger{ nullptr };
    // All kind of settings that can be modified via CLI options
    Settings m_settings{};
    OptionController m_optionController{};
    Option m_helpOption; // Inited in c-tor
    bool m_showFullHelp{ false };
    bool m_showHelpNow{ false };
    std::string m_helpText{};
    // Camera instance
    std::shared_ptr<Camera> m_camera{ nullptr };
    std::unique_ptr<Acquisition> m_acquisition{ nullptr };
    std::shared_ptr<FpsLimiter> m_fpsLimiter{ nullptr };

    // Needed to hold persistent pointers stored in combo box items
    std::vector<std::string> m_cameraNames{};
    std::vector<ParamEnumItem> m_clearModes{};
    std::vector<ParamEnumItem> m_pModes{};
    std::vector<ParamEnumItem> m_expTimeResolutions{};
    std::map<ParamEnum::T, std::pair<uint32_t, uint32_t>> m_expTimeResLimits{}; // [expTimeRes]=min,max
    std::vector<ParamEnumItem> m_triggerModes{};
    std::vector<ParamEnumItem> m_expOutModes{};
    std::vector<ParamEnumItem> m_centroidsModes{};
    std::vector<ParamEnumItem> m_centroidsBgCounts{};
    std::vector<ParamEnumItem> m_acqModes{};
    std::vector<std::pair<ParamEnumItem, ParamEnumItem>> m_binModes{}; // ser x par
    std::vector<ParamEnumItem> m_fillMethods{};
    std::vector<ParamEnumItem> m_zoomFactors{};
    std::vector<ParamEnumItem> m_customColorMasks{};
    std::vector<ParamEnumItem> m_debayerAlgorithms{};
    std::vector<ParamEnumItem> m_autoExposureAlgorithms{};

    // Window displaying captured image
    std::shared_ptr<ImageDlg> m_imageDlg{ nullptr }; // Init after listening to Log
    std::vector<std::shared_ptr<ImageDlg>> m_roiImageDlgs{};

    wxWindowPtr<ParamBrowserDlg> m_paramDlg{ nullptr };

    TaskType m_taskType{ TaskType::FIND_CAMERA };

    // Flag saying if camera has been removed
    std::atomic<bool> m_cameraRemovedFlag{ false };

    // Flag saying if user wants to abort current operation
    std::atomic<bool> m_userAbortFlag{ false };

    // UI control value cache that is not stored in Settings
    int m_snapFrameCount{ 0 };
    ImageDlg::FillMethod m_fillMethod{ ImageDlg::FillMethod::NoFill }; // Can be changed during acquisition
    bool m_imageAutoConbright{ true }; // Can be changed during acquisition
    int m_imageContrast{ 0 }; // Can be changed during acquisition
    int m_imageBrightness{ 0 }; // Can be changed during acquisition
    ImageDlg::ZoomFactor m_zoomFactor{ ImageDlg::ZoomFactor::None_100 }; // Can be changed during acquisition
    ParamEnumItem::T m_colorMask{ COLOR_NONE };
    bool m_colorCapable{ false };
    bool m_colorEnabled{ false }; // Can be changed during acquisition
    bool m_colorMaskOverridden{ false }; // Can be changed during acquisition
    ParamEnumItem::T m_customColorMask{ COLOR_RGGB }; // Can be changed during acquisition
    PH_COLOR_AUTOEXP_ALG m_autoExposureAlgorithm{ PH_COLOR_AUTOEXP_ALG_AVERAGE };
    bool m_trackShowTrajectory{ true }; // Can be changed during acquisition
    bool m_metaRoiShowOutline{ true }; // Can be changed during acquisition
    bool m_roiMultiWindow{ false };
    bool m_roiShowOutline{ true }; // Can be changed during acquisition
    bool m_showLiveImage{ true }; // Can be changed during acquisition
    std::vector<uint32_t> m_smartExposures{}; // Empty if n/a or disabled
    uint16_t m_smartExposuresMax{ 1 };

    wxRect m_camFullRect{};

    wxObjectDataPtr<RegionModel> m_regionModel{ nullptr }; // Init after listening to Log
    wxObjectDataPtr<RegionModel> m_centroidModel{ nullptr }; // Init after listening to Log

    ModelNotifier* m_regionModelNotifier{ nullptr };

    bool m_isAcqConfigured{ false };

    // UI cached values for periodic update
    std::mutex m_uiUpdateMutex{};
    wxTimer m_uiUpdateTimer{ this };
    std::queue<Log::Entry> m_uiLogEntries{};
    bool m_uiProgressUpdated{ false };
    bool m_uiProgressIsInf{ false };
    int m_uiProgressRange{ 10 };
    int m_uiProgressValue{ 5 };
    bool m_uiFrameSliderUpdated{ false };
    int m_uiFrameSliderIndex{ 0 };
    int m_uiFrameSliderFrameNr{ 0 };
    bool m_uiFrameRateUpdated{ false };
    double m_uiFrameRateAcq{ 0.0 };
    bool m_uiAcqStatsUpdated;
    size_t m_uiAcqStatsValid{ 0 };
    size_t m_uiAcqStatsLost{ 0 };
    size_t m_uiAcqStatsMax{ 0 };
    size_t m_uiAcqStatsCached{ 0 };
    bool m_uiDiskStatsUpdated{ false };
    size_t m_uiDiskStatsValid{ 0 };
    size_t m_uiDiskStatsLost{ 0 };
    size_t m_uiDiskStatsMax{ 0 };
    size_t m_uiDiskStatsCached{ 0 };
    bool m_uiFrameDescUpdated{ false };
    bool m_uiFrameDescVisible{ false };
    std::string m_uiFrameDescText{};

    ph_color_context* m_colorContext{ nullptr }; // Cannot be smart pointer
    wxWindowPtr<WhiteBalanceWizard> m_wbWizard{ nullptr };
};

} // namespace pm

#endif // PM_MAINDLG_H

/******************************************************************************/
/* Copyright (C) Teledyne Photometrics. All rights reserved.                  */
/******************************************************************************/

// WxWidgets headers have to be included first
#include <wx/app.h>

/* Local */
#include "PVCamTestApp.h"

wxIMPLEMENT_APP(pm::PVCamTestApp);

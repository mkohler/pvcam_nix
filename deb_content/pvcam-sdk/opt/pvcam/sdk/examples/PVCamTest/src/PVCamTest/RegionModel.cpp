/******************************************************************************/
/* Copyright (C) Teledyne Photometrics. All rights reserved.                  */
/******************************************************************************/
#include "RegionModel.h"

// WxWidgets headers have to be included first
#include <wx/log.h>

/* System */
#include <algorithm>
#include <cstdint>
#include <limits>

pm::RegionModel::RegionModel()
    : wxDataViewModel()
{
}

pm::RegionModel::~RegionModel()
{
    DeleteAllRegions();
}

bool pm::RegionModel::AppendRegion(std::shared_ptr<Region> region)
{
    if (!region)
        return false;

    {
        std::lock_guard<std::mutex> lock(m_regionsMutex);

        if (DoGetItem(region).IsOk())
            return false;

        if (m_regions.size() >= (std::numeric_limits<uint16_t>::max)())
            return false;

        if (m_maxRegionCount > -1 && m_maxRegionCount <= (int)m_regions.size())
            return false;

        m_regions.push_back(region);
    }

    ItemAdded(wxDataViewItem(), wxDataViewItem(region.get()));

    return true;
}

bool pm::RegionModel::DeleteRegion(std::shared_ptr<Region> region)
{
    if (!region)
        return false;

    std::vector<std::shared_ptr<Region>> changedRegions;
    {
        std::lock_guard<std::mutex> lock(m_regionsMutex);

        bool found = false;
        size_t row;
        for (row = 0; row < m_regions.size(); ++row)
        {
            if (m_regions.at(row) == region)
            {
                found = true;
                break;
            }
        }
        if (!found)
            return false;

        m_regions.erase(m_regions.begin() + row);

        for (size_t n = row; n < m_regions.size(); ++n)
        {
            changedRegions.push_back(m_regions.at(n));
        }
    }

    ItemDeleted(wxDataViewItem(), wxDataViewItem(region.get()));

    // Update Nr for all regions below removed
    for (auto changedRegion : changedRegions)
    {
        ValueChanged(wxDataViewItem(changedRegion.get()), 0);
    }

    return true;
}

bool pm::RegionModel::AppendRegions(
        const std::vector<std::shared_ptr<Region>>& regions)
{
    if (regions.empty())
        return false;

    wxDataViewItemArray items;
    {
        std::lock_guard<std::mutex> lock(m_regionsMutex);

        const size_t newSize = m_regions.size() + regions.size();

        if (newSize >= (std::numeric_limits<uint16_t>::max)())
            return false;

        if (m_maxRegionCount > -1 && m_maxRegionCount < (int)newSize)
            return false;

        // Do checks in first round
        for (auto region : regions)
        {
            if (!region)
                return false;

            if (DoGetItem(region).IsOk())
                return false;
        }

        // Add all regions when no error so far
        for (auto region : regions)
        {
            m_regions.push_back(region);

            items.Add(wxDataViewItem(region.get()));
        }
    }

    ItemsAdded(wxDataViewItem(), items);

    return true;
}

void pm::RegionModel::DeleteAllRegions()
{
    std::vector<std::shared_ptr<Region>> removedRegions;
    {
        std::lock_guard<std::mutex> lock(m_regionsMutex);

        removedRegions = m_regions;
        m_regions.clear();
    }

    wxDataViewItemArray items;
    for (auto removedRegion : removedRegions)
    {
        items.Add(wxDataViewItem(removedRegion.get()));
    }
    ItemsDeleted(wxDataViewItem(), items);
}

void pm::RegionModel::RegionChanged(std::shared_ptr<Region> region)
{
    ItemChanged(GetItem(region));
}

void pm::RegionModel::RegionValueChanged(std::shared_ptr<Region> region,
        unsigned int col)
{
    ValueChanged(GetItem(region), col);
}

std::shared_ptr<pm::Region>
    pm::RegionModel::GetRegion(const wxDataViewItem& item) const
{
    std::lock_guard<std::mutex> lock(m_regionsMutex);

    return DoGetRegion(item);
}

wxDataViewItem pm::RegionModel::GetItem(std::shared_ptr<Region> region) const
{
    if (!region)
        return wxDataViewItem();

    {
        std::lock_guard<std::mutex> lock(m_regionsMutex);

        return DoGetItem(region);
    }
}

void pm::RegionModel::SetMaxRegionCount(int count)
{
    m_maxRegionCount = count;
}

int pm::RegionModel::GetMaxRegionCount() const
{
    return m_maxRegionCount;
}

unsigned int pm::RegionModel::GetRegionCount() const
{
    std::lock_guard<std::mutex> lock(m_regionsMutex);

    return (unsigned int)m_regions.size();
}

std::shared_ptr<pm::Region> pm::RegionModel::GetRegion(unsigned int index) const
{
    std::lock_guard<std::mutex> lock(m_regionsMutex);

    if (index >= m_regions.size())
        return nullptr;

    return m_regions.at(index);
}

std::vector<std::shared_ptr<pm::Region>> pm::RegionModel::GetRegions() const
{
    std::lock_guard<std::mutex> lock(m_regionsMutex);

    return m_regions;
}

std::set<std::pair<std::shared_ptr<pm::Region>, std::shared_ptr<pm::Region>>>
    pm::RegionModel::GetOverlappingRegions() const
{
    std::lock_guard<std::mutex> lock(m_regionsMutex);

    std::set<std::pair<std::shared_ptr<Region>, std::shared_ptr<Region>>> regions;

    const size_t count = m_regions.size();
    if (count > 1)
    {
        for (size_t A = 0; A < count - 1; ++A)
        {
            for (size_t B = A + 1; B < count; ++B)
            {
                const wxRect rectA = m_regions[A]->GetClientRect();
                const wxRect rectB = m_regions[B]->GetClientRect();
                if (rectA.Intersects(rectB))
                {
                    regions.emplace(m_regions[A], m_regions[B]);
                }
            }
        }
    }

    return regions;
}

unsigned int pm::RegionModel::GetColumnCount() const
{
    return 5; // Nr, Left, Top, Width, Height
}

wxString pm::RegionModel::GetColumnType(unsigned int WXUNUSED(col)) const
{
    return "long";
}

void pm::RegionModel::GetValue(wxVariant& value, const wxDataViewItem& item,
        unsigned int col) const
{
    std::lock_guard<std::mutex> lock(m_regionsMutex);

    value = wxVariant();

    const std::shared_ptr<Region> region = DoGetRegion(item);

    if (!region)
        return;

    switch (col)
    {
    case 0: // Nr
        for (size_t n = 0; n < m_regions.size(); ++n)
        {
            if (m_regions.at(n) == region)
            {
                value = (long)(n + 1);
                break;
            }
        }
        break;
    case 1: // Left
        value = (long)region->GetClientRect().GetX();
        break;
    case 2: // Top
        value = (long)region->GetClientRect().GetY();
        break;
    case 3: // Width
        value = (long)region->GetClientRect().GetWidth();
        break;
    case 4: // Height
        value = (long)region->GetClientRect().GetHeight();
        break;
    default:
        return;
    }
}

bool pm::RegionModel::SetValue(const wxVariant& value,
        const wxDataViewItem& item, unsigned int col)
{
    std::lock_guard<std::mutex> lock(m_regionsMutex);

    if (value.IsNull() || !value.IsType("long"))
        return false;

    std::shared_ptr<Region> region = DoGetRegion(item);

    if (!region)
        return false;

    int X = region->GetClientRect().GetX();
    int Y = region->GetClientRect().GetY();
    int W = region->GetClientRect().GetWidth();
    int H = region->GetClientRect().GetHeight();

    switch (col)
    {
    case 1: // Left
        X = (int)value.GetLong();
        break;
    case 2: // Top
        Y = (int)value.GetLong();
        break;
    case 3: // Width
        W = (int)value.GetLong();
        break;
    case 4: // Height
        H = (int)value.GetLong();
        break;
    case 0: // Nr (is read-only)
    default:
        return false;
    }

    region->SetClientRect(wxRect(X, Y, W, H));

    return true;
}

bool pm::RegionModel::GetAttr(const wxDataViewItem& WXUNUSED(item),
        unsigned int WXUNUSED(col), wxDataViewItemAttr& WXUNUSED(attr)) const
{
    // TODO: Set background color to RED for overlapping regions
    return false;
}

bool pm::RegionModel::IsEnabled(const wxDataViewItem& WXUNUSED(item),
        unsigned int col) const
{
    return (col != 0); // Nr column is read-only
}

wxDataViewItem pm::RegionModel::GetParent(const wxDataViewItem& WXUNUSED(item))
    const
{
    return wxDataViewItem(); // Items never have valid parent in this model
}

bool pm::RegionModel::IsContainer(const wxDataViewItem& item) const
{
    return !item.IsOk(); // Only root item has children in this model
}

unsigned int pm::RegionModel::GetChildren(const wxDataViewItem& item,
        wxDataViewItemArray& children) const
{
    children.clear();

    if (item.IsOk())
        return 0; // Only root item has children in this model

    {
        std::lock_guard<std::mutex> lock(m_regionsMutex);

        for (auto region : m_regions)
        {
            children.push_back(DoGetItem(region));
        }
    }

    return (unsigned int)children.size();
}

std::shared_ptr<pm::Region>
    pm::RegionModel::DoGetRegion(const wxDataViewItem& item) const
{
    Region* regionRaw = (Region*)item.GetID();

    if (!regionRaw)
        return nullptr;

    auto regionIt = std::find_if(m_regions.cbegin(), m_regions.cend(),
            [&](const std::shared_ptr<Region>& region) {
                return region.get() == regionRaw;
            });

    if (regionIt == m_regions.cend())
        return nullptr;

    return *regionIt;
}

wxDataViewItem pm::RegionModel::DoGetItem(std::shared_ptr<Region> region) const
{
    if (!region)
        return wxDataViewItem();

    if (std::find(m_regions.cbegin(), m_regions.cend(), region) == m_regions.cend())
        return wxDataViewItem();

    return wxDataViewItem(region.get());
}

/******************************************************************************/
/* Copyright (C) Teledyne Photometrics. All rights reserved.                  */
/******************************************************************************/
#include "Region.h"

pm::Region::Region()
//    : m_clientRect(0, 0, 50, 50)
{
}

pm::Region::Region(const wxRect& clientRect)
    : m_clientRect(clientRect)
{
}

void pm::Region::SetClientRect(const wxRect& clientRect)
{
    m_clientRect = clientRect;
}

void pm::Region::SetVisible(bool visible)
{
    m_isVisible = visible;
}

void pm::Region::SetSelected(bool selected)
{
    m_isSelected = selected;
}

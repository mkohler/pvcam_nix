/******************************************************************************/
/* Copyright (C) Teledyne Photometrics. All rights reserved.                  */
/******************************************************************************/
#include "backend/File.h"

pm::File::File(const std::string& fileName)
    : m_fileName(fileName)
{
}

pm::File::~File()
{
}

const std::string& pm::File::GetFileName() const
{
    return m_fileName;
}

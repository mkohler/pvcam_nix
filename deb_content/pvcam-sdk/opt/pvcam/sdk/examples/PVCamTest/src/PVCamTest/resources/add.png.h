#ifndef ADD_PNG_H
#define ADD_PNG_H

#include <wx/mstream.h>
#include <wx/image.h>
#include <wx/bitmap.h>

static const unsigned char add_png[] = 
{
	0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A, 0x00, 0x00, 
	0x00, 0x0D, 0x49, 0x48, 0x44, 0x52, 0x00, 0x00, 0x00, 0x10, 
	0x00, 0x00, 0x00, 0x10, 0x08, 0x06, 0x00, 0x00, 0x00, 0x1F, 
	0xF3, 0xFF, 0x61, 0x00, 0x00, 0x00, 0x04, 0x67, 0x41, 0x4D, 
	0x41, 0x00, 0x00, 0xAF, 0xC8, 0x37, 0x05, 0x8A, 0xE9, 0x00, 
	0x00, 0x00, 0x19, 0x74, 0x45, 0x58, 0x74, 0x53, 0x6F, 0x66, 
	0x74, 0x77, 0x61, 0x72, 0x65, 0x00, 0x41, 0x64, 0x6F, 0x62, 
	0x65, 0x20, 0x49, 0x6D, 0x61, 0x67, 0x65, 0x52, 0x65, 0x61, 
	0x64, 0x79, 0x71, 0xC9, 0x65, 0x3C, 0x00, 0x00, 0x02, 0x6F, 
	0x49, 0x44, 0x41, 0x54, 0x38, 0xCB, 0xA5, 0x93, 0xEB, 0x4B, 
	0x93, 0x61, 0x18, 0x87, 0xFD, 0x5B, 0xB6, 0x2F, 0x05, 0x83, 
	0xA4, 0x86, 0x59, 0x28, 0xA8, 0x29, 0x25, 0x58, 0x28, 0x6F, 
	0xD9, 0x6C, 0x8A, 0x8A, 0x4E, 0xDB, 0x96, 0x73, 0x6B, 0x8A, 
	0xCE, 0xF6, 0x6E, 0x2E, 0x9D, 0xBA, 0xCD, 0x03, 0x2D, 0x9D, 
	0x13, 0x0F, 0x15, 0xB5, 0xA1, 0xDB, 0x68, 0xA4, 0x3B, 0x38, 
	0x0F, 0xC3, 0x03, 0x66, 0xE2, 0xD4, 0xD2, 0x45, 0xEA, 0xEB, 
	0x50, 0xA2, 0x8D, 0x22, 0x08, 0x6A, 0xC3, 0xAF, 0xBF, 0xDE, 
	0x15, 0x4D, 0x47, 0xCB, 0x88, 0x1E, 0xF8, 0x7D, 0x79, 0xE0, 
	0xBA, 0x9E, 0xE7, 0xBE, 0xB9, 0xEF, 0x24, 0x00, 0x49, 0xFF, 
	0x93, 0xDF, 0x2E, 0x74, 0xD3, 0x75, 0xCC, 0x0E, 0x8F, 0x84, 
	0x6C, 0x75, 0x89, 0xA8, 0xE6, 0x09, 0x41, 0x58, 0xFE, 0xA2, 
	0x3A, 0xDC, 0xF0, 0xBC, 0x82, 0x92, 0x5A, 0xCB, 0x48, 0xD1, 
	0x68, 0x31, 0xF3, 0x44, 0x81, 0x6E, 0x5A, 0x4A, 0xB4, 0x4F, 
	0x4A, 0x42, 0xCF, 0xD6, 0x7B, 0xB1, 0x1A, 0x5A, 0x84, 0xFF, 
	0xF3, 0x06, 0xD6, 0x3F, 0xAD, 0x60, 0x32, 0x60, 0x87, 0xD2, 
	0x53, 0x03, 0xBE, 0x89, 0x13, 0xE2, 0x3D, 0xB9, 0x4E, 0x24, 
	0x14, 0xFC, 0x84, 0xC5, 0x91, 0xE9, 0x3D, 0x3B, 0x0E, 0xBE, 
	0xED, 0x61, 0xF6, 0x83, 0x0B, 0x26, 0x6A, 0x10, 0x8F, 0x77, 
	0x0C, 0xB0, 0xEF, 0x9B, 0xB0, 0xF4, 0x71, 0x0E, 0xDA, 0x05, 
	0x19, 0x4A, 0x47, 0xF2, 0x23, 0xDC, 0xC1, 0x3C, 0x22, 0x4E, 
	0xA0, 0x9D, 0x92, 0x32, 0x68, 0x38, 0xE8, 0xDE, 0xB5, 0x60, 
	0xEF, 0xEB, 0x36, 0x86, 0xB7, 0xF5, 0x78, 0xB8, 0xD6, 0x81, 
	0x6E, 0x5F, 0x2B, 0x0C, 0x7E, 0x1D, 0xFA, 0xFC, 0x5A, 0x74, 
	0x6F, 0xB6, 0xC0, 0x7D, 0x60, 0x07, 0xE9, 0x11, 0xA2, 0xD0, 
	0x78, 0x25, 0x58, 0xD0, 0x9B, 0xCD, 0x88, 0x09, 0x68, 0x58, 
	0xD1, 0xBF, 0xAC, 0xC6, 0xBB, 0x2F, 0x9B, 0xF4, 0x8B, 0x7D, 
	0xE8, 0xDD, 0xD2, 0x42, 0xF3, 0x4A, 0x89, 0x5F, 0x47, 0xBD, 
	0x26, 0x83, 0x7C, 0x51, 0x08, 0xC5, 0x72, 0x2D, 0x9C, 0xFB, 
	0x36, 0xDC, 0x1C, 0xC9, 0x41, 0xDE, 0x83, 0x0C, 0x45, 0x4C, 
	0xD0, 0xE2, 0xAC, 0xA1, 0x5C, 0x01, 0x1B, 0xFD, 0x55, 0x33, 
	0x3A, 0x57, 0x55, 0x68, 0x5B, 0x91, 0x43, 0x36, 0x2B, 0x8A, 
	0x09, 0x04, 0x36, 0x2E, 0x1A, 0x66, 0xCA, 0x20, 0x9D, 0x2A, 
	0xC6, 0xC0, 0x1B, 0x1D, 0xF4, 0x4B, 0xCD, 0xB8, 0xDC, 0x9D, 
	0x46, 0xC5, 0x04, 0x8A, 0x71, 0xFE, 0xA1, 0xF7, 0xBD, 0x0B, 
	0xFD, 0x6F, 0x75, 0x34, 0xDC, 0x84, 0x3F, 0x1D, 0xF1, 0x64, 
	0x11, 0x14, 0xF3, 0x7C, 0x58, 0xFC, 0xC3, 0xC8, 0xD2, 0xA5, 
	0x1E, 0xC6, 0x04, 0x4D, 0x76, 0xDE, 0xE1, 0x44, 0x60, 0x0C, 
	0xDA, 0x0D, 0x12, 0x2A, 0x5F, 0xFD, 0x89, 0x02, 0xD2, 0x5B, 
	0x0D, 0xAB, 0x7F, 0x08, 0xE9, 0x1D, 0xEC, 0x23, 0x41, 0xBD, 
	0xAD, 0x9C, 0x32, 0xFA, 0xB4, 0x30, 0x6C, 0x69, 0xD0, 0xF4, 
	0x52, 0x08, 0xF1, 0x7C, 0x05, 0x78, 0xD6, 0x1B, 0x71, 0x60, 
	0x34, 0x77, 0x3D, 0x5C, 0xF4, 0xFB, 0xD4, 0xE8, 0xF2, 0xDE, 
	0xC3, 0x05, 0x75, 0xF2, 0x51, 0x09, 0xE2, 0xB1, 0x12, 0xC5, 
	0x6D, 0x2B, 0x01, 0x47, 0xC0, 0x02, 0xD9, 0x7C, 0x25, 0x24, 
	0xDE, 0xD2, 0x1F, 0x35, 0x1F, 0x17, 0x88, 0x9C, 0x1C, 0xD4, 
	0xB9, 0x8B, 0xE1, 0xD8, 0x35, 0xE3, 0x52, 0x4F, 0x2A, 0xD8, 
	0xF7, 0x59, 0x47, 0x4D, 0x14, 0x9A, 0x8B, 0x18, 0x55, 0x4F, 
	0x0B, 0x83, 0xA4, 0x47, 0x80, 0x71, 0x6A, 0x34, 0xD6, 0xB0, 
	0x28, 0x58, 0xEB, 0x8C, 0x26, 0x0A, 0x73, 0x31, 0xB1, 0x63, 
	0xC2, 0x1D, 0xCB, 0xAD, 0x28, 0x1C, 0x4C, 0x56, 0x9E, 0x66, 
	0xC4, 0x0D, 0x52, 0xF9, 0xA3, 0x02, 0xA2, 0x64, 0xE8, 0x6A, 
	0xA4, 0xD1, 0x51, 0x09, 0x27, 0x2D, 0x31, 0xFA, 0xDA, 0x41, 
	0xCE, 0x54, 0x41, 0x3E, 0x55, 0x09, 0xE3, 0x6A, 0x1B, 0x1C, 
	0x34, 0x2C, 0xB4, 0x70, 0xC0, 0x56, 0xB1, 0x22, 0x34, 0x4C, 
	0x24, 0x1C, 0x65, 0xCE, 0x40, 0x2E, 0x41, 0x18, 0x72, 0x42, 
	0xF9, 0x03, 0x99, 0xE8, 0x59, 0x20, 0x61, 0x7E, 0x6D, 0x80, 
	0x79, 0xC3, 0x00, 0x8D, 0xB7, 0x11, 0x59, 0x5D, 0x29, 0x51, 
	0x38, 0x74, 0x1C, 0x4E, 0xB8, 0x4C, 0xD7, 0xF4, 0x99, 0xCC, 
	0xDC, 0x9E, 0x74, 0x32, 0xBB, 0xF3, 0x22, 0x95, 0xA1, 0x49, 
	0x09, 0xA7, 0xB5, 0x9F, 0x0D, 0x9F, 0x6F, 0x3D, 0x43, 0x9D, 
	0x53, 0xB1, 0xC8, 0x64, 0xE5, 0x29, 0xE6, 0x5F, 0xB7, 0xF1, 
	0x5F, 0xF3, 0x1D, 0x41, 0x46, 0xCB, 0x1F, 0x00, 0x28, 0xD3, 
	0xC1, 0x00, 0x00, 0x00, 0x00, 0x49, 0x45, 0x4E, 0x44, 0xAE, 
	0x42, 0x60, 0x82, 
};

wxBitmap& add_png_to_wx_bitmap()
{
	static wxMemoryInputStream memIStream( add_png, sizeof( add_png ) );
	static wxImage image( memIStream, wxBITMAP_TYPE_PNG );
	static wxBitmap bmp( image );
	return bmp;
};


#endif //ADD_PNG_H

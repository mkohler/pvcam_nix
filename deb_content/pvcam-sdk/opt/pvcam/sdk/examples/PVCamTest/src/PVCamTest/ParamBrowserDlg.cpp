/******************************************************************************/
/* Copyright (C) Teledyne Photometrics. All rights reserved.                  */
/******************************************************************************/
#include "ParamBrowserDlg.h"

// WxWidgets headers have to be included first
#include <wx/propgrid/editors.h>
#include <wx/propgrid/propgriddefs.h>

/* Local */
#include "ArrayUintProperty.h"
#include "backend/Camera.h"
#include "backend/Log.h"
#include "backend/ParamInfoMap.h"
#include "backend/Params.h"
#include "backend/Utils.h"
#include "resources/arrow_refresh_small.png.h"
#include "resources/cross.png.h"
#include "resources/information.png.h"
#include "resources/textfield_delete.png.h"
#include "resources/textfield_key.png.h"

#define ID_SHOW_UNAVAILABLE 20000
#define ID_SHOW_READ_ONLY   20001

template<class T>
wxVariant PM2WXVARIANT(const T& WXUNUSED(value));
template<> inline wxVariant PM2WXVARIANT(const int8_t& value)
{ return wxVariant(wxLongLong(value)); }
template<> inline wxVariant PM2WXVARIANT(const int16_t& value)
{ return wxVariant(wxLongLong(value)); }
template<> inline wxVariant PM2WXVARIANT(const int32_t& value)
{ return wxVariant(wxLongLong(value)); }
template<> inline wxVariant PM2WXVARIANT(const int64_t& value)
{ return wxVariant(wxLongLong(value)); }
template<> inline wxVariant PM2WXVARIANT(const uint8_t& value)
{ return wxVariant(wxULongLong(value)); }
template<> inline wxVariant PM2WXVARIANT(const uint16_t& value)
{ return wxVariant(wxULongLong(value)); }
template<> inline wxVariant PM2WXVARIANT(const uint32_t& value)
{ return wxVariant(wxULongLong(value)); }
template<> inline wxVariant PM2WXVARIANT(const uint64_t& value)
{ return wxVariant(wxULongLong(value)); }
template<> inline wxVariant PM2WXVARIANT(const float& value)
{ return wxVariant(double(value)); }
template<> inline wxVariant PM2WXVARIANT(const double& value)
{ return wxVariant(value); }
template<> inline wxVariant PM2WXVARIANT(const std::string& value)
{ return wxVariant(value); }

#define PM_NO_EXCEPTION(cmd) do { \
    try { cmd; } \
    catch (const Exception&) {} \
} ONCE
#define PM_NO_EXCEPTION2(cmd, onExAction) do { \
    try { cmd; } \
    catch (const Exception& ex) \
    { UNUSED(ex); onExAction; } \
} ONCE

static const std::vector<int16_t> g_sortedAttrIds {
    ATTR_AVAIL,
    ATTR_TYPE,
    ATTR_ACCESS,
    ATTR_COUNT,
    ATTR_CURRENT,
    ATTR_DEFAULT,
    ATTR_MIN,
    ATTR_MAX,
    ATTR_INCREMENT,
};

static const wxColour bgColor(*wxWHITE);
static const wxColour bgColorError((*wxRED).ChangeLightness(160));
static const wxColour bgColorRo(wxColour(bgColor).ChangeLightness(95));
static const wxColour bgColorRoError((*wxRED).ChangeLightness(170));

class ParamClientData : public wxClientData
{
public:
        ParamClientData(std::shared_ptr<pm::ParamBase> param)
            : m_param(param)
        {}

        std::shared_ptr<pm::ParamBase> GetParam() const
        { return m_param; }

protected:
        std::shared_ptr<pm::ParamBase> m_param;
};

// ChoiceAndOneButtonEditor

WX_PG_DECLARE_EDITOR(ChoiceAndOneButton);

class wxPGChoiceAndOneButtonEditor : public wxPGChoiceEditor
{
    wxDECLARE_DYNAMIC_CLASS(wxPGChoiceAndOneButtonEditor);
public:
    wxPGChoiceAndOneButtonEditor() {}
    virtual ~wxPGChoiceAndOneButtonEditor()
    { wxPG_EDITOR(ChoiceAndOneButton) = NULL; }
    virtual wxString GetName() const override;
    virtual wxPGWindowList CreateControls(wxPropertyGrid* propGrid,
            wxPGProperty* property, const wxPoint& pos, const wxSize& size)
        const override
    {
        wxPGMultiButton* buttons = new wxPGMultiButton(propGrid, size);
        buttons->Add(wxArtProvider::GetBitmap(wxART_GOTO_LAST, wxART_BUTTON), wxID_APPLY);
        wxPGWindowList wndList = wxPGChoiceEditor::CreateControls(
                propGrid, property, pos, buttons->GetPrimarySize());
        buttons->Finalize(propGrid, pos);
        buttons->Enable(!property->HasFlag(wxPG_PROP_READONLY));
        wndList.SetSecondary(buttons);
        return wndList;
    }
    virtual bool OnEvent(wxPropertyGrid* propGrid, wxPGProperty* property,
            wxWindow* ctrl, wxEvent& event) const override
    {
        if (event.GetEventType() == wxEVT_BUTTON)
            if (event.GetId() == wxID_APPLY)
                return true; // Force value change
        return wxPGChoiceEditor::OnEvent(propGrid, property, ctrl, event);
    }
};

WX_PG_IMPLEMENT_INTERNAL_EDITOR_CLASS(ChoiceAndOneButton,
        wxPGChoiceAndOneButtonEditor, wxPGChoiceEditor);

// wxPGTextCtrlAndOneButtonEditor

WX_PG_DECLARE_EDITOR(TextCtrlAndOneButton);

class wxPGTextCtrlAndOneButtonEditor : public wxPGTextCtrlEditor
{
    wxDECLARE_DYNAMIC_CLASS(wxPGTextCtrlAndOneButtonEditor);
public:
    wxPGTextCtrlAndOneButtonEditor() {}
    virtual ~wxPGTextCtrlAndOneButtonEditor()
    { wxPG_EDITOR(TextCtrlAndOneButton) = NULL; }
    virtual wxString GetName() const override;
    virtual wxPGWindowList CreateControls(wxPropertyGrid* propGrid,
            wxPGProperty* property, const wxPoint& pos, const wxSize& size)
        const override
    {
        wxPGMultiButton* buttons = new wxPGMultiButton(propGrid, size);
        buttons->Add(wxArtProvider::GetBitmap(wxART_GOTO_LAST, wxART_BUTTON), wxID_APPLY);
        wxPGWindowList wndList = wxPGTextCtrlEditor::CreateControls(
                propGrid, property, pos, buttons->GetPrimarySize());
        buttons->Finalize(propGrid, pos);
        buttons->Enable(!property->HasFlag(wxPG_PROP_READONLY));
        wndList.SetSecondary(buttons);
        return wndList;
    }
    virtual bool OnEvent(wxPropertyGrid* propGrid, wxPGProperty* property,
            wxWindow* ctrl, wxEvent& event) const override
    {
        if (event.GetEventType() == wxEVT_BUTTON)
            if (event.GetId() == wxID_APPLY)
                return true; // Force value change
        return wxPGTextCtrlEditor::OnEvent(propGrid, property, ctrl, event);
    }
};

WX_PG_IMPLEMENT_INTERNAL_EDITOR_CLASS(TextCtrlAndOneButton,
        wxPGTextCtrlAndOneButtonEditor, wxPGTextCtrlEditor);

// wxPGTextCtrlAndTwoButtonsEditor

WX_PG_DECLARE_EDITOR(TextCtrlAndTwoButtons);

class wxPGTextCtrlAndTwoButtonsEditor : public wxPGTextCtrlEditor
{
    wxDECLARE_DYNAMIC_CLASS(wxPGTextCtrlAndTwoButtonsEditor);
public:
    wxPGTextCtrlAndTwoButtonsEditor() {}
    virtual ~wxPGTextCtrlAndTwoButtonsEditor()
    { wxPG_EDITOR(TextCtrlAndTwoButtons) = NULL; }
    virtual wxString GetName() const override;
    virtual wxPGWindowList CreateControls(wxPropertyGrid* propGrid,
            wxPGProperty* property, const wxPoint& pos, const wxSize& size)
        const override
    {
        wxPGMultiButton* buttons = new wxPGMultiButton(propGrid, size);
        buttons->Add("...");
        buttons->Add(wxArtProvider::GetBitmap(wxART_GOTO_LAST, wxART_BUTTON), wxID_APPLY);
        wxPGWindowList wndList = wxPGTextCtrlEditor::CreateControls(
                propGrid, property, pos, buttons->GetPrimarySize());
        buttons->Finalize(propGrid, pos);
        buttons->Enable(!property->HasFlag(wxPG_PROP_READONLY));
        wndList.SetSecondary(buttons);
        return wndList;
    }
    virtual bool OnEvent(wxPropertyGrid* propGrid, wxPGProperty* property,
            wxWindow* ctrl, wxEvent& event) const override
    {
        if (event.GetEventType() == wxEVT_BUTTON)
            if (event.GetId() == wxID_APPLY)
                return true; // Force value change
        return wxPGTextCtrlEditor::OnEvent(propGrid, property, ctrl, event);
    }
};

WX_PG_IMPLEMENT_INTERNAL_EDITOR_CLASS(TextCtrlAndTwoButtons,
        wxPGTextCtrlAndTwoButtonsEditor, wxPGTextCtrlEditor);

// ParamBrowserDlg

pm::ParamBrowserDlg::ParamBrowserDlg(std::shared_ptr<Camera> camera)
    : pm::ui::ParamBrowserDlg(nullptr),
    m_camera(camera)
{
    m_pgInfo->Hide();
    for (auto attrId : g_sortedAttrIds)
    {
        const auto& name = ParamInfoMap::GetParamAttrIdName(attrId);
        auto prop = new wxStringProperty(name);
        m_pgInfo->Append(prop);
        m_pgInfo->SetPropertyReadOnly(prop);
        m_attrPropMap[attrId] = prop;
    }
    m_enumItemsProp =
        new wxStringProperty(_("Enum Items"), wxPG_LABEL, wxT("<composed>"));
    m_pgInfo->Append(m_enumItemsProp);
    m_pgInfo->SetPropertyReadOnly(m_enumItemsProp);

    m_pgParams->SetBoolChoices(_("TRUE"), _("FALSE"));

    wxPGRegisterEditorClass(ChoiceAndOneButton);
    wxPGRegisterEditorClass(TextCtrlAndOneButton);
    wxPGRegisterEditorClass(TextCtrlAndTwoButtons);

    m_pgManager->SetDescBoxHeight(70);
    m_pgManager->GetGrid()->SetCaptionTextColour(
            m_pgManager->GetGrid()->GetCellTextColour());

    m_pgManager->GetToolBar()->AddSeparator();

    m_btnInfo = m_pgManager->GetToolBar()->AddCheckTool(wxID_INFO,
            _("Detailed Info"), information_png_to_wx_bitmap());
    m_btnInfo->SetShortHelp(m_btnInfo->GetLabel());

    m_btnUnavail = m_pgManager->GetToolBar()->AddCheckTool(ID_SHOW_UNAVAILABLE,
            _("Show Unavailable Items"), textfield_delete_png_to_wx_bitmap());
    m_btnUnavail->SetShortHelp(m_btnUnavail->GetLabel());
    m_btnUnavail->Toggle(m_showUnavailable);

    m_btnReadOnly = m_pgManager->GetToolBar()->AddCheckTool(ID_SHOW_READ_ONLY,
            _("Show Read-only Items"), textfield_key_png_to_wx_bitmap());
    m_btnReadOnly->SetShortHelp(m_btnReadOnly->GetLabel());
    m_btnReadOnly->Toggle(m_showReadOnly);

    m_btnReload =
        new wxButton(m_pgManager->GetToolBar(), wxID_REFRESH, _("Reload All"));
    m_btnReload->SetBitmap(arrow_refresh_small_png_to_wx_bitmap());
    m_pgManager->GetToolBar()->AddControl(m_btnReload);

    m_pgManager->GetToolBar()->AddStretchableSpace();

    // Search box has to be re-parented to not hang on exit
    m_search = new wxTextCtrl(this, wxID_FIND);
    // SetHint disallows the control to hold text on Linux
    m_search->SetToolTip(_("Search Filter"));
    m_search->Reparent(m_pgManager->GetToolBar());
    m_pgManager->GetToolBar()->AddControl(m_search);

    m_btnClearSearch = m_pgManager->GetToolBar()->AddTool(wxID_DELETE,
            _("Clear Search"), cross_png_to_wx_bitmap());
    m_btnClearSearch->SetShortHelp(m_btnClearSearch->GetLabel());
    m_pgManager->GetToolBar()->EnableTool(wxID_DELETE, false);

    m_pgManager->GetToolBar()->Realize();

    // Negative value to set size of bottom/right pane
    m_sashPos = -m_pgInfo->GetRoot()->GetChildrenHeight(m_pgInfo->GetRowHeight());
    // Hide Info panel by default
    m_splitter->Unsplit();

    // Connect events
    m_pgManager->GetToolBar()->Bind(wxEVT_TOOL,
            &ParamBrowserDlg::OnInfoToggled, this, wxID_INFO);
    m_pgManager->GetToolBar()->Bind(wxEVT_TOOL,
            &ParamBrowserDlg::OnUnavailableToggled, this, ID_SHOW_UNAVAILABLE);
    m_pgManager->GetToolBar()->Bind(wxEVT_TOOL,
            &ParamBrowserDlg::OnReadOnlyToggled, this, ID_SHOW_READ_ONLY);
    m_btnReload->Bind(wxEVT_BUTTON,
            &ParamBrowserDlg::OnReloadAllClicked, this);
    m_search->Bind(wxEVT_TEXT,
            &ParamBrowserDlg::OnSearchFilterChanged, this);
    m_pgManager->GetToolBar()->Bind(wxEVT_TOOL,
            &ParamBrowserDlg::OnClearSearchClicked, this, wxID_DELETE);
    m_pgManager->Bind(wxEVT_PG_SELECTED,
            &ParamBrowserDlg::OnPropertySelected, this);
    m_pgManager->Bind(wxEVT_PG_CHANGED,
            &ParamBrowserDlg::OnPropertyChanged, this);
    m_pgManager->Bind(wxEVT_COMMAND_BUTTON_CLICKED,
            &ParamBrowserDlg::OnPropertyButtonClicked, this);

    DoReloadAll(true);
    DoApplyFilter(m_filter, m_showUnavailable, m_showReadOnly);
    m_pgParams->Sort();

    m_search->SetFocus();
}

pm::ParamBrowserDlg::~ParamBrowserDlg()
{
    UnregisterParamChangeHandlers();
}

void pm::ParamBrowserDlg::ActivateWindow(bool active)
{
    Show(active);
    if (active)
    {
        Raise(); // Move to the top in Z-order
    }
}

void pm::ParamBrowserDlg::EnableEditor(bool enabled)
{
    m_splitter->Enable(enabled);
}

void pm::ParamBrowserDlg::OnSearchFilterChanged(wxCommandEvent& WXUNUSED(event))
{
    const auto filter = m_search->GetValue();

    m_pgManager->GetToolBar()->EnableTool(wxID_DELETE, !filter.IsEmpty());

    ApplyFilter(filter, m_showUnavailable, m_showReadOnly);
}

void pm::ParamBrowserDlg::OnClearSearchClicked(wxCommandEvent& WXUNUSED(event))
{
    m_search->Clear();
}

void pm::ParamBrowserDlg::OnPropertySelected(wxPropertyGridEvent& WXUNUSED(event))
{
    do
    {
        auto prop = m_pgManager->GetGrid()->GetSelectedProperty();
        if (!prop)
            break;
        auto clientObj = static_cast<ParamClientData*>(prop->GetClientObject());
        if (!clientObj)
            break;
        auto param = clientObj->GetParam();
        if (!param)
            break;

        param->ResetCacheAllFlags();
        OnParamChanged(*param, true);
    }
    ONCE;

    ReloadAttrs();
}

void pm::ParamBrowserDlg::OnPropertyChanged(wxPropertyGridEvent& WXUNUSED(event))
{
    HandlePropertyChange();
}

void pm::ParamBrowserDlg::OnPropertyButtonClicked(wxCommandEvent& event)
{
    auto prop = m_pgManager->GetGrid()->GetSelectedProperty();
    if (!prop)
        return;
    if (prop->GetEditorClass() == wxPG_EDITOR(TextCtrlAndTwoButtons)
            && event.GetId() != wxID_APPLY)
        return;

    HandlePropertyChange();
}

void pm::ParamBrowserDlg::HandlePropertyChange()
{
    auto prop = m_pgManager->GetGrid()->GetSelectedProperty();
    if (!prop)
        return;
    auto clientObj = static_cast<ParamClientData*>(prop->GetClientObject());
    if (!clientObj)
        return;
    auto param = clientObj->GetParam();
    if (!param)
        return;

    std::string valueStr;

    uint16_t typeId = ParamInfoMap::GetMap().at(param->GetId()).GetType();
    PM_NO_EXCEPTION(typeId = param->GetType());
    switch (typeId)
    {
    case TYPE_ENUM: {
        valueStr = std::string(prop->GetValue());
        break;
    }
    case TYPE_CHAR_PTR: {
        wxString rawStr;
        wxString escStr(prop->GetValueAsString());
        m_pgManager->GetGrid()->ExpandEscapeSequences(rawStr, escStr);
        valueStr = rawStr.mb_str().data();
        break;
    }
    default:
        valueStr = prop->GetValueAsString().mb_str().data();
        break;
    }

    try
    {
        param->SetFromString(valueStr);
    }
    catch (const Exception& ex)
    {
        Log::LogE("%s", ex.what());
    }
}

void pm::ParamBrowserDlg::OnInfoToggled(wxCommandEvent& WXUNUSED(event))
{
    if (m_btnInfo->IsToggled())
    {
        m_splitter->SplitHorizontally(m_panelParams, m_panelInfo, m_sashPos);
        m_pgInfo->Show();
        m_panelInfo->Layout();
    }
    else
    {
        m_sashPos = m_splitter->GetSashPosition();
        m_splitter->Unsplit();
    }

    ReloadAttrs();
}

void pm::ParamBrowserDlg::OnUnavailableToggled(wxCommandEvent& WXUNUSED(event))
{
    // m_btnUnavail->IsToggled() doesn't work here
    const bool showUnavailable =
        m_pgManager->GetToolBar()->GetToolState(ID_SHOW_UNAVAILABLE);
    ApplyFilter(m_filter, showUnavailable, m_showReadOnly);
}

void pm::ParamBrowserDlg::OnReadOnlyToggled(wxCommandEvent& WXUNUSED(event))
{
    // m_btnReadOnly->IsToggled() doesn't work here
    const bool showReadOnly =
        m_pgManager->GetToolBar()->GetToolState(ID_SHOW_READ_ONLY);
    ApplyFilter(m_filter, m_showUnavailable, showReadOnly);
}

void pm::ParamBrowserDlg::OnReloadAllClicked(wxCommandEvent& WXUNUSED(event))
{
    ReloadAll(false);
}

void pm::ParamBrowserDlg::ApplyFilter(const wxString& filter,
        bool showUnavailable, bool showReadOnly)
{
    if (m_filter == filter
            && m_showUnavailable == showUnavailable
            && m_showReadOnly == showReadOnly)
        return;

    m_pgManager->Freeze();

    DoApplyFilter(filter, showUnavailable, showReadOnly);

    m_pgManager->Thaw();

    auto selProp = m_pgManager->GetSelectedProperty();
    if (selProp)
        m_pgManager->EnsureVisible(selProp);
}

void pm::ParamBrowserDlg::ReloadAll(bool init)
{
    m_pgManager->Freeze();

    DoReloadAll(init);

    m_pgManager->Thaw();
}

void pm::ParamBrowserDlg::ReloadAttrs()
{
    m_pgInfo->Freeze();

    DoReloadAttrs();

    m_pgInfo->Thaw();
}

wxPGProperty* pm::ParamBrowserDlg::CreateProperty(uint32_t paramId,
        bool& errorOccurred)
{
    errorOccurred = false;
    wxPGProperty* prop(nullptr);

    const auto& pi = ParamInfoMap::GetMap().at(paramId);
    const auto& params = m_camera->GetParams().GetParams();
    auto param = params.at(paramId);

    wxString name = pi.GetName();
    wxString label = name;
    name.StartsWith("PARAM_", &label);

    try
    {
        if (!param->IsAvail())
        {
            prop = new wxStringProperty(label, name, "");
            prop->Enable(false);
            prop->SetHelpString(_("Not available"));
        }
        else
        {
            const auto typeId = param->GetType();
            const auto access = param->GetAccess();
            switch (typeId)
            {
            case TYPE_ENUM: {
                prop = new wxEnumProperty(label, name);
                prop->SetEditor(wxPG_EDITOR(ChoiceAndOneButton));
                break;
            }
            case TYPE_BOOLEAN: {
                prop = new wxBoolProperty(label, name);
                prop->SetAttribute(wxPG_BOOL_USE_DOUBLE_CLICK_CYCLING, true);
                prop->SetEditor(wxPG_EDITOR(ChoiceAndOneButton));
                break;
            }
            case TYPE_INT8:
            case TYPE_INT16:
            case TYPE_INT32:
            case TYPE_INT64: {
                prop = new wxIntProperty(label, name);
                prop->SetEditor(wxPG_EDITOR(TextCtrlAndOneButton));
                break;
            }
            case TYPE_UNS8:
            case TYPE_UNS16:
            case TYPE_UNS32:
            case TYPE_UNS64: {
                prop = new wxUIntProperty(label, name);
                prop->SetEditor(wxPG_EDITOR(TextCtrlAndOneButton));
                break;
            }
            case TYPE_FLT32:
            case TYPE_FLT64: {
                prop = new wxFloatProperty(label, name);
                //prop->SetAttribute(wxPG_FLOAT_PRECISION, 2);
                prop->SetEditor(wxPG_EDITOR(TextCtrlAndOneButton));
                break;
            }
            case TYPE_CHAR_PTR: {
                prop = new wxLongStringProperty(label, name);
                prop->SetEditor(wxPG_EDITOR(TextCtrlAndOneButton));
                break;
            }
            case TYPE_SMART_STREAM_TYPE_PTR: {
                prop = new wxArrayUintProperty(label, name);
                prop->SetAttribute(wxPG_ARRAY_DELIMITER, ',');
                prop->SetEditor(wxPG_EDITOR(TextCtrlAndTwoButtons));
                break;
            }
            case TYPE_SMART_STREAM_TYPE: // Not used in PVCAM
            case TYPE_VOID_PTR: // Not used in PVCAM
            case TYPE_VOID_PTR_PTR: // Not used in PVCAM
            default:
                throw Exception("Unknown type " + std::to_string(typeId));
            }
        }
    }
    catch (const Exception& ex)
    {
        prop = new wxStringProperty(label, name, _("Failed to access parameter"));
        prop->Enable(false);
        prop->SetHelpString(ex.what());
        errorOccurred = true;
    }

    return prop;
}

void pm::ParamBrowserDlg::OnParamChanged(ParamBase& param, bool allAttrsChanged)
{
    // Some parameters could fail also during checking availability
    PM_NO_EXCEPTION2(if (!param.IsAvail()) return, return);

    auto prop = m_paramPropMap.at(&param);
    const auto& pi = ParamInfoMap::GetMap().at(param.GetId());

    if (!allAttrsChanged)
    {
        prop->SetHelpString(wxString());
    }

    bool isReadOnly = true;

    try
    {
        const auto access = param.GetAccess();
        isReadOnly = !(access == ACC_READ_WRITE || access == ACC_WRITE_ONLY);

        #define ADD_NUM_CASE(type) \
            case type: { \
                auto p = static_cast<ParamTypeToT<type>::T*>(&param); \
                if (allAttrsChanged) { \
                    PM_NO_EXCEPTION((prop->SetAttribute(wxPG_ATTR_MIN, PM2WXVARIANT(p->GetMin())))); \
                    PM_NO_EXCEPTION((prop->SetAttribute(wxPG_ATTR_MAX, PM2WXVARIANT(p->GetMax())))); \
                    PM_NO_EXCEPTION((prop->SetAttribute(wxPG_ATTR_SPINCTRL_STEP, PM2WXVARIANT(p->GetInc())))); \
                } \
                if (access == ACC_WRITE_ONLY) \
                    break; \
                prop->SetValue(PM2WXVARIANT(p->GetCur())); \
            } break

        const auto typeId = param.GetType();
        switch (typeId)
        {
        case TYPE_ENUM: {
            auto p = static_cast<ParamTypeToT<TYPE_ENUM>::T*>(&param);
            if (allAttrsChanged)
            {
                wxPGChoices choices;
                prop->SetChoices(choices);
                const auto& items = p->GetItems();
                for (const auto& item : items)
                {
                    choices.Add(ParamInfoMap::GetParamEnumItemName(item, true),
                            item.GetValue());
                }
                prop->SetChoices(choices);
            }
            prop->SetValueFromInt(p->GetCur(), wxPG_FULL_VALUE);
            break;
        }
        case TYPE_BOOLEAN: {
            auto p = static_cast<ParamTypeToT<TYPE_BOOLEAN>::T*>(&param);
            if (access == ACC_WRITE_ONLY)
                break;
            const bool value = (access == ACC_EXIST_CHECK_ONLY) ? true : p->GetCur();
            prop->SetValue(value);
            break;
        }
        ADD_NUM_CASE(TYPE_INT8);
        ADD_NUM_CASE(TYPE_INT16);
        ADD_NUM_CASE(TYPE_INT32);
        ADD_NUM_CASE(TYPE_INT64);
        ADD_NUM_CASE(TYPE_UNS8);
        ADD_NUM_CASE(TYPE_UNS16);
        ADD_NUM_CASE(TYPE_UNS32);
        ADD_NUM_CASE(TYPE_UNS64);
        ADD_NUM_CASE(TYPE_FLT32);
        ADD_NUM_CASE(TYPE_FLT64);
        case TYPE_CHAR_PTR: {
            auto p = static_cast<ParamTypeToT<TYPE_CHAR_PTR>::T*>(&param);
            // Replace new lines with escaped string,
            // wxPGProperty expands it back when editing in modal editor
            wxString rawStr(p->GetCur());
            wxString escStr;
            m_pgManager->GetGrid()->CreateEscapeSequences(escStr, rawStr);
            prop->SetValue(escStr);
            //m_pgManager->GetGrid()->ExpandEscapeSequences(rawStr, escStr);
            prop->SetHelpString(rawStr);
            break;
        }
        case TYPE_SMART_STREAM_TYPE_PTR: {
            auto p = static_cast<ParamTypeToT<TYPE_SMART_STREAM_TYPE_PTR>::T*>(&param);
            wxArrayUint values;
            auto ss = p->GetCur();
            for (auto n = 0; n < ss->entries; ++n)
                values.Add(ss->params[n]);
            prop->SetValue(WXVARIANT(values));
            break;
        }
        case TYPE_SMART_STREAM_TYPE: // Not used in PVCAM
        case TYPE_VOID_PTR: // Not used in PVCAM
        case TYPE_VOID_PTR_PTR: // Not used in PVCAM
        default:
            throw Exception("Unknown type " + std::to_string(typeId));
        }

        #undef ADD_NUM_CASE

        if (allAttrsChanged)
        {
            m_pgParams->SetPropertyReadOnly(prop, isReadOnly);
        }
        prop->SetBackgroundColour(isReadOnly ? bgColorRo : bgColor);

        ReloadAttrs();
    }
    catch (const Exception& ex)
    {
        Log::LogE("Failed to access parameter %s, %s",
                pi.GetName().c_str(), ex.what());
        prop->Enable(false);
        prop->SetHelpString(ex.what());
        prop->SetBackgroundColour(isReadOnly ? bgColorRoError : bgColorError);
    }
}

void pm::ParamBrowserDlg::UnregisterParamChangeHandlers()
{
    for (const auto& paramHandlePair : m_paramChangeHandleMap)
    {
        auto param = paramHandlePair.first;
        auto handle = paramHandlePair.second;
        param->UnregisterChangeHandler(handle);
    }
    m_paramChangeHandleMap.clear();
}

void pm::ParamBrowserDlg::DoApplyFilter(const wxString& filter,
        bool showUnavailable, bool showReadOnly)
{
    m_filter = filter;
    m_showUnavailable = showUnavailable;
    m_showReadOnly = showReadOnly;

    for (const auto& paramPropPair : m_paramPropMap)
    {
        auto param = paramPropPair.first;
        auto prop = paramPropPair.second;

        bool isAvail = false;
        PM_NO_EXCEPTION(isAvail = param->IsAvail());
        uint16_t acc = ACC_READ_ONLY;
        PM_NO_EXCEPTION(acc = param->GetAccess());
        const bool isWritable = acc == ACC_READ_WRITE || acc == ACC_WRITE_ONLY;

        const bool show = (m_showUnavailable || isAvail)
            && (m_showReadOnly || isWritable)
            && (m_filter.IsEmpty() || MatchesFilter(m_filter, prop->GetLabel()));
        prop->Hide(!show, wxPG_DONT_RECURSE);
    }
}

void pm::ParamBrowserDlg::DoReloadAll(bool init)
{
    if (init)
    {
        UnregisterParamChangeHandlers();

        m_pgParams->Clear();
        m_propParentMap.clear();
        m_paramPropMap.clear();

        for (const auto id : ParamInfoMap::GetSortedParamIds())
        {
            const auto& pi = ParamInfoMap::GetMap().at(id);
            const auto& params = m_camera->GetParams().GetParams();
            auto param = params.at(id);

            auto grpProp = m_pgParams->GetProperty(pi.GetGroupName());
            if (!grpProp)
            {
                grpProp = new wxPropertyCategory(pi.GetGroupName());
                m_pgParams->Append(grpProp);
                m_propParentMap[grpProp] = nullptr;
            }

            bool errorOccurred;
            auto prop = CreateProperty(id, errorOccurred);

            prop->SetClientObject(new ParamClientData(param));

            m_paramPropMap[param.get()] = prop;
            m_paramChangeHandleMap[param.get()] = param->RegisterChangeHandler(
                    std::bind(&ParamBrowserDlg::OnParamChanged,
                        this, std::placeholders::_1, std::placeholders::_2));

            m_pgParams->AppendIn(grpProp, prop);
            m_propParentMap[prop] = grpProp;

            const bool isEnabled = prop->IsEnabled();
            // Color can be set only once the property is appended to parent
            if (errorOccurred)
                prop->SetBackgroundColour(isEnabled ? bgColorError : bgColorRoError);
            else
                prop->SetBackgroundColour(isEnabled ? bgColor : bgColorRo);
        }
    }

    for (const auto& paramPropPair : m_paramPropMap)
    {
        auto param = paramPropPair.first;
        param->ResetCacheAllFlags();
        OnParamChanged(*param, true);
    }
}

void pm::ParamBrowserDlg::DoReloadAttrs()
{
    const bool showAttrs = m_pgManager->GetToolBar()->GetToolState(wxID_INFO);
    if (!showAttrs)
        return;

    do
    {
        auto paramProp = m_pgManager->GetGrid()->GetSelectedProperty();
        if (!paramProp || !paramProp->IsVisible())
            break;
        auto clientObj =
            static_cast<ParamClientData*>(paramProp->GetClientObject());
        if (!clientObj)
            break;
        auto param = clientObj->GetParam();
        if (!param)
            break;

        uint16_t typeId = 0; // Unknown type
        PM_NO_EXCEPTION(typeId = param->GetType());
        uint16_t accessId = 0; // Unknown access
        PM_NO_EXCEPTION(accessId = param->GetAccess());

        // Update attribute properties
        for (auto attrId : g_sortedAttrIds)
        {
            const auto& prop = m_attrPropMap.at(attrId);
            prop->SetBackgroundColour(bgColorRo);
            prop->SetHelpString(wxString());
            try
            {
                auto value = param->GetValue(attrId);

                switch (attrId)
                {
                case ATTR_AVAIL:
                    prop->SetValue(param->IsAvail() ? "TRUE" : "FALSE");
                    break;
                case ATTR_TYPE:
                    prop->SetValue(
                            ParamInfoMap::GetParamTypeIdName(typeId, true));
                    break;
                case ATTR_ACCESS:
                    prop->SetValue(
                            ParamInfoMap::GetParamAccessIdName(accessId, true));
                    break;
                case ATTR_COUNT:
                    prop->SetValue(value->ToString());
                    break;
                case ATTR_CURRENT:
                case ATTR_DEFAULT:
                case ATTR_MIN:
                case ATTR_MAX:
                case ATTR_INCREMENT: {
                    switch (typeId)
                    {
                    case TYPE_ENUM: {
                        auto val = value->GetValueT<ParamTypeToT<TYPE_ENUM>::T::T>();
                        auto p = std::static_pointer_cast<ParamTypeToT<TYPE_ENUM>::T>(param);
                        auto str = value->ToString();
                        PM_NO_EXCEPTION(
                                str += " (" + p->GetItem(val).GetName() + ")");
                        prop->SetValue(str);
                        break;
                    }
                    case TYPE_BOOLEAN: {
                        auto val = value->GetValueT<ParamTypeToT<TYPE_BOOLEAN>::T::T>();
                        prop->SetValue(val ? "TRUE" : "FALSE");
                        break;
                    }
                    case TYPE_INT8:
                    case TYPE_INT16:
                    case TYPE_INT32:
                    case TYPE_INT64:
                    case TYPE_UNS8:
                    case TYPE_UNS16:
                    case TYPE_UNS32:
                    case TYPE_UNS64:
                    case TYPE_FLT32:
                    case TYPE_FLT64:
                    case TYPE_SMART_STREAM_TYPE_PTR: {
                        prop->SetValue(value->ToString());
                        break;
                    }
                    case TYPE_CHAR_PTR: {
                        wxString rawStr(value->ToString());
                        wxString escStr;
                        m_pgManager->GetGrid()->CreateEscapeSequences(
                                escStr, rawStr);
                        prop->SetValue(escStr);
                        break;
                    }
                    case TYPE_SMART_STREAM_TYPE: // Not used in PVCAM
                    case TYPE_VOID_PTR: // Not used in PVCAM
                    case TYPE_VOID_PTR_PTR: // Not used in PVCAM
                    default:
                        throw Exception("Unknown type " + std::to_string(typeId));
                    }
                    break;
                }
                default:
                    throw Exception("Unknown attribute " + std::to_string(attrId));
                }
            }
            catch (const Exception& ex)
            {
                prop->SetValue(_("Failed to get value"));
                prop->SetHelpString(ex.what());
                prop->SetBackgroundColour(bgColorRoError);
            }
        }

        // Create sub-properties for enum items
        m_enumItemsProp->Hide(typeId != TYPE_ENUM);
        m_enumItemsProp->DeleteChildren();
        m_enumItemsProp->SetValue(wxString());
        m_enumItemsProp->SetHelpString(wxString());
        m_enumItemsProp->SetBackgroundColour(bgColorRo);
        if (typeId == TYPE_ENUM)
        {
            auto p = std::static_pointer_cast<ParamEnum>(param);
            try
            {
                const auto& items = p->GetItems();
                for (const auto& item : items)
                {
                    auto subProp = new wxStringProperty(item.GetName(),
                            wxPG_LABEL, std::to_string(item.GetValue()));
                    m_pgInfo->SetPropertyReadOnly(subProp);
                    m_pgInfo->AppendIn(m_enumItemsProp, subProp);
                    subProp->SetBackgroundColour(bgColorRo);
                }
            }
            catch (const Exception& ex)
            {
                m_enumItemsProp->SetValue(_("Failed to get enum items"));
                m_enumItemsProp->SetHelpString(ex.what());
                m_enumItemsProp->SetBackgroundColour(bgColorRoError);
            }
        }

        return;
    }
    ONCE;

    // Clean up all properties
    for (auto attrId : g_sortedAttrIds)
    {
        const auto& prop = m_attrPropMap.at(attrId);
        prop->SetBackgroundColour(bgColorRo);
        prop->SetHelpString(wxString());
        prop->SetValue(wxString());
    }
    m_enumItemsProp->Hide(true);
}

bool pm::ParamBrowserDlg::MatchesFilter(const wxString& filter,
        const wxString& text)
{
    return wxNOT_FOUND != text.Lower().Find(filter.Lower());
}

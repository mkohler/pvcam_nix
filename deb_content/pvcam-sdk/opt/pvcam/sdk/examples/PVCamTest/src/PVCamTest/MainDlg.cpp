/******************************************************************************/
/* Copyright (C) Teledyne Photometrics. All rights reserved.                  */
/******************************************************************************/
#include "MainDlg.h"

// WxWidgets headers have to be included first
#include <wx/app.h>
#include <wx/busyinfo.h>
#include <wx/choicdlg.h>
#include <wx/dirdlg.h>
#include <wx/filefn.h>
#include <wx/log.h>
#include <wx/menu.h>
#include <wx/msgdlg.h>
#include <wx/mstream.h>

/* Local */
#include "backend/ColorRuntimeLoader.h"
#include "backend/ColorUtils.h"
#include "backend/exceptions/Exception.h"
#include "backend/FakeCamera.h"
#include "backend/Log.h"
#include "backend/OptionController.h"
#include "backend/Params.h"
#include "backend/PvcamRuntimeLoader.h"
#include "backend/RealCamera.h"
#include "backend/TrackRuntimeLoader.h"
#include "backend/Utils.h"
#include "resources/control-all.ico.h"
#include "version.h"

/* System */
#include <algorithm>
#include <cmath>
#include <limits>
#include <memory>

// Custom CLI options
static constexpr uint32_t OptionId_MetaRoiOutline =
    static_cast<uint32_t>(pm::OptionId::CustomBase) + 0;
static constexpr uint32_t OptionId_RoidMultiWindow =
    static_cast<uint32_t>(pm::OptionId::CustomBase) + 1;
static constexpr uint32_t OptionId_RoiOutline =
    static_cast<uint32_t>(pm::OptionId::CustomBase) + 2;
static constexpr uint32_t OptionId_ImageAutoConbright =
    static_cast<uint32_t>(pm::OptionId::CustomBase) + 3;
static constexpr uint32_t OptionId_ImageContract =
    static_cast<uint32_t>(pm::OptionId::CustomBase) + 4;
static constexpr uint32_t OptionId_ImageBrightness =
    static_cast<uint32_t>(pm::OptionId::CustomBase) + 5;

// Both constants must be positive number
static constexpr float WB_SCALE_MIN = (float)0.01;
static constexpr float WB_SCALE_MAX = (float)100.0;
static constexpr float WB_SCALE_INCREMENT = (float)0.02;

wxDEFINE_EVENT(PM_EVT_START_TASK, wxThreadEvent);
wxDEFINE_EVENT(PM_EVT_TASK_DONE, wxThreadEvent);
wxDEFINE_EVENT(PM_EVT_SHOW_HELP, wxCommandEvent);
wxDEFINE_EVENT(PM_EVT_ACQ_STARTED, wxCommandEvent);
wxDEFINE_EVENT(PM_EVT_ACQ_FINISHED, wxCommandEvent);

using HandlerProto = bool(pm::MainDlg::*)(const std::string&);

pm::MainDlg::MainDlg()
    : pm::ui::MainDlg(nullptr),
    //wxThreadHelper(wxTHREAD_JOINABLE), // Must not be here!
    m_helpOption(
            { "-Help", "-help", "--help", "-h", "/?" },
            { "" },
            { "false" },
            "Shows description for all supported options.",
            static_cast<uint32_t>(OptionId::Help),
            std::bind(static_cast<HandlerProto>(&MainDlg::HandleHelpOption),
                    this, std::placeholders::_1))
{
    Log::AddListener(this);

    m_regionModel = new(std::nothrow) RegionModel;
    m_centroidModel = new(std::nothrow) RegionModel;
    m_regionModelNotifier = new(std::nothrow) ModelNotifier(this);

    // Initialize all values and states

    {
        // Suppress error popup, it doesn't load all sizes from ICO with wx 3.0.0
        wxLogNull suppressWxErrorPopups;
        wxMemoryInputStream iconStream(control_all_ico, sizeof(control_all_ico));
        SetIcons(wxIconBundle(iconStream, wxBITMAP_TYPE_ICO));
    }

    m_lblLogs->Clear();

    m_uiFrameDescVisible = m_lblFrameDesc->IsShownOnScreen();

    // TODO: Remove this code later once the table with metadata-ROIs is moved
    //       somewhere else from old Tracking page.
    //       Don't forget to remove also IsShownOnScreen check in DoUpdateCentroidModel!
    const int pageIndex = m_tabsSettings->FindPage(m_tabSettingsTracking);
    if (pageIndex != wxNOT_FOUND)
    {
        m_tabsSettings->GetPage(pageIndex)->Show(false);
        m_tabsSettings->RemovePage(pageIndex);
    }

    UiSetAllControlsEnabled(false);

    // Connect Events

    Bind(wxEVT_CLOSE_WINDOW,
            &MainDlg::OnClose, this);
    Bind(wxEVT_ICONIZE,
            &MainDlg::OnIconize, this);

    m_cbPortSpeed->Bind(wxEVT_CHOICE,
            &MainDlg::OnPortSpeedChanged, this);
    m_cbGain->Bind(wxEVT_CHOICE,
            &MainDlg::OnGainChanged, this);
    m_editEmGain->Bind(wxEVT_SPINCTRL,
            &MainDlg::OnEmGainChanged, this);
    m_editExpTimes->Bind(wxEVT_TEXT,
            &MainDlg::OnExposuresChanged, this);
    m_editExpTimes->Bind(wxEVT_KILL_FOCUS,
            &MainDlg::OnExposuresFocusLost, this);
    m_cbExpTimeRes->Bind(wxEVT_CHOICE,
            &MainDlg::OnExpTimeResChanged, this);
    m_editTimeLapseDelay->Bind(wxEVT_SPINCTRL,
            &MainDlg::OnTimeLapseDelayChanged, this);
    m_cbTriggerMode->Bind(wxEVT_CHOICE,
            &MainDlg::OnTriggerModeChanged, this);
    m_cbExpOutMode->Bind(wxEVT_CHOICE,
            &MainDlg::OnExpOutModeChanged, this);
    m_cbClearMode->Bind(wxEVT_CHOICE,
            &MainDlg::OnClearModeChanged, this);
    m_editClearCycles->Bind(wxEVT_SPINCTRL,
            &MainDlg::OnClearCyclesChanged, this);
    m_cbPMode->Bind(wxEVT_CHOICE,
            &MainDlg::OnPModeChanged, this);

    m_editCircFrameCount->Bind(wxEVT_SPINCTRL,
            &MainDlg::OnCircBufferFrameCountChanged, this);

    // wxEVT_SLIDER is emit in too many cases, hook up only some of them
    m_sliderFrame->Bind(wxEVT_SCROLL_LINEUP,
            &MainDlg::OnCircBufferFrameChanged, this);
    m_sliderFrame->Bind(wxEVT_SCROLL_LINEDOWN,
            &MainDlg::OnCircBufferFrameChanged, this);
    m_sliderFrame->Bind(wxEVT_SCROLL_PAGEUP,
            &MainDlg::OnCircBufferFrameChanged, this);
    m_sliderFrame->Bind(wxEVT_SCROLL_PAGEDOWN,
            &MainDlg::OnCircBufferFrameChanged, this);
    m_sliderFrame->Bind(wxEVT_SCROLL_THUMBTRACK,
            &MainDlg::OnCircBufferFrameChanged, this);

    m_cbSaveAs->Bind(wxEVT_CHOICE,
            &MainDlg::OnSaveAsChanged, this);
    m_btnSaveDir->Bind(wxEVT_BUTTON,
            &MainDlg::OnSaveDirClicked, this);
    m_editSaveDir->Bind(wxEVT_TEXT,
            &MainDlg::OnSaveDirChanged, this);
    m_editSaveDigits->Bind(wxEVT_SPINCTRL,
            &MainDlg::OnSaveDigitsChanged, this);
    m_chbSaveFirst->Bind(wxEVT_CHECKBOX,
            &MainDlg::OnSaveFirstClicked, this);
    m_editSaveFirst->Bind(wxEVT_SPINCTRL,
            &MainDlg::OnSaveFirstChanged, this);
    m_chbSaveLast->Bind(wxEVT_CHECKBOX,
            &MainDlg::OnSaveLastClicked, this);
    m_editSaveLast->Bind(wxEVT_SPINCTRL,
            &MainDlg::OnSaveLastChanged, this);
    m_chbSaveStack->Bind(wxEVT_CHECKBOX,
            &MainDlg::OnSaveStackClicked, this);
    m_editSaveStack->Bind(wxEVT_SPINCTRL,
            &MainDlg::OnSaveStackChanged, this);

    m_cbAcqMode->Bind(wxEVT_CHOICE,
            &MainDlg::OnAcqModeChanged, this);
    m_editSnapFrameCount->Bind(wxEVT_SPINCTRL,
            &MainDlg::OnSnapFrameCountChanged, this);

    m_btnStart->Bind(wxEVT_BUTTON,
            &MainDlg::OnStartClicked, this);
    m_btnStop->Bind(wxEVT_BUTTON,
            &MainDlg::OnStopClicked, this);

    m_tabsInfos->Bind(wxEVT_NOTEBOOK_PAGE_CHANGED,
            &MainDlg::OnInfosPageChanged, this);

    m_btnRoiAdd->Bind(wxEVT_BUTTON,
            &MainDlg::OnRoiAddClicked, this);
    m_btnRoiRemove->Bind(wxEVT_BUTTON,
            &MainDlg::OnRoiRemoveClicked, this);

    m_dvlRois->Bind(wxEVT_DATAVIEW_SELECTION_CHANGED,
            &MainDlg::OnRoiTableSelectionChanged, this);

    m_cbRoiBin->Bind(wxEVT_CHOICE,
            &MainDlg::OnRoiBinChanged, this);
    m_editRoiBinSer->Bind(wxEVT_SPINCTRL,
            &MainDlg::OnRoiBinSerChanged, this);
    m_editRoiBinPar->Bind(wxEVT_SPINCTRL,
            &MainDlg::OnRoiBinParChanged, this);

    m_dvlRois->Bind(wxEVT_DATAVIEW_ITEM_CONTEXT_MENU,
            &MainDlg::OnRoiTableItemContextMenu, this);

    m_chbRoiMultiWin->Bind(wxEVT_CHECKBOX,
            &MainDlg::OnRoiMultiWindowClicked, this);

    m_chbCentroidsEnabled->Bind(wxEVT_CHECKBOX,
            &MainDlg::OnCentroidsEnabledClicked, this);
    m_chCentroidsMode->Bind(wxEVT_CHOICE,
            &MainDlg::OnCentroidsModeChanged, this);
    m_editCentroidsCount->Bind(wxEVT_SPINCTRL,
            &MainDlg::OnCentroidsCountChanged, this);
    m_editCentroidsRadius->Bind(wxEVT_SPINCTRL,
            &MainDlg::OnCentroidsRadiusChanged, this);
    m_chCentroidsBgCount->Bind(wxEVT_CHOICE,
            &MainDlg::OnCentroidsBgCountChanged, this);
    m_editDblCentroidsThreshold->Bind(wxEVT_SPINCTRLDOUBLE,
            &MainDlg::OnCentroidsThresholdChanged, this);

    m_editTrackLinkFrames->Bind(wxEVT_SPINCTRL,
            &MainDlg::OnTrackLinkFramesChanged, this);
    m_editTrackMaxDist->Bind(wxEVT_SPINCTRL,
            &MainDlg::OnTrackMaxDistChanged, this);
    m_chbTrackCpuOnly->Bind(wxEVT_CHECKBOX,
            &MainDlg::OnTrackCpuOnlyClicked, this);
    m_chbTrackTraj->Bind(wxEVT_CHECKBOX,
            &MainDlg::OnTrackTrajectoryShowClicked, this);
    m_editTrackTraj->Bind(wxEVT_SPINCTRL,
            &MainDlg::OnTrackTrajectoryDurationChanged, this);

    m_chbMetaRoiOutline->Bind(wxEVT_CHECKBOX,
            &MainDlg::OnMetaRoiOutlineClicked, this);

    m_cbFillMethod->Bind(wxEVT_CHOICE,
            &MainDlg::OnFillMethodChanged, this);
    m_chbAutoConbright->Bind(wxEVT_CHECKBOX,
            &MainDlg::OnAutoConbrightClicked, this);

    m_sliderContrast->Bind(wxEVT_SCROLL_LINEUP,
            &MainDlg::OnContrastSliderChanged, this);
    m_sliderContrast->Bind(wxEVT_SCROLL_LINEDOWN,
            &MainDlg::OnContrastSliderChanged, this);
    m_sliderContrast->Bind(wxEVT_SCROLL_PAGEUP,
            &MainDlg::OnContrastSliderChanged, this);
    m_sliderContrast->Bind(wxEVT_SCROLL_PAGEDOWN,
            &MainDlg::OnContrastSliderChanged, this);
    m_sliderContrast->Bind(wxEVT_SCROLL_THUMBTRACK,
            &MainDlg::OnContrastSliderChanged, this);
    m_editContrast->Bind(wxEVT_SPINCTRL,
            &MainDlg::OnContrastSpinChanged, this);

    m_sliderBrightness->Bind(wxEVT_SCROLL_LINEUP,
            &MainDlg::OnBrightnessSliderChanged, this);
    m_sliderBrightness->Bind(wxEVT_SCROLL_LINEDOWN,
            &MainDlg::OnBrightnessSliderChanged, this);
    m_sliderBrightness->Bind(wxEVT_SCROLL_PAGEUP,
            &MainDlg::OnBrightnessSliderChanged, this);
    m_sliderBrightness->Bind(wxEVT_SCROLL_PAGEDOWN,
            &MainDlg::OnBrightnessSliderChanged, this);
    m_sliderBrightness->Bind(wxEVT_SCROLL_THUMBTRACK,
            &MainDlg::OnBrightnessSliderChanged, this);
    m_editBrightness->Bind(wxEVT_SPINCTRL,
            &MainDlg::OnBrightnessSpinChanged, this);

    m_btnOpenParameterBrowser->Bind(wxEVT_BUTTON,
            &MainDlg::OnOpenParameterBrowserClicked, this);
    m_cbZoomFactor->Bind(wxEVT_CHOICE,
            &MainDlg::OnZoomFactorChanged, this);

    m_chbColorEnable->Bind(wxEVT_CHECKBOX,
            &MainDlg::OnColorEnableClicked, this);
    m_chbColorOverrideMask->Bind(wxEVT_CHECKBOX,
            &MainDlg::OnOverrideColorMaskClicked, this);
    m_cbColorCustomMask->Bind(wxEVT_CHOICE,
            &MainDlg::OnCustomColorMaskChanged, this);

    m_btnAutoWhiteBalance->Bind(wxEVT_BUTTON,
            &MainDlg::OnWbAutoClicked, this);
    m_btnDefaultWhiteBalance->Bind(wxEVT_BUTTON,
            &MainDlg::OnWbDefaultClicked, this);

    m_sliderRed->Bind(wxEVT_SCROLL_LINEUP,
            &MainDlg::OnWbSliderRedChanged, this);
    m_sliderRed->Bind(wxEVT_SCROLL_LINEDOWN,
            &MainDlg::OnWbSliderRedChanged, this);
    m_sliderRed->Bind(wxEVT_SCROLL_PAGEUP,
            &MainDlg::OnWbSliderRedChanged, this);
    m_sliderRed->Bind(wxEVT_SCROLL_PAGEDOWN,
            &MainDlg::OnWbSliderRedChanged, this);
    m_sliderRed->Bind(wxEVT_SCROLL_THUMBTRACK,
            &MainDlg::OnWbSliderRedChanged, this);
    m_editDblRed->Bind(wxEVT_SPINCTRLDOUBLE,
            &MainDlg::OnWbSpinRedChanged, this);

    m_sliderGreen->Bind(wxEVT_SCROLL_LINEUP,
            &MainDlg::OnWbSliderGreenChanged, this);
    m_sliderGreen->Bind(wxEVT_SCROLL_LINEDOWN,
            &MainDlg::OnWbSliderGreenChanged, this);
    m_sliderGreen->Bind(wxEVT_SCROLL_PAGEUP,
            &MainDlg::OnWbSliderGreenChanged, this);
    m_sliderGreen->Bind(wxEVT_SCROLL_PAGEDOWN,
            &MainDlg::OnWbSliderGreenChanged, this);
    m_sliderGreen->Bind(wxEVT_SCROLL_THUMBTRACK,
            &MainDlg::OnWbSliderGreenChanged, this);
    m_editDblGreen->Bind(wxEVT_SPINCTRLDOUBLE,
            &MainDlg::OnWbSpinGreenChanged, this);

    m_sliderBlue->Bind(wxEVT_SCROLL_LINEUP,
            &MainDlg::OnWbSliderBlueChanged, this);
    m_sliderBlue->Bind(wxEVT_SCROLL_LINEDOWN,
            &MainDlg::OnWbSliderBlueChanged, this);
    m_sliderBlue->Bind(wxEVT_SCROLL_PAGEUP,
            &MainDlg::OnWbSliderBlueChanged, this);
    m_sliderBlue->Bind(wxEVT_SCROLL_PAGEDOWN,
            &MainDlg::OnWbSliderBlueChanged, this);
    m_sliderBlue->Bind(wxEVT_SCROLL_THUMBTRACK,
            &MainDlg::OnWbSliderBlueChanged, this);
    m_editDblBlue->Bind(wxEVT_SPINCTRLDOUBLE,
            &MainDlg::OnWbSpinBlueChanged, this);

    m_cbDebayerAlgorithm->Bind(wxEVT_CHOICE,
            &MainDlg::OnDebayerAlgorithmChanged, this);
    m_cbAutoExposureAlgorithm->Bind(wxEVT_CHOICE,
            &MainDlg::OnAutoExposureAlgorithmChanged, this);
    m_chbColorCpuOnly->Bind(wxEVT_CHECKBOX,
            &MainDlg::OnColorCpuOnlyClicked, this);

    m_chbRoiOutline->Bind(wxEVT_CHECKBOX,
            &MainDlg::OnRoiOutlineClicked, this);
    m_chbLiveImage->Bind(wxEVT_CHECKBOX,
            &MainDlg::OnShowLiveImageClicked, this);

    Bind(PM_EVT_START_TASK, &MainDlg::OnEventStartTask, this);
    Bind(PM_EVT_TASK_DONE, &MainDlg::OnEventTaskDone, this);
    Bind(PM_EVT_SHOW_HELP, &MainDlg::OnEventShowHelp, this);
    Bind(PM_EVT_ACQ_STARTED, &MainDlg::OnEventAcqStarted, this);
    Bind(PM_EVT_ACQ_FINISHED, &MainDlg::OnEventAcqFinished, this);

    Bind(wxEVT_TIMER, &MainDlg::OnUiUpdateTimerTimeout, this);

    // Start update timer as soon as possible
    m_uiUpdateTimer.Start(1000 / 25, wxTIMER_CONTINUOUS); // 25x per second

    // On Windows attach stdout & stderr to console if any
#if defined(_WIN32)
    do
    {
        if (GetConsoleWindow())
            break;

        if (!AttachConsole(ATTACH_PARENT_PROCESS))
            break;

        if (!GetConsoleWindow())
            break;

        FILE* fStdOut = freopen("CONOUT$", "w", stdout);
        if (IsError(fStdOut != NULL,
                    "Failure switching console's output handle to stdout"))
            break;

        FILE* fStdErr = freopen("CONOUT$", "w", stderr);
        if (IsError(fStdErr != NULL,
                    "Failure switching console's error handle to stderr"))
            break;

        *stdout = *fStdOut;
        setvbuf(stdout, NULL, _IONBF, 0);

        *stderr = *fStdErr;
        setvbuf(stderr, NULL, _IONBF, 0);

        // Make cout, wcout, cerr, etc. point to console as well
        std::ios::sync_with_stdio();

        Log::LogI("Connected outputs to parent console");
        m_hasConsole = true;
    }
    ONCE;
#else
    m_hasConsole = true;
#endif

    if (m_hasConsole)
    {
        try
        {
            m_consoleLogger = std::make_shared<ConsoleLogger>();
        }
        catch (...)
        {
            // Suppress all errors, console logger will be off
        }
    }

    Log::LogI(GetTitle().ToStdString());
    Log::LogI("Version %s", VERSION_NUMBER_STR);
    SetTitle(GetTitle() + " " VERSION_NUMBER_STR);

    uns16 verMajor;
    uns16 verMinor;
    uns16 verBuild;

    auto pvcam = PvcamRuntimeLoader::Get();
    try
    {
        pvcam->Load();

        Log::LogI("-------------------");
        Log::LogI("Found %s", pvcam->GetFileName().c_str());
        Log::LogI("Path '%s'", pvcam->GetFilePath().c_str());

        pvcam->LoadSymbols();

        // All symbols loaded properly

        // TODO: There is no direct way to get PVCAM library version. We can get
        //       PVCAM version only that is not related to library version at all.
        uns16 version;
        if (PV_OK != PVCAM->pl_pvcam_get_ver(&version))
        {
            Log::LogE("PVCAM version UNKNOWN, library unloaded");
            pvcam->Unload();
        }
        else
        {
            verMajor = (version >> 8) & 0xFF;
            verMinor = (version >> 4) & 0x0F;
            verBuild = (version >> 0) & 0x0F;
            Log::LogI("PVCAM version %u.%u.%u", verMajor, verMinor, verBuild);
#if !defined(_WIN32)
            // On Linux loading PVCAM CORE library at run-time is supported
            // since PVCAM version 3.7.4.0 that has modified PVCAM<->driver API.
            // Min. PVCAM library versions: CORE >= 2.4.51, DDI >= 2.0.111.
            // Keep in mind here that PVCAM (i.e. installer) version differs
            // from PVCAM *library* version. Those are two unrelated things.
            if (verMajor < 3
                    || (verMajor == 3 && verMinor < 7)
                    || (verMajor == 3 && verMinor == 7 && verBuild < 4))
            {
                Log::LogE("Loading PVCAM library at run-time is supported since version 3.7.4.0");
                pvcam->Unload();
            }
#endif
        }
    }
    catch (RuntimeLoader::Exception ex)
    {
        if (pvcam->IsLoaded())
        {
            Log::LogE("Failed to load some symbols from PVCAM library, library unloaded");
            pvcam->Unload();
        }
        else
        {
            Log::LogE("Failed to load PVCAM library");
        }
    }
    if (IsError(pvcam->IsLoaded(), "Failure loading mandatory PVCAM library!!!"))
        return;

    auto ph_color = ColorRuntimeLoader::Get();
    try
    {
        ph_color->Load();

        Log::LogI("-------------------");
        Log::LogI("Found %s", ph_color->GetFileName().c_str());
        Log::LogI("Path '%s'", ph_color->GetFilePath().c_str());

        ph_color->LoadSymbols();

        // All symbols loaded properly

        if (PH_COLOR_ERROR_NONE != PH_COLOR->get_lib_version(
                    &verMajor, &verMinor, &verBuild))
        {
            Log::LogE("Version UNKNOWN, library unloaded");
            ph_color->Unload();
        }
        else
        {
            Log::LogI("Version %u.%u.%u", verMajor, verMinor, verBuild);

            if (PH_COLOR_VERSION_MAJOR != verMajor)
            {
                Log::LogE("Required major version %u.x.x, library unloaded",
                        PH_COLOR_VERSION_MAJOR);
                ph_color->Unload();
            }
            else if (PH_COLOR_VERSION_MINOR > verMinor)
            {
                Log::LogE("Required minor version x.%u.x or newer, library unloaded",
                        PH_COLOR_VERSION_MINOR);
                ph_color->Unload();
            }
        }
    }
    catch (RuntimeLoader::Exception ex)
    {
        if (ph_color->IsLoaded())
        {
            Log::LogW("Failed to load some symbols from color helper library, library unloaded");
            ph_color->Unload();
        }
    }

    auto ph_track = TrackRuntimeLoader::Get();
    try
    {
        ph_track->Load();

        Log::LogI("-------------------");
        Log::LogI("Found %s", ph_track->GetFileName().c_str());
        Log::LogI("Path '%s'", ph_track->GetFilePath().c_str());

        ph_track->LoadSymbols();

        // All symbols loaded properly

        if (PH_TRACK_ERROR_NONE != PH_TRACK->get_lib_version(&verMajor, &verMinor,
                    &verBuild))
        {
            Log::LogE("Version UNKNOWN, library unloaded");
            ph_track->Unload();
        }
        else
        {
            Log::LogI("Version %u.%u.%u", verMajor, verMinor, verBuild);

            if (PH_TRACK_VERSION_MAJOR != verMajor)
            {
                Log::LogE("Required major version %u.x.x, library unloaded",
                        PH_TRACK_VERSION_MAJOR);
                ph_track->Unload();
            }
            else if (PH_TRACK_VERSION_MINOR > verMinor)
            {
                Log::LogE("Required minor version x.%u.x or newer, library unloaded",
                        PH_TRACK_VERSION_MINOR);
                ph_track->Unload();
            }
        }
    }
    catch (RuntimeLoader::Exception ex)
    {
        if (ph_track->IsLoaded())
        {
            Log::LogW("Failed to load some symbols from track helper library, library unloaded");
            ph_track->Unload();
        }
    }

    Log::LogI("===================\n");

    // Set the current working directory before parsing CLI options
    m_settings.SetSaveDir(wxGetCwd().ToStdString());

    // Add options specific for this application
    if (!AddCliOptions())
        return;

    // Add all generic options
    if (!m_settings.AddOptions(m_optionController))
        return;

    const auto& cliOptions = m_optionController.GetOptions();
    const auto& fpsOption = *std::find_if(cliOptions.cbegin(), cliOptions.cend(),
            [](const Option& o) {
                return o.GetId() == static_cast<uint32_t>(OptionId::FakeCamFps);
            });
    const auto& camIdxOption = *std::find_if(cliOptions.cbegin(), cliOptions.cend(),
            [](const Option& o) {
                return o.GetId() == static_cast<uint32_t>(OptionId::CamIndex);
            });

    const std::vector<Option> initOptions{ m_helpOption, fpsOption, camIdxOption };
    const bool cliParseOk = m_optionController.ProcessOptions(
            wxTheApp->argc, wxTheApp->argv, initOptions, true);
    if (!cliParseOk || m_showFullHelp)
    {
        SetHelpText((m_showFullHelp)
                ? cliOptions
                : m_optionController.GetFailedProcessedOptions());
        m_showHelpNow = true;
    }

    if (PH_COLOR)
    {
        // Get color context
        if (PH_COLOR_ERROR_NONE != PH_COLOR->context_create(&m_colorContext))
        {
            ColorUtils::LogError("Failure initializing color helper context");
            return;
        }
    }

    m_imageDlg = std::make_shared<ImageDlg>();

    // Get Camera instance
    try
    {
        auto fakeCamFps = m_settings.GetFakeCamFps();
        if (fakeCamFps != 0)
        {
            m_camera = std::make_shared<FakeCamera>(fakeCamFps);
        }
        else
        {
            m_camera = std::make_shared<RealCamera>();
        }
    }
    catch (...)
    {
        Log::LogE("Failure getting Camera instance!!!");
        return;
    }

    if (IsError(m_camera->InitLibrary(), "Failure initializing camera library"))
        return;

    // Get Acquisition instance
    try
    {
        m_acquisition = std::make_unique<Acquisition>(m_camera);
    }
    catch (...)
    {
        Log::LogE("Failure getting Acquisition instance!!!");
        return;
    }

    // Get FPS limiter instance
    try
    {
        m_fpsLimiter = std::make_shared<FpsLimiter>();
    }
    catch (...)
    {
        Log::LogE("Failure getting FPS limiter instance!!!");
        return;
    }

    if (IsError(m_regionModel != nullptr, "Failure getting region model instance!!!"))
        return;
    m_regionModel->AddNotifier(m_regionModelNotifier);

    if (IsError(m_centroidModel != nullptr, "Failure getting centroid model instance!!!"))
        return;

    // No error happened during initialization, let's wait for result of
    // 'find camera' task and show help pop-up later if help text is set
    m_showHelpNow = false;

    // Start enumerating the cameras in a background thread.
    // When done the OnFindCameraTaskDone() will be called.
    StartTask(pm::TaskType::FIND_CAMERA);
}

pm::MainDlg::~MainDlg()
{
    StopTask();

    m_uiUpdateTimer.Stop();

    if (m_camera)
    {
        // Ignore errors
        if (m_camera->IsOpen())
        {
            IsError(m_camera->Close(), "Failure closing camera");
        }
        IsError(m_camera->UninitLibrary(), "Failure uninitializing camera library");
    }

    m_paramDlg = nullptr;

    // Image dialogs have to be destroyed before color context that they use
    m_imageDlg = nullptr;
    m_roiImageDlgs.clear(); // Items' memory released automatically

    if (PH_COLOR)
    {
        PH_COLOR->context_release(&m_colorContext);
    }
    m_colorContext = nullptr;

    if (m_regionModel && m_regionModelNotifier)
    {
        m_regionModel->RemoveNotifier(m_regionModelNotifier);
    }

    m_fpsLimiter = nullptr;
    m_acquisition = nullptr;
    m_camera = nullptr;

    m_wbWizard = nullptr;

    try
    {
        auto ph_track = TrackRuntimeLoader::Get();
        if (ph_track->IsLoaded())
        {
            ph_track->Unload();
        }
        ph_track->Release();
    }
    catch (RuntimeLoader::Exception ex)
    {
        Log::LogE(ex.what());
    }

    try
    {
        auto ph_color = ColorRuntimeLoader::Get();
        if (ph_color->IsLoaded())
        {
            ph_color->Unload();
        }
        ph_color->Release();
    }
    catch (RuntimeLoader::Exception ex)
    {
        Log::LogE(ex.what());
    }

    try
    {
        auto pvcam = PvcamRuntimeLoader::Get();
        if (pvcam->IsLoaded())
        {
            pvcam->Unload();
        }
        pvcam->Release();
    }
    catch (RuntimeLoader::Exception ex)
    {
        Log::LogE(ex.what());
    }

    m_consoleLogger = nullptr;

    Log::RemoveListener(this);
}

void pm::MainDlg::ActivateWindow(bool active)
{
    if (m_imageDlg)
    {
        m_imageDlg->ActivateWindow(active && m_camera && m_camera->IsOpen());
    }
    for (auto roiImageDlg : m_roiImageDlgs)
    {
        roiImageDlg->ActivateWindow(active && m_camera && m_camera->IsOpen());
    }

    if (active)
    {
        Show();
        Raise(); // Move to the top in Z-order
    }
}

void pm::MainDlg::ShowHelp()
{
    if (m_helpText.empty())
        return;
    if (!m_showHelpNow)
        return;

    m_showHelpNow = false;

    // Showing the help output in separate window instead of log area
    // Queue an event so the help shows after all log entries are processed
    //Log::LogI("\n%s", help.c_str());
    wxCommandEvent* event = new(std::nothrow) wxCommandEvent(PM_EVT_SHOW_HELP);
    if (event)
    {
        event->SetString(wxString(m_helpText));
        wxQueueEvent(GetEventHandler(), event);
    }
}

void pm::MainDlg::UpdateProgress(int min, int max, int current)
{
    std::lock_guard<std::mutex> lock(m_uiUpdateMutex);
    DoUpdateProgress(min, max, current);
}

wxThread::ExitCode pm::MainDlg::Entry()
{
    switch (m_taskType)
    {
    case pm::TaskType::FIND_CAMERA:
        return FindCameraTaskEntry();
    case pm::TaskType::SETUP_ACQUISITION:
        return SetupAcquisitionTaskEntry();
    case pm::TaskType::RUN_ACQUISITION:
        return RunAcquisitionTaskEntry();
    case pm::TaskType::AUTO_EXP_AND_WHITE_BALANCE:
        return AutoExpAndWhiteBalanceTaskEntry();
    }
    return (wxThread::ExitCode)-2;
}

void pm::MainDlg::OnClose(wxCloseEvent& event)
{
    UiSetAllControlsEnabled(false);

    StopAcq();

    for (auto roiImageDlg : m_roiImageDlgs)
    {
        roiImageDlg->Hide();
    }
    if (m_imageDlg)
    {
        m_imageDlg->Hide();
    }

    event.Skip(); // Let base class handle this event
}

void pm::MainDlg::OnIconize(wxIconizeEvent& event)
{
    ActivateWindow(!event.IsIconized());

    event.Skip(); // Let base class handle this event
}

void pm::MainDlg::OnPortSpeedChanged(wxCommandEvent& WXUNUSED(event))
{
    const int selection = m_cbPortSpeed->GetSelection();
    if (selection == wxNOT_FOUND)
        return;
    const auto speed = static_cast<Camera::Speed* const>(
            m_cbPortSpeed->GetClientData((unsigned int)selection));

    m_camera->GetParams().Get<PARAM_READOUT_PORT>()->SetCur(speed->port.GetValue());
    m_camera->GetParams().Get<PARAM_SPDTAB_INDEX>()->SetCur(speed->speedIndex);

    // Select first gain, some cameras keep port/speed/gain index comb. invalid
    m_camera->GetParams().Get<PARAM_GAIN_INDEX>()->SetCur(speed->gains.at(0).index);
}

void pm::MainDlg::OnGainChanged(wxCommandEvent& WXUNUSED(event))
{
    const int selection = m_cbGain->GetSelection();
    if (selection == wxNOT_FOUND)
        return;
    const auto gain = static_cast<Camera::Speed::Gain* const>(
            m_cbGain->GetClientData((unsigned int)selection));

    m_camera->GetParams().Get<PARAM_GAIN_INDEX>()->SetCur(gain->index);
}

void pm::MainDlg::OnEmGainChanged(wxSpinEvent& WXUNUSED(event))
{
    m_camera->GetParams().Get<PARAM_GAIN_MULT_FACTOR>()->SetCur(
            m_editEmGain->GetValue());
}

void pm::MainDlg::OnExposuresChanged(wxCommandEvent& WXUNUSED(event))
{
    const auto currentVals = m_editExpTimes->GetValue().ToStdString();
    const auto expStrings = StrToArray(currentVals, ',');

    if (expStrings.size() == 0)
        return;

    const auto expRes = m_settings.GetExposureResolution();
    auto min = m_expTimeResLimits.at(expRes).first;
    auto max = m_expTimeResLimits.at(expRes).second;

    m_smartExposures.clear();

    if (m_settings.GetTrigMode() == VARIABLE_TIMED_MODE)
    {
        min = pm::Clamp<uint32_t>(min, 1, std::numeric_limits<uint16_t>::max());
        max = pm::Clamp<uint32_t>(max, 1, std::numeric_limits<uint16_t>::max());
        std::vector<uint16_t> exposures;
        for (auto expStr : expStrings)
        {
            uint16_t exposure;
            if (!StrToNumber<uint16_t>(expStr, exposure))
            {
                Log::LogE("VTM exposure value '%s' is not valid", expStr.c_str());
                return;
            }
            if (exposure < min || exposure > max)
            {
                Log::LogE("VTM exposure is out of range [%u, %u]", min, max);
                return;
            }
            exposures.push_back(exposure);
        }
        m_settings.SetVtmExposures(exposures);
    }
    else
    {
        auto paramSsEn = m_camera->GetParams().Get<PARAM_SMART_STREAM_MODE_ENABLED>();
        auto paramSsExps = m_camera->GetParams().Get<PARAM_SMART_STREAM_EXP_PARAMS>();
        if (paramSsEn->IsAvail() && paramSsEn->GetCur() && paramSsExps->IsAvail())
        {
            std::vector<uint32_t> exposures;
            for (auto expStr : expStrings)
            {
                uint32_t exposure;
                if (!StrToNumber<uint32_t>(expStr, exposure))
                {
                    Log::LogE("Smart Streaming exposure value '%s' is not valid",
                            expStr.c_str());
                    return;
                }
                if (exposure < min || exposure > max)
                {
                    Log::LogE("Smart Streaming exposure is out of range [%u, %u]",
                            min, max);
                    return;
                }
                exposures.push_back(exposure);
            }
            m_smartExposures = exposures;
            if (m_smartExposures.size() > m_smartExposuresMax)
            {
                Log::LogW("Smart Streaming support up to %u exposures only",
                        m_smartExposuresMax);
                m_smartExposures.resize(m_smartExposuresMax);
            }
        }
        else
        {
            const auto& expStr = expStrings.at(0);
            uint32_t exposure;
            if (!StrToNumber<uint32_t>(expStr, exposure))
            {
                Log::LogE("Exposure value '%s' is not valid", expStr.c_str());
                return;
            }
            if (exposure < min || exposure > max)
            {
                Log::LogE("Exposure is out of range [%u, %u]", min, max);
                return;
            }
            m_settings.SetExposure(exposure);
        }
    }
}

void pm::MainDlg::OnExposuresFocusLost(wxFocusEvent& event)
{
    if (!m_smartExposures.empty())
    {
        auto paramSsExps = m_camera->GetParams().Get<PARAM_SMART_STREAM_EXP_PARAMS>();
        // Calls UiUpdateExposures
        paramSsExps->SetFromString(ArrayToStr(m_smartExposures, ','));
    }
    else
    {
        UiUpdateExposures();
    }

    event.Skip(); // Let base class handle this event
}

void pm::MainDlg::OnExpTimeResChanged(wxCommandEvent& WXUNUSED(event))
{
    const int selection = m_cbExpTimeRes->GetSelection();
    if (selection == wxNOT_FOUND)
        return;
    const auto item = static_cast<ParamEnumItem* const>(
            m_cbExpTimeRes->GetClientData((unsigned int)selection));

    // PARAM_EXP_RES was read-only in older PVCAMs, use the other one
    auto param = m_camera->GetParams().Get<PARAM_EXP_RES_INDEX>();
    param->SetCur(static_cast<uint16_t>(item->GetValue()));
}

void pm::MainDlg::OnTimeLapseDelayChanged(wxSpinEvent& WXUNUSED(event))
{
    m_settings.SetTimeLapseDelay(m_editTimeLapseDelay->GetValue());
}

void pm::MainDlg::OnTriggerModeChanged(wxCommandEvent& WXUNUSED(event))
{
    const int selection = m_cbTriggerMode->GetSelection();
    if (selection == wxNOT_FOUND)
        return;
    const auto item = static_cast<ParamEnumItem* const>(
            m_cbTriggerMode->GetClientData((unsigned int)selection));

    m_settings.SetTrigMode(item->GetValue());
    // TODO: Call m_camera->SetupExp and then use Param class instead of Settings.
    //       Following invocation could be removed then.
    OnParamExposureModeChanged(
            *m_camera->GetParams().Get<PARAM_EXPOSURE_MODE>(), false);
}

void pm::MainDlg::OnExpOutModeChanged(wxCommandEvent& WXUNUSED(event))
{
    const int selection = m_cbExpOutMode->GetSelection();
    if (selection == wxNOT_FOUND)
        return;
    const auto item = static_cast<ParamEnumItem* const>(
            m_cbExpOutMode->GetClientData((unsigned int)selection));

    m_settings.SetExpOutMode(item->GetValue());
    // TODO: Call m_camera->SetupExp and then use Param class instead of Settings.
    //       Following invocation could be removed then.
    OnParamExposeOutModeChanged(
            *m_camera->GetParams().Get<PARAM_EXPOSE_OUT_MODE>(), false);
}

void pm::MainDlg::OnClearModeChanged(wxCommandEvent& WXUNUSED(event))
{
    const int selection = m_cbClearMode->GetSelection();
    if (selection == wxNOT_FOUND)
        return;
    const auto item = static_cast<ParamEnumItem* const>(
            m_cbClearMode->GetClientData((unsigned int)selection));

    m_camera->GetParams().Get<PARAM_CLEAR_MODE>()->SetCur(item->GetValue());
}

void pm::MainDlg::OnClearCyclesChanged(wxSpinEvent& WXUNUSED(event))
{
    m_camera->GetParams().Get<PARAM_CLEAR_CYCLES>()->SetCur(
            m_editClearCycles->GetValue());
}

void pm::MainDlg::OnPModeChanged(wxCommandEvent& WXUNUSED(event))
{
    const int selection = m_cbPMode->GetSelection();
    if (selection == wxNOT_FOUND)
        return;
    const auto item = static_cast<ParamEnumItem* const>(
            m_cbPMode->GetClientData((unsigned int)selection));

    m_camera->GetParams().Get<PARAM_PMODE>()->SetCur(item->GetValue());
}

void pm::MainDlg::OnCircBufferFrameCountChanged(wxSpinEvent& WXUNUSED(event))
{
    m_settings.SetBufferFrameCount(m_editCircFrameCount->GetValue());
}

void pm::MainDlg::OnCircBufferFrameChanged(wxScrollEvent& event)
{
    // Taking the position from event, not from m_sliderFrame as we need to
    // update the value also from code when acquisition starts
    const auto position = event.GetPosition();

    auto frame = m_camera->GetFrameAt(position);

    const uint32_t frameNr = (frame) ? frame->GetInfo().GetFrameNr() : 0;
    DoUpdateFrameSlider(position, (int)frameNr);

    DoUpdateFrameDesc(frame);

    m_imageDlg->UpdateImage(frame);
    for (size_t n = 0; n < m_roiImageDlgs.size(); ++n)
    {
        m_roiImageDlgs.at(n)->UpdateImage(frame);
    }

    DoUpdateCentroidModel();
}

void pm::MainDlg::OnSaveAsChanged(wxCommandEvent& WXUNUSED(event))
{
    const int selection = m_cbSaveAs->GetSelection();
    if (selection == wxNOT_FOUND)
        return;
    m_settings.SetStorageType(static_cast<StorageType>(
            (size_t)m_cbSaveAs->GetClientData((unsigned int)selection)));
}

void pm::MainDlg::OnSaveDirClicked(wxCommandEvent& WXUNUSED(event))
{
    wxString currentDir = m_editSaveDir->GetValue();
    wxDirDialog dirDialog(this, wxDirSelectorPromptStr, currentDir);
    if (dirDialog.ShowModal() != wxID_OK)
        return;
    currentDir = dirDialog.GetPath();
    m_editSaveDir->SetValue(currentDir);
}

void pm::MainDlg::OnSaveDirChanged(wxCommandEvent& WXUNUSED(event))
{
    wxString currentDir = m_editSaveDir->GetValue();
    m_settings.SetSaveDir(currentDir.ToStdString());
}

void pm::MainDlg::OnSaveDigitsChanged(wxSpinEvent& WXUNUSED(event))
{
    int digits = m_editSaveDigits->GetValue();
    if (digits < 0)
        digits = 0;
    else if (digits > 255)
        digits = 255;
    m_settings.SetSaveDigits((uns8)digits);
}

void pm::MainDlg::OnSaveFirstClicked(wxCommandEvent& WXUNUSED(event))
{
    const bool checked = m_chbSaveFirst->GetValue();
    const int frames = (checked) ? m_editSaveFirst->GetValue() : 0;
    m_settings.SetSaveFirst((size_t)frames);

    UiSetAllControlsEnabled(true);
}

void pm::MainDlg::OnSaveFirstChanged(wxSpinEvent& WXUNUSED(event))
{
    const bool checked = m_chbSaveFirst->GetValue();
    const int frames = (checked) ? m_editSaveFirst->GetValue() : 0;
    m_settings.SetSaveFirst((size_t)frames);
}

void pm::MainDlg::OnSaveLastClicked(wxCommandEvent& WXUNUSED(event))
{
    const bool checked = m_chbSaveLast->GetValue();
    const int frames = (checked) ? m_editSaveLast->GetValue() : 0;
    m_settings.SetSaveLast((size_t)frames);

    UiSetAllControlsEnabled(true);
}

void pm::MainDlg::OnSaveLastChanged(wxSpinEvent& WXUNUSED(event))
{
    const bool checked = m_chbSaveLast->GetValue();
    const int frames = (checked) ? m_editSaveLast->GetValue() : 0;
    m_settings.SetSaveLast((size_t)frames);
}

void pm::MainDlg::OnSaveStackClicked(wxCommandEvent& WXUNUSED(event))
{
    const bool checked = m_chbSaveStack->GetValue();
    const size_t kb = (checked) ? 1024 * 1024 * (size_t)m_editSaveStack->GetValue() : 0;
    m_settings.SetMaxStackSize(kb);

    UiSetAllControlsEnabled(true);
}

void pm::MainDlg::OnSaveStackChanged(wxSpinEvent& WXUNUSED(event))
{
    const bool checked = m_chbSaveStack->GetValue();
    const size_t kb = (checked) ? 1024 * 1024 * (size_t)m_editSaveStack->GetValue() : 0;
    m_settings.SetMaxStackSize(kb);
}

void pm::MainDlg::OnAcqModeChanged(wxCommandEvent& WXUNUSED(event))
{
    const int selection = m_cbAcqMode->GetSelection();
    if (selection == wxNOT_FOUND)
        return;
    const auto item = static_cast<ParamEnumItem* const>(
            m_cbAcqMode->GetClientData((unsigned int)selection));

    m_settings.SetAcqMode((AcqMode)item->GetValue());
    UpdateAcqModeTooltip();

    UiSetAllControlsEnabled(true);
}

void pm::MainDlg::OnSnapFrameCountChanged(wxSpinEvent& WXUNUSED(event))
{
    m_snapFrameCount = m_editSnapFrameCount->GetValue();
}

void pm::MainDlg::OnStartClicked(wxCommandEvent& WXUNUSED(event))
{
    StartAcq();
}

void pm::MainDlg::OnStopClicked(wxCommandEvent& WXUNUSED(event))
{
    // On first click it gives a chance to finish processing.
    // On second click it forces full stop.
    StopAcq(m_userAbortFlag);
}

void pm::MainDlg::OnInfosPageChanged(wxBookCtrlEvent& event)
{
    const int pageIndex = event.GetSelection();
    if (pageIndex == wxNOT_FOUND)
        return;

    m_uiFrameDescVisible = m_tabsInfos->GetPage(pageIndex) == m_tabFrameDesc;

    auto frame = m_camera->GetFrameAt(m_sliderFrame->GetValue());
    DoUpdateFrameDesc(frame);
}

void pm::MainDlg::OnRoiAddClicked(wxCommandEvent& WXUNUSED(event))
{
    if (IsError(m_regionModel->AppendRegion(std::make_shared<Region>()),
                "Failure adding new region"))
        return;
}

void pm::MainDlg::OnRoiRemoveClicked(wxCommandEvent& WXUNUSED(event))
{
    std::shared_ptr<Region> region =
        m_regionModel->GetRegion(m_dvlRois->GetSelection());

    if (IsError(m_regionModel->DeleteRegion(region),
                "Failure removing region (none selected?)"))
        return;
}

void pm::MainDlg::OnRoiBinChanged(wxCommandEvent& WXUNUSED(event))
{
    const int selection = m_cbRoiBin->GetSelection();
    if (selection == wxNOT_FOUND)
        return;
    const auto roiBin =
        static_cast<std::pair<ParamEnumItem, ParamEnumItem>* const>(
            m_cbRoiBin->GetClientData((unsigned int)selection));

    const auto binSer = static_cast<uint16_t>(roiBin->first.GetValue());
    const auto binPar = static_cast<uint16_t>(roiBin->second.GetValue());

    m_settings.SetBinningSerial(binSer);
    m_settings.SetBinningParallel(binPar);

    m_editRoiBinSer->SetValue(binSer);
    m_editRoiBinPar->SetValue(binPar);

    DoUpdateColorCapable();
    UiUpdateColorEnabled();
}

void pm::MainDlg::OnRoiBinSerChanged(wxSpinEvent& WXUNUSED(event))
{
    m_settings.SetBinningSerial(m_editRoiBinSer->GetValue());

    DoUpdateColorCapable();
    UiUpdateColorEnabled();
}

void pm::MainDlg::OnRoiBinParChanged(wxSpinEvent& WXUNUSED(event))
{
    m_settings.SetBinningParallel(m_editRoiBinPar->GetValue());

    DoUpdateColorCapable();
    UiUpdateColorEnabled();
}

void pm::MainDlg::OnRoiTableSelectionChanged(wxDataViewEvent& event)
{
    UiSetRoiButtonsEnabled(true);
    event.Skip(); // Let RegionListCtrl handle this event too
}

void pm::MainDlg::OnRoiTableItemContextMenu(wxDataViewEvent& event)
{
    wxMenu menu;

    std::shared_ptr<Region> region = m_regionModel->GetRegion(event.GetItem());

    const bool addRoiEnabled =
        (int)m_regionModel->GetRegionCount() < m_regionModel->GetMaxRegionCount();

    menu.Append(wxID_ADD)->Enable(addRoiEnabled);
    menu.Append(wxID_DELETE)->Enable(region != nullptr);

    const int id = GetPopupMenuSelectionFromUser(menu, event.GetPosition());

    switch (id)
    {
    case wxID_ADD:
        if (IsError(m_regionModel->AppendRegion(std::make_shared<Region>()),
                    "Failure adding new region"))
            return;
        break;
    case wxID_DELETE:
        if (IsError(m_regionModel->DeleteRegion(region),
                    "Failure removing selected region"))
            return;
        break;
    default:
        break;
    }
}

void pm::MainDlg::OnRoiMultiWindowClicked(wxCommandEvent& WXUNUSED(event))
{
    m_roiMultiWindow = m_chbRoiMultiWin->GetValue();
}

void pm::MainDlg::OnCentroidsEnabledClicked(wxCommandEvent& WXUNUSED(event))
{
    const bool checked = m_chbCentroidsEnabled->GetValue();
    m_camera->GetParams().Get<PARAM_CENTROIDS_ENABLED>()->SetCur(checked);
}

void pm::MainDlg::OnCentroidsModeChanged(wxCommandEvent& WXUNUSED(event))
{
    const int selection = m_chCentroidsMode->GetSelection();
    if (selection == wxNOT_FOUND)
        return;
    const auto item = static_cast<ParamEnumItem* const>(
            m_chCentroidsMode->GetClientData((unsigned int)selection));
    m_camera->GetParams().Get<PARAM_CENTROIDS_MODE>()->SetCur(item->GetValue());
}

void pm::MainDlg::OnCentroidsCountChanged(wxSpinEvent& WXUNUSED(event))
{
    m_camera->GetParams().Get<PARAM_CENTROIDS_COUNT>()->SetCur(
            m_editCentroidsCount->GetValue());
}

void pm::MainDlg::OnCentroidsRadiusChanged(wxSpinEvent& WXUNUSED(event))
{
    m_camera->GetParams().Get<PARAM_CENTROIDS_RADIUS>()->SetCur(
            m_editCentroidsRadius->GetValue());
}

void pm::MainDlg::OnCentroidsBgCountChanged(wxCommandEvent& WXUNUSED(event))
{
    const int selection = m_chCentroidsBgCount->GetSelection();
    if (selection == wxNOT_FOUND)
        return;
    const auto item = static_cast<ParamEnumItem* const>(
            m_chCentroidsBgCount->GetClientData((unsigned int)selection));
    m_camera->GetParams().Get<PARAM_CENTROIDS_BG_COUNT>()->SetCur(
            item->GetValue());
}

void pm::MainDlg::OnCentroidsThresholdChanged(wxSpinDoubleEvent& WXUNUSED(event))
{
    const auto threshold = RealToFixedPoint<double, uint32_t>(8, 4,
            m_editDblCentroidsThreshold->GetValue());
    m_camera->GetParams().Get<PARAM_CENTROIDS_THRESHOLD>()->SetCur(threshold);
}

void pm::MainDlg::OnTrackLinkFramesChanged(wxSpinEvent& WXUNUSED(event))
{
    m_settings.SetTrackLinkFrames(m_editTrackLinkFrames->GetValue());
}

void pm::MainDlg::OnTrackMaxDistChanged(wxSpinEvent& WXUNUSED(event))
{
    m_settings.SetTrackMaxDistance(m_editTrackMaxDist->GetValue());
}

void pm::MainDlg::OnTrackCpuOnlyClicked(wxCommandEvent& WXUNUSED(event))
{
    const bool checked = m_chbTrackCpuOnly->GetValue();
    m_settings.SetTrackCpuOnly(checked);
}

void pm::MainDlg::OnTrackTrajectoryShowClicked(wxCommandEvent& WXUNUSED(event))
{
    m_trackShowTrajectory = m_chbTrackTraj->GetValue();

    UiSetCentroidControlsEnabled(true);

    if (m_imageDlg)
    {
        m_imageDlg->SetTrackShowTrajectory(m_trackShowTrajectory);
    }
    for (auto roiImageDlg : m_roiImageDlgs)
    {
        roiImageDlg->SetTrackShowTrajectory(m_trackShowTrajectory);
    }
}

void pm::MainDlg::OnTrackTrajectoryDurationChanged(wxSpinEvent& WXUNUSED(event))
{
    m_settings.SetTrackTrajectoryDuration(m_editTrackTraj->GetValue());
}

void pm::MainDlg::OnMetaRoiOutlineClicked(wxCommandEvent& WXUNUSED(event))
{
    m_metaRoiShowOutline = m_chbMetaRoiOutline->GetValue();

    UiSetCentroidControlsEnabled(true);

    if (m_imageDlg)
    {
        m_imageDlg->SetMetaRoiShowOutline(m_metaRoiShowOutline);
    }
    for (auto roiImageDlg : m_roiImageDlgs)
    {
        roiImageDlg->SetMetaRoiShowOutline(m_metaRoiShowOutline);
    }
}

void pm::MainDlg::OnOpenParameterBrowserClicked(wxCommandEvent& WXUNUSED(event))
{
    if (!m_paramDlg)
    {
        m_paramDlg = new(std::nothrow) ParamBrowserDlg(m_camera);
        if (IsError(m_paramDlg != nullptr, "Failure creating parameter browser"))
            return;

        m_paramDlg->Bind(wxEVT_CLOSE_WINDOW,
                [&](wxCloseEvent& WXUNUSED(event)){ m_paramDlg = nullptr; });
    }
    m_paramDlg->ActivateWindow();
}

void pm::MainDlg::OnFillMethodChanged(wxCommandEvent& WXUNUSED(event))
{
    const int selection = m_cbFillMethod->GetSelection();
    if (selection == wxNOT_FOUND)
        return;
    const auto item = static_cast<ParamEnumItem* const>(
            m_cbFillMethod->GetClientData((unsigned int)selection));

    m_fillMethod = (ImageDlg::FillMethod)item->GetValue();

    if (m_imageDlg)
    {
        m_imageDlg->SetFillMethod(m_fillMethod);
    }
    for (auto roiImageDlg : m_roiImageDlgs)
    {
        roiImageDlg->SetFillMethod(m_fillMethod);
    }
}

void pm::MainDlg::OnAutoConbrightClicked(wxCommandEvent& WXUNUSED(event))
{
    m_imageAutoConbright = m_chbAutoConbright->GetValue();

    m_sliderContrast->Enable(!m_imageAutoConbright);
    m_editContrast->Enable(!m_imageAutoConbright);
    m_sliderBrightness->Enable(!m_imageAutoConbright);
    m_editBrightness->Enable(!m_imageAutoConbright);

    if (m_imageDlg)
    {
        m_imageDlg->SetAutoConbright(m_imageAutoConbright);
    }
    for (auto roiImageDlg : m_roiImageDlgs)
    {
        roiImageDlg->SetAutoConbright(m_imageAutoConbright);
    }
}

void pm::MainDlg::OnContrastSliderChanged(wxCommandEvent& WXUNUSED(event))
{
    m_imageContrast = m_sliderContrast->GetValue();

    m_editContrast->SetValue(m_imageContrast);

    if (m_imageDlg)
    {
        m_imageDlg->SetContrast(m_imageContrast);
    }
    for (auto roiImageDlg : m_roiImageDlgs)
    {
        roiImageDlg->SetContrast(m_imageContrast);
    }
}

void pm::MainDlg::OnContrastSpinChanged(wxSpinEvent& WXUNUSED(event))
{
    m_imageContrast = m_editContrast->GetValue();

    m_sliderContrast->SetValue(m_imageContrast);

    if (m_imageDlg)
    {
        m_imageDlg->SetContrast(m_imageContrast);
    }
    for (auto roiImageDlg : m_roiImageDlgs)
    {
        roiImageDlg->SetContrast(m_imageContrast);
    }
}

void pm::MainDlg::OnBrightnessSliderChanged(wxCommandEvent& WXUNUSED(event))
{
    m_imageBrightness = m_sliderBrightness->GetValue();

    m_editBrightness->SetValue(m_imageBrightness);

    if (m_imageDlg)
    {
        m_imageDlg->SetBrightness(m_imageBrightness);
    }
    for (auto roiImageDlg : m_roiImageDlgs)
    {
        roiImageDlg->SetBrightness(m_imageBrightness);
    }
}

void pm::MainDlg::OnBrightnessSpinChanged(wxSpinEvent& WXUNUSED(event))
{
    m_imageBrightness = m_editBrightness->GetValue();

    m_sliderBrightness->SetValue(m_imageBrightness);

    if (m_imageDlg)
    {
        m_imageDlg->SetBrightness(m_imageBrightness);
    }
    for (auto roiImageDlg : m_roiImageDlgs)
    {
        roiImageDlg->SetBrightness(m_imageBrightness);
    }
}

void pm::MainDlg::OnZoomFactorChanged(wxCommandEvent& WXUNUSED(event))
{
    const int selection = m_cbZoomFactor->GetSelection();
    if (selection == wxNOT_FOUND)
        return;
    const auto item = static_cast<ParamEnumItem* const>(
            m_cbZoomFactor->GetClientData((unsigned int)selection));

    m_zoomFactor = (ImageDlg::ZoomFactor)item->GetValue();

    if (m_imageDlg)
    {
        m_imageDlg->SetZoomFactor(m_zoomFactor);
    }
    for (auto roiImageDlg : m_roiImageDlgs)
    {
        roiImageDlg->SetZoomFactor(m_zoomFactor);
    }
}

void pm::MainDlg::OnRoiOutlineClicked(wxCommandEvent& WXUNUSED(event))
{
    m_roiShowOutline = m_chbRoiOutline->GetValue();

    if (m_acquisition->IsRunning())
    {
        DoUpdateRoiOutlines(m_roiShowOutline);
    }
}

void pm::MainDlg::OnShowLiveImageClicked(wxCommandEvent& WXUNUSED(event))
{
    m_showLiveImage = m_chbLiveImage->GetValue();
    if (m_showLiveImage)
    {
        auto frame = m_camera->GetFrameAt(m_sliderFrame->GetValue());
        DoUpdateFrameDesc(frame);
        m_imageDlg->UpdateImage(frame);
        for (size_t n = 0; n < m_roiImageDlgs.size(); ++n)
        {
            m_roiImageDlgs.at(n)->UpdateImage(frame);
        }
    }
}

void pm::MainDlg::OnColorEnableClicked(wxCommandEvent& WXUNUSED(event))
{
    m_colorEnabled = m_chbColorEnable->GetValue();

    UiUpdateColorContext();
    UiUpdateColorEnabled();
}

void pm::MainDlg::OnOverrideColorMaskClicked(wxCommandEvent& WXUNUSED(event))
{
    m_colorMaskOverridden = m_chbColorOverrideMask->GetValue();

    UiUpdateColorContext();
    UiUpdateColorEnabled();
}

void pm::MainDlg::OnCustomColorMaskChanged(wxCommandEvent& WXUNUSED(event))
{
    const int selection = m_cbColorCustomMask->GetSelection();
    if (selection == wxNOT_FOUND)
        return;
    const auto item = static_cast<ParamEnumItem* const>(
            m_cbColorCustomMask->GetClientData((unsigned int)selection));

    m_customColorMask = static_cast<ParamEnumItem::T>(item->GetValue());

    UiUpdateColorContext();
    UiUpdateColorEnabled();
}

void pm::MainDlg::OnWbAutoClicked(wxCommandEvent& WXUNUSED(event))
{
    m_wbWizard = new(std::nothrow) WhiteBalanceWizard();
    if (IsError(m_wbWizard != nullptr, "Failure creating white balance wizard"))
        return;

    auto OnCancelHandler = [this]() {
        m_wbWizard = nullptr;
        m_btnAutoWhiteBalance->Enable(true);
    };

    auto OnFinishHandler = [this]() {
        m_wbWizard = nullptr;

        UiSetAllControlsEnabled(false);
        // Stop button is enabled when the task is successfully started

        // Start the task in a background thread.
        // When done the OnAutoExpAndWhiteBalanceTaskDone() will be called.
        StartTask(pm::TaskType::AUTO_EXP_AND_WHITE_BALANCE);
    };

    if (m_wbWizard->ShowModelessWizard(OnCancelHandler, OnFinishHandler))
    {
        m_btnAutoWhiteBalance->Enable(false);
    }
}

void pm::MainDlg::OnWbDefaultClicked(wxCommandEvent& WXUNUSED(event))
{
    const float scaleRed = 1.0;
    const float scaleGreen = 1.0;
    const float scaleBlue = 1.0;

    m_settings.SetColorWbScaleRed(scaleRed);
    m_settings.SetColorWbScaleGreen(scaleGreen);
    m_settings.SetColorWbScaleBlue(scaleBlue);

    m_sliderRed->SetValue(ConvertWbScaleToSliderValue(scaleRed));
    m_sliderGreen->SetValue(ConvertWbScaleToSliderValue(scaleGreen));
    m_sliderBlue->SetValue(ConvertWbScaleToSliderValue(scaleBlue));

    m_editDblRed->SetValue(scaleRed);
    m_editDblGreen->SetValue(scaleGreen);
    m_editDblBlue->SetValue(scaleBlue);

    UiUpdateColorContext();
}

void pm::MainDlg::OnWbSliderRedChanged(wxCommandEvent& WXUNUSED(event))
{
    const float scale = ConvertWbSliderValueToScale(m_sliderRed->GetValue());

    m_settings.SetColorWbScaleRed(scale);
    m_editDblRed->SetValue(scale);

    UiUpdateColorContext();
}

void pm::MainDlg::OnWbSpinRedChanged(wxSpinDoubleEvent& WXUNUSED(event))
{
    const float scale = (float)m_editDblRed->GetValue();

    m_settings.SetColorWbScaleRed(scale);
    m_sliderRed->SetValue(ConvertWbScaleToSliderValue(scale));

    UiUpdateColorContext();
}

void pm::MainDlg::OnWbSliderGreenChanged(wxCommandEvent& WXUNUSED(event))
{
    const float scale = ConvertWbSliderValueToScale(m_sliderGreen->GetValue());

    m_settings.SetColorWbScaleGreen(scale);
    m_editDblGreen->SetValue(scale);

    UiUpdateColorContext();
}

void pm::MainDlg::OnWbSpinGreenChanged(wxSpinDoubleEvent& WXUNUSED(event))
{
    const float scale = (float)m_editDblGreen->GetValue();

    m_settings.SetColorWbScaleGreen(scale);
    m_sliderGreen->SetValue(ConvertWbScaleToSliderValue(scale));

    UiUpdateColorContext();
}

void pm::MainDlg::OnWbSliderBlueChanged(wxCommandEvent& WXUNUSED(event))
{
    const float scale = ConvertWbSliderValueToScale(m_sliderBlue->GetValue());

    m_settings.SetColorWbScaleBlue(scale);
    m_editDblBlue->SetValue(scale);

    UiUpdateColorContext();
}

void pm::MainDlg::OnWbSpinBlueChanged(wxSpinDoubleEvent& WXUNUSED(event))
{
    const float scale = (float)m_editDblBlue->GetValue();

    m_settings.SetColorWbScaleBlue(scale);
    m_sliderBlue->SetValue(ConvertWbScaleToSliderValue(scale));

    UiUpdateColorContext();
}

void pm::MainDlg::OnDebayerAlgorithmChanged(wxCommandEvent& WXUNUSED(event))
{
    const int selection = m_cbDebayerAlgorithm->GetSelection();
    if (selection == wxNOT_FOUND)
        return;
    const auto item = static_cast<ParamEnumItem* const>(
            m_cbDebayerAlgorithm->GetClientData((unsigned int)selection));

    m_settings.SetColorDebayerAlgorithm(item->GetValue());

    UiUpdateColorContext();
}

void pm::MainDlg::OnAutoExposureAlgorithmChanged(wxCommandEvent& WXUNUSED(event))
{
    const int selection = m_cbAutoExposureAlgorithm->GetSelection();
    if (selection == wxNOT_FOUND)
        return;
    const auto item = static_cast<ParamEnumItem* const>(
            m_cbAutoExposureAlgorithm->GetClientData((unsigned int)selection));

    m_autoExposureAlgorithm =
        static_cast<PH_COLOR_AUTOEXP_ALG>(item->GetValue());

    UiUpdateColorContext();
}

void pm::MainDlg::OnColorCpuOnlyClicked(wxCommandEvent& WXUNUSED(event))
{
    const bool checked = m_chbColorCpuOnly->GetValue();
    m_settings.SetColorCpuOnly(checked);

    UiUpdateColorContext();
}

void pm::MainDlg::OnParamPortSpeedChanged(ParamBase& /*param*/,
        bool allAttrsChanged)
{
    if (allAttrsChanged)
    {
        // Combo box content update not needed in this UI
    }
    // Update selection
    const auto& speeds = m_camera->GetSpeedTable();
    const auto portIndex =
        m_camera->GetParams().Get<PARAM_READOUT_PORT>()->GetCur();
    const auto speedIndex =
        m_camera->GetParams().Get<PARAM_SPDTAB_INDEX>()->GetCur();
    for (size_t n = 0; n < speeds.size(); n++)
    {
        const Camera::Speed* speed = &speeds.at(n);
        if (speed->port.GetValue() == portIndex
                && speed->speedIndex == speedIndex)
        {
            m_cbPortSpeed->SetSelection(n);
            break;
        }
    }
}

void pm::MainDlg::OnParamGainIndexChanged(ParamBase&  /*param*/,
        bool allAttrsChanged)
{
    const Camera::Speed* curSpeed = nullptr;
    const auto& speeds = m_camera->GetSpeedTable();
    const auto portIndex =
        m_camera->GetParams().Get<PARAM_READOUT_PORT>()->GetCur();
    const auto speedIndex =
        m_camera->GetParams().Get<PARAM_SPDTAB_INDEX>()->GetCur();
    for (size_t n = 0; n < speeds.size(); n++)
    {
        const Camera::Speed* speed = &speeds.at(n);
        if (speed->port.GetValue() == portIndex
                && speed->speedIndex == speedIndex)
        {
            curSpeed = speed;
            break;
        }
    }
    if (IsError(curSpeed != nullptr,
                "Invalid combination of port and speed index"))
        return;

    const auto p = m_camera->GetParams().Get<PARAM_GAIN_INDEX>();
    const auto cur = p->GetCur();
    if (allAttrsChanged)
    {
        // Update items
        m_cbGain->Clear();
        for (size_t n = 0; n < curSpeed->gains.size(); n++)
        {
            const auto gain = &curSpeed->gains.at(n);
            m_cbGain->Append(gain->label, const_cast<Camera::Speed::Gain*>(gain));
        }
    }
    // Update selection
    for (size_t n = 0; n < curSpeed->gains.size(); n++)
    {
        const auto gain = &curSpeed->gains.at(n);
        if (gain->index == cur)
        {
            m_cbGain->SetSelection(n);
            break;
        }
    }
}

void pm::MainDlg::OnParamColorModeChanged(ParamBase& /*param*/,
        bool /*allAttrsChanged*/)
{
    m_colorMask = m_camera->GetParams().Get<PARAM_COLOR_MODE>()->IsAvail()
        ? m_camera->GetParams().Get<PARAM_COLOR_MODE>()->GetCur()
        : COLOR_NONE;

    UiUpdateColorContext();
}

void pm::MainDlg::OnParamBitDepthChanged(ParamBase& /*param*/,
        bool /*allAttrsChanged*/)
{
    UiUpdateColorContext();
}

void pm::MainDlg::OnParamEmGainChanged(ParamBase& /*param*/,
        bool allAttrsChanged)
{
    auto p = m_camera->GetParams().Get<PARAM_GAIN_MULT_FACTOR>();
    if (allAttrsChanged)
    {
        if (p->IsAvail())
            m_editEmGain->SetRange(p->GetMin(), p->GetMax());
        else
            m_editEmGain->SetRange(0, 0);
    }
    m_editEmGain->SetValue(p->IsAvail() ? p->GetCur() : 0);
}

void pm::MainDlg::OnParamExpResIndexChanged(ParamBase& /*param*/,
        bool allAttrsChanged)
{
    auto p = m_camera->GetParams().Get<PARAM_EXP_RES_INDEX>();
    const int32_t cur = (p->IsAvail()) ? (int32_t)p->GetCur() : EXP_RES_ONE_MILLISEC;
    if (allAttrsChanged)
    {
        // Combo box content update not needed in this UI
    }
    // Update selection
    for (size_t n = 0; n < m_expTimeResolutions.size(); n++)
    {
        const auto item = &m_expTimeResolutions.at(n);
        if (item->GetValue() == cur)
        {
            m_cbExpTimeRes->SetSelection(n);
            break;
        }
    }

    // Sync. value in Settings
    m_settings.SetExposureResolution(cur);

    // Re-validate exposure time values
    auto min = m_expTimeResLimits.at(cur).first;
    auto max = m_expTimeResLimits.at(cur).second;

    if (m_settings.GetTrigMode() == VARIABLE_TIMED_MODE)
    {
        min = pm::Clamp<uint32_t>(min, 1, std::numeric_limits<uint16_t>::max());
        max = pm::Clamp<uint32_t>(max, 1, std::numeric_limits<uint16_t>::max());
        auto exposures = m_settings.GetVtmExposures();
        for (size_t n = 0; n < exposures.size(); ++n)
        {
            if (exposures[n] < min)
            {
                exposures[n] = min;
                Log::LogW("Corrected %zu. VTM exposure time to min. value %u",
                        n, min);
            }
            if (exposures[n] > max)
            {
                exposures[n] = max;
                Log::LogW("Corrected %zu. VTM exposure time to max. value %u",
                        n, max);
            }
        }
        m_settings.SetVtmExposures(exposures);
    }
    else if (!m_smartExposures.empty())
    {
        for (size_t n = 0; n < m_smartExposures.size(); ++n)
        {
            if (m_smartExposures[n] < min)
            {
                m_smartExposures[n] = min;
                Log::LogW("Corrected %zu. Smart Streaming exposure time to min. value %u",
                        n, min);
            }
            if (m_smartExposures[n] > max)
            {
                m_smartExposures[n] = max;
                Log::LogW("Corrected %zu. Smart Streaming exposure time to max. value %u",
                        n, max);
            }
        }
    }
    else
    {
        auto exposure = m_settings.GetExposure();
        if (exposure < min)
        {
            exposure = min;
            Log::LogW("Corrected exposure time to min. value %u", min);
        }
        if (exposure > max)
        {
            exposure = max;
            Log::LogW("Corrected exposure time to max. value %u", max);
        }
        m_settings.SetExposure(exposure);
    }

    UiUpdateExposures();
}

void pm::MainDlg::OnParamExposureModeChanged(ParamBase& /*param*/,
        bool allAttrsChanged)
{
    auto p = m_camera->GetParams().Get<PARAM_EXPOSURE_MODE>();
    // TODO: Switch to commented line once we do acq. setup on every change
    const auto cur = m_settings.GetTrigMode();
    //const auto cur = p->GetCur();
    if (allAttrsChanged)
    {
        // Update items
        m_triggerModes = p->GetItems();
        m_cbTriggerMode->Clear();
        for (size_t n = 0; n < m_triggerModes.size(); n++)
        {
            const auto item = &m_triggerModes.at(n);
            m_cbTriggerMode->Append(item->GetName(), item);
        }
    }
    // Update selection
    for (size_t n = 0; n < m_triggerModes.size(); n++)
    {
        const auto item = &m_triggerModes.at(n);
        if (item->GetValue() == cur)
        {
            m_cbTriggerMode->SetSelection(n);
            break;
        }
    }

    if (cur == VARIABLE_TIMED_MODE)
    {
        m_cbTriggerMode->SetToolTip(
                _("'Variable Timed' mode works in 'time-lapse' acquisition modes only"));
    }
    else
    {
        m_cbTriggerMode->UnsetToolTip();

        // Updates m_smartExposures in case SS is enabled
        auto paramSsEn = m_camera->GetParams().Get<PARAM_SMART_STREAM_MODE_ENABLED>();
        OnParamSmartStreamModeEnabledChanged(*paramSsEn, false);
    }

    UiUpdateExposures();
}

void pm::MainDlg::OnParamExposeOutModeChanged(ParamBase& /*param*/,
        bool allAttrsChanged)
{
    auto p = m_camera->GetParams().Get<PARAM_EXPOSE_OUT_MODE>();
    if (!p->IsAvail())
    {
        m_expOutModes.clear();
        m_cbExpOutMode->Clear();
        m_cbExpOutMode->Append("No expose out modes");
        m_cbExpOutMode->SetSelection(0);
        return;
    }
    // TODO: Switch to commented line once we do acq. setup on every change
    const auto cur = m_settings.GetExpOutMode();
    //const auto cur = p->GetCur();
    if (allAttrsChanged)
    {
        // Update items
        m_expOutModes = p->GetItems();
        m_cbExpOutMode->Clear();
        for (size_t n = 0; n < m_expOutModes.size(); n++)
        {
            const auto item = &m_expOutModes.at(n);
            m_cbExpOutMode->Append(item->GetName(), item);
        }
    }
    // Update selection
    for (size_t n = 0; n < m_expOutModes.size(); n++)
    {
        const auto item = &m_expOutModes.at(n);
        if (item->GetValue() == cur)
        {
            m_cbExpOutMode->SetSelection(n);
            break;
        }
    }
}

void pm::MainDlg::OnParamClearModeChanged(ParamBase& /*param*/,
        bool allAttrsChanged)
{
    auto p = m_camera->GetParams().Get<PARAM_CLEAR_MODE>();
    const auto cur = p->GetCur();
    if (allAttrsChanged)
    {
        // Update items
        m_clearModes = p->GetItems();
        m_cbClearMode->Clear();
        for (size_t n = 0; n < m_clearModes.size(); n++)
        {
            const auto item = &m_clearModes.at(n);
            m_cbClearMode->Append(item->GetName(), item);
        }
    }
    // Update selection
    for (size_t n = 0; n < m_clearModes.size(); n++)
    {
        const auto item = &m_clearModes.at(n);
        if (item->GetValue() == cur)
        {
            m_cbClearMode->SetSelection(n);
            break;
        }
    }
}

void pm::MainDlg::OnParamClearCyclesChanged(ParamBase& /*param*/,
        bool allAttrsChanged)
{
    auto p = m_camera->GetParams().Get<PARAM_CLEAR_CYCLES>();
    if (allAttrsChanged)
    {
        if (p->IsAvail())
            m_editClearCycles->SetRange(p->GetMin(), p->GetMax());
        else
            m_editClearCycles->SetRange(0, 0);
    }
    m_editClearCycles->SetValue(p->IsAvail() ? p->GetCur() : 0);
}

void pm::MainDlg::OnParamPModeChanged(ParamBase& /*param*/,
        bool allAttrsChanged)
{
    auto p = m_camera->GetParams().Get<PARAM_PMODE>();
    const auto cur = p->GetCur();
    if (allAttrsChanged)
    {
        // Update items
        m_pModes = p->GetItems();
        m_cbPMode->Clear();
        for (size_t n = 0; n < m_pModes.size(); n++)
        {
            const auto item = &m_pModes.at(n);
            m_cbPMode->Append(item->GetName(), item);
        }
    }
    // Update selection
    for (size_t n = 0; n < m_pModes.size(); n++)
    {
        const auto item = &m_pModes.at(n);
        if (item->GetValue() == cur)
        {
            m_cbPMode->SetSelection(n);
            break;
        }
    }
}

void pm::MainDlg::OnParamSmartStreamModeEnabledChanged(ParamBase& /*param*/,
        bool /*allAttrsChanged*/)
{
    auto paramSsEn = m_camera->GetParams().Get<PARAM_SMART_STREAM_MODE_ENABLED>();
    auto paramSsExps = m_camera->GetParams().Get<PARAM_SMART_STREAM_EXP_PARAMS>();
    if (paramSsEn->IsAvail() && paramSsEn->GetCur() && paramSsExps->IsAvail())
    {
        const auto ssExps = paramSsExps->GetCur();
        m_smartExposures.assign(ssExps->params, ssExps->params + ssExps->entries);
        m_smartExposuresMax = paramSsExps->GetMax()->entries;
    }
    else
    {
        m_smartExposures.clear();
    }

    UiUpdateExposures();
}

void pm::MainDlg::OnParamSmartStreamExpParamsChanged(ParamBase& /*param*/,
        bool /*allAttrsChanged*/)
{
    auto paramSsEn = m_camera->GetParams().Get<PARAM_SMART_STREAM_MODE_ENABLED>();
    auto paramSsExps = m_camera->GetParams().Get<PARAM_SMART_STREAM_EXP_PARAMS>();
    if (paramSsEn->IsAvail() && paramSsEn->GetCur() && paramSsExps->IsAvail())
    {
        const auto ssExps = paramSsExps->GetCur();
        m_smartExposures.assign(ssExps->params, ssExps->params + ssExps->entries);
        m_smartExposuresMax = paramSsExps->GetMax()->entries;
    }
    else
    {
        m_smartExposures.clear();
    }

    UiUpdateExposures();
}

void pm::MainDlg::OnParamCentroidsEnabledChanged(ParamBase& /*param*/,
        bool /*allAttrsChanged*/)
{
    const auto usesMetadata =
        m_camera->GetParams().Get<PARAM_METADATA_ENABLED>()->IsAvail()
        && m_camera->GetParams().Get<PARAM_METADATA_ENABLED>()->GetCur();
    const auto usesCentroids = usesMetadata
        && m_camera->GetParams().Get<PARAM_CENTROIDS_ENABLED>()->IsAvail()
        && m_camera->GetParams().Get<PARAM_CENTROIDS_ENABLED>()->GetCur();
    m_chbCentroidsEnabled->SetValue(usesCentroids);

    UiSetCentroidControlsEnabled(true);
}

void pm::MainDlg::OnParamCentroidsModeChanged(ParamBase& /*param*/,
        bool allAttrsChanged)
{
    auto p = m_camera->GetParams().Get<PARAM_CENTROIDS_MODE>();
    if (!p->IsAvail())
    {
        m_centroidsModes.clear();
        m_chCentroidsMode->Clear();
        m_chCentroidsMode->Append("Locate");
        m_chCentroidsMode->SetSelection(0);
        return;
    }
    const auto cur = p->GetCur();
    if (allAttrsChanged)
    {
        // Update items
        m_centroidsModes = p->GetItems();
        m_chCentroidsMode->Clear();
        for (size_t n = 0; n < m_centroidsModes.size(); n++)
        {
            const auto item = &m_centroidsModes.at(n);
            m_chCentroidsMode->Append(item->GetName(), item);
        }
    }
    // Update selection
    for (size_t n = 0; n < m_centroidsModes.size(); n++)
    {
        const auto item = &m_centroidsModes.at(n);
        if (item->GetValue() == cur)
        {
            m_chCentroidsMode->SetSelection(n);
            break;
        }
    }

    UiSetCentroidControlsEnabled(true);

    // Update centroid model items
#if 0
    const int centroidsCountMax =
        m_camera->GetParams().Get<PARAM_CENTROIDS_COUNT>()->GetMax();
    m_centroidModel->SetMaxRegionCount(centroidsCountMax);
#endif
}

void pm::MainDlg::OnParamCentroidsCountChanged(ParamBase& /*param*/,
        bool allAttrsChanged)
{
    auto p = m_camera->GetParams().Get<PARAM_CENTROIDS_COUNT>();
    if (allAttrsChanged)
    {
        if (p->IsAvail())
            m_editCentroidsCount->SetRange(p->GetMin(), p->GetMax());
        else
            m_editCentroidsCount->SetRange(0, 0);
    }
    m_editCentroidsCount->SetValue(p->IsAvail() ? p->GetCur() : 0);
}

void pm::MainDlg::OnParamCentroidsRadiusChanged(ParamBase& /*param*/,
        bool allAttrsChanged)
{
    auto p = m_camera->GetParams().Get<PARAM_CENTROIDS_RADIUS>();
    if (allAttrsChanged)
    {
        if (p->IsAvail())
            m_editCentroidsRadius->SetRange(p->GetMin(), p->GetMax());
        else
            m_editCentroidsRadius->SetRange(0, 0);
    }
    m_editCentroidsRadius->SetValue(p->IsAvail() ? p->GetCur() : 0);
}

void pm::MainDlg::OnParamCentroidsBgCountChanged(ParamBase& /*param*/,
        bool allAttrsChanged)
{
    auto p = m_camera->GetParams().Get<PARAM_CENTROIDS_BG_COUNT>();
    if (!p->IsAvail())
    {
        m_centroidsBgCounts.clear();
        m_chCentroidsBgCount->Clear();
        m_chCentroidsBgCount->Append("0");
        m_chCentroidsBgCount->SetSelection(0);
        return;
    }
    const auto cur = p->GetCur();
    if (allAttrsChanged)
    {
        // Update items
        m_centroidsBgCounts = p->GetItems();
        m_chCentroidsBgCount->Clear();
        for (size_t n = 0; n < m_centroidsBgCounts.size(); n++)
        {
            const auto item = &m_centroidsBgCounts.at(n);
            m_chCentroidsBgCount->Append(item->GetName(), item);
        }
    }
    // Update selection
    for (size_t n = 0; n < m_centroidsBgCounts.size(); n++)
    {
        const auto item = &m_centroidsBgCounts.at(n);
        if (item->GetValue() == cur)
        {
            m_chCentroidsBgCount->SetSelection(n);
            break;
        }
    }
}

void pm::MainDlg::OnParamCentroidsThresholdChanged(ParamBase& /*param*/,
        bool allAttrsChanged)
{
    auto p = m_camera->GetParams().Get<PARAM_CENTROIDS_THRESHOLD>();
    if (allAttrsChanged)
    {
        if (p->IsAvail())
        {
            m_editDblCentroidsThreshold->SetRange(
                    FixedPointToReal<double, uint32_t>(8, 4, p->GetMin()),
                    FixedPointToReal<double, uint32_t>(8, 4, p->GetMax()));
            m_editDblCentroidsThreshold->SetIncrement(
                    FixedPointToReal<double, uint32_t>(8, 4, p->GetInc()));
        }
        else
        {
            m_editDblCentroidsThreshold->SetRange(0.0, 0.0);
        }
    }
    if (p->IsAvail())
        m_editDblCentroidsThreshold->SetValue(
                FixedPointToReal<double, uint32_t>(8, 4, p->GetCur()));
    else
        m_editDblCentroidsThreshold->SetValue(0.0);
}

void pm::MainDlg::OnUiUpdateTimerTimeout(wxTimerEvent& WXUNUSED(event))
{
    std::lock_guard<std::mutex> lock(m_uiUpdateMutex);

    if (m_fpsLimiter)
    {
        m_fpsLimiter->InputTimerTick();
    }

    if (!m_uiLogEntries.empty())
    {
        std::string text;
        while (!m_uiLogEntries.empty())
        {
            const Log::Entry entry = m_uiLogEntries.front();
            m_uiLogEntries.pop();

            char level = '-';
            bool consoleOnly = false;
            switch (entry.GetLevel())
            {
            case Log::Level::Error:
                level = 'E';
                break;
            case Log::Level::Warning:
                level = 'W';
                break;
            case Log::Level::Info:
                level = 'I';
                break;
            case Log::Level::Debug:
                level = 'D';
                consoleOnly = true;
                break;
            case Log::Level::Progress:
                level = 'P';
                consoleOnly = true;
                break;
            // default section missing intentionally so compiler warns when new value added
            }

            if (!consoleOnly)
            {
                const std::string msg =
                    std::string("[") + level + "] " + entry.GetText() + '\n';
                text += msg;
            }
        }
        if (!text.empty())
        {
            m_lblLogs->AppendText(text);
            m_lblLogs->ScrollLines(2);
            m_lblLogs->ShowPosition(m_lblLogs->GetLastPosition());
        }
    }

    if (m_uiFrameSliderUpdated)
    {
        m_uiFrameSliderUpdated = false;
        UiUpdateFrameSliderValue(m_uiFrameSliderIndex, m_uiFrameSliderFrameNr);
    }
    if (m_uiFrameRateUpdated)
    {
        m_uiFrameRateUpdated = false;
        UiUpdateFrameRate(m_uiFrameRateAcq);
    }
    if (m_uiAcqStatsUpdated)
    {
        m_uiAcqStatsUpdated = false;
        UiUpdateAcqStats(m_uiAcqStatsValid, m_uiAcqStatsLost, m_uiAcqStatsMax,
                m_uiAcqStatsCached);
    }
    if (m_uiDiskStatsUpdated)
    {
        m_uiDiskStatsUpdated = false;
        UiUpdateDiskStats(m_uiDiskStatsValid, m_uiDiskStatsLost,
                m_uiDiskStatsMax, m_uiDiskStatsCached);
    }
    if (m_uiProgressUpdated)
    {
        m_uiProgressUpdated = false;
        UiUpdateProgressValue(m_uiProgressIsInf, m_uiProgressRange, m_uiProgressValue);
    }

    if (m_uiFrameDescUpdated)
    {
        m_uiFrameDescUpdated = false;
        UiUpdateFrameDescText(m_uiFrameDescText);
    }
}

void pm::MainDlg::OnEventStartTask(wxThreadEvent& WXUNUSED(event))
{
    if (CreateThread(wxTHREAD_JOINABLE) != wxTHREAD_NO_ERROR)
    {
        IsError(false, "Could not create the worker thread!");
    }
    else if (GetThread()->Run() != wxTHREAD_NO_ERROR)
    {
        IsError(false, "Could not run the worker thread!");
    }

    // Set progress to start
    DoUpdateProgress(0, m_uiProgressRange, 0);

    // Clear list of centroids, the model is updated at the end of acquisition
    m_centroidModel->DeleteAllRegions();
}

void pm::MainDlg::OnEventTaskDone(wxThreadEvent& event)
{
    switch (m_taskType)
    {
    case pm::TaskType::FIND_CAMERA:
        OnFindCameraTaskDone(event);
        m_showHelpNow = true;
        ShowHelp();
        break;
    case pm::TaskType::SETUP_ACQUISITION:
        OnSetupAcquisitionTaskDone(event);
        break;
    case pm::TaskType::RUN_ACQUISITION:
        OnRunAcquisitionTaskDone(event);
        break;
    case pm::TaskType::AUTO_EXP_AND_WHITE_BALANCE:
        OnAutoExpAndWhiteBalanceTaskDone(event);
        break;
    }
}

void pm::MainDlg::OnEventShowHelp(wxCommandEvent& event)
{
    // Show help message when log queue is empty, re-post otherwise
    if (!Log::Flush())
    {
        wxQueueEvent(GetEventHandler(), &event);
    }
    else
    {
        const wxString message = event.GetString();

        wxMessageDialog helpDialog(this, message, _("Help"));
        helpDialog.SetLayoutAdaptationMode(wxDIALOG_ADAPTATION_MODE_ENABLED);
        helpDialog.ShowModal();
    }
}

void pm::MainDlg::OnEventAcqStarted(wxCommandEvent& WXUNUSED(event))
{
    m_btnStop->Enable(true);

    if (m_taskType != TaskType::AUTO_EXP_AND_WHITE_BALANCE)
    {
        UiSetColorControlsEnabled(true);
        UiSetCentroidControlsEnabled(true);
    }
}

void pm::MainDlg::OnEventAcqFinished(wxCommandEvent& WXUNUSED(event))
{
    bool expected = false;
    // Atomically sets m_userAbortFlag to true only if it was false
    if (m_userAbortFlag.compare_exchange_strong(expected, true))
    {
        // Stop button stays enabled for abort
        m_btnStop->SetLabelText("Abort");
    }
}

void pm::MainDlg::OnFindCameraTaskDone(wxThreadEvent& WXUNUSED(event))
{
    if (IsError(!m_cameraNames.empty(), "No cameras found!"))
        return;

    // If the camera index was not set via CLI options, let user to select it
    const auto& cliOptions = m_optionController.GetAllProcessedOptions();
    const bool wasCamIndexSet =
        cliOptions.cend() != std::find_if(cliOptions.cbegin(), cliOptions.cend(),
            [](const Option& o) {
                return o.GetId() == static_cast<uint32_t>(OptionId::CamIndex);
            });
    if (!wasCamIndexSet)
    {
        if (m_cameraNames.size() == 1)
        {
            m_settings.SetCamIndex(0);
        }
        else
        {
            wxArrayString choices;
            for (auto name : m_cameraNames)
            {
                choices.Add(name);
            }

            wxSingleChoiceDialog dialog(this, _("Please select camera to open"),
                    _("Camera Selection"), choices);
            dialog.SetSelection(0);
            if (dialog.ShowModal() != wxID_OK)
            {
                // Exit application on cancel
                Close();
                return; // Close() just posts an event which is handled later
            }
            m_settings.SetCamIndex(static_cast<int16_t>(dialog.GetSelection()));
        }
    }

    bool ok;
    try
    {
        // ATM can fail either by throwing an exception or returning false
        ok = OnFindCameraTaskDone_InitData() && OnFindCameraTaskDone_InitGui();
    }
    catch (const Exception& ex)
    {
        Log::LogE("Failure initializing camera - %s", ex.what());
        ok = false;
    }
    if (!ok)
    {
        if (m_camera->IsOpen())
        {
            // Ignore errors
            m_camera->Close();
        }
    }
}

bool pm::MainDlg::OnFindCameraTaskDone_InitData()
{
    if (IsError(static_cast<size_t>(m_settings.GetCamIndex()) < m_cameraNames.size(),
                std::string("There is not so many cameras to select from index ")
                    + std::to_string(m_settings.GetCamIndex())))
        return false;

    const std::string& camName =
        m_cameraNames.at(static_cast<size_t>(m_settings.GetCamIndex()));
    Log::LogI("Chosen camera at index %d: '%s'", m_settings.GetCamIndex(),
            camName.c_str());

    if (!m_camera->Open(camName, &MainDlg::CameraRemovalCallback, this))
        return false;

    if (!m_camera->AddCliOptions(m_optionController, true))
        return false;

    const auto& cliAllOptions = m_optionController.GetOptions();
    const bool cliParseOk = m_optionController.ProcessOptions(
            wxTheApp->argc, wxTheApp->argv, cliAllOptions);
    if (!cliParseOk || m_showFullHelp)
    {
        SetHelpText((m_showFullHelp)
                ? cliAllOptions
                : m_optionController.GetFailedProcessedOptions());

        if (!cliParseOk)
        {
            Log::LogW("Trying to recover default values for failed option handlers...\n");
        }
    }

    // TODO: Remove once param options moved from Settings to Camera
    if (!m_camera->ReviseSettings(m_settings, m_optionController, true))
        return false;

    const auto width = m_camera->GetParams().Get<PARAM_SER_SIZE>()->GetCur();
    const auto height = m_camera->GetParams().Get<PARAM_PAR_SIZE>()->GetCur();
    m_camFullRect = wxRect(0, 0, width, height);

    m_expTimeResolutions.clear();
    auto paramExpRes = m_camera->GetParams().Get<PARAM_EXP_RES>();
    if (!paramExpRes->IsAvail())
    {
        // Parameter not available, camera supports only milliseconds resolution
        m_expTimeResolutions.emplace_back(EXP_RES_ONE_MILLISEC, "ms");
    }
    else
    {
        auto items = paramExpRes->GetItems();
        // Override too long item names with time units
        for (auto& uiItem : items)
        {
            // Use custom strings
            std::string name;
            switch (uiItem.GetValue())
            {
            case EXP_RES_ONE_MICROSEC:
                name = "\u00b5s"; // "us" with Unicode micro sign
                break;
            case EXP_RES_ONE_MILLISEC:
                name = "ms";
                break;
            case EXP_RES_ONE_SEC:
                name = "s";
                break;
            default:
                Log::LogE("Unknown exposure time resolution");
                return false;
            }
            m_expTimeResolutions.emplace_back(uiItem.GetValue(), name);
        }
    }
    auto paramExposureTime = m_camera->GetParams().Get<PARAM_EXPOSURE_TIME>();
    auto paramExpResIndex = m_camera->GetParams().Get<PARAM_EXP_RES_INDEX>();
    // PARAM_EXP_RES if available was read-only in older PVCAMs
    if (paramExposureTime->IsAvail() && paramExpResIndex->IsAvail())
    {
        // Save current value before scan
        const auto curRes = paramExpResIndex->GetCur();
        // Scan for exposure time range for each resolution
        for (size_t n = 0; n < m_expTimeResolutions.size(); n++)
        {
            const auto expRes = m_expTimeResolutions.at(n).GetValue();
            paramExpResIndex->SetCur(static_cast<uint16_t>(expRes));
            const auto min = static_cast<uint32_t>(std::min<uint64_t>(
                        paramExposureTime->GetMin(),
                        std::numeric_limits<uint32_t>::max()));
            const auto max = static_cast<uint32_t>(std::min<uint64_t>(
                        paramExposureTime->GetMax(),
                        std::numeric_limits<uint32_t>::max()));
            m_expTimeResLimits[expRes] = std::make_pair(min, max);
        }
        // Restore original value after scan
        paramExpResIndex->SetCur(curRes);
    }
    else
    {
        for (size_t n = 0; n < m_expTimeResolutions.size(); n++)
        {
            const auto expRes = m_expTimeResolutions.at(n).GetValue();
            m_expTimeResLimits[expRes] =
                std::make_pair<uint32_t>(0, std::numeric_limits<uint32_t>::max());
        }
    }

    m_binModes.clear();
    if (m_camera->GetParams().Get<PARAM_BINNING_SER>()->IsAvail()
            && m_camera->GetParams().Get<PARAM_BINNING_PAR>()->IsAvail())
    {
        const auto& binSerItems =
            m_camera->GetParams().Get<PARAM_BINNING_SER>()->GetItems();
        const auto& binParItems =
            m_camera->GetParams().Get<PARAM_BINNING_PAR>()->GetItems();
        for (size_t n = 0; n < binSerItems.size(); n++)
        {
            m_binModes.emplace_back(binSerItems.at(n), binParItems.at(n));
        }
    }

    // TODO: Make UI control for that, cannot use default value (it's false)
    const auto& cliOptions = m_optionController.GetAllProcessedOptions();
    const bool wasMetadataSet =
        cliOptions.cend() != std::find_if(cliOptions.cbegin(), cliOptions.cend(),
            [](const Option& o) {
                return o.GetId() == PARAM_METADATA_ENABLED;
            });
    if (!wasMetadataSet)
    {
        if (m_camera->GetParams().Get<PARAM_METADATA_ENABLED>()->IsAvail())
            m_camera->GetParams().Get<PARAM_METADATA_ENABLED>()->SetCur(true);
    }

    // Older PVCAMs don't have this parameter yet, otherwise it's always available
    const uint16_t regionCountMax =
        (m_camera->GetParams().Get<PARAM_ROI_COUNT>()->IsAvail())
        ? m_camera->GetParams().Get<PARAM_ROI_COUNT>()->GetMax() : 1;
    // Update region model items
    m_regionModel->SetMaxRegionCount(regionCountMax);
    // Remove existing regions if any
    m_regionModel->DeleteAllRegions();
    // Add new regions from settings
    for (const rgn_type& rgn : m_settings.GetRegions())
    {
        const wxRect rect(rgn.s1, rgn.p1,
                rgn.s2 + 1 - rgn.s1,
                rgn.p2 + 1 - rgn.p1);
        // Ignore error on appending
        m_regionModel->AppendRegion(std::make_shared<Region>(rect));
    }

    // Once we have possibly empty m_regionModel, fix regions in settings
    if (m_settings.GetRegions().empty())
    {
        // With no region specified use full sensor size
        std::vector<rgn_type> regions;
        rgn_type rgn;
        rgn.s1 = 0;
        rgn.s2 = width - 1;
        rgn.sbin = m_settings.GetBinningSerial();
        rgn.p1 = 0;
        rgn.p2 = height - 1;
        rgn.pbin = m_settings.GetBinningParallel();
        regions.push_back(rgn);
        // Cannot fail, the only region uses correct binning factors
        m_settings.SetRegions(regions);
    }

    m_trackShowTrajectory = (m_settings.GetTrackTrajectoryDuration() > 0);

    // Update centroid model items
#if 0
    const int centroidsCountMax =
        (m_camera->GetParams().Get<PARAM_CENTROIDS_COUNT>()->IsAvail())
        ? m_camera->GetParams().Get<PARAM_CENTROIDS_COUNT>()->GetMax()
        // Used regionCountMax as a fallback as the table actually shows a list
        // of regions in metadata in general, not only with centroids
        : regionCountMax;
    m_centroidModel->SetMaxRegionCount(centroidsCountMax);
#endif
    // Remove existing centroids if any
    m_centroidModel->DeleteAllRegions();

    return true;
}

bool pm::MainDlg::OnFindCameraTaskDone_InitGui()
{
    #define BIND_PARAM(paramId, OnChangeFn) { \
        auto param = m_camera->GetParams().Get<paramId>(); \
        OnChangeFn(*param, true); \
        param->RegisterChangeHandler(std::bind(&MainDlg::OnChangeFn, \
                    this, std::placeholders::_1, std::placeholders::_2)); \
    }

    // Update combo box with speeds, done only once and never changed
    auto paramPort = m_camera->GetParams().Get<PARAM_READOUT_PORT>();
    auto paramSpeed = m_camera->GetParams().Get<PARAM_SPDTAB_INDEX>();
    m_cbPortSpeed->Clear();
    const auto& speeds = m_camera->GetSpeedTable();
    for (size_t n = 0; n < speeds.size(); n++)
    {
        const Camera::Speed* speed = &speeds.at(n);
        m_cbPortSpeed->Append(speed->label, const_cast<Camera::Speed*>(speed));
    }
    OnParamPortSpeedChanged(*paramSpeed, false);
    paramPort->RegisterChangeHandler(
            std::bind(&MainDlg::OnParamPortSpeedChanged,
                this, std::placeholders::_1, std::placeholders::_2));
    paramSpeed->RegisterChangeHandler(
            std::bind(&MainDlg::OnParamPortSpeedChanged,
                this, std::placeholders::_1, std::placeholders::_2));

    BIND_PARAM(PARAM_GAIN_INDEX, OnParamGainIndexChanged);
    BIND_PARAM(PARAM_COLOR_MODE, OnParamColorModeChanged);
    BIND_PARAM(PARAM_BIT_DEPTH, OnParamBitDepthChanged);
    BIND_PARAM(PARAM_GAIN_MULT_FACTOR, OnParamEmGainChanged);

    // Update combo box with exposure resolutions, done only once and never changed
    auto paramExpResIndex = m_camera->GetParams().Get<PARAM_EXP_RES_INDEX>();
    m_cbExpTimeRes->Clear();
    for (size_t n = 0; n < m_expTimeResolutions.size(); n++)
    {
        const auto item = &m_expTimeResolutions.at(n);
        m_cbExpTimeRes->Append(
#ifdef _MSC_VER
                item->GetName(),
#else
                wxString::FromUTF8(item->GetName().c_str()),
#endif
                item);
    }
    OnParamExpResIndexChanged(*paramExpResIndex, false);
    paramExpResIndex->RegisterChangeHandler(
            std::bind(&MainDlg::OnParamExpResIndexChanged,
                this, std::placeholders::_1, std::placeholders::_2));

    m_editTimeLapseDelay->SetValue(m_settings.GetTimeLapseDelay());

    BIND_PARAM(PARAM_EXPOSURE_MODE, OnParamExposureModeChanged);
    BIND_PARAM(PARAM_EXPOSE_OUT_MODE, OnParamExposeOutModeChanged);
    BIND_PARAM(PARAM_CLEAR_MODE, OnParamClearModeChanged);
    BIND_PARAM(PARAM_CLEAR_CYCLES, OnParamClearCyclesChanged);
    BIND_PARAM(PARAM_PMODE, OnParamPModeChanged);

    BIND_PARAM(PARAM_SMART_STREAM_MODE_ENABLED, OnParamSmartStreamModeEnabledChanged);
    BIND_PARAM(PARAM_SMART_STREAM_EXP_PARAMS, OnParamSmartStreamExpParamsChanged);

    m_editCircFrameCount->SetValue(m_settings.GetBufferFrameCount());

    m_sliderFrame->SetMax(m_settings.GetBufferFrameCount() - 1);
    m_sliderFrame->SetValue(0);

    std::vector<ParamEnumItem> saveAsItems;
    saveAsItems.emplace_back(static_cast<ParamEnum::T>(StorageType::None), "Do not save");
    saveAsItems.emplace_back(static_cast<ParamEnum::T>(StorageType::Prd), "Save as PRD");
    saveAsItems.emplace_back(static_cast<ParamEnum::T>(StorageType::Tiff), "Save as TIFF");
    saveAsItems.emplace_back(static_cast<ParamEnum::T>(StorageType::BigTiff),
            "Save as BIG TIFF (not all viewers open it!)");
    m_cbSaveAs->Clear();
    for (size_t n = 0; n < saveAsItems.size(); n++)
    {
        const auto saveAsItem = saveAsItems.at(n);
        m_cbSaveAs->Append(saveAsItem.GetName(), (void*)(size_t)saveAsItem.GetValue());
        if (saveAsItem.GetValue() == static_cast<ParamEnum::T>(m_settings.GetStorageType()))
        {
            m_cbSaveAs->SetSelection(n);
        }
    }

    const auto saveDir = m_settings.GetSaveDir();
    m_editSaveDir->SetValue(saveDir);

    const auto saveDigits = m_settings.GetSaveDigits();
    m_editSaveDigits->SetValue(static_cast<int>(saveDigits));

    const auto saveFirst = m_settings.GetSaveFirst();
    m_chbSaveFirst->SetValue(saveFirst > 0);
    m_editSaveFirst->SetValue(static_cast<int>(saveFirst));

    const auto saveLast = m_settings.GetSaveLast();
    m_chbSaveLast->SetValue(saveLast > 0);
    m_editSaveLast->SetValue(static_cast<int>(saveLast));

    const auto saveStack = m_settings.GetMaxStackSize();
    m_chbSaveStack->SetValue(saveStack > 0);
    if (saveStack > 0)
    {
        const auto saveStackMB = pm::Clamp<size_t>(
                saveStack / 1024 / 1024, 1, std::numeric_limits<int>::max());
        m_editSaveStack->SetValue(static_cast<int>(saveStackMB));
    }

    const bool circBufferCapable =
        m_camera->GetParams().Get<PARAM_CIRC_BUFFER>()->IsAvail();
    m_acqModes.clear();
    m_acqModes.emplace_back(
            static_cast<ParamEnum::T>(AcqMode::SnapSequence),
            "Snap (sequence)");
    if (circBufferCapable)
    {
        m_acqModes.emplace_back(
                static_cast<ParamEnum::T>(AcqMode::SnapCircBuffer),
                "Snap (circ. buffer)");
    }
    m_acqModes.emplace_back(
            static_cast<ParamEnum::T>(AcqMode::SnapTimeLapse),
            "Snap (time-lapse)");
    if (circBufferCapable)
    {
        m_acqModes.emplace_back(
                static_cast<ParamEnum::T>(AcqMode::LiveCircBuffer),
                "Live (circ. buffer)");
    }
    m_acqModes.emplace_back(
            static_cast<ParamEnum::T>(AcqMode::LiveTimeLapse),
            "Live (time-lapse)");
    m_cbAcqMode->Clear();
    for (size_t n = 0; n < m_acqModes.size(); n++)
    {
        const auto acqMode = &m_acqModes.at(n);
        m_cbAcqMode->Append(acqMode->GetName(), acqMode);
        if (acqMode->GetValue() == static_cast<ParamEnum::T>(m_settings.GetAcqMode()))
        {
            m_cbAcqMode->SetSelection(n);
        }
    }
    UpdateAcqModeTooltip();

    m_fillMethods.clear();
    m_fillMethods.emplace_back(
            static_cast<ParamEnum::T>(ImageDlg::FillMethod::NoFill),
            "Nothing (no fill)");
    m_fillMethods.emplace_back(
            static_cast<ParamEnum::T>(ImageDlg::FillMethod::MeanValue),
            "Mean value");
    m_fillMethods.emplace_back(
            static_cast<ParamEnum::T>(ImageDlg::FillMethod::BlackColor),
            "Black color");
    // TODO: Uncomment once there is UI for custom color
    //m_fillMethods.emplace_back(
    //        static_cast<ParamEnum::T>(ImageDlg::FillMethod::CustomColor),
    //        "Custom color");
    m_cbFillMethod->Clear();
    for (size_t n = 0; n < m_fillMethods.size(); n++)
    {
        const auto fillMethod = &m_fillMethods.at(n);
        m_cbFillMethod->Append(fillMethod->GetName(), fillMethod);
        if (fillMethod->GetValue() == static_cast<ParamEnum::T>(m_fillMethod))
        {
            m_cbFillMethod->SetSelection(n);
        }
    }

    m_zoomFactors.clear();
    m_zoomFactors.emplace_back(
            static_cast<ParamEnum::T>(ImageDlg::ZoomFactor::In_400), "400%");
    m_zoomFactors.emplace_back(
            static_cast<ParamEnum::T>(ImageDlg::ZoomFactor::In_200), "200%");
    m_zoomFactors.emplace_back(
            static_cast<ParamEnum::T>(ImageDlg::ZoomFactor::None_100), "1:1");
    m_zoomFactors.emplace_back(
            static_cast<ParamEnum::T>(ImageDlg::ZoomFactor::Out_50), "50%");
    m_zoomFactors.emplace_back(
            static_cast<ParamEnum::T>(ImageDlg::ZoomFactor::Out_25), "25%");
    m_zoomFactors.emplace_back(
            static_cast<ParamEnum::T>(ImageDlg::ZoomFactor::Out_12_5), "12.5%");
    m_cbZoomFactor->Clear();
    for (size_t n = 0; n < m_zoomFactors.size(); n++)
    {
        const auto zoomFactor = &m_zoomFactors.at(n);
        m_cbZoomFactor->Append(zoomFactor->GetName(), zoomFactor);
        if (zoomFactor->GetValue() == static_cast<ParamEnum::T>(m_zoomFactor))
        {
            m_cbZoomFactor->SetSelection(n);
        }
    }

    DoUpdateColorCapable();

    // Enable debayering for color cameras by default
    m_colorEnabled = m_colorMask != COLOR_NONE;
    m_chbColorEnable->SetValue(m_colorEnabled);

    // Force overriding of color mask to be always checked for mono cameras
    m_colorMaskOverridden = m_colorMask == COLOR_NONE;
    m_chbColorOverrideMask->SetValue(m_colorMaskOverridden);

    if (m_colorMask != COLOR_NONE)
    {
        m_customColorMask = m_colorMask;
    }

    m_customColorMasks.clear();
    m_customColorMasks.emplace_back(static_cast<ParamEnum::T>(COLOR_RGGB),
            (m_colorMask == COLOR_RGGB) ? "R-G-G-B (native)" : "R-G-G-B");
    m_customColorMasks.emplace_back(static_cast<ParamEnum::T>(COLOR_GRBG),
            (m_colorMask == COLOR_GRBG) ? "G-R-B-G (native)" : "G-R-B-G");
    m_customColorMasks.emplace_back(static_cast<ParamEnum::T>(COLOR_GBRG),
            (m_colorMask == COLOR_GBRG) ? "G-B-R-G (native)" : "G-B-R-G");
    m_customColorMasks.emplace_back(static_cast<ParamEnum::T>(COLOR_BGGR),
            (m_colorMask == COLOR_BGGR) ? "B-G-G-R (native)" : "B-G-G-R");
    m_cbColorCustomMask->Clear();
    for (size_t n = 0; n < m_customColorMasks.size(); n++)
    {
        const auto customMask = &m_customColorMasks.at(n);
        m_cbColorCustomMask->Append(customMask->GetName(), customMask);
        if (customMask->GetValue() == static_cast<ParamEnum::T>(m_customColorMask))
        {
            m_cbColorCustomMask->SetSelection(n);
        }
    }

    m_lblScaleMin->SetLabelText(wxString::Format("%.2f", WB_SCALE_MIN));
    m_lblScaleMax->SetLabelText(wxString::Format("%.0f", WB_SCALE_MAX));

    const float wbScaleRed = m_settings.GetColorWbScaleRed();
    m_sliderRed->SetRange(ConvertWbScaleToSliderValue(WB_SCALE_MIN),
            ConvertWbScaleToSliderValue(WB_SCALE_MAX));
    m_sliderRed->SetValue(ConvertWbScaleToSliderValue(wbScaleRed));
    m_editDblRed->SetRange(WB_SCALE_MIN, WB_SCALE_MAX);
    m_editDblRed->SetValue(wbScaleRed);
    m_editDblRed->SetIncrement(WB_SCALE_INCREMENT);
    m_editDblRed->SetDigits(2);

    const float wbScaleGreen = m_settings.GetColorWbScaleGreen();
    m_sliderGreen->SetRange(ConvertWbScaleToSliderValue(WB_SCALE_MIN),
            ConvertWbScaleToSliderValue(WB_SCALE_MAX));
    m_sliderGreen->SetValue(ConvertWbScaleToSliderValue(wbScaleGreen));
    m_editDblGreen->SetRange(WB_SCALE_MIN, WB_SCALE_MAX);
    m_editDblGreen->SetValue(wbScaleGreen);
    m_editDblGreen->SetIncrement(WB_SCALE_INCREMENT);
    m_editDblGreen->SetDigits(2);

    const float wbScaleBlue = m_settings.GetColorWbScaleBlue();
    m_sliderBlue->SetRange(ConvertWbScaleToSliderValue(WB_SCALE_MIN),
            ConvertWbScaleToSliderValue(WB_SCALE_MAX));
    m_sliderBlue->SetValue(ConvertWbScaleToSliderValue(wbScaleBlue));
    m_editDblBlue->SetRange(WB_SCALE_MIN, WB_SCALE_MAX);
    m_editDblBlue->SetValue(wbScaleBlue);
    m_editDblBlue->SetIncrement(WB_SCALE_INCREMENT);
    m_editDblBlue->SetDigits(2);

    m_debayerAlgorithms.clear();
    m_debayerAlgorithms.emplace_back(
            static_cast<ParamEnum::T>(PH_COLOR_DEBAYER_ALG_NEAREST),
            "Nearest neighbor");
    m_debayerAlgorithms.emplace_back(
            static_cast<ParamEnum::T>(PH_COLOR_DEBAYER_ALG_BILINEAR),
            "Bilinear averaging");
    // TODO: Add bicubic algorithm back
    //m_debayerAlgorithms.emplace_back(
    //        static_cast<ParamEnum::T>(PH_COLOR_DEBAYER_ALG_BICUBIC),
    //        "Bicubic");
    m_cbDebayerAlgorithm->Clear();
    for (size_t n = 0; n < m_debayerAlgorithms.size(); n++)
    {
        const auto debayerAlgorithm = &m_debayerAlgorithms.at(n);
        m_cbDebayerAlgorithm->Append(debayerAlgorithm->GetName(), debayerAlgorithm);
        if (debayerAlgorithm->GetValue() == static_cast<ParamEnum::T>(m_settings.GetColorDebayerAlgorithm()))
        {
            m_cbDebayerAlgorithm->SetSelection(n);
        }
    }

    m_autoExposureAlgorithms.clear();
    m_autoExposureAlgorithms.emplace_back(
            static_cast<ParamEnum::T>(PH_COLOR_AUTOEXP_ALG_AVERAGE), "Average");
    m_autoExposureAlgorithms.emplace_back(
            static_cast<ParamEnum::T>(PH_COLOR_AUTOEXP_ALG_HISTOGRAM), "Histogram");
    m_cbAutoExposureAlgorithm->Clear();
    for (size_t n = 0; n < m_autoExposureAlgorithms.size(); n++)
    {
        const auto autoExposureAlgorithm = &m_autoExposureAlgorithms.at(n);
        m_cbAutoExposureAlgorithm->Append(autoExposureAlgorithm->GetName(),
                autoExposureAlgorithm);
        if (autoExposureAlgorithm->GetValue() == static_cast<ParamEnum::T>(m_autoExposureAlgorithm))
        {
            m_cbAutoExposureAlgorithm->SetSelection(n);
        }
    }

    m_chbColorCpuOnly->SetValue(m_settings.GetColorCpuOnly());

    m_snapFrameCount = m_settings.GetAcqFrameCount();
    m_editSnapFrameCount->SetValue(m_snapFrameCount);

    const auto width = m_camera->GetParams().Get<PARAM_SER_SIZE>()->GetCur();
    const auto height = m_camera->GetParams().Get<PARAM_PAR_SIZE>()->GetCur();
    const auto sbin = m_settings.GetBinningSerial();
    const auto pbin = m_settings.GetBinningParallel();

    // Update combo box with binning factors
    m_cbRoiBin->Clear();
    if (m_binModes.empty())
    {
        m_cbRoiBin->Append("Any comb.");
        m_cbRoiBin->SetSelection(0);
    }
    else
    {
        for (size_t n = 0; n < m_binModes.size(); n++)
        {
            const auto roiBin = &m_binModes.at(n);
            m_cbRoiBin->Append(roiBin->first.GetName(), roiBin);
            if (roiBin->first.GetValue() == static_cast<ParamEnum::T>(sbin)
                    && roiBin->second.GetValue() == static_cast<ParamEnum::T>(pbin))
            {
                m_cbRoiBin->SetSelection(n);
            }
        }
    }

    m_editRoiBinSer->SetRange(1, width);
    m_editRoiBinSer->SetValue(sbin);
    m_editRoiBinPar->SetRange(1, height);
    m_editRoiBinPar->SetValue(pbin);

    const int regionCountMax = m_regionModel->GetMaxRegionCount();

    if (!m_dvlRois->Initialize(wxSize(width, height), regionCountMax,
                wxDATAVIEW_CELL_EDITABLE))
        return false;
    if (!m_dvlRois->AssociateModel(m_regionModel.get()))
        return false;

    if (!m_dvlCentroids->Initialize(wxSize(width, height), regionCountMax,
                wxDATAVIEW_CELL_INERT))
        return false;
    if (!m_dvlCentroids->AssociateModel(m_centroidModel.get()))
        return false;

    m_chbRoiMultiWin->SetValue(m_roiMultiWindow);

    BIND_PARAM(PARAM_METADATA_ENABLED, OnParamCentroidsEnabledChanged);
    BIND_PARAM(PARAM_CENTROIDS_ENABLED, OnParamCentroidsEnabledChanged);
    BIND_PARAM(PARAM_CENTROIDS_COUNT, OnParamCentroidsCountChanged);
    BIND_PARAM(PARAM_CENTROIDS_RADIUS, OnParamCentroidsRadiusChanged);
    BIND_PARAM(PARAM_CENTROIDS_MODE, OnParamCentroidsModeChanged);
    BIND_PARAM(PARAM_CENTROIDS_BG_COUNT, OnParamCentroidsBgCountChanged);
    BIND_PARAM(PARAM_CENTROIDS_THRESHOLD, OnParamCentroidsThresholdChanged);
    m_editDblCentroidsThreshold->SetDigits(4); // Fixed-point real in format Q8.4
    m_editDblCentroidsThreshold->SetSnapToTicks(true); // Rounds to nearest increment

    m_editTrackLinkFrames->SetValue(m_settings.GetTrackLinkFrames());
    m_editTrackMaxDist->SetRange(1, (int)std::hypot(width, height));
    m_editTrackMaxDist->SetValue(m_settings.GetTrackMaxDistance());
    m_chbTrackCpuOnly->SetValue(m_settings.GetTrackCpuOnly());
    m_chbTrackTraj->SetValue(m_trackShowTrajectory);
    m_editTrackTraj->SetValue(m_settings.GetTrackTrajectoryDuration());
    m_chbMetaRoiOutline->SetValue(m_metaRoiShowOutline);

    m_chbRoiOutline->SetValue(m_roiShowOutline);
    m_chbLiveImage->SetValue(m_showLiveImage);

    m_chbAutoConbright->SetValue(m_imageAutoConbright);
    m_editContrast->SetValue(m_imageContrast);
    m_sliderContrast->SetValue(m_imageContrast);
    m_editBrightness->SetValue(m_imageBrightness);
    m_sliderBrightness->SetValue(m_imageBrightness);

    #undef BIND_PARAM

    // Update and show preview image window.
    // Before doing so, we need to pass settings to camera.
    // Ignore error, if any, it's not critical and user can correct it in UI
    try
    {
        m_camera->SetupExp(m_settings);
    }
    catch (const Exception& ex)
    {
        Log::LogW("Failure applying settings - %s, ignoring", ex.what());
    }

    if (IsError(m_imageDlg != nullptr, "Image window not open properly"))
        return false;

    // Position has to be known before SetImageFormat is called
    m_imageDlg->SetPosition(GetRect().GetTopRight());
    if (IsError(m_imageDlg->SetImageFormat(-1, m_camera.get(),
                    m_regionModel.get()),
                "Preview image window not configured properly"))
        return false;

    m_imageDlg->SetFillMethod(m_fillMethod);
    m_imageDlg->SetAutoConbright(m_imageAutoConbright);
    m_imageDlg->SetContrast(m_imageContrast);
    m_imageDlg->SetBrightness(m_imageBrightness);
    m_imageDlg->SetZoomFactor(m_zoomFactor);
    m_imageDlg->SetColorEnabled(m_colorCapable && m_colorEnabled);
    UiUpdateColorContext();
    m_imageDlg->SetTrackShowTrajectory(m_trackShowTrajectory);
    m_imageDlg->SetMetaRoiShowOutline(m_metaRoiShowOutline);
    m_imageDlg->SetRoiShowOutline(m_roiShowOutline);
    ActivateWindow();

    // Reset stats
    DoUpdateFrameRate(0.0);
    DoUpdateAcqStats(0, 0, 1, 0);
    DoUpdateDiskStats(0, 0, 1, 0);

    // Enable controls
    UiSetAllControlsEnabled(true);

    return true;
}

void pm::MainDlg::OnSetupAcquisitionTaskDone(wxThreadEvent& event)
{
    if (m_isAcqConfigured)
    {
        m_isAcqConfigured = false;
        do
        {
            // Update slider range
            m_sliderFrame->SetMax(m_settings.GetBufferFrameCount() - 1);
            m_sliderFrame->SetValue(0); // Doesn't emit any event
            // Emit one of bound event to update the frame view
            wxScrollEvent evt(wxEVT_SCROLL_THUMBTRACK, m_sliderFrame->GetId(), 0);
            m_sliderFrame->GetEventHandler()->ProcessEvent(evt);

            // Update preview image window
            if (IsError(m_imageDlg->SetImageFormat(-1, m_camera.get(),
                            m_regionModel.get()),
                        "Preview image window not configured properly"))
                break;

            // Update sub-region image windows if needed
            if (!m_roiMultiWindow)
            {
                m_roiImageDlgs.clear(); // Items' memory release automatically
            }
            else
            {
                const unsigned int regionCount = m_regionModel->GetRegionCount();
                // Remove surplus windows
                if (m_roiImageDlgs.size() > regionCount)
                {
                    m_roiImageDlgs.erase(m_roiImageDlgs.begin() + regionCount,
                            m_roiImageDlgs.end());
                }
                // Add missing windows
                const unsigned int countBeforeAdd =
                    (unsigned int)m_roiImageDlgs.size();
                if (countBeforeAdd < regionCount)
                {
                    for (uns16 n = countBeforeAdd; n < regionCount; ++n)
                    {
                        std::shared_ptr<ImageDlg> roiImageDlg =
                            std::make_shared<ImageDlg>();
                        m_roiImageDlgs.push_back(roiImageDlg);
                    }
                }
                if (m_roiImageDlgs.size() != regionCount)
                    break;
                // Setup all windows
                for (uns16 n = 0; n < regionCount; ++n)
                {
                    std::shared_ptr<ImageDlg> roiImageDlg = m_roiImageDlgs.at(n);
                    if (IsError(roiImageDlg->SetImageFormat(n,
                                    m_camera.get(), m_regionModel.get()),
                                "Preview image window not configured properly"))
                        break;
                    // Setup newly added
                    if (n >= countBeforeAdd)
                    {
                        roiImageDlg->SetFillMethod(m_fillMethod);
                        roiImageDlg->SetAutoConbright(m_imageAutoConbright);
                        roiImageDlg->SetContrast(m_imageContrast);
                        roiImageDlg->SetBrightness(m_imageBrightness);
                        roiImageDlg->SetZoomFactor(m_zoomFactor);
                        roiImageDlg->SetColorEnabled(
                                m_colorCapable && m_colorEnabled);
                        // SetColorContext is called for all in UiUpdateColorContext
                        roiImageDlg->SetTrackShowTrajectory(m_trackShowTrajectory);
                        roiImageDlg->SetMetaRoiShowOutline(m_metaRoiShowOutline);
                    }
                }
                UiUpdateColorContext();
            }

            DoUpdateRoiOutlines(m_roiShowOutline);

            // Activate all image windows
            m_imageDlg->ActivateWindow();
            for (auto roiImageDlg : m_roiImageDlgs)
            {
                roiImageDlg->ActivateWindow();
            }

            m_isAcqConfigured = true;
        }
        ONCE;
    }

    // There should be no way how m_userAbortFlag could be set
    // but let's check it to be sure
    if (m_isAcqConfigured && !m_userAbortFlag)
    {
        StartTask(pm::TaskType::RUN_ACQUISITION);
    }
    else
    {
        OnRunAcquisitionTaskDone(event);
    }
}

void pm::MainDlg::OnRunAcquisitionTaskDone(wxThreadEvent& WXUNUSED(event))
{
    m_fpsLimiter->Stop(true);

    m_isAcqConfigured = false;

    // Set progress to done
    DoUpdateProgress(0, m_uiProgressRange, m_uiProgressRange);

    // Reset stats
    const auto& acqStats = m_acquisition->GetAcqStats();
    DoUpdateFrameRate(0.0);
    DoUpdateAcqStats(acqStats.GetFramesAcquired(), acqStats.GetFramesLost(),
            acqStats.GetQueueCapacity(), 0);
    const auto& diskStats = m_acquisition->GetDiskStats();
    DoUpdateDiskStats(diskStats.GetFramesAcquired(), diskStats.GetFramesLost(),
            diskStats.GetQueueCapacity(), 0);

    // Update ROI outline visibility
    DoUpdateRoiOutlines(true);

    // Update list of centroids for last frame
    DoUpdateCentroidModel();

    UiSetAllControlsEnabled(true);
    m_btnStop->SetLabelText("Stop");
}

void pm::MainDlg::OnAutoExpAndWhiteBalanceTaskDone(wxThreadEvent& WXUNUSED(event))
{
    // Set progress to done
    DoUpdateProgress(0, m_uiProgressRange, m_uiProgressRange);

    // m_userAbortFlag explicitly set also if procedure failed
    if (!m_userAbortFlag)
    {
        UiUpdateExposures();

        const float scaleRed = m_settings.GetColorWbScaleRed();
        const float scaleGreen = m_settings.GetColorWbScaleGreen();
        const float scaleBlue = m_settings.GetColorWbScaleBlue();

        m_sliderRed->SetValue(ConvertWbScaleToSliderValue(scaleRed));
        m_sliderGreen->SetValue(ConvertWbScaleToSliderValue(scaleGreen));
        m_sliderBlue->SetValue(ConvertWbScaleToSliderValue(scaleBlue));

        m_editDblRed->SetValue(scaleRed);
        m_editDblGreen->SetValue(scaleGreen);
        m_editDblBlue->SetValue(scaleBlue);

        UiUpdateColorContext();
    }

    UiSetAllControlsEnabled(true);
}

void pm::MainDlg::StartTask(TaskType taskType)
{
    StopTask();

    m_taskType = taskType;

    m_userAbortFlag = false;

    wxThreadEvent* event = new(std::nothrow) wxThreadEvent(
            PM_EVT_START_TASK, static_cast<int>(m_taskType));
    if (event)
    {
        wxQueueEvent(GetEventHandler(), event);
    }
}

void pm::MainDlg::StopTask()
{
    if (GetThread() && GetThread()->IsRunning())
    {
        //GetThread()->Wait(); // Does NOT set TestDestroy() to true!!!
        GetThread()->Delete(); // Works for joinable threads too
    }
}

wxThread::ExitCode pm::MainDlg::FindCameraTaskEntry()
{
    m_cameraNames.clear();

    int16_t totalCams;
    if (IsError(m_camera->GetCameraCount(totalCams), "Failure getting camera count"))
    {
        totalCams = 0;
    }

    Log::LogI("We have %d camera(s)", totalCams);

    // Set min progress to -2 so -1 and 0 value show already some progress
    const int16_t progressMin = -2;
    UpdateProgress(progressMin, totalCams, -1);

    // Collect camera names
    for (int16_t i = 0; i < totalCams && !GetThread()->TestDestroy(); ++i)
    {
        std::string name;
        if (!m_camera->GetName(i, name))
        {
            Log::LogE("Failure getting name of camera at index %d", i);
            // On Error generate a fake name, it won't open later anyway
            name = std::to_string(i);
        }
        m_cameraNames.push_back(name);

        UpdateProgress(progressMin, totalCams, i);
    }

    UpdateProgress(progressMin, totalCams, totalCams);

    wxThreadEvent* eventDone = new(std::nothrow) wxThreadEvent(
            PM_EVT_TASK_DONE, static_cast<int>(m_taskType));
    if (eventDone)
    {
        wxQueueEvent(GetEventHandler(), eventDone);
    }

    return (wxThread::ExitCode)GetThread()->TestDestroy();
}

wxThread::ExitCode pm::MainDlg::SetupAcquisitionTaskEntry()
{
    m_isAcqConfigured = false;
    do
    {
        // Convert regions from model to settings
        std::vector<rgn_type> regions;
        auto AppendRegion = [&](const wxRect& rect) {
            rgn_type rgn;
            rgn.s1 = (uns16)rect.GetLeft();
            rgn.s2 = (uns16)rect.GetRight();
            rgn.sbin = m_settings.GetBinningSerial();
            rgn.p1 = (uns16)rect.GetTop();
            rgn.p2 = (uns16)rect.GetBottom();
            rgn.pbin = m_settings.GetBinningParallel();

            regions.push_back(rgn);
        };
        if (m_regionModel->GetRegions().empty())
        {
            // With no region specified use full sensor size
            AppendRegion(m_camFullRect);
        }
        else
        {
            for (auto region : m_regionModel->GetRegions())
            {
                AppendRegion(region->GetClientRect());
            }
        }
        // Cannot fail, all regions have the same binning factors
        m_settings.SetRegions(regions);

        // Set frame count based on acq. mode
        const AcqMode acqMode = m_settings.GetAcqMode();
        uns32 frameCount = m_snapFrameCount;
        if (acqMode == AcqMode::LiveCircBuffer || acqMode == AcqMode::LiveTimeLapse)
        {
            frameCount = m_settings.GetBufferFrameCount();
        }
        m_settings.SetAcqFrameCount(frameCount);

        // Setup camera
        try
        {
            if (!m_camera->SetupExp(m_settings))
                break;
        }
        catch (const Exception& ex)
        {
            Log::LogE("Failure applying settings - %s", ex.what());
            break;
        }

        m_isAcqConfigured = true;
    }
    ONCE;

    wxThreadEvent* eventDone = new(std::nothrow) wxThreadEvent(
            PM_EVT_TASK_DONE, static_cast<int>(m_taskType));
    if (eventDone)
    {
        wxQueueEvent(GetEventHandler(), eventDone);
    }

    return (wxThread::ExitCode)GetThread()->TestDestroy();
}

wxThread::ExitCode pm::MainDlg::RunAcquisitionTaskEntry()
{
    do
    {
        if (!m_isAcqConfigured)
            break;

        // Start FPS limiter before any thread
        if (IsError(m_fpsLimiter->Start(this), "Failure starting FPS Limiter"))
            break;

        if (m_acquisition->Start(m_fpsLimiter))
        {
            wxCommandEvent* eventAcqStarted =
                new(std::nothrow) wxCommandEvent(PM_EVT_ACQ_STARTED);
            if (eventAcqStarted)
            {
                wxQueueEvent(GetEventHandler(), eventAcqStarted);
            }

            m_acquisition->WaitForStop(true);
        }
    }
    ONCE;

    wxThreadEvent* eventDone = new(std::nothrow) wxThreadEvent(
            PM_EVT_TASK_DONE, static_cast<int>(m_taskType));
    if (eventDone)
    {
        wxQueueEvent(GetEventHandler(), eventDone);
    }

    return (wxThread::ExitCode)GetThread()->TestDestroy();
}

wxThread::ExitCode pm::MainDlg::AutoExpAndWhiteBalanceTaskEntry()
{
    UpdateProgress(0, 0, 0);

    wxRect rect;
    if (m_regionModel->GetRegionCount() == 0)
    {
        // With no region specified use full sensor size
        rect = m_camFullRect;
    }
    else
    {
        if (m_regionModel->GetRegionCount() > 1)
        {
            Log::LogW("More regions defined."
                    " Auto expose for white balance uses the first one.");
        }
        rect = m_regionModel->GetRegions().at(0)->GetClientRect();
    }
    rgn_type rgn;
    rgn.s1 = (uns16)rect.GetLeft();
    rgn.s2 = (uns16)rect.GetRight();
    rgn.sbin = m_settings.GetBinningSerial();
    rgn.p1 = (uns16)rect.GetTop();
    rgn.p2 = (uns16)rect.GetBottom();
    rgn.pbin = m_settings.GetBinningParallel();

    // Before calling ph_color_auto_exposure_and_white_balance ensure
    // all callbacks are unregistered.
    // Fortunately, Camera class registers callbacks only during
    // acquisition, so here is nothing to be done.

    uns32 autoExpTime;
    flt32 scaleRed;
    flt32 scaleGreen;
    flt32 scaleBlue;

    wxCommandEvent* eventAcqStarted =
        new(std::nothrow) wxCommandEvent(PM_EVT_ACQ_STARTED);
    if (eventAcqStarted)
    {
        wxQueueEvent(GetEventHandler(), eventAcqStarted);
    }

    // This type of task will not run if !PH_COLOR
    if (PH_COLOR_ERROR_NONE == PH_COLOR->auto_exposure_and_white_balance(
                m_colorContext, m_camera->GetHandle(), rgn, &autoExpTime,
                &scaleRed, &scaleGreen, &scaleBlue))
    {
        Log::LogI("Auto exposure and white balance finished");

        m_settings.SetExposure(autoExpTime);
        m_settings.SetColorWbScaleRed(scaleRed);
        m_settings.SetColorWbScaleGreen(scaleGreen);
        m_settings.SetColorWbScaleBlue(scaleBlue);
    }
    else
    {
        if (m_userAbortFlag)
        {
            Log::LogI("Auto exposure and white balance aborted");
        }
        else
        {
            ColorUtils::LogError("Failure during auto exposure and white balance");
        }

        // This flag is tested in TaskDone handler. If not set the UI is
        // updated. So on error and on abort it should be true.
        m_userAbortFlag = true;
    }

    // Here can be original callback handlers registered again.

    wxThreadEvent* eventDone = new(std::nothrow) wxThreadEvent(
            PM_EVT_TASK_DONE, static_cast<int>(m_taskType));
    if (eventDone)
    {
        wxQueueEvent(GetEventHandler(), eventDone);
    }

    return (wxThread::ExitCode)GetThread()->TestDestroy();
}

bool pm::MainDlg::IsError(bool cond, const std::string& message)
{
    if (cond)
        return false;

    Log::LogE(message);
    return true;
}

void pm::MainDlg::StartAcq()
{
    UiSetAllControlsEnabled(false);
    // Stop button is enabled when acquisition is successfully started

    // Start acquisition in a background thread.
    // When done the OnSetupAcquisitionTaskDone() will be called and if
    // the acquisition has been configured properly, RUN_ACQUISITION task
    // is started.
    StartTask(pm::TaskType::SETUP_ACQUISITION);
}

void pm::MainDlg::StopAcq(bool abortBufferedFramesProcessing)
{
    if (m_taskType == TaskType::AUTO_EXP_AND_WHITE_BALANCE)
    {
        // Ensure full stop when aborting auto white balance
        abortBufferedFramesProcessing = true;
    }

    if (!abortBufferedFramesProcessing)
    {
        // Stop button stays enabled for abort
        m_btnStop->SetLabelText("Abort");
    }
    else
    {
        // Disable Stop button immediately to not allow double stopping
        m_btnStop->Enable(false);
    }

    if (m_acquisition)
    {
        m_acquisition->RequestAbort(abortBufferedFramesProcessing);
    }

    if (m_taskType == TaskType::AUTO_EXP_AND_WHITE_BALANCE)
    {
        // This type of task will not run if !PH_COLOR, ignore any error
        PH_COLOR->auto_exposure_abort(m_colorContext);
    }

    m_userAbortFlag = true;

    if (abortBufferedFramesProcessing)
    {
        // Wait for full task stop
        StopTask();
    }
}

void pm::MainDlg::SetHelpText(const std::vector<Option>& options)
{
    m_helpText  = "Usage\n";
    m_helpText += "=====\n";
    m_helpText += "\n";
    m_helpText += "Run without or with almost any combination of options listed below.\n";
    m_helpText += "\n";
    m_helpText += "Return value\n";
    m_helpText += "------------\n";
    m_helpText += "\n";
    m_helpText += "  Application always exits gracefully.\n";
    m_helpText += "  All errors are shown in log output.\n";
    m_helpText += "\n";

    m_helpText += m_optionController.GetOptionsDescription(options);

    if (std::find_if(options.cbegin(), options.cend(),
                [](const pm::Option& o) {
                    return o.GetId() == static_cast<uint32_t>(pm::OptionId::Help);
                })
            == options.cend())
    {
        m_helpText += m_optionController.GetOptionsDescription(
                { m_helpOption }, false);
    }
}

bool pm::MainDlg::AddCliOptions()
{
    if (!m_optionController.AddOption(m_helpOption))
        return false;

    if (!m_optionController.AddOption(Option(
            { "-MetaRoiOutline", "--meta-roi-outline" },
            { "" },
            { "true" },
            "Draws circular outline for blobs and rectangle around other objects.",
            OptionId_MetaRoiOutline,
            std::bind(&MainDlg::HandleMetaRoiShowOutlineOption,
                    this, std::placeholders::_1))))
        return false;

    if (!m_optionController.AddOption(Option(
            { "-RoiMultiWindow", "--roi-multi-window" },
            { "" },
            { "false" },
            "Displays every region in separate window in addition to Image Preview.",
            OptionId_RoidMultiWindow,
            std::bind(&MainDlg::HandleRoiMultiWindowOption,
                    this, std::placeholders::_1))))
        return false;

    if (!m_optionController.AddOption(Option(
            { "-RoiOutline", "--roi-outline" },
            { "" },
            { "true" },
            "Draws rectangle around regions during acquisition.",
            OptionId_RoiOutline,
            std::bind(&MainDlg::HandleRoiShowOutlineOption,
                    this, std::placeholders::_1))))
        return false;

    if (!m_optionController.AddOption(Option(
            { "-ImageAutoConbright", "--image-auto-conbright" },
            { "" },
            { "true" },
            "Automatically sets image contrast and brightness.\n"
            "Basically expands image histogram based on min/max pixel values.",
            OptionId_ImageAutoConbright,
            std::bind(&MainDlg::HandleImageAutoConbrightOption,
                    this, std::placeholders::_1))))
        return false;

    if (!m_optionController.AddOption(Option(
            { "-ImageContract", "--image-contrast" },
            { "value" },
            { "0" },
            "Specifies manual image contrast, e.i. used if --image-auto-conbright is false.\n"
            "Valid values are from -255 to +255 where 0 means no changes are done.",
            OptionId_ImageContract,
            std::bind(&MainDlg::HandleImageContrastOption,
                    this, std::placeholders::_1))))
        return false;

    if (!m_optionController.AddOption(Option(
            { "-ImageBrightness", "--image-brightness" },
            { "value" },
            { "0" },
            "Specifies manual image brightness, e.i. used if --image-auto-conbright is false.\n"
            "Valid values are from -255 to +255 where 0 means no changes are done.",
            OptionId_ImageBrightness,
            std::bind(&MainDlg::HandleImageBrightnessOption,
                    this, std::placeholders::_1))))
        return false;

    return true;
}

bool pm::MainDlg::HandleHelpOption(const std::string& value)
{
    if (value.empty())
    {
        m_showFullHelp = true;
    }
    else
    {
        if (!StrToBool(value, m_showFullHelp))
            return false;
    }
    return true;
}

bool pm::MainDlg::HandleMetaRoiShowOutlineOption(const std::string& value)
{
    if (value.empty())
    {
        m_metaRoiShowOutline = true;
        return true;
    }

    return StrToBool(value, m_metaRoiShowOutline);
}

bool pm::MainDlg::HandleRoiMultiWindowOption(const std::string& value)
{
    if (value.empty())
    {
        m_roiMultiWindow = false;
        return true;
    }

    return StrToBool(value, m_roiMultiWindow);
}

bool pm::MainDlg::HandleRoiShowOutlineOption(const std::string& value)
{
    if (value.empty())
    {
        m_roiShowOutline = true;
        return true;
    }

    return StrToBool(value, m_roiShowOutline);
}

bool pm::MainDlg::HandleImageAutoConbrightOption(const std::string& value)
{
    if (value.empty())
    {
        m_imageAutoConbright = true;
        return true;
    }

    return StrToBool(value, m_imageAutoConbright);
}

bool pm::MainDlg::HandleImageContrastOption(const std::string& value)
{
    int imageContrast;
    if (!StrToNumber<int>(value, imageContrast))
        return false;
    if (imageContrast < -255 || imageContrast > +255)
        return false;
    m_imageContrast = imageContrast;
    return true;
}

bool pm::MainDlg::HandleImageBrightnessOption(const std::string& value)
{
    int imageBrightness;
    if (!StrToNumber<int>(value, imageBrightness))
        return false;
    if (imageBrightness < -255 || imageBrightness > +255)
        return false;
    m_imageBrightness = imageBrightness;
    return true;
}

void pm::MainDlg::DoUpdateProgress(int min, int max, int current)
{
    m_uiProgressIsInf = (min == max);
    if (!m_uiProgressIsInf)
    {
        m_uiProgressRange = max - min;
        m_uiProgressValue = current - min;
    }
    m_uiProgressUpdated = true;
}

void pm::MainDlg::DoUpdateFrameSlider(int index, int frameNr)
{
    m_uiFrameSliderIndex = index;
    m_uiFrameSliderFrameNr = frameNr;
    m_uiFrameSliderUpdated = true;
}

void pm::MainDlg::DoUpdateFrameRate(double acqFps)
{
    m_uiFrameRateAcq = acqFps;
    m_uiFrameRateUpdated = true;
}

void pm::MainDlg::DoUpdateAcqStats(size_t valid, size_t lost, size_t max,
        size_t cached)
{
    m_uiAcqStatsValid = valid;
    m_uiAcqStatsLost = lost;
    m_uiAcqStatsMax = max;
    m_uiAcqStatsCached = cached;
    m_uiAcqStatsUpdated = true;
}

void pm::MainDlg::DoUpdateDiskStats(size_t valid, size_t lost, size_t max,
        size_t cached)
{
    m_uiDiskStatsValid = valid;
    m_uiDiskStatsLost = lost;
    m_uiDiskStatsMax = max;
    m_uiDiskStatsCached = cached;
    m_uiDiskStatsUpdated = true;
}

void pm::MainDlg::DoUpdateFrameDesc(std::shared_ptr<pm::Frame> frame)
{
    if (!m_uiFrameDescVisible)
        return;

    std::ostringstream imageDesc;

    do
    {
        if (!frame)
        {
            imageDesc << "<null>";
            break;
        }
        if (!frame->IsValid())
        {
            imageDesc << "<invalid>";
            break;
        }

        imageDesc << "=== EOF frame info ===";

        const Frame::Info& fi = frame->GetInfo();
        imageDesc
            << "\nFrame nr.: " << fi.GetFrameNr()
            << "\nReadout time: " << fi.GetReadoutTime() << "00us"
            << "\nBOF time: " << fi.GetTimestampBOF() << "00us"
            << "\nEOF time: " << fi.GetTimestampEOF() << "00us";

        const Frame::AcqCfg& cfg = frame->GetAcqCfg();
        if (!cfg.HasMetadata())
        {
            imageDesc << "\n=== No embedded metadata ===";
            break;
        }

        imageDesc << "\n=== Embedded metadata ===";

        if (!frame->DecodeMetadata())
        {
            imageDesc << "\n<decoding failed>";
            break;
        }

        const md_frame* meta = frame->GetMetadata();

        const uint64_t rdfTimeNs = (uint64_t)meta->header->timestampResNs
            * (meta->header->timestampEOF - meta->header->timestampBOF);
        const uint64_t expTimeNs = (uint64_t)meta->header->exposureTimeResNs
            * meta->header->exposureTime;
        const rgn_type& irgn = meta->impliedRoi;
        // uns8 type is handled as underlying char by stream, cast it to uint16_t
        imageDesc
            << "\nheader.version=" << (uint16_t)meta->header->version
            << "\nheader.frameNr=" << meta->header->frameNr
            << "\nheader.roiCount=" << meta->header->roiCount
            << "\nheader.timeBof=" << meta->header->timestampBOF
            << "\nheader.timeEof=" << meta->header->timestampEOF
            << "\n  (diff=" << rdfTimeNs << "ns)"
            << "\nheader.timeResNs=" << meta->header->timestampResNs
            << "\nheader.expTime=" << meta->header->exposureTime
            << "\n  (" << expTimeNs << "ns)"
            << "\nheader.expTimeResNs=" << meta->header->exposureTimeResNs
            << "\nheader.roiTimeResNs=" << meta->header->roiTimestampResNs
            << "\nheader.bitDepth=" << (uint16_t)meta->header->bitDepth
            << "\nheader.colorMask=" << (uint16_t)meta->header->colorMask
            << "\nheader.flags=0x"
                << std::hex << (uint16_t)meta->header->flags << std::dec
            << "\nheader.extMdSize=" << meta->header->extendedMdSize
            << "\nextMdSize=" << meta->extMdDataSize
            << "\nimpliedRoi=["
                << irgn.s1 << "," << irgn.s2 << "," << irgn.sbin << ","
                << irgn.p1 << "," << irgn.p2 << "," << irgn.pbin << "]"
            << "\nroiCapacity=" << meta->roiCapacity
            << "\nroiCount=" << meta->roiCount;
        for (int n = 0; n < meta->roiCount; ++n)
        {
            const md_frame_roi& roi = meta->roiArray[n];
            const auto roiHdr = roi.header;
            if (roiHdr->flags & PL_MD_ROI_FLAG_INVALID)
                continue; // Skip invalid regions
            const rgn_type& rgn = roiHdr->roi;
            imageDesc
                << "\nroi[" << n << "].header.roiNr=" << roiHdr->roiNr;
            if (meta->header->flags & PL_MD_FRAME_FLAG_ROI_TS_SUPPORTED)
            {
                const uint64_t rdrTimeNs =
                    (uint64_t)meta->header->roiTimestampResNs
                    * (roiHdr->timestampEOR - roiHdr->timestampBOR);
                imageDesc
                    << "\nroi[" << n << "].header.timeBor=" << roiHdr->timestampBOR
                    << "\nroi[" << n << "].header.timeEor=" << roiHdr->timestampEOR
                    << "\n  (diff=" << rdrTimeNs << "ns)";
            }
            imageDesc
                << "\nroi[" << n << "].header.roi=["
                    << rgn.s1 << "," << rgn.s2 << "," << rgn.sbin << ","
                    << rgn.p1 << "," << rgn.p2 << "," << rgn.pbin << "]";
            imageDesc
                << "\nroi[" << n << "].header.flags=0x"
                    << std::hex << (uint16_t)roiHdr->flags << std::dec
                << "\nroi[" << n << "].header.extMdSize=" << roiHdr->extendedMdSize
                << "\nroi[" << n << "].dataSize=" << roi.dataSize
                << "\nroi[" << n << "].extMdSize=" << roi.extMdDataSize;
        }

        const Frame::Trajectories& trajectories = frame->GetTrajectories();
        if (trajectories.data.empty())
            break;

        imageDesc << "\n=== Trajectories ===";

        imageDesc
            << "\nheader.maxTrajectories=" << trajectories.header.maxTrajectories
            << "\nheader.maxTrajectoryPoints=" << trajectories.header.maxTrajectoryPoints
            << "\nheader.trajectoryCount=" << trajectories.header.trajectoryCount;
        for (size_t t = 0; t < trajectories.data.size(); t++)
        {
            const auto& traj = trajectories.data.at(t);
            imageDesc
                << "\ntraj[" << t << "].header.roiNr=" << traj.header.roiNr
                << "\ntraj[" << t << "].header.particleId=" << traj.header.particleId
                << "\ntraj[" << t << "].header.lifetime=" << traj.header.lifetime
                << "\ntraj[" << t << "].header.pointCount=" << traj.header.pointCount;
            std::string pointsStr;
            for (const auto& point : traj.data)
            {
                if (!point.isValid)
                    continue;
                pointsStr += std::to_string(point.x) + 'x' + std::to_string(point.x) + ',';
            }
            if (pointsStr.length() > 1)
            {
                pointsStr.pop_back(); // Remove delimiter at the end
            }
            imageDesc
                << "\ntraj[" << t << "].points={" << pointsStr << "}";
        }
    }
    ONCE;

    m_uiFrameDescText = imageDesc.str();
    m_uiFrameDescUpdated = true;
}

void pm::MainDlg::DoUpdateCentroidModel()
{
    // Remove existing regions if any
    m_centroidModel->DeleteAllRegions();

    // TODO: Remove this code later once the table with metadata-ROIs is moved
    //       somewhere else from old Tracking page.
    //       Don't forget to remove also m_tabSettingsTracking hiding in constructor!
    if (!m_dvlCentroids->IsShownOnScreen())
        return;

    auto frame = m_camera->GetFrameAt(m_sliderFrame->GetValue());
    if (!frame || !frame->IsValid())
        return;

    if (!frame->GetAcqCfg().HasMetadata())
        return;

    if (!frame->DecodeMetadata())
        return;

    const md_frame* frameMeta = frame->GetMetadata();
    if (!frameMeta)
        return;

    // TODO: Continue only if frame really contains centroids
    //       (take from given frame, not from m_settings)

    const auto usesMetadata =
        m_camera->GetParams().Get<PARAM_METADATA_ENABLED>()->IsAvail()
        && m_camera->GetParams().Get<PARAM_METADATA_ENABLED>()->GetCur();
    const auto usesCentroids = usesMetadata
        && m_camera->GetParams().Get<PARAM_CENTROIDS_ENABLED>()->IsAvail()
        && m_camera->GetParams().Get<PARAM_CENTROIDS_ENABLED>()->GetCur();
    const auto centroidsMode =
        m_camera->GetParams().Get<PARAM_CENTROIDS_MODE>()->IsAvail()
        ? m_camera->GetParams().Get<PARAM_CENTROIDS_MODE>()->GetCur()
        : PL_CENTROIDS_MODE_LOCATE;
    //const auto centroidsRadius =
    //    m_camera->GetParams().Get<PARAM_CENTROIDS_RADIUS>()->IsAvail()
    //    ? m_camera->GetParams().Get<PARAM_CENTROIDS_RADIUS>()->GetCur()
    //    : 0;

    if (!usesCentroids)
        return;

    auto AddRegion = [](std::vector<std::shared_ptr<Region>>& container, const rgn_type& rgn) {
        const wxRect rect(rgn.s1, rgn.p1,
                rgn.s2 + 1 - rgn.s1,
                rgn.p2 + 1 - rgn.p1);
        container.emplace_back(std::make_shared<Region>(rect));
    };

    std::vector<std::shared_ptr<Region>> regions;

    switch (centroidsMode)
    {
    case PL_CENTROIDS_MODE_LOCATE:
        for (uint16_t i = 0; i < frameMeta->roiCount; ++i)
        {
            md_frame_roi_header* roiHdr = frameMeta->roiArray[i].header;
            AddRegion(regions, roiHdr->roi);
        }
        break;
    case PL_CENTROIDS_MODE_TRACK:
        for (uint16_t i = 0; i < frameMeta->roiCount; ++i)
        {
            md_frame_roi_header* roiHdr = frameMeta->roiArray[i].header;
            if (!(roiHdr->flags & PL_MD_ROI_FLAG_HEADER_ONLY))
                continue;
            AddRegion(regions, roiHdr->roi);
        }
        break;
    case PL_CENTROIDS_MODE_BLOB:
        for (uint16_t i = 0; i < frameMeta->roiCount; ++i)
        {
            md_frame_roi_header* roiHdr = frameMeta->roiArray[i].header;
            if (!(roiHdr->flags & PL_MD_ROI_FLAG_HEADER_ONLY))
                continue;
#if 0
            rgn_type rgn = roiHdr->roi;
            if (rgn.s1 == rgn.s2 && rgn.p1 == rgn.p2)
            {
                // Fix crippled blob size
                rgn.s1 -= centroidsRadius;
                rgn.s2 += centroidsRadius;
                rgn.p1 -= centroidsRadius;
                rgn.p2 += centroidsRadius;
            }
            AddRegion(regions, rgn);
#else
            AddRegion(regions, roiHdr->roi);
#endif
        }
        break;
    // default missing intentionally, compiler warns us if new enum is added
    }

    // Ignore errors
    m_centroidModel->AppendRegions(regions);
}

void pm::MainDlg::DoUpdateColorCapable()
{
    m_colorCapable = PH_COLOR && m_camera && m_camera->IsOpen()
            && m_settings.GetBinningParallel() == 1
            && m_settings.GetBinningSerial() == 1;
}

void pm::MainDlg::DoUpdateRoiOutlines(bool visible)
{
    bool changed = false;
    for (auto region : m_regionModel->GetRegions())
    {
        if (region->IsVisible() != visible)
        {
            region->SetVisible(visible);
            m_regionModel->RegionChanged(region);
            changed = true;
        }
    }
    if (changed)
    {
        if (m_imageDlg)
        {
            m_imageDlg->SetRoiShowOutline(visible);
        }
        for (auto roiImageDlg : m_roiImageDlgs)
        {
            roiImageDlg->SetRoiShowOutline(visible);
        }
    }
}

void pm::MainDlg::UpdateAcqModeTooltip()
{
    const auto acqMode = m_settings.GetAcqMode();
    switch (acqMode)
    {
    case AcqMode::SnapSequence:
        m_cbAcqMode->SetToolTip(
                "Captures configured number of frames in one acquisition.\n"
                "This uses a native PVCAM acquisition mode - a 'sequence'. "
                "Acquisition stops automatically when configured number of frames arrive.\n"
                "Number of frames is limited as whole sequence has to fit in 2GB or 4GB "
                "buffer (depends on camera interface mode).");
        break;
    case AcqMode::SnapCircBuffer:
        m_cbAcqMode->SetToolTip(
                "Captures configured number of frames in one acquisition.\n"
                "This uses a native PVCAM acquisition mode - a 'circular buffer' - "
                "when whole buffer is filled, the new frames overwrite the buffer "
                "from beginning.\n"
                "The application aborts the acquisition when configured number of "
                "frames arrive.\n"
                "Number of frames in circular buffer is limited and has to fit in "
                "2GB or 4GB buffer (depends on camera interface mode), while "
                "the total number of captured frames is not limited.");
        break;
    case AcqMode::SnapTimeLapse:
        m_cbAcqMode->SetToolTip(
                "Captures configured number of frames, each in its own acquisition.\n"
                "This acquisition mode is implemented on application side and uses "
                "PVCAM 'sequence' mode to capture single frame in one acquisition.\n"
                "The application aborts the loop when configured number of frames arrive. "
                "The total number of captured frames is not limited.\n"
                "\n"
                "This mode allows user to configure a delay between frames. "
                "It also emulates 'Smart Streaming' behavior or 'trigger first' "
                "triggering mode.");
        break;
    case AcqMode::LiveCircBuffer:
        m_cbAcqMode->SetToolTip(
                "Captures undefined number of frames in one acquisition.\n"
                "This uses a native PVCAM acquisition mode - a 'circular buffer' - "
                "when whole buffer is filled, the next frames overwrite the buffer "
                "from beginning.\n"
                "Number of frames in circular buffer is limited and has to fit in "
                "2GB or 4GB buffer (depends on camera interface mode), while "
                "the total number of captured frames is not limited.");
        break;
    case AcqMode::LiveTimeLapse:
        m_cbAcqMode->SetToolTip(
                "Captures undefined number of frames, each in its own acquisition.\n"
                "This acquisition mode is implemented on application side and uses "
                "PVCAM 'sequence' mode to capture single frame in one acquisition. "
                "The total number of captured frames is not limited.\n"
                "\n"
                "This mode allows user to configure a delay between frames. "
                "It also emulates 'Smart Streaming' behavior or 'trigger first' "
                "triggering mode.");
        break;
    }
}

void pm::MainDlg::OnFpsLimiterEvent(FpsLimiter* sender,
        std::shared_ptr<pm::Frame> frame)
{
    if (sender != m_fpsLimiter.get())
        return;

    {
        std::lock_guard<std::mutex> lock(m_uiUpdateMutex);

        if (frame)
        {
            const uint32_t frameNr = frame->GetInfo().GetFrameNr();

            // Prepare progress update
            const AcqMode acqMode = m_settings.GetAcqMode();
            if (acqMode == AcqMode::LiveCircBuffer || acqMode == AcqMode::LiveTimeLapse)
            {
                DoUpdateProgress(0, 0, 0);
            }
            else
            {
                // Starts on 0, then cycles between 1 and m_frameProgessMax
                const uint16_t frameCount = m_settings.GetAcqFrameCount();
                const uint16_t progress = ((frameNr - 1) % frameCount) + 1;

                DoUpdateProgress(0, frameCount, progress);
            }

            // Prepare slider update
            size_t index;
            if (m_camera->GetFrameIndex(*frame, index))
            {
                DoUpdateFrameSlider(index, (int)frameNr);
            }
            else
            {
                const size_t frameCount = m_settings.GetBufferFrameCount();
                index = (frameNr - 1) % frameCount;
                DoUpdateFrameSlider(index, (int)frameNr);
            }
        }

        // Update frame description
        DoUpdateFrameDesc(frame);

        if (m_showLiveImage)
        {
            // Update image(s)
            m_imageDlg->UpdateImage(frame);
            for (size_t n = 0; n < m_roiImageDlgs.size(); ++n)
            {
                m_roiImageDlgs.at(n)->UpdateImage(frame);
            }
        }

        // Update frame rate and cache stats
        const auto& acqStats = m_acquisition->GetAcqStats();
        DoUpdateFrameRate(acqStats.GetAvgFrameRate());
        DoUpdateAcqStats(acqStats.GetFramesAcquired(), acqStats.GetFramesLost(),
                acqStats.GetQueueCapacity(),
                std::max<size_t>(1, acqStats.GetQueueSize()));
        const auto& diskStats = m_acquisition->GetDiskStats();
        DoUpdateDiskStats(diskStats.GetFramesAcquired(), diskStats.GetFramesLost(),
                diskStats.GetQueueCapacity(),
                std::max<size_t>(1, diskStats.GetQueueSize()));
    }
}

void pm::MainDlg::OnFpsLimiterAcqFinished(FpsLimiter* sender)
{
    if (sender != m_fpsLimiter.get())
        return;

    wxCommandEvent* event =
        new(std::nothrow) wxCommandEvent(PM_EVT_ACQ_FINISHED);
    if (event)
    {
        wxQueueEvent(GetEventHandler(), event);
    }
}

void pm::MainDlg::OnLogEntryAdded(const Log::Entry& entry)
{
    std::lock_guard<std::mutex> lock(m_uiUpdateMutex);
    m_uiLogEntries.push(entry);
}

void pm::MainDlg::UiSetAllControlsEnabled(bool enabled)
{
    enabled = enabled && !m_cameraRemovedFlag;

    const bool cameraReady = m_camera && m_camera->IsOpen();

    const bool enabledAcq = enabled && cameraReady && !m_acquisition->IsRunning();

    const AcqMode acqMode = m_settings.GetAcqMode();

    m_cbPortSpeed->Enable(enabledAcq);
    m_cbGain->Enable(enabledAcq);
    m_editEmGain->Enable(enabledAcq
            && m_camera->GetParams().Get<PARAM_GAIN_MULT_FACTOR>()->IsAvail());
    m_editExpTimes->Enable(enabledAcq);
    m_cbExpTimeRes->Enable(enabledAcq
            && m_camera->GetParams().Get<PARAM_EXP_RES_INDEX>()->IsAvail());
    m_editTimeLapseDelay->Enable(enabledAcq
            && (acqMode == AcqMode::SnapTimeLapse
                || acqMode == AcqMode::LiveTimeLapse));
    m_cbTriggerMode->Enable(enabledAcq
            && m_camera->GetParams().Get<PARAM_EXPOSURE_MODE>()->IsAvail());
    m_cbExpOutMode->Enable(enabledAcq
            && m_camera->GetParams().Get<PARAM_EXPOSE_OUT_MODE>()->IsAvail());
    m_cbClearMode->Enable(enabledAcq);
    m_editClearCycles->Enable(enabledAcq);
    m_cbPMode->Enable(enabledAcq);
    m_btnOpenParameterBrowser->Enable(enabledAcq);
    if (m_paramDlg)
    {
        m_paramDlg->EnableEditor(enabledAcq);
    }

    m_editCircFrameCount->Enable(enabledAcq);
    m_sliderFrame->Enable(enabledAcq);

    m_cbSaveAs->Enable(enabledAcq);
    m_editSaveDir->Enable(enabledAcq);
    m_btnSaveDir->Enable(enabledAcq);
    m_editSaveDigits->Enable(enabledAcq);
    m_chbSaveFirst->Enable(enabledAcq);
    m_editSaveFirst->Enable(enabledAcq && m_chbSaveFirst->GetValue());
    m_chbSaveLast->Enable(enabledAcq);
    m_editSaveLast->Enable(enabledAcq && m_chbSaveLast->GetValue());
    m_chbSaveStack->Enable(enabledAcq);
    m_editSaveStack->Enable(enabledAcq && m_chbSaveStack->GetValue());

    m_cbAcqMode->Enable(enabledAcq);
    m_editSnapFrameCount->Enable(enabledAcq
            && (acqMode == AcqMode::SnapSequence
                || acqMode == AcqMode::SnapCircBuffer
                || acqMode == AcqMode::SnapTimeLapse));

    m_btnStart->Enable(enabledAcq);
    m_btnStop->Enable(false); // Stop button has to be enabled explicitly

    UiSetRoiButtonsEnabled(enabledAcq);
    m_cbRoiBin->Enable(enabledAcq && !m_binModes.empty());
    m_editRoiBinSer->Enable(enabledAcq && m_binModes.empty());
    m_editRoiBinPar->Enable(enabledAcq && m_binModes.empty());
    m_dvlRois->Enable(enabledAcq);
    m_chbRoiMultiWin->Enable(enabledAcq);

    UiSetCentroidControlsEnabled(enabled);

    if (m_imageDlg)
    {
        m_imageDlg->EnableEditor(enabledAcq);
    }
    for (auto roiImageDlg : m_roiImageDlgs)
    {
        roiImageDlg->EnableEditor(enabledAcq);
    }

    m_cbFillMethod->Enable(cameraReady);
    m_chbAutoConbright->Enable(cameraReady);
    m_sliderContrast->Enable(!m_imageAutoConbright && cameraReady);
    m_editContrast->Enable(!m_imageAutoConbright && cameraReady);
    m_sliderBrightness->Enable(!m_imageAutoConbright && cameraReady);
    m_editBrightness->Enable(!m_imageAutoConbright && cameraReady);
    m_cbZoomFactor->Enable(cameraReady);
    m_chbRoiOutline->Enable(cameraReady);

    UiSetColorControlsEnabled(enabled);
    m_btnAutoWhiteBalance->Enable(enabledAcq && m_btnAutoWhiteBalance->IsEnabled());
}

void pm::MainDlg::UiSetRoiButtonsEnabled(bool enabled)
{
    const bool cameraReady = m_camera && m_camera->IsOpen();

    bool hasSelection = false;
    for (auto region : m_regionModel->GetRegions())
    {
        if (region->IsSelected())
        {
            hasSelection = true;
            break;
        }
    }

    m_btnRoiAdd->Enable(enabled
            && (int)m_regionModel->GetRegionCount() < m_regionModel->GetMaxRegionCount());
    m_btnRoiRemove->Enable(enabled && hasSelection);
}

void pm::MainDlg::UiSetColorControlsEnabled(bool enabled)
{
    enabled = enabled && m_colorCapable;

    m_chbColorEnable->Enable(enabled);

    enabled = enabled && m_colorEnabled;

    const bool enabledAcq = enabled && !m_acquisition->IsRunning();

    m_chbColorOverrideMask->Enable(enabled && m_colorMask != COLOR_NONE);
    m_cbColorCustomMask->Enable(enabled && m_colorMaskOverridden);
    m_btnAutoWhiteBalance->Enable(enabledAcq && !m_wbWizard);
    m_btnDefaultWhiteBalance->Enable(enabled);
    m_sliderRed->Enable(enabled);
    m_editDblRed->Enable(enabled);
    m_sliderGreen->Enable(enabled);
    m_editDblGreen->Enable(enabled);
    m_sliderBlue->Enable(enabled);
    m_editDblBlue->Enable(enabled);
    m_cbDebayerAlgorithm->Enable(enabled);
    m_cbAutoExposureAlgorithm->Enable(enabledAcq);
    m_chbColorCpuOnly->Enable(enabled);
}

void pm::MainDlg::UiSetCentroidControlsEnabled(bool enabled)
{
    const bool cameraReady = m_camera && m_camera->IsOpen();

    const bool enabledAcq = enabled && cameraReady && !m_acquisition->IsRunning();

    const auto usesMetadata = cameraReady
        && m_camera->GetParams().Get<PARAM_METADATA_ENABLED>()->IsAvail()
        && m_camera->GetParams().Get<PARAM_METADATA_ENABLED>()->GetCur();
    const bool centroidsCapable = usesMetadata
        && m_camera->GetParams().Get<PARAM_CENTROIDS_ENABLED>()->IsAvail();
    const bool usesCentroids = centroidsCapable
        && m_camera->GetParams().Get<PARAM_CENTROIDS_ENABLED>()->GetCur();

    m_chbCentroidsEnabled->Enable(enabledAcq && centroidsCapable);
    m_chCentroidsMode->Enable(enabledAcq && usesCentroids
            && m_camera->GetParams().Get<PARAM_CENTROIDS_MODE>()->IsAvail());
    m_editCentroidsCount->Enable(enabledAcq && usesCentroids
            && m_camera->GetParams().Get<PARAM_CENTROIDS_COUNT>()->IsAvail());
    m_editCentroidsRadius->Enable(enabledAcq && usesCentroids
            && m_camera->GetParams().Get<PARAM_CENTROIDS_RADIUS>()->IsAvail());
    m_chCentroidsBgCount->Enable(enabledAcq && usesCentroids
            && m_camera->GetParams().Get<PARAM_CENTROIDS_BG_COUNT>()->IsAvail());
    m_editDblCentroidsThreshold->Enable(enabledAcq && usesCentroids
            && m_camera->GetParams().Get<PARAM_CENTROIDS_THRESHOLD>()->IsAvail());

    m_dvlCentroids->Enable(enabledAcq && usesCentroids);

    const bool trackEnabled = enabled && PH_TRACK && usesCentroids
        && m_camera->GetParams().Get<PARAM_CENTROIDS_MODE>()->IsAvail()
        && m_camera->GetParams().Get<PARAM_CENTROIDS_MODE>()->GetCur()
            == PL_CENTROIDS_MODE_TRACK;

    m_editTrackLinkFrames->Enable(enabledAcq && trackEnabled);
    m_editTrackMaxDist->Enable(enabledAcq && trackEnabled);
    m_chbTrackCpuOnly->Enable(enabledAcq && trackEnabled);
    m_editTrackTraj->Enable(enabledAcq && trackEnabled
            && m_chbTrackTraj->GetValue());

    // These can be changed during acquisition
    m_chbTrackTraj->Enable(trackEnabled);
    m_chbMetaRoiOutline->Enable(usesCentroids);
}

void pm::MainDlg::UiUpdateColorEnabled()
{
    UiSetColorControlsEnabled(true);

    if (m_imageDlg)
    {
        m_imageDlg->SetColorEnabled(m_colorCapable && m_colorEnabled);
    }
    for (auto roiImageDlg : m_roiImageDlgs)
    {
        roiImageDlg->SetColorEnabled(m_colorCapable && m_colorEnabled);
    }
}

void pm::MainDlg::UiUpdateColorContext()
{
    const int32_t colorMask =
        (m_colorCapable && m_colorEnabled && m_colorMaskOverridden)
        ? m_customColorMask
        : m_colorMask;

    if (!PH_COLOR || colorMask == COLOR_NONE)
        return;

    const int32_t imageFormat =
        m_camera->GetParams().Get<PARAM_IMAGE_FORMAT>()->IsAvail()
        ? m_camera->GetParams().Get<PARAM_IMAGE_FORMAT>()->GetCur()
        : (colorMask == COLOR_NONE) ? PL_IMAGE_FORMAT_MONO16 : PL_IMAGE_FORMAT_BAYER16;
    int32_t rgbFormat;
    switch (imageFormat)
    {
    case PL_IMAGE_FORMAT_MONO8:
    case PL_IMAGE_FORMAT_BAYER8:
        rgbFormat = PH_COLOR_RGB_FORMAT_RGB24;
        break;
    case PL_IMAGE_FORMAT_MONO16:
    case PL_IMAGE_FORMAT_BAYER16:
        rgbFormat = PH_COLOR_RGB_FORMAT_RGB48;
        break;
    default:
        Log::LogE("Color processing not supported for current image format");
        return;
    }

    m_colorContext->algorithm = m_settings.GetColorDebayerAlgorithm();
    m_colorContext->pattern = colorMask;
    m_colorContext->bitDepth =
        m_camera->GetParams().Get<PARAM_BIT_DEPTH>()->GetCur();
    m_colorContext->rgbFormat = rgbFormat;
    m_colorContext->redScale = m_settings.GetColorWbScaleRed();
    m_colorContext->greenScale = m_settings.GetColorWbScaleGreen();
    m_colorContext->blueScale = m_settings.GetColorWbScaleBlue();
    m_colorContext->autoExpAlgorithm = m_autoExposureAlgorithm;
    m_colorContext->forceCpu = (rs_bool)(m_settings.GetColorCpuOnly() ? TRUE : FALSE);
    m_colorContext->sensorWidth = m_camFullRect.width;
    m_colorContext->sensorHeight = m_camFullRect.height;

    if (PH_COLOR_ERROR_NONE != PH_COLOR->context_apply_changes(m_colorContext))
    {
        ColorUtils::LogError("Failure applying color context changes");
        return;
    }

    if (m_imageDlg)
    {
        m_imageDlg->SetColorContext(m_colorContext);
    }
    for (auto roiImageDlg : m_roiImageDlgs)
    {
        roiImageDlg->SetColorContext(m_colorContext);
    }
}

void pm::MainDlg::UiUpdateProgressValue(bool isInf, int range, int value)
{
    if (isInf)
    {
        m_progressBar->Pulse();
    }
    else
    {
        // After Pulse() the range and value are undefined, set it unconditionally
        m_progressBar->SetRange(range);
        m_progressBar->SetValue(value);
    }
}

void pm::MainDlg::UiUpdateFrameSliderValue(int index, int frameNr)
{
    if (frameNr > 0)
    {
        m_sliderFrame->SetValue(index);
    }
    m_lblCircFrameNr->SetLabelText(wxString::Format("%d", frameNr));
}

void pm::MainDlg::UiUpdateExposures()
{
    if (m_settings.GetTrigMode() == VARIABLE_TIMED_MODE)
    {
        UiUpdateVtmExposures();
    }
    else if (!m_smartExposures.empty())
    {
        UiUpdateSmartExposures();
    }
    else
    {
        UiUpdateExposure();
    }
}

void pm::MainDlg::UiUpdateExposure()
{
    m_editExpTimes->SetValue(std::to_string(m_settings.GetExposure()));

    // TODO: Set the tooltip on better place and not so often
    const auto expRes = m_settings.GetExposureResolution();
    const auto min = m_expTimeResLimits.at(expRes).first;
    const auto max = m_expTimeResLimits.at(expRes).second;
    std::string toolTipStr = "Exposure time in range from "
        + std::to_string(min) + " to " + std::to_string(max) + ".";
    m_editExpTimes->SetToolTip(toolTipStr);
}

void pm::MainDlg::UiUpdateVtmExposures()
{
    m_editExpTimes->SetValue(ArrayToStr(m_settings.GetVtmExposures(), ','));

    // TODO: Set the tooltip on better place and not so often
    const auto expRes = m_settings.GetExposureResolution();
    auto min = m_expTimeResLimits.at(expRes).first;
    auto max = m_expTimeResLimits.at(expRes).second;
    min = pm::Clamp<uint32_t>(min, 1, std::numeric_limits<uint16_t>::max());
    max = pm::Clamp<uint32_t>(max, 1, std::numeric_limits<uint16_t>::max());
    std::string toolTipStr = "Exposures for Variable Timed trigger mode.\n";
    toolTipStr += "Comma-separated numbers in range from "
        + std::to_string(min) + " to " + std::to_string(max) + ".";
    m_editExpTimes->SetToolTip(toolTipStr);
}

void pm::MainDlg::UiUpdateSmartExposures()
{
    m_editExpTimes->SetValue(ArrayToStr(m_smartExposures, ','));

    // TODO: Set the tooltip on better place and not so often
    const auto expRes = m_settings.GetExposureResolution();
    const auto min = m_expTimeResLimits.at(expRes).first;
    const auto max = m_expTimeResLimits.at(expRes).second;
    std::string toolTipStr = "Exposures for Smart Streaming mode.\n";
    toolTipStr += "Max. " + std::to_string(m_smartExposuresMax)
        + " comma-separated numbers in range from "
        + std::to_string(min) + " to " + std::to_string(max) + ".";
    m_editExpTimes->SetToolTip(toolTipStr);
}

void pm::MainDlg::UiUpdateFrameRate(double acqFps)
{
    m_lblAcqFps->SetLabelText(wxString::Format("%.1f", acqFps));
}

void pm::MainDlg::UiUpdateAcqStats(size_t /*valid*/, size_t lost, size_t max,
        size_t cached)
{
    // wxString::Format before 3.1.0 is unable to format size_t with "%zu".
    // Used "%llu" and casting due to 32 bits.
    m_lblBufferAcqLost->SetLabelText((lost > 0)
            ? wxString::Format("(%llu lost)", (unsigned long long)lost)
            : "");
    m_lblBufferAcqMax->SetLabelText(wxString::Format("%llu / %llu",
                (unsigned long long)cached, (unsigned long long)max));
    const int range = (max > (size_t)std::numeric_limits<int>::max())
        ? std::numeric_limits<int>::max()
        : (int)max;
    const int value = (int)(((double)range / max) * cached);
    m_gaugeBufferAcq->SetRange(range);
    m_gaugeBufferAcq->SetValue(value);
    // Workaround for layout bug of right aligned labels after setting their text
    m_gaugeBufferAcq->GetContainingSizer()->Layout();
}

void pm::MainDlg::UiUpdateDiskStats(size_t /*valid*/, size_t lost, size_t max,
        size_t cached)
{
    // wxString::Format before 3.1.0 is unable to format size_t with "%zu".
    // Used "%llu" and casting due to 32 bits.
    m_lblBufferDiskLost->SetLabelText((lost > 0)
            ? wxString::Format("(%llu dropped)", (unsigned long long)lost)
            : "");
    m_lblBufferDiskMax->SetLabelText(wxString::Format("%llu / %llu",
                (unsigned long long)cached, (unsigned long long)max));
    const int range = (max > (size_t)std::numeric_limits<int>::max())
        ? std::numeric_limits<int>::max()
        : (int)max;
    const int value = (int)(((double)range / max) * cached);
    m_gaugeBufferDisk->SetRange(range);
    m_gaugeBufferDisk->SetValue(value);
    // Workaround for layout bug of right aligned labels after setting their text
    m_gaugeBufferDisk->GetContainingSizer()->Layout();
}

void pm::MainDlg::UiUpdateFrameDescText(const std::string& text)
{
    m_lblFrameDesc->ChangeValue(text);
}

float pm::MainDlg::ConvertWbSliderValueToScale(int value)
{
    if (value < ConvertWbScaleToSliderValue(WB_SCALE_MIN))
    {
        value = WB_SCALE_MIN;
    }
    else if (value > ConvertWbScaleToSliderValue(WB_SCALE_MAX))
    {
        value = WB_SCALE_MAX;
    }

    return std::pow(10, value / 200.0);
}

int pm::MainDlg::ConvertWbScaleToSliderValue(float value)
{
    if (value < WB_SCALE_MIN)
    {
        value = WB_SCALE_MIN;
    }
    else if (value > WB_SCALE_MAX)
    {
        value = WB_SCALE_MAX;
    }

    return (int)std::round(std::log10(value) * 200.0);
}

void PV_DECL pm::MainDlg::CameraRemovalCallback(FRAME_INFO* /*frameInfo*/,
        void* MainDlg_pointer)
{
    MainDlg* thiz = static_cast<MainDlg*>(MainDlg_pointer);
    thiz->m_cameraRemovedFlag = true;
    thiz->UiSetAllControlsEnabled(false);
    thiz->StopAcq();
    Log::LogW("Camera has been disconnected\n");
}

bool pm::MainDlg::ItemAdded(const wxDataViewItem& WXUNUSED(parent),
        const wxDataViewItem& WXUNUSED(item))
{
    UiSetRoiButtonsEnabled(true);
    return true;
}

bool pm::MainDlg::ItemDeleted(const wxDataViewItem& WXUNUSED(parent),
        const wxDataViewItem& WXUNUSED(item))
{
    UiSetRoiButtonsEnabled(true);
    return true;
}

bool pm::MainDlg::ItemChanged(const wxDataViewItem& WXUNUSED(item))
{
    UiSetRoiButtonsEnabled(true);
    return true;
}

bool pm::MainDlg::ItemsAdded(const wxDataViewItem& WXUNUSED(parent),
        const wxDataViewItemArray& WXUNUSED(items))
{
    UiSetRoiButtonsEnabled(true);
    return true;
}

bool pm::MainDlg::ItemsDeleted(const wxDataViewItem& WXUNUSED(parent),
        const wxDataViewItemArray& WXUNUSED(items))
{
    UiSetRoiButtonsEnabled(true);
    return true;
}

bool pm::MainDlg::ItemsChanged(const wxDataViewItemArray& WXUNUSED(items))
{
    UiSetRoiButtonsEnabled(true);
    return true;
}

bool pm::MainDlg::Cleared()
{
    UiSetRoiButtonsEnabled(true);
    return true;
}

void pm::MainDlg::Resort()
{
}

bool pm::MainDlg::ValueChanged(const wxDataViewItem& WXUNUSED(item),
        unsigned int WXUNUSED(col))
{
    return true;
}

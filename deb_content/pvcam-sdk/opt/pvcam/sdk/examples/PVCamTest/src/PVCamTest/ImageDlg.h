/******************************************************************************/
/* Copyright (C) Teledyne Photometrics. All rights reserved.                  */
/******************************************************************************/
#pragma once
#ifndef PM_IMAGEDLG_H
#define PM_IMAGEDLG_H

// WxWidgets headers have to be included first
#include <wx/event.h>
// Cannot use wxOverlay in wxScrolled due to http://trac.wxwidgets.org/ticket/14415
//#include <wx/overlay.h>
#include "PVCamTest_Ui.h"

/* Local */
#include "backend/Frame.h"
#include "backend/FrameProcessor.h"
#include "backend/TaskSet_ConvertToRgb8.h"
#include "IRegionMouseMoveListener.h"

/* PVCAM */
#include "master.h"
#include "pvcam.h"

/* pvcam_helper_color */
#include "pvcam_helper_color.h"

/* System */
#include <atomic>
#include <memory>
#include <mutex>
#include <string>
#include <vector>

// Holds wxSize as payload data
wxDECLARE_EVENT(PM_EVT_UPDATE_CANVAS_SIZE, wxThreadEvent);

namespace pm {

/* Forward declarations */
class Bitmap;
class Camera;
class RegionModel;

class ImageDlg final : public ui::ImageDlg, private IRegionMouseMoveListener
{
public:
    enum class FillMethod : int32_t
    {
        NoFill = -2,
        MeanValue = -1,
        BlackColor = 0,
        CustomColor = 1,
    };

    enum class ZoomFactor : int32_t
    {
        Out_12_5 = -3, // 2 ^ -3
        Out_25 = -2, // 2 ^ -2
        Out_50 = -1, // 2 ^ -1
        None_100 = 0, // 2 ^ 0
        In_200 = 1, // 2 ^ +1
        In_400 = 2, // 2 ^ +2
    };

private:
    // Values to be stored first in m_workOtfSetup.
    // m_workThread will apply it to m_otfSetup.
    struct OnTheFlySetup
    {
        // Settable members

        FillMethod fillMethod{ FillMethod::MeanValue };
        bool autoConbright{ true };
        int contrast{ 0 };
        int brightness{ 0 };
        ZoomFactor zoomFactor{ ZoomFactor::None_100 };
        bool colorEnabled{ false };
        ph_color_context* colorContext{ nullptr }; // Cannot be smart pointer
        bool trackShowTrajectory{ true };
        bool metaRoiShowOutline{ true };
        bool roiShowOutline{ true };

        // Helpers

        // Real zoom to be applied on image
        double zoom{ 1.0 };
        // Cached value, true only if colorEnabled && m_sbin == 1 && m_pbin == 1
        bool isColor{ false };
    };

public:
    ImageDlg();
    ImageDlg(const ImageDlg&) = delete;
    ImageDlg& operator=(const ImageDlg&) = delete;
    virtual ~ImageDlg();

public:
    void ActivateWindow(bool active = true);
    void EnableEditor(bool enabled = true);

public: // OnTheFlySetup setters, settings that can be changed during acquisition
    void SetFillMethod(FillMethod fillMethod);
    void SetAutoConbright(bool enabled);
    void SetContrast(int contrast);
    void SetBrightness(int brightness);
    void SetZoomFactor(ZoomFactor zoomFactor);
    void SetColorEnabled(bool enabled);
    void SetColorContext(const ph_color_context* context);
    void SetTrackShowTrajectory(bool enabled);
    void SetMetaRoiShowOutline(bool enabled);
    void SetRoiShowOutline(bool enabled);

public:
    // (Re)allocates image buffers.
    // roiIdx < -1 is means uninitialized window.
    // roiIdx == -1 means main full-image and resizable window that allows
    //   drawing regions using mouse. In this case the rect argument is full
    //   sensor rectangle or implied ROI in case of multiple regions.
    //   If one ROI without metadata is captured, the actual rect is given to
    //   UpdateImage (differs from rect here).
    // roiIdx >= 0 is means window with ROI image data.
    bool SetImageFormat(short roiIdx, Camera* camera, RegionModel* regionModel);
    // The size of the image has to match parameters given via SetImageFormat.
    void UpdateImage(std::shared_ptr<Frame> frame);
    // Saves image in TIFF format to given file
    void SaveImage(const std::string& fullFileName);

private: // UI event handlers
    void OnClose(wxCloseEvent& event);
    void OnCanvasContextMenu(wxContextMenuEvent& event);

private: // Any thread to GUI thread "forwarders"
    wxSize FireEventUpdateCanvasSize();
    void DoUpdateCanvasSize(wxSize size);

private: // IRegionMouseMoveListener
    virtual void OnRegionMouseMoved(int x, int y) override;

private:
    // Saves image in TIFF format to given file without locking m_mutex
    void DoSaveImage(const std::string& fullFileName);

    // Releases all memory allocated in SetImageFormat
    void FreeBuffers();

    // Processing thread body
    void WorkThreadFunc();
    // This method is called from a processing thread. The thread is waiting for
    // Frame or OnTheFlySetup to be set.
    void ProcessImage();

    // Converts particles to WxRects
    void ConvertOutlinesToWxRects();
    // Reconstructs frame from particular regions/centroids
    void RecomposeFrame(Bitmap* dstBmp, FrameProcessor::UseBmp srcBmpType,
            double fillValue);

private:
    // Mutex that secures all members
    mutable std::mutex m_mutex{};

    std::string m_defaultTitle{};

    // <-1 ~ uninitialized, -1 ~ main full-frame, >=0 ~ ROI index
    short m_roiIdx{ -2 };
    // Frame/roi size
    wxRect m_rect{};
    // Frame/roi size (either full-frame or same as m_rect)
    wxRect m_rectFull{};

    // Serial binning
    uint16_t m_sbin{ 0 };
    // Parallel binning
    uint16_t m_pbin{ 0 };
    // Frame acquisition configuration
    Frame::AcqCfg m_frameAcqCfg{};
    // Frame consists of centroids, not regular regions
    bool m_usesCentroids{ false };
    // The mode of centroids, locate, track, blob
    int32_t m_centroidsMode{ PL_CENTROIDS_MODE_LOCATE };
    // Radius of centroids (mainly to fix blob size that FW sends as 1x1)
    uint16_t m_centroidsRadius{ 1 };
    // Exposure time resolution from Camera class
    int32_t m_expTimeRes{ EXP_RES_ONE_MILLISEC };

    std::shared_ptr<Frame> m_frame{ nullptr };
    // m_frame is copied to m_frameOld before gets replaced by m_workFrame
    // The data flow is: app -> m_workFrame -> (m_frame -> m_frameOld)
    std::shared_ptr<Frame> m_frameOld{ nullptr };

    FrameProcessor m_frameProcessor{};

    // Points either to m_nativeFrameFullBmp or m_rgbFrameFullBmp
    Bitmap* m_frameFullBmp{ nullptr };
    // Wrapper with buffer for recomposed Mono frame (without metadata)
    Bitmap* m_nativeFrameFullBmp{ nullptr };
    // Wrapper with buffer for recomposed debayered RGB frame (without metadata)
    Bitmap* m_rgbFrameFullBmp{ nullptr };

    // Pixel value used to fill areas without real pixels
    double m_fillValue{ 0 };

    OnTheFlySetup m_otfSetup{};
    // m_otfSetup is copied to m_otfSetupOld before gets replaced by m_workOtfSetup
    // The data flow is: app -> m_workOtfSetup -> (m_otfSetup -> m_otfSetupOld)
    OnTheFlySetup m_otfSetupOld{};

    std::vector<wxRect> m_outlines{};

    std::unique_ptr<std::thread> m_workThread{ nullptr };
    std::atomic<bool>            m_workThreadAbortFlag{ false };
    std::mutex                   m_workMutex{};
    std::condition_variable      m_workCond{};
    bool                         m_workUpdatedFlag{ false };
    std::shared_ptr<Frame>       m_workFrame{ nullptr };
    OnTheFlySetup                m_workOtfSetup{};

    TaskSet_ConvertToRgb8 m_tasksConvToRgb8; // Initialized in constructor
    std::vector<uint8_t> m_convToRgb8bitLookupMap{};
};

} // namespace pm

#endif // PM_IMAGEDLG_H

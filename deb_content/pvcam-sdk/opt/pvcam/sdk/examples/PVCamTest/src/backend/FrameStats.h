/******************************************************************************/
/* Copyright (C) Teledyne Photometrics. All rights reserved.                  */
/******************************************************************************/
#pragma once
#ifndef PM_FRAME_STATS_H
#define PM_FRAME_STATS_H

/* System */
#include <cstdint>

namespace pm {

class FrameStats
{
public:
    explicit FrameStats();

public:
    void Clear();
    void Set(const FrameStats& stats);
    void Set(double min, double max, double sum, uint32_t pixelCount);

    double GetMin() const;
    void SetMin(double min);

    double GetMax() const;
    void SetMax(double max);

    double GetMean() const;
    void SetMean(double mean);

    double GetSum() const;
    void SetSum(double sum);

    uint32_t GetPixelCount() const;
    void SetPixelCount(uint32_t pixelCount);

private:
    double m_min;
    double m_max;
    double m_mean;
    double m_sum;
    uint32_t m_pixelCount;
};

} // namespace pm

#endif /* PM_FRAME_STATS_H */

This project requires WX widgets.
Optional helper libraries are searched and loaded at run-time.


On Windows:
===========


Run-time dependences:
---------------------

None


Build dependences:
------------------

The application is statically linked with WX widgets and libtiff libraries.

You need to build WX widgets manually with Visual Studio Command prompt for both
x86 and x64 configurations.
Use build_PMQI_wxWidgets.bat script with default settings and follow instructions in it.

Once you have built the WX widgets open Visual Studio solution,
go to Property Manager double click on "PropertySheet" under each configuration,
choose "User Macros" and make sure the WXDIR points to the correct directory.


On Linux:
=========


Run-time dependences:
---------------------

On Ubuntu 16.04 install following packages:
- libwxgtk3.0-0v5
- libtiff5


Build dependences:
------------------

The application is dynamically linked with WX widgets and libtiff libraries.

On Ubuntu 16.04 install following packages:
- libwxgtk3.0-dev
- libtiff5-dev

/******************************************************************************/
/* Copyright (C) Teledyne Photometrics. All rights reserved.                  */
/******************************************************************************/

// Local
#include "Common.h"

// This sample application demonstrates acquisition of single frames with
// callback notification in a loop which collects 5 frames.
// Each acquisition is started by the host with a software trigger
// (pl_exp_start_seq()).
// Please note pl_exp_finish_seq() needs to be called after each frame is
// acquired before new software trigger pl_exp_start_seq() is sent.
int main(int argc, char* argv[])
{
    if (!ShowAppInfo(argc, argv))
        return APP_EXIT_ERROR;

    std::vector<CameraContext*> contexts;
    if (!InitAndOpenOneCamera(contexts, cSingleCamIndex))
        return APP_EXIT_ERROR;

    CameraContext* ctx = contexts[cSingleCamIndex];

    // Install generic ctr+c handler
    if (!InstallGenericCliTerminationHandler(contexts))
    {
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }

    // Register callback to receive a notification when EOF event arrives.
    // The handler function will be called by PVCAM, last parameter
    // might hold a pointer to user-specific content that will be passed with
    // the callback once it is invoked.
    if (PV_OK != pl_cam_register_callback_ex3(ctx->hcam, PL_CALLBACK_EOF,
                (void*)GenericEofHandler, ctx))
    {
        PrintErrorMessage(pl_error_code(), "pl_cam_register_callback() error");
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }
    printf("EOF callback handler registered on camera %d\n", ctx->hcam);

    uns32 exposureBytes;
    const uns32 exposureTime = 40; // milliseconds

    // Setup the acquisition
    // TIMED_MODE flag means acquisition will start with software trigger,
    // other flags such as STROBED_MODE can be used for hardware trigger.
    if (PV_OK != pl_exp_setup_seq(ctx->hcam, 1, 1, &ctx->region, TIMED_MODE,
                exposureTime, &exposureBytes))
    {
        PrintErrorMessage(pl_error_code(), "pl_exp_setup_seq() error");
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }
    printf("Acquisition setup successful on camera %d\n", ctx->hcam);
    UpdateCtxImageFormat(ctx);

    // Allocate buffer, used uns8 to allocate raw bytes
    uns8* frameInMemory = new (std::nothrow) uns8[exposureBytes];
    if (!frameInMemory)
    {
        printf("Unable to allocate buffer for camera %d\n", ctx->hcam);
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }

    bool errorOccurred = false;
    uns32 imageCounter = 0;
    while (imageCounter < 5)
    {
        // Start the acquisition - it is used as software trigger in TIMED
        // trigger mode. In hardware trigger mode (Strobe or Bulb) after this
        // call camera waits for external trigger signal.
        if (PV_OK != pl_exp_start_seq(ctx->hcam, frameInMemory))
        {
            PrintErrorMessage(pl_error_code(), "pl_exp_start_seq() error");
            errorOccurred = true;
            break;
        }
        printf("Acquisition started on camera %d\n", ctx->hcam);

        // Here we need to wait for a frame readout notification signaled by
        // event raised in the callback.
        printf("Waiting for EOF event to occur on camera %d\n", ctx->hcam);
        if (!WaitForEofEvent(ctx, 5000, errorOccurred))
            break;

        printf("Frame #%u has been delivered from camera %d\n",
                imageCounter + 1, ctx->hcam);
        ShowImage(ctx, frameInMemory, exposureBytes);

        // When acquiring single frames with callback notifications call this
        // after each frame before new acquisition is started with pl_exp_start_seq()
        if (PV_OK != pl_exp_finish_seq(ctx->hcam, frameInMemory, 0))
        {
            PrintErrorMessage(pl_error_code(), "pl_exp_finish_seq() error");
        }
        else
        {
            printf("Acquisition finished on camera %d\n", ctx->hcam);
        }

        imageCounter++;
    }

    if (PV_OK != pl_exp_abort(ctx->hcam, CCS_NO_CHANGE))
    {
        PrintErrorMessage(pl_error_code(), "pl_exp_abort() error");
    }

    delete [] frameInMemory;

    CloseAllCamerasAndUninit(contexts);

    if (errorOccurred)
        return APP_EXIT_ERROR;
    return 0;
}

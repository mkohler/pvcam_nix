/******************************************************************************/
/* Copyright (C) Teledyne Photometrics. All rights reserved.                  */
/******************************************************************************/

// Local
#include "Common.h"

// This sample application configures the camera for Centroids acquisition.
// Centroids are basically multiple ROIs that are chosen by the camera based
// on image analysis so the approach for decoding the ROIs (centroids) from
// the buffer is exactly the same as in User-defined-multiple-ROIS case.
// Please see the MultipleRegions example for reference.
// The example acquires a couple of centroid-enabled frames and prints all
// relevant centroid information to console output.
int main(int argc, char* argv[])
{
    if (!ShowAppInfo(argc, argv))
        return APP_EXIT_ERROR;

    std::vector<CameraContext*> contexts;
    if (!InitAndOpenOneCamera(contexts, cSingleCamIndex))
        return APP_EXIT_ERROR;

    CameraContext* ctx = contexts[cSingleCamIndex];

    // Check if the camera supports Centroids feature
    if (!IsParamAvailable(ctx->hcam, PARAM_CENTROIDS_ENABLED, "PARAM_CENTROIDS_ENABLED"))
    {
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }

    // Read the number of centroids supported by the camera
    uns16 centroidsCountMin = 0;
    if (PV_OK != pl_get_param(ctx->hcam, PARAM_CENTROIDS_COUNT, ATTR_MIN,
                (void*)&centroidsCountMin))
    {
        PrintErrorMessage(pl_error_code(),
                "pl_get_param(PARAM_CENTROIDS_COUNT, ATTR_MIN) error");
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }
    uns16 centroidsCountMax = 0;
    if (PV_OK != pl_get_param(ctx->hcam, PARAM_CENTROIDS_COUNT, ATTR_MAX,
                (void*)&centroidsCountMax))
    {
        PrintErrorMessage(pl_error_code(),
                "pl_get_param(PARAM_CENTROIDS_COUNT, ATTR_MAX) error");
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }

    printf("How many centroids would you like to configure the camera for?\n");
    const int desiredCentroidsCount =
        ConsoleReadNumber(centroidsCountMin, centroidsCountMax, centroidsCountMax);

    // Set the desired number of centroids
    if (PV_OK != pl_set_param(ctx->hcam, PARAM_CENTROIDS_COUNT,
                (void*)&desiredCentroidsCount))
    {
        PrintErrorMessage(pl_error_code(),
                "pl_set_param(PARAM_CENTROIDS_COUNT) error");
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }

    // Read the allowed centroids radius range from the camera
    uns16 centroidsRadiusMin = 0;
    if (PV_OK != pl_get_param(ctx->hcam, PARAM_CENTROIDS_RADIUS, ATTR_MIN,
                (void*)&centroidsRadiusMin))
    {
        PrintErrorMessage(pl_error_code(),
                "pl_get_param(PARAM_CENTROIDS_RADIUS, ATTR_MIN) error");
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }
    uns16 centroidsRadiusMax = 0;
    if (PV_OK != pl_get_param(ctx->hcam, PARAM_CENTROIDS_RADIUS, ATTR_MAX,
                (void*)&centroidsRadiusMax))
    {
        PrintErrorMessage(pl_error_code(),
                "pl_get_param(PARAM_CENTROIDS_RADIUS, ATTR_MAX) error");
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }

    printf("What centroids radius would you like to use?\n");
    const int desiredCentroidsRadius =
        ConsoleReadNumber(centroidsRadiusMin, centroidsRadiusMax, centroidsRadiusMax);

    // Set the desired centroids radius
    if (PV_OK != pl_set_param(ctx->hcam, PARAM_CENTROIDS_RADIUS,
                (void*)&desiredCentroidsRadius))
    {
        PrintErrorMessage(pl_error_code(),
                "pl_get_param(PARAM_CENTROIDS_RADIUS, ATTR_MAX) error");
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }

    // Install generic ctr+c handler
    if (!InstallGenericCliTerminationHandler(contexts))
    {
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }

    printf("Enabling embedded frame metadata...\n");

    // Centroids feature requires metadata to be enabled
    if (!IsParamAvailable(ctx->hcam, PARAM_METADATA_ENABLED, "PARAM_METADATA_ENABLED"))
    {
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }
    rs_bool bEnable = TRUE;
    if (PV_OK != pl_set_param(ctx->hcam, PARAM_METADATA_ENABLED, (void*)&bEnable))
    {
        PrintErrorMessage(pl_error_code(),
                "pl_set_param(PARAM_METADATA_ENABLED, TRUE) error");
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }

    // Register callback to receive a notification when EOF event arrives.
    // The handler function will be called by PVCAM, last parameter
    // might hold a pointer to user-specific content that will be passed with
    // the callback once it is invoked.
    if (PV_OK != pl_cam_register_callback_ex3(ctx->hcam, PL_CALLBACK_EOF,
                (void*)GenericEofHandler, ctx))
    {
        PrintErrorMessage(pl_error_code(), "pl_cam_register_callback() error");
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }
    printf("EOF callback handler registered on camera %d\n", ctx->hcam);

    uns32 exposureBytes;
    const uns32 exposureTime = 10; // milliseconds
    const uns16 framesToAcquire = 5;

    if (PV_OK != pl_exp_setup_seq(ctx->hcam, framesToAcquire, 1, &ctx->region,
                TIMED_MODE, exposureTime, &exposureBytes))
    {
        PrintErrorMessage(pl_error_code(), "pl_exp_setup_seq() error");
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }
    printf("Acquisition setup successful on camera %d\n", ctx->hcam);
    UpdateCtxImageFormat(ctx);

    // Calculate size of each frame
    const uns32 oneFrameBytes = exposureBytes / framesToAcquire;

    // Allocate buffer, used uns8 to allocate raw bytes
    uns8* pSequenceBuffer = new (std::nothrow) uns8[exposureBytes];
    if (!pSequenceBuffer)
    {
        printf("Unable to allocate buffer for camera %d\n", ctx->hcam);
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }

    // Prepare the frame descriptor that will be used to extract the centroids
    // from the image buffer. Since we know how many centroids we requested
    // we can pre-allocate a single descriptor and reuse it for each frame.
    md_frame* pFrameDesc = NULL;
    if (PV_OK != pl_md_create_frame_struct_cont(&pFrameDesc,
                (uns16)desiredCentroidsCount))
    {
        PrintErrorMessage(pl_error_code(),
                "pl_md_create_frame_struct_cont() error");
        delete [] pSequenceBuffer;
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }

    // Start the acquisition
    if (PV_OK != pl_exp_start_seq(ctx->hcam, pSequenceBuffer))
    {
        PrintErrorMessage(pl_error_code(), "pl_exp_start_seq() error");
        pl_md_release_frame_struct(pFrameDesc);
        delete [] pSequenceBuffer;
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }
    printf("Acquisition started on camera %d\n", ctx->hcam);

    bool errorOccurred = false;
    uns32 framesAcquired = 0;
    while (framesAcquired < framesToAcquire)
    {
        // Here we need to wait for a frame readout notification signaled by
        // event raised in the callback.
        printf("Waiting for EOF event to occur on camera %d\n", ctx->hcam);
        if (!WaitForEofEvent(ctx, 5000, errorOccurred))
            break;

        // Extract the frame metadata and data to our frame descriptor
        if (PV_OK != pl_md_frame_decode(pFrameDesc, ctx->eofFrame, oneFrameBytes))
        {
            PrintErrorMessage(pl_error_code(), "pl_md_frame_decode() error");
            errorOccurred = true;
            break;
        }

        printf("Frame #%u out of %u has been delivered from camera %d:\n",
                framesAcquired + 1, framesToAcquire, ctx->hcam);

        // Now we can decide whether to display the Centroids one by one or
        // create a black filled frame and display the entire image the same way
        // as we would retrieve an RAW frame from PVCAM.
        // At the end of the day the Centroids are simple ROIs, but generated by
        // the camera.
        // Please see the MultipleRegions and pl_md_frame_recompose() for more
        // details. In this example, we just print the metadata and pixel values
        // to console.
        PrintMetaFrame(ctx, pFrameDesc, false);

        framesAcquired++;
    }

    if (PV_OK != pl_exp_abort(ctx->hcam, CCS_NO_CHANGE))
    {
        PrintErrorMessage(pl_error_code(), "pl_exp_abort() error");
    }

    if (PV_OK != pl_exp_finish_seq(ctx->hcam, pSequenceBuffer, 0))
    {
        PrintErrorMessage(pl_error_code(), "pl_exp_finish_seq() error");
    }
    else
    {
        printf("Acquisition finished on camera %d\n", ctx->hcam);
    }

    if (PV_OK != pl_md_release_frame_struct(pFrameDesc))
    {
        PrintErrorMessage(pl_error_code(), "pl_md_release_frame_struct() error");
    }

    delete [] pSequenceBuffer;

    CloseAllCamerasAndUninit(contexts);

    if (errorOccurred)
        return APP_EXIT_ERROR;
    return 0;
}

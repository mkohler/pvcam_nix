/******************************************************************************/
/* Copyright (C) Teledyne Photometrics. All rights reserved.                  */
/******************************************************************************/

// Local
#include "Common.h"

// System
#include <algorithm> // std::min
#include <iostream> // std::cin

// This sample application demonstrates acquisition of single frames with
// callback notification in a loop which collects 5 frames.
// Each acquisition is started by the host with a software trigger
// (pl_exp_start_seq()).
// Please note pl_exp_finish_seq() needs to be called after each frame is
// acquired before new software trigger pl_exp_start_seq() is sent.
//
// Also note that extended binning factors are fully supported on new cameras
// only. The extended binning factors are reported via pair of parameters
// PARAM_BINNING_SER and PARAM_BINNING_PAR. The acquisition is then configured
// via setup function as usually.
int main(int argc, char* argv[])
{
    if (!ShowAppInfo(argc, argv))
        return APP_EXIT_ERROR;

    std::vector<CameraContext*> contexts;
    if (!InitAndOpenOneCamera(contexts, cSingleCamIndex))
        return APP_EXIT_ERROR;

    CameraContext* ctx = contexts[cSingleCamIndex];

    // Fill NVP containers with serial and parallel binning factors
    NVPC binsSer;
    if (!ReadEnumeration(ctx->hcam, &binsSer, PARAM_BINNING_SER, "PARAM_BINNING_SER"))
    {
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }
    NVPC binsPar;
    if (!ReadEnumeration(ctx->hcam, &binsPar, PARAM_BINNING_PAR, "PARAM_BINNING_PAR"))
    {
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }

    // Even the size of NVP containers for serial and parallel binning factors
    // should be the same, let's ensure here we don't touch out of container.
    const uns32 binCount =
        (uns32)std::min<size_t>(binsSer.size(), binsPar.size());

    // Install generic ctr+c handler
    if (!InstallGenericCliTerminationHandler(contexts))
    {
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }

    // Register callback to receive a notification when EOF event arrives.
    // The handler function will be called by PVCAM, last parameter
    // might hold a pointer to user-specific content that will be passed with
    // the callback once it is invoked.
    if (PV_OK != pl_cam_register_callback_ex3(ctx->hcam, PL_CALLBACK_EOF,
                (void*)GenericEofHandler, ctx))
    {
        PrintErrorMessage(pl_error_code(), "pl_cam_register_callback() error");
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }
    printf("EOF callback handler registered on camera %d\n", ctx->hcam);

    uns32 exposureBytes;
    const uns32 exposureTime = 40; // milliseconds

    // Let user choose binning factors
    // Repeat the acquisition until invalid or no value is entered
    bool errorOccurred = false;
    std::string binFactors;
    for (;;)
    {
        // Print the values and names returned from PVCAM
        // Related string description for each enum value combines serial and
        // parallel binning already. There is no need to re-create the string
        // from numeric values. Those string are the same for serial and
        // parallel NVP.
        printf("\n");
        printf("Supported extended binning factors:\n");
        printf("==============================================\n");
        for (uns32 n = 0; n < binCount; n++)
        {
            printf("    %s (serial %d x parallel %d)\n",
                    binsSer[n].name.c_str(), binsSer[n].value, binsPar[n].value);
        }
        printf("\n");

        printf("Type required binning factors combination and press <Enter>: ");
        binFactors = WaitForInput();
        // If pressed ctrl+c while waiting to user input, stream sets a fail flag
        if (std::cin.fail())
        {
            errorOccurred = true;
            break;
        }

        // Break the loop if no value entered
        if (binFactors.empty())
        {
            printf("No binning factors entered, exiting...\n");
            break;
        }

        uns32 n;
        for (n = 0; n < binCount; n++)
        {
            if (binsSer[n].name == binFactors)
                break;
        }
        // Break the loop if invalid value entered
        if (n >= binCount)
        {
            printf("Invalid binning factors entered, exiting...\n");
            break;
        }

        // Update binning factors in region variable
        ctx->region.sbin = (uns16)binsSer[n].value;
        ctx->region.pbin = (uns16)binsPar[n].value;

        printf("Capturing data with binning factors %ux%u\n",
                ctx->region.sbin, ctx->region.pbin);

        // Setup a sequence of 1 frames with a 40ms exposure
        if (PV_OK != pl_exp_setup_seq(ctx->hcam, 1, 1, &ctx->region, TIMED_MODE,
                    exposureTime, &exposureBytes))
        {
            PrintErrorMessage(pl_error_code(), "pl_exp_setup_seq() error");
            errorOccurred = true;
            break;
        }
        printf("Acquisition setup successful on camera %d\n", ctx->hcam);
        UpdateCtxImageFormat(ctx);

        // Allocate enough memory to capture all the frames.
        // The frame size is the same for all exposure modes, so we don't
        // need to re-allocate the frame memory again and again. But because
        // we get the size after first call to pl_exp_setup_seq we do it
        // this way to keep example simple.
        uns8* frameInMemory = new (std::nothrow) uns8[exposureBytes];
        if (!frameInMemory)
        {
            printf("Unable to allocate buffer for camera %d\n", ctx->hcam);
            errorOccurred = true;
            break;
        }

        // Send the go signal
        if (PV_OK != pl_exp_start_seq(ctx->hcam, frameInMemory))
        {
            PrintErrorMessage(pl_error_code(), "pl_exp_start_seq() error");
            delete [] frameInMemory;
            errorOccurred = true;
            break;
        }
        printf("Acquisition started on camera %d\n", ctx->hcam);

        // Here we need to wait for a frame readout notification signaled by
        // event raised in the callback.
        printf("Waiting for EOF event to occur on camera %d\n", ctx->hcam);
        if (!WaitForEofEvent(ctx, 5000, errorOccurred))
        {
            delete [] frameInMemory;
            break;
        }

        printf("Frame has been delivered from camera %d\n", ctx->hcam);
        ShowImage(ctx, frameInMemory, exposureBytes);

        if (PV_OK != pl_exp_finish_seq(ctx->hcam, frameInMemory, 0))
        {
            PrintErrorMessage(pl_error_code(), "pl_exp_finish_seq() error");
        }
        else
        {
            printf("Acquisition finished on camera %d\n", ctx->hcam);
        }

        delete [] frameInMemory;
    }

    if (PV_OK != pl_exp_abort(ctx->hcam, CCS_NO_CHANGE))
    {
        PrintErrorMessage(pl_error_code(), "pl_exp_abort() error");
    }

    CloseAllCamerasAndUninit(contexts);

    if (errorOccurred)
        return APP_EXIT_ERROR;
    return 0;
}

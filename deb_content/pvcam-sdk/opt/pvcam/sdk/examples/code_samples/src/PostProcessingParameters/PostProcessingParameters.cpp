/******************************************************************************/
/* Copyright (C) Teledyne Photometrics. All rights reserved.                  */
/******************************************************************************/

// Local
#include "Common.h"

// This sample application demonstrates usage of post processing features
// and parameters. The function shows how to access post processing features
// of a camera without hard coding values, independently of which camera
// is detected and using.
//
// Every camera may support different set of post processing 'features'.
// Features are only groups, they group one or more 'parameters'.
// These 'parameters' are similar to regular PVCAM parameters but they
// need to be discovered by the application at run-time. The 'parameters'
// are currently limited to UNS32 data type.
// The application will usually build a tree similar to the following one:
// - Feature: "B.E.R.T" (PARAM_PP_INDEX=0)
//   - Parameter: "ENABLED" (PARAM_PP_PARAM_INDEX=0)
//   - Parameter: "Threshold" (PARAM_PP_PARAM_INDEX=1)
// - Feature: "DENOISING" (PARAM_PP_INDEX=1)
//   - Parameter: "ENABLED" (PARAM_PP_PARAM_INDEX=0)
//   - Parameter: "Number of iterations" (PARAM_PP_PARAM_INDEX=1)
//   - Parameter: "Sigma" (PARAM_PP_PARAM_INDEX=2)
//
// Because the indexes and their order may be different for every camera
// the PVCAM also provides a PARAM_PP_FEAT_ID and PARAM_PP_PARAM_ID parameters.
// These IDs are uniquely defined in pvcam.h and they are assigned to a particular
// feature/parameter.
// If the application wants to programmatically control a particular PP parameter
// then it should use the ID to find to corresponding indexes first, then set the
// indexes to PVCAM and control the parameter state via PARAM_PP_PARAM parameter.
// The above process is shown in the following code example.
int main(int argc, char* argv[])
{
    if (!ShowAppInfo(argc, argv))
        return APP_EXIT_ERROR;

    std::vector<CameraContext*> contexts;
    if (!InitAndOpenOneCamera(contexts, cSingleCamIndex))
        return APP_EXIT_ERROR;

    CameraContext* ctx = contexts[cSingleCamIndex];

    // Check and determine if there are any post processing features available
    if (!IsParamAvailable(ctx->hcam, PARAM_PP_INDEX, "PARAM_PP_INDEX"))
    {
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }

    // Build the post processing tree first
    const std::vector<PvcamPpFeature> ppFeatures = DiscoverCameraPostProcessing(ctx);
    if (ppFeatures.empty())
    {
        // Some error must have occurred in the above function
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }

    // Print the tree to console, just for information
    printf("Camera Post Processing tree:\n");
    for (const PvcamPpFeature& ppFeature : ppFeatures)
    {
        // Print IDs as signed to display invalid value as -1 instead of huge number.
        // Valid ID is usually small number based on current pattern in pvcam.h.
        printf("- FEATURE at index %d: id=%d, name='%s'\n",
                ppFeature.index, (int32)ppFeature.id, ppFeature.name.c_str());
        for (const PvcamPpParameter& ppParam : ppFeature.parameterList)
        {
            printf("  - PARAMETER at index %d: id=%d, name='%s'\n",
                    ppParam.index, (int32)ppParam.id, ppParam.name.c_str());
        }
    }
    printf("\n");

    int32 menuSelection;

    // Get list of available Post Processing Features for menu
    NVPC menuPpFeatures;
    for (const PvcamPpFeature& ppFeature : ppFeatures)
    {
        NVP nvpFeature;
        nvpFeature.value = ppFeature.index; // Post Processing Feature selection
        nvpFeature.name = ppFeature.name;
        menuPpFeatures.push_back(nvpFeature);
    }
    // Get required feature selection from the interactive menu
    if (!GetMenuSelection("Choose Post Processing Feature",
                menuPpFeatures, menuSelection))
    {
        printf("Not selected valid Feature, exiting...\n");
        CloseAllCamerasAndUninit(contexts);
        return 0; // Not an error, normal way how to exit the app
    }
    const PvcamPpFeature& ppFeature = ppFeatures[menuSelection];
    // Show currently selected Post Processing Feature
    printf("Selected Post Processing Feature: index=%d, name='%s'\n",
            ppFeature.index, ppFeature.name.c_str());

    // Allow user to interact with menus as long as valid input is entered
    for (;;)
    {
        uns32 curValue;

        // Get list of available Parameters of selected Post Processing Feature
        NVPC menuPpParams;
        for (const PvcamPpParameter& ppParam : ppFeature.parameterList)
        {
            if (!GetPpParamValue(ctx, ppFeature.index, ppParam.index, curValue))
            {
                CloseAllCamerasAndUninit(contexts);
                return APP_EXIT_ERROR;
            }
            NVP nvpParam;
            nvpParam.value = ppParam.index; // Post Processing Parameter selection
            nvpParam.name = ppParam.name + ", current value " + std::to_string(curValue);
            menuPpParams.push_back(nvpParam);
        }
        // Get required Parameter selection from the interactive menu
        if (!GetMenuSelection("Choose Post Processing Parameter",
                    menuPpParams, menuSelection))
        {
            printf("Not selected valid Parameter, exiting...\n");
            break; // Not an error, normal way how to exit the app
        }
        // Get selected Post Processing Parameter
        const PvcamPpParameter& ppParam = ppFeature.parameterList[menuSelection];
        // Show currently selected Post Processing Parameter
        printf("Selected Post Processing Parameter: index=%d, name='%s'\n",
                ppParam.index, ppParam.name.c_str());

        // Get new value for set up from the console
        const int number = ConsoleReadNumber(
                (int)ppParam.minValue, (int)ppParam.maxValue, (int)ppParam.defValue);

        // Set new value of selected Post Processing Parameter
        uns32 newValue = (uns32)number;
        if (!SetPpParamValue(ctx, ppFeature.index, ppParam.index, newValue))
        {
            CloseAllCamerasAndUninit(contexts);
            return APP_EXIT_ERROR;
        }
        printf("Change successful!\n");
        if (!GetPpParamValue(ctx, ppFeature.index, ppParam.index, curValue))
        {
            CloseAllCamerasAndUninit(contexts);
            return APP_EXIT_ERROR;
        }
        printf("The value reported by camera is now: %u\n", curValue);
    }

    CloseAllCamerasAndUninit(contexts);

    return 0;
}

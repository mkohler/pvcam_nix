/******************************************************************************/
/* Copyright (C) Teledyne Photometrics. All rights reserved.                  */
/******************************************************************************/

// System
#include <cstring> // std::memset

// Local
#include "Common.h"

// Number of regions used in this example.
constexpr uns16 cRoiCount = 2;

// This sample application demonstrates acquisition of single frames with
// callback notification in a loop which collects 5 frames where each frame
// consists of  2 regions.
// Each acquisition is started by the host with a software trigger
// (pl_exp_start_seq()).
// Please note pl_exp_finish_seq() needs to be called after each frame is
// acquired before new software trigger pl_exp_start_seq() is sent.
//
// Also note that multiple regions are fully supported on new cameras only.
// The acquisition with multiple regions stores additional metadata in user
// image buffer and requires metadata to be enabled via PARAM_METADATA_ENABLED
// parameter. If metadata is not enabled the acquisition setup functions fails.
int main(int argc, char* argv[])
{
    if (!ShowAppInfo(argc, argv))
        return APP_EXIT_ERROR;

    std::vector<CameraContext*> contexts;
    if (!InitAndOpenOneCamera(contexts, cSingleCamIndex))
        return APP_EXIT_ERROR;

    CameraContext* ctx = contexts[cSingleCamIndex];

    // Ensure that camera supports multiple regions
    if (!IsParamAvailable(ctx->hcam, PARAM_ROI_COUNT, "PARAM_ROI_COUNT"))
    {
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }
    uns16 roiCount;
    // ATTR_COUNT or ATTR_MAX can be checked here, both give the same value
    if (PV_OK != pl_get_param(ctx->hcam, PARAM_ROI_COUNT, ATTR_MAX,
                (void*)&roiCount))
    {
        PrintErrorMessage(pl_error_code(), "pl_get_param(PARAM_ROI_COUNT) error");
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }
    if (roiCount < cRoiCount)
    {
        printf("Camera %d does not support %u regions\n", ctx->hcam, cRoiCount);
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }

    // The acquisition with multiple regions stores additional metadata in user
    // image buffer, ensure it's enabled.
    if (!IsParamAvailable(ctx->hcam, PARAM_METADATA_ENABLED, "PARAM_METADATA_ENABLED"))
    {
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }
    rs_bool metaEnabled = TRUE;
    if (PV_OK != pl_set_param(ctx->hcam, PARAM_METADATA_ENABLED, (void*)&metaEnabled))
    {
        PrintErrorMessage(pl_error_code(),
                "pl_set_param(PARAM_METADATA_ENABLED) error");
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }

    printf("Camera %d supports up to %u regions with metadata\n\n",
            ctx->hcam, roiCount);

    // Set regions diagonally from top-left corner to bottom-right corner with
    // serial and parallel binning set to 1.
    // Regions cannot overlap!
    rgn_type regions[cRoiCount];
    for (uns16 n = 0; n < cRoiCount; n++)
    {
        regions[n].s1 = ctx->sensorResX / cRoiCount * n;
        regions[n].s2 = (ctx->sensorResX / cRoiCount * (n + 1)) - 1;
        regions[n].sbin = 1;
        regions[n].p1 = ctx->sensorResY / cRoiCount * n;
        regions[n].p2 = (ctx->sensorResY / cRoiCount * (n + 1)) - 1;
        regions[n].pbin = 1;
    }

    // Install generic ctr+c handler
    if (!InstallGenericCliTerminationHandler(contexts))
    {
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }

    // Register callback to receive a notification when EOF event arrives.
    // The handler function will be called by PVCAM, last parameter
    // might hold a pointer to user-specific content that will be passed with
    // the callback once it is invoked.
    if (PV_OK != pl_cam_register_callback_ex3(ctx->hcam, PL_CALLBACK_EOF,
                (void*)GenericEofHandler, ctx))
    {
        PrintErrorMessage(pl_error_code(), "pl_cam_register_callback() error");
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }
    printf("EOF callback handler registered on camera %d\n", ctx->hcam);

    uns32 exposureBytes;
    const uns32 exposureTime = 40; // milliseconds

    // Setup the acquisition
    // TIMED_MODE flag means acquisition will start with software trigger,
    // other flags such as STROBED_MODE can be used for hardware trigger.
    if (PV_OK != pl_exp_setup_seq(ctx->hcam, 1, cRoiCount, &regions[0],
                TIMED_MODE, exposureTime, &exposureBytes))
    {
        PrintErrorMessage(pl_error_code(), "pl_exp_setup_seq() error");
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }
    printf("Acquisition setup successful on camera %d\n", ctx->hcam);
    UpdateCtxImageFormat(ctx);

    // Allocate frame memory assuming this is a 16-bit camera (or more than 8 bit)
    // Since each pixel is 2 bytes allocate uns16 array, therefore divide the size
    // in bytes returned by pl_exp_setup_seq() by 2.
    uns8* frameInMemory = new (std::nothrow) uns8[exposureBytes];
    if (!frameInMemory)
    {
        printf("Unable to allocate acq. buffer for camera %d\n", ctx->hcam);
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }

    // In this example we will recompose all ROIs into full frame picture so we
    // allocate memory for full frame image.
    // ATM, the pl_md_frame_recompose function works with uns16 pixels only anyway.
    const uns32 fullFrameBytes = ctx->sensorResX * ctx->sensorResY * sizeof(uns16);
    uns8 *fullFrame = new (std::nothrow) uns8[fullFrameBytes];
    if (!fullFrame)
    {
        printf("Unable to allocate recompose buffer for camera %d\n", ctx->hcam);
        delete [] frameInMemory;
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }
    // Do not forget to fill whole frame with black pixels otherwise recomposed
    // frame will contain random values for pixels outside given regions.
    std::memset(fullFrame, 0, fullFrameBytes);

    // Allocate memory for metadata structures.
    // Do not access members before it's filled via pl_md_frame_decode function!
    md_frame* mdFrameInMemory;
    if (PV_OK != pl_md_create_frame_struct_cont(&mdFrameInMemory, cRoiCount))
    {
        PrintErrorMessage(pl_error_code(),
                "pl_md_create_frame_struct_cont(cRoiCount) error");
        delete [] fullFrame;
        delete [] frameInMemory;
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }
    printf("Structures for metadata created\n");

    bool errorOccurred = false;
    uns32 imageCounter = 0;
    while (imageCounter < 5)
    {
        // Start the acquisition - it is used as software trigger in TIMED
        // trigger mode. In hardware trigger mode (Strobe or Bulb) after this
        // call camera waits for external trigger signal.
        if (PV_OK != pl_exp_start_seq(ctx->hcam, frameInMemory))
        {
            PrintErrorMessage(pl_error_code(), "pl_exp_start_seq() error");
            errorOccurred = true;
            break;
        }
        printf("Acquisition started on camera %d\n", ctx->hcam);

        // Here we need to wait for a frame readout notification signaled by
        // event raised in the callback.
        printf("Waiting for EOF event to occur on camera %d\n", ctx->hcam);
        if (!WaitForEofEvent(ctx, 5000, errorOccurred))
        {
            if (!errorOccurred)
            {
                // This happens on ctrl+c, but in this sample we have to
                // handle the acquisition abort explicitly
                if (PV_OK != pl_exp_abort(ctx->hcam, CCS_NO_CHANGE))
                {
                    PrintErrorMessage(pl_error_code(), "pl_exp_abort() error");
                    errorOccurred = true;
                }
                printf("Processing aborted on camera %d\n", ctx->hcam);
            }
            break;
        }

        printf("Frame #%u has been delivered from camera %d\n",
                imageCounter + 1, ctx->hcam);

        // Decode the frame and extract metadata structures
        if (PV_OK != pl_md_frame_decode(mdFrameInMemory, frameInMemory,
                    exposureBytes))
        {
            PrintErrorMessage(pl_error_code(), "pl_md_frame_decode() error");
            errorOccurred = true;
            break;
        }
        if (cRoiCount != mdFrameInMemory->roiCount)
        {
            printf("Required (%u) and received (%u) ROI count do not match\n",
                    cRoiCount, mdFrameInMemory->roiCount);
            errorOccurred = true;
            break;
        }

        for (uns16 n = 0; n < cRoiCount; n++)
        {
            std::string title;
            title += "ROI " + std::to_string(n);
            ShowImage(ctx, mdFrameInMemory->roiArray[n].data,
                    mdFrameInMemory->roiArray[n].dataSize, title.c_str());
        }

        // Optionally, recompose all ROIs to full frame image.
        // The whole frame is already filled with black pixels, here we just
        // overwrite ROI pixels with new data because regions don't move.
        if (PV_OK != pl_md_frame_recompose(fullFrame,
                    mdFrameInMemory->impliedRoi.s1, mdFrameInMemory->impliedRoi.p1,
                    ctx->sensorResX, ctx->sensorResY, mdFrameInMemory))
        {
            PrintErrorMessage(pl_error_code(), "pl_md_frame_recompose() error");
            errorOccurred = true;
            break;
        }
        printf("Frame #%u has been recomposed to full frame\n", imageCounter + 1);
        ShowImage(ctx, fullFrame, fullFrameBytes, "recomposed");

        // When acquiring single frames with callback notifications call this
        // after each frame before new acquisition is started with pl_exp_start_seq()
        if (PV_OK != pl_exp_finish_seq(ctx->hcam, frameInMemory, 0))
        {
            PrintErrorMessage(pl_error_code(), "pl_exp_finish_seq() error");
        }
        else
        {
            printf("Acquisition finished on camera %d\n", ctx->hcam);
        }

        imageCounter++;
    }

    if (PV_OK != pl_md_release_frame_struct(mdFrameInMemory))
    {
        PrintErrorMessage(pl_error_code(), "pl_md_release_frame_struct() error");
    }

    delete [] fullFrame;
    delete [] frameInMemory;

    CloseAllCamerasAndUninit(contexts);

    if (errorOccurred)
        return APP_EXIT_ERROR;
    return 0;
}

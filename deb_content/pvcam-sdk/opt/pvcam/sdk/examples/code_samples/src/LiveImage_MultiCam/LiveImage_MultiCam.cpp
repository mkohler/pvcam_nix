/******************************************************************************/
/* Copyright (C) Teledyne Photometrics. All rights reserved.                  */
/******************************************************************************/

// Local
#include "Common.h"

// Mutex for synchronized printing to standard output
std::mutex g_printMutex;

void ThreadFunc(CameraContext* ctx)
{
    // Register callback to receive a notification when EOF event arrives.
    // The handler function will be called by PVCAM, last parameter
    // might hold a pointer to user-specific content that will be passed with
    // the callback once it is invoked.
    if (PV_OK != pl_cam_register_callback_ex3(ctx->hcam, PL_CALLBACK_EOF,
                (void*)GenericEofHandler, ctx))
    {
        PrintErrorMessage(pl_error_code(), "pl_cam_register_callback() error");
        return;
    }
    printf("EOF callback handler registered on camera %d\n", ctx->hcam);

    uns32 exposureBytes;
    const uns32 exposureTime = 5; // milliseconds

    const uns16 circBufferFrames = 6;
    const int16 bufferMode = CIRC_OVERWRITE;

    // Setup continuous acquisition with circular buffer mode. TIMED_MODE
    // indicates this is software trigger mode and each acquisition is after
    // initial pl_exp_start_cont() call started internally from the camera.
    // To run in hardware trigger mode use either EDGE_MODE/STROBE_MODE,
    // BULB_MODE or TRIGGER_FIRST_MODE.
    if (PV_OK != pl_exp_setup_cont(ctx->hcam, 1, &ctx->region, TIMED_MODE,
                exposureTime, &exposureBytes, bufferMode))
    {
        PrintErrorMessage(pl_error_code(), "pl_exp_setup_cont() error");
        return;
    }
    printf("Acquisition setup successful on camera %d\n", ctx->hcam);
    UpdateCtxImageFormat(ctx);

    const uns32 circBufferBytes = circBufferFrames * exposureBytes;

    // Allocate memory for circular buffer of frames, size of one frame is
    // returned in exposureBytes by pl_exp_setup_cont()
    uns8* circBufferInMemory = new (std::nothrow) uns8[circBufferBytes];
    if (!circBufferInMemory)
    {
        printf("Unable to allocate buffer for camera %d\n", ctx->hcam);
        return;
    }

    // Start the continuous acquisition, again tell this function size of buffer
    // it has for the frames. Camera starts the first acquisition based on this
    // host command and subsequent acquisitions are triggered internally by the
    // camera itself. In hardware trigger mode this sets the camera to waiting
    // state awaiting external trigger signals.
    if (PV_OK != pl_exp_start_cont(ctx->hcam, circBufferInMemory, circBufferBytes))
    {
        PrintErrorMessage(pl_error_code(), "pl_exp_start_seq() error");
        delete [] circBufferInMemory;
        return;
    }
    printf("Acquisition started on camera %d\n", ctx->hcam);

    uns32 framesAcquired = 0;
    while (framesAcquired < 20)
    {
        // Here we need to wait for a frame readout notification signaled by
        // event raised in the callback.
        bool errorOccurred;
        if (!WaitForEofEvent(ctx, 5000, errorOccurred))
            break;

        g_printMutex.lock();

        printf("Frame #%u has been delivered from camera %d\n",
                framesAcquired + 1, ctx->hcam);
        ShowImage(ctx, ctx->eofFrame, exposureBytes);

        g_printMutex.unlock();

        framesAcquired++;
    }

    // Once we have acquired the number of frames needed the acquisition can be
    // stopped, no other call is needed to stop the acquisition.
    if (PV_OK != pl_exp_abort(ctx->hcam, CCS_NO_CHANGE))
    {
        PrintErrorMessage(pl_error_code(), "pl_exp_abort() error");
    }
    else
    {
        printf("Acquisition stopped on camera %d\n", ctx->hcam);
    }

    delete [] circBufferInMemory;
}

// This sample application shows how to setup and run multiple cameras in
// continuous mode with circular buffer with callback frame readout notification,
// additionally extended callback is used that delivers extended frame
// information (e.g. frame number, time-stamp etc). This circular buffer mode is
// typically used to run the camera until user stops the acquisition, or until
// an external condition is met, or system gets in focus etc.
// This sample code collects 100 frames, but this condition can be replaced by
// a UI event such as user pressing a stop button etc.
int main(int argc, char* argv[])
{
    if (!ShowAppInfo(argc, argv))
        return APP_EXIT_ERROR;

    std::vector<CameraContext*> contexts;
    uns16 nrOfCameras = cMultiCamCount;
    if (!InitAndOpenMultipleCameras(contexts, nrOfCameras))
        return APP_EXIT_ERROR;

    // Install generic ctr+c handler
    if (!InstallGenericCliTerminationHandler(contexts))
    {
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }

    bool errorOccurred = false;

    // Start acquisition threads for all cameras
    for (int16 i = 0; i < nrOfCameras; i++)
    {
        CameraContext* ctx = contexts[i];

        ctx->threadAbortFlag = false;
        ctx->thread = new(std::nothrow) std::thread(ThreadFunc, ctx);
        if (!ctx->thread)
        {
            errorOccurred = true;

            g_printMutex.lock();
            printf("Failed to start acquisition thread for camera %d\n", ctx->hcam);
            g_printMutex.unlock();

            // Don't start remaining threads and abort those already running
            for (int16 j = 0; j < i; j++)
            {
                CameraContext* ctx2 = contexts[j];
                {
                    std::lock_guard<std::mutex> lock(ctx2->eofEvent.mutex);
                    // Set abort flag, eof flag doesn't matter
                    ctx2->threadAbortFlag = true;
                }
                ctx2->eofEvent.cond.notify_all();
            }
            break;
        }
    }

    // Wait for all acq. threads to finish
    for (int i = 0; i < nrOfCameras; i++)
    {
        CameraContext* ctx = contexts[i];

        if (ctx->thread && ctx->thread->joinable())
        {
            ctx->thread->join();
        }
        delete ctx->thread;
        ctx->thread = nullptr;
    }

    CloseAllCamerasAndUninit(contexts);

    if (errorOccurred)
        return APP_EXIT_ERROR;
    return 0;
}

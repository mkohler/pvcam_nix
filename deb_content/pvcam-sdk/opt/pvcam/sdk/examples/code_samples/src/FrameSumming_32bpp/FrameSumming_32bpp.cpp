/******************************************************************************/
/* Copyright (C) Teledyne Photometrics. All rights reserved.                  */
/******************************************************************************/

// System
#include <algorithm> // std::min
#include <cmath> // std::ceil, std::log2

// Local
#include "Common.h"

const char* ImageFormatToStr(int32 format)
{
    switch (format)
    {
    case PL_IMAGE_FORMAT_MONO8:     return "MONO8";
    case PL_IMAGE_FORMAT_BAYER8:    return "BAYER8";
    case PL_IMAGE_FORMAT_MONO16:    return "MONO16";
    case PL_IMAGE_FORMAT_BAYER16:   return "BAYER16";
    case PL_IMAGE_FORMAT_MONO32:    return "MONO32";
    case PL_IMAGE_FORMAT_BAYER32:   return "BAYER32";
    default:                        return "<unsupported>";
    }
}

// This sample application demonstrates acquisition of single frame with
// callback notification with user selectable settings of Frame Summing
// post processing feature.
// Enabling Frame Summing PP feature changes pixel size from two bytes per pixel
// used all the time, to 4 bytes per pixel, i.e. 32 bits per pixel.
// Please keep in mind that 2 or 4 bytes per pixel is just a size of the pixel
// "container", the actual bit depth and thus max. pixel value the "container"
// can hold is reported by PARAM_BIT_DEPTH as before. It means that e.g. camera
// can report 14 bits per pixel, but "container" size is still 2 bytes, two
// highest bits remain zeroed and unused. With Frame Summing feature turned on
// camera could report bit depth e.g. 20 bits per pixel while "container" size
// is still 4 bytes, i.e. highest 12 bits remain zeroed and unused.
int main(int argc, char* argv[])
{
    if (!ShowAppInfo(argc, argv))
        return APP_EXIT_ERROR;

    std::vector<CameraContext*> contexts;
    if (!InitAndOpenOneCamera(contexts, cSingleCamIndex))
        return APP_EXIT_ERROR;

    CameraContext* ctx = contexts[cSingleCamIndex];

    // Check and determine if there are any post processing features available
    if (!IsParamAvailable(ctx->hcam, PARAM_PP_INDEX, "PARAM_PP_INDEX"))
    {
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }

    // Install generic ctr+c handler
    if (!InstallGenericCliTerminationHandler(contexts))
    {
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }

    // Remember original bit depth for current port/speed/gain before we touch anything
    int16 origBitDepth;
    if (PV_OK != pl_get_param(ctx->hcam, PARAM_BIT_DEPTH, ATTR_CURRENT, (void*)&origBitDepth))
    {
        PrintErrorMessage(pl_error_code(), "pl_get_param(PARAM_BIT_DEPTH) error");
        return APP_EXIT_ERROR;
    }
    const uns16 origMaxAdu = ((uns16)1 << origBitDepth) - 1;

    // Register callback to receive a notification when EOF event arrives.
    // The handler function will be called by PVCAM, last parameter
    // might hold a pointer to user-specific content that will be passed with
    // the callback once it is invoked.
    if (PV_OK != pl_cam_register_callback_ex3(ctx->hcam, PL_CALLBACK_EOF,
                (void*)GenericEofHandler, ctx))
    {
        PrintErrorMessage(pl_error_code(), "pl_cam_register_callback() error");
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }
    printf("EOF callback handler registered on camera %d\n\n", ctx->hcam);

    // Build the post processing table first
    const std::vector<PvcamPpFeature> ppFeatures = DiscoverCameraPostProcessing(ctx);
    if (ppFeatures.empty())
    {
        // Some error must have occurred in the above function
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }

    // Print the table to console, just for information
    printf("Camera Post Processing table:\n");
    for (const PvcamPpFeature& ppFeature : ppFeatures)
    {
        // Print IDs as signed to display invalid value as -1 instead of huge number.
        // Valid ID is usually small number based on current pattern in pvcam.h.
        printf("- FEATURE at index %d: id=%d, name='%s'\n",
                ppFeature.index, (int32)ppFeature.id, ppFeature.name.c_str());
        for (const PvcamPpParameter& ppParam : ppFeature.parameterList)
        {
            printf("  - PARAMETER at index %d: id=%d, name='%s'\n",
                    ppParam.index, (int32)ppParam.id, ppParam.name.c_str());
        }
    }
    printf("\n");

    // Find Frame Summing PP Feature
    int16 sumFeatureIdx;
    const bool sumFound = FindPpFeatureIndex(ppFeatures,
            PP_FEATURE_FRAME_SUMMING, sumFeatureIdx);
    if (!sumFound)
    {
        printf("Camera %d doesn't support Frame Summing post-processing feature\n",
                ctx->hcam);
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }
    const PvcamPpFeature& ppFeature = ppFeatures[sumFeatureIdx];

    // Show currently selected Post Processing Feature
    printf("Selected Post Processing Feature: index=%d, name='%s'\n",
            ppFeature.index, ppFeature.name.c_str());

    // We have to find indexes of related feature and parameters by unique IDs
    int16 sumParamEnIdx = -1; // Invalid index
    int16 sumParamCountIdx = -1; // Invalid index
    uns32 sumParamEnValue = 0; // Whole feature disabled
    uns32 sumParamCountValue = 1; // Default count, like no summing

    if (!FindPpParamIndex(ppFeature.parameterList,
                PP_FEATURE_FRAME_SUMMING_ENABLED, sumParamEnIdx))
    {
        printf("Camera %d doesn't support Frame Summing Enable PP Parameter\n",
                ctx->hcam);
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }
    if (!FindPpParamIndex(ppFeature.parameterList,
                PP_FEATURE_FRAME_SUMMING_COUNT, sumParamCountIdx))
    {
        printf("Camera %d doesn't support Frame Summing Count PP Parameter\n",
                ctx->hcam);
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }

    if (!GetPpParamValue(ctx, sumFeatureIdx, sumParamEnIdx, sumParamEnValue))
    {
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }
    if (!GetPpParamValue(ctx, sumFeatureIdx, sumParamCountIdx, sumParamCountValue))
    {
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }

    uns32 exposureBytes;
    const uns32 exposureTime = 10; // milliseconds
    // For simplicity used hard-coded readout time for 10 FPS
    const uns32 readoutTime = 100; // milliseconds

    // Repeat until no action is selected
    bool errorOccurred = false;
    for (;;)
    {
        uns32 curValue;

        NVPC menuPpParams;
        // Add one extra item to the list, will be shown in action selection menu
        {
            NVP nvpParam;
            nvpParam.value = -1; // Custom value, negative to not collide with PP indexes
            nvpParam.name = "Capture one frame with current settings";
            menuPpParams.push_back(nvpParam);
        }
        // Get list of available Parameters of selected Post Processing Feature
        for (const PvcamPpParameter& ppParam : ppFeature.parameterList)
        {
            if (!GetPpParamValue(ctx, ppFeature.index, ppParam.index, curValue))
            {
                errorOccurred = true;
                break;
            }
            NVP nvpParam;
            nvpParam.value = ppParam.index; // Post Processing Parameter selection
            nvpParam.name = ppParam.name + ", current value " + std::to_string(curValue);
            menuPpParams.push_back(nvpParam);
        }

        // Get required Parameter selection from the interactive menu
        int32 menuSelection;
        if (!GetMenuSelection("Choose action or Post Processing Parameter to change",
                    menuPpParams, menuSelection))
        {
            printf("Not selected valid action or parameter, exiting...\n");
            break; // Not an error, normal way how to exit the app
        }

        if (menuSelection >= 0)
        {
            // Get selected Post Processing Parameter
            const PvcamPpParameter& ppParam = ppFeature.parameterList[menuSelection];
            // Show currently selected Post Processing Parameter
            printf("Selected Post Processing Parameter: index=%d, name='%s'\n",
                    ppParam.index, ppParam.name.c_str());

            // Get new value for set up from the console
            const int number = ConsoleReadNumber(
                    (int)ppParam.minValue, (int)ppParam.maxValue, (int)ppParam.defValue);

            // Set new value of selected Post Processing Parameter
            uns32 newValue = (uns32)number;
            if (!SetPpParamValue(ctx, ppFeature.index, ppParam.index, newValue))
            {
                errorOccurred = true;
                break;
            }
            printf("Change successful!\n");
            if (!GetPpParamValue(ctx, ppFeature.index, ppParam.index, curValue))
            {
                errorOccurred = true;
                break;
            }
            printf("The value reported by camera is now: %u\n", curValue);

            // Update cached value
            if (menuSelection == sumParamEnIdx)
            {
                sumParamEnValue = curValue;
            }
            else if (menuSelection == sumParamCountIdx)
            {
                sumParamCountValue = curValue;
            }

            continue;
        }
        else
        {
            printf("Capturing one frame with current settings...\n");

            // Setup a sequence of 1 frame
            if (PV_OK != pl_exp_setup_seq(ctx->hcam, 1, 1, &ctx->region, TIMED_MODE,
                        exposureTime, &exposureBytes))
            {
                PrintErrorMessage(pl_error_code(), "pl_exp_setup_seq() error");
                errorOccurred = true;
                break;
            }
            printf("Acquisition setup successful on camera %d\n", ctx->hcam);
            // Call to UpdateCtxImageFormat is really important here as turning
            // on and off frame summing feature changes the size of each pixel
            // in returned frame.
            UpdateCtxImageFormat(ctx);

            int16 curBitDepth;
            if (PV_OK != pl_get_param(ctx->hcam, PARAM_BIT_DEPTH, ATTR_CURRENT,
                        (void*)&curBitDepth))
            {
                PrintErrorMessage(pl_error_code(),
                        "pl_get_param(PARAM_BIT_DEPTH) error");
                errorOccurred = true;
                break;
            }

            const int16 calcBitDepth =
                origBitDepth + (int16)std::ceil(std::log2(sumParamCountValue));
            const uns32 calcMaxAdu = origMaxAdu * sumParamCountValue;

            printf("Current settings:\n"
                    "  Frame Summing feature %s\n",
                    (sumParamEnValue == 0) ? "OFF" : "ON");
            if (ctx->imageFormat == PL_IMAGE_FORMAT_MONO32
                    || ctx->imageFormat == PL_IMAGE_FORMAT_BAYER32)
            {
                printf("  summing frames: %u\n"
                        "  image format: %s\n"
                        "  bit depth reported: %d bpp\n"
                        "  max. pixel value for 1 frame: %u (i.e. %u bpp)\n"
                        "  max. pixel value for %u frames: %u (i.e. %u bpp)\n",
                        sumParamCountValue,
                        ImageFormatToStr(ctx->imageFormat),
                        curBitDepth,
                        origMaxAdu, origBitDepth,
                        sumParamCountValue, calcMaxAdu, calcBitDepth);
            }
            else
            {
                printf("  image format: %s\n"
                        "  bit depth reported: %d bpp\n"
                        "  max. pixel value for 1 frame: %u (i.e. %u bpp)\n",
                        ImageFormatToStr(ctx->imageFormat),
                        curBitDepth,
                        (1u << origBitDepth) - 1, origBitDepth);
            }

            // Allocate enough memory to capture all the frames.
            // The frame size is the same for all exposure modes, so we don't
            // need to re-allocate the frame memory again and again. But because
            // we get the size after first call to pl_exp_setup_seq we do it
            // this way to keep example simple.
            uns8* frameInMemory = new (std::nothrow) uns8[exposureBytes];
            if (!frameInMemory)
            {
                printf("Unable to allocate buffer for camera %d\n", ctx->hcam);
                errorOccurred = true;
                break;
            }

            // Send the go signal
            if (PV_OK != pl_exp_start_seq(ctx->hcam, frameInMemory))
            {
                PrintErrorMessage(pl_error_code(), "pl_exp_start_seq() error");
                delete [] frameInMemory;
                errorOccurred = true;
                break;
            }
            printf("Acquisition started on camera %d\n", ctx->hcam);

            // Timeout based on new frame count
            const uns32 frameCount = (sumParamEnValue == 0) ? 1 : sumParamCountValue;
            const uns32 timeoutMs = 5000 + (exposureTime + readoutTime) * frameCount;

            // Here we need to wait for a frame readout notification signaled by
            // event raised in the callback.
            printf("Waiting up to %ums for EOF event to occur on camera %d\n",
                    timeoutMs, ctx->hcam);
            if (!WaitForEofEvent(ctx, timeoutMs, errorOccurred))
            {
                delete [] frameInMemory;
                if (errorOccurred)
                {
                    // Let's not error out on timeout in this code sample
                    errorOccurred = false;
                    continue;
                }
                break;
            }

            printf("Frame has been delivered from camera %d\n", ctx->hcam);
            ShowImage(ctx, frameInMemory, exposureBytes);

            if (PV_OK != pl_exp_finish_seq(ctx->hcam, frameInMemory, 0))
            {
                PrintErrorMessage(pl_error_code(), "pl_exp_finish_seq() error");
            }
            else
            {
                printf("Acquisition finished on camera %d\n", ctx->hcam);
            }

            delete [] frameInMemory;
        }
    }

    // Just in case of any error, ensure the acquisition is idle before exit
    if (PV_OK != pl_exp_abort(ctx->hcam, CCS_NO_CHANGE))
    {
        PrintErrorMessage(pl_error_code(), "pl_exp_abort() error");
    }

    CloseAllCamerasAndUninit(contexts);

    if (errorOccurred)
        return APP_EXIT_ERROR;
    return 0;
}

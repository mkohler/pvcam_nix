/******************************************************************************/
/* Copyright (C) Teledyne Photometrics. All rights reserved.                  */
/******************************************************************************/

// Local
#include "Common.h"

bool adjustmentNeeded = false;
uns32 exposureTime = 5; // milliseconds

void ThreadFunc(CameraContext* ctx)
{
    // Register callback to receive a notification when EOF event arrives.
    // The handler function will be called by PVCAM, last parameter
    // might hold a pointer to user-specific content that will be passed with
    // the callback once it is invoked.
    if (PV_OK != pl_cam_register_callback_ex3(ctx->hcam, PL_CALLBACK_EOF,
                (void*)GenericEofHandler, ctx))
    {
        PrintErrorMessage(pl_error_code(), "pl_cam_register_callback() error");
        return;
    }
    printf("EOF callback handler registered on camera %d\n", ctx->hcam);

    uns32 exposureBytes;
    const uns16 circBufferFrames = 20;
    const int16 bufferMode = CIRC_OVERWRITE;

    // Setup continuous acquisition with circular buffer mode. TIMED_MODE
    // indicates this is software trigger mode and each acquisition is after
    // initial pl_exp_start_cont() call started internally from the camera.
    // To run in hardware trigger mode use either EDGE/STROBED_MODE, BULB_MODE
    // or TRIGGER_FIRST_MODE.
    if (PV_OK != pl_exp_setup_cont(ctx->hcam, 1, &ctx->region, TIMED_MODE,
                exposureTime, &exposureBytes, bufferMode))
    {
        PrintErrorMessage(pl_error_code(), "pl_exp_setup_cont() error");
        return;
    }
    printf("Acquisition setup successful on camera %d\n", ctx->hcam);
    UpdateCtxImageFormat(ctx);

    const uns32 circBufferBytes = circBufferFrames * exposureBytes;

    // Allocate memory for circular buffer of frames, size of one frame is
    // returned in exposureBytes by pl_exp_setup_cont()
    uns8* circBufferInMemory = new (std::nothrow) uns8[circBufferBytes];
    if (!circBufferInMemory)
    {
        printf("Unable to allocate buffer for camera %d\n", ctx->hcam);
        return;
    }

    // Start the continuous acquisition, again tell this function size of buffer
    // it has for the frames. Camera starts the first acquisition based on this
    // host command and subsequent acquisitions are triggered internally by the
    // camera itself. In hardware trigger mode this sets the camera to waiting
    // state awaiting external trigger signals.
    if (PV_OK != pl_exp_start_cont(ctx->hcam, circBufferInMemory, circBufferBytes))
    {
        PrintErrorMessage(pl_error_code(), "pl_exp_start_seq() error");
        delete [] circBufferInMemory;
        return;
    }
    printf("Acquisition started on camera %d\n", ctx->hcam);

    uns32 framesAcquired = 0;
    auto timeLastUpdate =
        std::chrono::high_resolution_clock::now() - std::chrono::seconds(1);
    for (;;)
    {
        if (adjustmentNeeded)
        {
            adjustmentNeeded = false;

            framesAcquired = 0;

            // To change exposure time current acquisition has to be stopped first
            if (PV_OK != pl_exp_abort(ctx->hcam, CCS_NO_CHANGE))
            {
                PrintErrorMessage(pl_error_code(), "pl_exp_abort() error");
                break;
            }
            printf("Acquisition stopped on camera %d\n", ctx->hcam);
            std::this_thread::sleep_for(std::chrono::milliseconds(100));

            // Next, setup the camera in circular buffer mode with new exposure time
            if (PV_OK != pl_exp_setup_cont(ctx->hcam, 1, &ctx->region, TIMED_MODE,
                        exposureTime, &exposureBytes, bufferMode))
            {
                PrintErrorMessage(pl_error_code(), "pl_exp_setup_cont() error");
                break;
            }
            printf("Acquisition setup successful on camera %d, "
                    "new exposure time = %ums\n", exposureTime, ctx->hcam);
            UpdateCtxImageFormat(ctx);

            // After the camera has been set up with the new exposure time,
            // start the acquisition again
            if (PV_OK != pl_exp_start_cont(ctx->hcam, circBufferInMemory,
                        circBufferFrames * exposureBytes))
            {
                PrintErrorMessage(pl_error_code(), "pl_exp_start_seq() error");
                break;
            }
            printf("Acquisition started on camera %d\n", ctx->hcam);
        }

        const auto timeStart = std::chrono::high_resolution_clock::now();

        // Here we need to wait for a frame readout notification signaled by
        // event raised in the callback.
        //printf("Waiting for EOF event to occur on camera %d\n", ctx->hcam);
        bool errorOccurred;
        if (!WaitForEofEvent(ctx, 5000, errorOccurred))
            break;

        const auto timeNow = std::chrono::high_resolution_clock::now();
        if (timeNow - timeLastUpdate > std::chrono::seconds(1))
        {
            timeLastUpdate = timeNow;
            const auto timeTotalMs =
                std::chrono::duration<double, std::milli>(timeNow - timeStart);
            printf("Frame #%d acquired on camera %d, last frame readout time: %1.1fms\n",
                    ctx->eofFrameInfo.FrameNr, ctx->hcam, timeTotalMs.count());
            ShowImage(ctx, ctx->eofFrame, exposureBytes);
        }

        framesAcquired++;
    }

    // Once we have acquired the number of frames needed the acquisition can be
    // stopped, no other call is needed to stop the acquisition.
    if (PV_OK != pl_exp_abort(ctx->hcam, CCS_NO_CHANGE))
    {
        PrintErrorMessage(pl_error_code(), "pl_exp_abort() error");
    }
    else
    {
        printf("Acquisition stopped on camera %d\n", ctx->hcam);
    }

    delete [] circBufferInMemory;
}

// This sample application shows how to setup and run camera in continuous mode
// with circular buffer and how to change the exposure time in this mode.
// In order to change the exposure time, the currently running acquisition must
// stopped, new exposure time has to be applied via pl_exp_setup_cont() function
// and the acquisition needs to be started again.
int main(int argc, char* argv[])
{
    if (!ShowAppInfo(argc, argv))
        return APP_EXIT_ERROR;

    printf("Instructions:\n");
    printf("  Press '+' to increase exposure time by 5ms\n");
    printf("  Press '-' to decrease exposure time by 5ms\n");
    printf("  Press any other key to stop acquisition and exit\n");
    printf("\nPress <Enter> to start...\n");
    WaitForInput();

    std::vector<CameraContext*> contexts;
    if (!InitAndOpenOneCamera(contexts, cSingleCamIndex))
        return APP_EXIT_ERROR;

    CameraContext* ctx = contexts[cSingleCamIndex];

    ctx->threadAbortFlag = false;
    ctx->thread = new(std::nothrow) std::thread(ThreadFunc, ctx);
    if (!ctx->thread)
    {
        printf("Failed to start acquisition thread for camera %d\n", ctx->hcam);
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }

    while (!ctx->threadAbortFlag)
    {
        // Give PVCAM some time to reconfigure before another change
        std::this_thread::sleep_for(std::chrono::milliseconds(100));

        const int key = GetCh();
        switch (key)
        {
        case '+':
            exposureTime += 5;
            adjustmentNeeded = true;
            printf("Exposure time increased to %ums\n", exposureTime);
            break;
        case '-':
            if (exposureTime >= 5)
            {
                exposureTime -= 5;
                adjustmentNeeded = true;
                printf("Exposure time decreased to %ums\n", exposureTime);
            }
            break;
        default:
            printf("Exiting application...\n");
            // Unblock acquisition thread
            {
                std::lock_guard<std::mutex> lock(ctx->eofEvent.mutex);
                ctx->threadAbortFlag = true;
            }
            ctx->eofEvent.cond.notify_all();
            // Wait for acq. thread to finish
            if (ctx->thread->joinable())
            {
                ctx->thread->join();
            }
            delete ctx->thread;
            ctx->thread = nullptr;
            break;
        }
    }

    CloseAllCamerasAndUninit(contexts);

    return 0;
}

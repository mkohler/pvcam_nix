/******************************************************************************/
/* Copyright (C) Teledyne Photometrics. All rights reserved.                  */
/******************************************************************************/

// Local
#include "Common.h"

// This sample application shows how to setup and run camera in continuous mode
// with circular buffer and polling for frame readout notification, this is
// typically used to run the camera until user stops the acquisition, or until
// an external condition is met, or system gets in focus etc.
// This sample code collects 50 frames, so this condition can be replaced by
// a UI event such as user pressing a stop button etc.
int main(int argc, char* argv[])
{
    if (!ShowAppInfo(argc, argv))
        return APP_EXIT_ERROR;

    std::vector<CameraContext*> contexts;
    if (!InitAndOpenOneCamera(contexts, cSingleCamIndex))
        return APP_EXIT_ERROR;

    CameraContext* ctx = contexts[cSingleCamIndex];

    // Install generic ctr+c handler
    if (!InstallGenericCliTerminationHandler(contexts))
    {
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }

    uns32 exposureBytes;
    const uns32 exposureTime = 40; // milliseconds

    const uns16 circBufferFrames = 20;
    int16 bufferMode = CIRC_OVERWRITE;

    // Setup the acquisition
    if (PV_OK != pl_exp_setup_cont(ctx->hcam, 1, &ctx->region, TIMED_MODE,
                exposureTime, &exposureBytes, bufferMode))
    {
        PrintErrorMessage(pl_error_code(), "pl_exp_setup_cont() error");
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }
    printf("Acquisition setup successful on camera %d\n", ctx->hcam);
    UpdateCtxImageFormat(ctx);

    const uns32 circBufferBytes = circBufferFrames * exposureBytes;

    // Allocate memory for circular buffer of frames, size of one frame is
    // returned in exposureBytes by pl_exp_setup_cont()
    uns8* circBufferInMemory = new (std::nothrow) uns8[circBufferBytes];
    if (!circBufferInMemory)
    {
        printf("Unable to allocate buffer for camera %d\n", ctx->hcam);
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }

    // Start the continuous acquisition, again tell this function size of buffer
    // it has for the frames this is called only once after camera setup with
    // pl_exp_setup_cont().
    // This is a software trigger call in TIMED trigger mode. In hardware
    // trigger modes (Strobe, Bulb etc) after this call camera waits for
    // external trigger signals to start every exposures.
    if (PV_OK != pl_exp_start_cont(ctx->hcam, circBufferInMemory, circBufferBytes))
    {
        PrintErrorMessage(pl_error_code(), "pl_exp_start_cont() error");
        CloseAllCamerasAndUninit(contexts);
        delete [] circBufferInMemory;
        return APP_EXIT_ERROR;
    }
    printf("Acquisition started on camera %d\n", ctx->hcam);

    bool errorOccurred = false;
    uns32 framesAcquired = 0;
    while (framesAcquired < 50)
    {
        int16 status;
        uns32 byte_cnt;
        uns32 buffer_cnt;

        // Keep checking readout status
        // Function returns FALSE if status is READOUT_FAILED
        //
        // WARNING:
        // When the acquisition is already running the status changes in
        // following sequence: EXPOSURE_IN_PROGRESS -> READOUT_IN_PROGRESS ->
        // FRAME_AVAILABLE and so on. It means the status FRAME_AVAILABLE can be
        // obtained only between the frames (between last EOF and next BOF).
        // When you use polling instead of callbacks during live mode you have
        // to take into account that some frames may be lost!
        while (PV_OK == pl_exp_check_cont_status(ctx->hcam, &status, &byte_cnt, &buffer_cnt)
                && status != FRAME_AVAILABLE && status != READOUT_NOT_ACTIVE
                && !ctx->threadAbortFlag)
        {
            // Waiting for frame exposure and readout
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
        }
        if (ctx->threadAbortFlag)
        {
            // This flag is set on ctrl+c, just break the loop, the acquisition
            // is aborted after that
            printf("Processing aborted on camera %d\n", ctx->hcam);
            break;
        }
        if (status == READOUT_FAILED)
        {
            printf("Frame #%u readout failed on camera %d\n",
                    framesAcquired + 1, ctx->hcam);
            errorOccurred = true;
            break;
        }

        // Get address of the latest frame in the buffer
        void* frameAddress;
        if (PV_OK != pl_exp_get_latest_frame(ctx->hcam, &frameAddress))
        {
            PrintErrorMessage(pl_error_code(), "pl_exp_get_latest_frame() error");
            errorOccurred = true;
            break;
        }

        printf("Frame #%u readout successfully completed on camera %d\n",
                framesAcquired + 1, ctx->hcam);
        ShowImage(ctx, frameAddress, exposureBytes);

        framesAcquired++;
    }

    // Once we have acquired the number of frames needed the acquisition can be
    // stopped, no other call is needed to stop the acquisition.
    if (PV_OK != pl_exp_abort(ctx->hcam, CCS_NO_CHANGE))
    {
        PrintErrorMessage(pl_error_code(), "pl_exp_abort() error");
    }
    else
    {
        printf("Acquisition stopped on camera %d\n", ctx->hcam);
    }

    delete [] circBufferInMemory;

    CloseAllCamerasAndUninit(contexts);

    if (errorOccurred)
        return APP_EXIT_ERROR;
    return 0;
}

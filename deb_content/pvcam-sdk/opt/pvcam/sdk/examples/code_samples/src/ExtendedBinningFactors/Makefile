PLATFORM_ARCH := $(shell uname -m)
ifndef ARCH
  MAKEFILE := $(abspath $(lastword $(MAKEFILE_LIST)))
  MAKEFILE_DIR := $(abspath $(dir $(MAKEFILE)))
  # Makefile is currently stored in <root>/src/<app>
  PRJ_ROOT := $(abspath $(MAKEFILE_DIR)/../..)

  ARCH := $(PLATFORM_ARCH)

  CXX_$(ARCH) := g++
  CXXFLAGS_$(ARCH) :=
  LDFLAGS_$(ARCH) :=
else
  ifneq ($(filter undefined,$(foreach tool,CXX,$(origin $(tool)_$(ARCH)))),)
    $(error Cannot cross-compile for target platform $(ARCH) on this $(PLATFORM_ARCH) platform)
  endif
endif

#####

#MAKEFILE_VERSION := $(MAKEFILE_DIR)/Makefile.version
#include $(MAKEFILE_VERSION)

################################################################################

BIN_NAME := ExtendedBinningFactors
#FULL_VERSION := $(VERSION_MAJOR).$(VERSION_MINOR).$(VERSION_PATCH).$(VERSION_BUILD)

BUILD_ARCH := linux-$(ARCH)

SRC := src
BIN := bin
BUILD := .build

SRC_DIRS := CommonFiles $(BIN_NAME)

BIN_DIR := $(BIN)/$(BUILD_ARCH)/$(BUILD_TYPE)
OBJ_DIR := $(BUILD)/$(BUILD_ARCH)/$(BUILD_TYPE)/$(BIN_NAME)

OBJ_SUBDIRS := $(SRC_DIRS)

################################################################################

DEPS := $(sort $(foreach src_dir,$(SRC_DIRS),$(wildcard $(SRC)/$(src_dir)/*.h)))
SRCS := $(sort $(foreach src_dir,$(SRC_DIRS),$(wildcard $(SRC)/$(src_dir)/*.cpp)))
OBJS := $(sort $(patsubst $(SRC)/%.cpp,$(OBJ_DIR)/%.o,$(SRCS)))

################################################################################

# Uncomment to use pkg-config tool
#USE_PKG_CONFIG := 1

CXX := $(CXX_$(ARCH))

WARNS := -Wall
WARNS += -Wextra
#WARNS += -Wconversion
WARNS += -Wno-unused-variable -Wno-unused-parameter \
         -Wno-unused-but-set-variable -Wno-unknown-pragmas
WARNS += -Werror=implicit-function-declaration

ifdef USE_PKG_CONFIG
  PKGCFG_$(ARCH) := $(PVCAM_SDK_PATH)/pkgconfig/$(ARCH)
else
  LIBDIR_$(ARCH) := $(PVCAM_SDK_PATH)/library/$(ARCH)
endif

CXXFLAGS = $(WARNS)
CXXFLAGS += $(CXXFLAGS_EXTRA)
CXXFLAGS += $(CXXFLAGS_$(ARCH))
CXXFLAGS += -std=c++11
CXXFLAGS += -I$(SRC)
CXXFLAGS += $(foreach src_dir,$(SRC_DIRS),-I$(SRC)/$(src_dir))
ifdef USE_PKG_CONFIG
  CXXFLAGS += $(shell PKG_CONFIG_PATH=$(PKGCFG_$(ARCH)) pkg-config --cflags pvcam)
else
  CXXFLAGS += -I$(PVCAM_SDK_PATH)/include
endif
#CXXFLAGS += -DMK_VERSION_MAJOR=$(VERSION_MAJOR)
#CXXFLAGS += -DMK_VERSION_MINOR=$(VERSION_MINOR)
#CXXFLAGS += -DMK_VERSION_PATCH=$(VERSION_PATCH)
#CXXFLAGS += -DMK_VERSION_BUILD=$(VERSION_BUILD)

LDFLAGS := -ldl -lpthread
# Enable compilation on older systems
LDFLAGS += -lrt
LDFLAGS += $(LDFLAGS_$(ARCH))
ifdef USE_PKG_CONFIG
  LDFLAGS += $(shell PKG_CONFIG_PATH=$(PKGCFG_$(ARCH)) pkg-config --libs pvcam)
else
  LDFLAGS += -L$(LIBDIR_$(ARCH)) -lpvcam
endif

CXXFLAGS_DEBUG := -g -O0

CXXFLAGS_RELEASE := -O3

################################################################################
# THOSE ARE THE TARGETS USABLE FROM COMMAND LINE

.PHONY: default
default: | all

.PHONY: all
all: | debug release

.PHONY: clean
clean: | clean_debug clean_release

.PHONY: distbuild
distbuild: | all

.PHONY: distclean
distclean: | distclean_debug distclean_release

#####

.PHONY: debug clean_debug distclean_debug
debug clean_debug distclean_debug: export BUILD_TYPE := debug

.PHONY: release clean_release distclean_release
release clean_release distclean_release: export BUILD_TYPE := release

debug: export CXXFLAGS_EXTRA := $(CXXFLAGS_DEBUG)

release: export CXXFLAGS_EXTRA := $(CXXFLAGS_RELEASE)

debug release:
	$(MAKE) -C $(PRJ_ROOT) --no-print-directory -f $(MAKEFILE) _app

clean_debug clean_release:
	$(MAKE) -C $(PRJ_ROOT) --no-print-directory -f $(MAKEFILE) _clean

distclean_debug distclean_release:
	$(MAKE) -C $(PRJ_ROOT) --no-print-directory -f $(MAKEFILE) _distclean

################################################################################

_app: $(OBJS) | $(BIN_DIR)
	$(CXX) -o $(BIN_DIR)/$(BIN_NAME) $^ $(LDFLAGS)

_distclean: | _clean
	-rm -f $(BIN_DIR)/$(BIN_NAME)

_clean:
	-rm -rf $(OBJ_DIR)

$(OBJ_DIR)/%.o : $(SRC)/%.cpp $(DEPS)
	$(CXX) -c -o $@ $< $(CXXFLAGS)

$(OBJS): | $(OBJ_SUBDIRS)

$(OBJ_SUBDIRS):
	mkdir -p $(OBJ_DIR)/$@

$(BIN_DIR):
	mkdir -p $(BIN_DIR)

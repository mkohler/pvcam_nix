/******************************************************************************/
/* Copyright (C) Teledyne Photometrics. All rights reserved.                  */
/******************************************************************************/

// Local
#include "Common.h"

// System
#include <iostream> // std::cin

// This sample application loops through all the available modes sending the
// exposure mode to the camera and capturing a frame. The user is expected to
// press an enter key between captures to move on to the next frame.
//
// In the past, enum values & descriptions were hard-coded in PVCAM to supply
// slightly more information to the developer about enums that were present in
// pvcam.h. However, now it has been extended to not only describe those values,
// but ALSO describe settings that only the camera firmware knows about, and are
// not present in header files. This example is written to introduce the
// extensions of pl_get_enum_param and pl_enum_str_length, that will be expanded
// as more cameras come out.
//
// Assumptions:
// The program assumes that a trigger source is attached to the trigger input of
// the camera.
int main(int argc, char* argv[])
{
    if (!ShowAppInfo(argc, argv))
        return APP_EXIT_ERROR;

    std::vector<CameraContext*> contexts;
    if (!InitAndOpenOneCamera(contexts, cSingleCamIndex))
        return APP_EXIT_ERROR;

    CameraContext* ctx = contexts[cSingleCamIndex];

    // Get the triggering/exposure names.
    // "Expose modes" are synonymous with "trigger modes" and "timing modes".
    // In this example we are combining what we are now calling "trigger mode"
    // with an "expose out" mode, to create an input for the "exposure mode".
    // A valid selection of "expose out" mode, is also none of the camera-
    // specified values. This allows for backwards compatibility with cameras
    // that don't support the dynamic portion of this feature or cameras that
    // don't support enumeration of "expose out" modes at all.
    NVPC triggerModes;
    if (!ReadEnumeration(ctx->hcam, &triggerModes, PARAM_EXPOSURE_MODE,
                "PARAM_EXPOSURE_MODE"))
    {
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }

    // Print the values and names returned from PVCAM
    printf("\nExposure Mode NVPs:\n");
    printf("==============================================\n");
    for (size_t i = 0; i < triggerModes.size(); i++)
    {
        printf("    Name Value Pair %zu (%d - %s)\n",
                i, triggerModes[i].value, triggerModes[i].name.c_str());
    }

    // Next, let's get the expose-out modes we have
    NVPC exposeOutModes;
    if (!ReadEnumeration(ctx->hcam, &exposeOutModes, PARAM_EXPOSE_OUT_MODE,
                "PARAM_EXPOSE_OUT_MODE"))
    {
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }

    // Print the values and names returned from PVCAM
    printf("\nExpose Out NVPs:\n");
    printf("==============================================\n");
    for (size_t i = 0; i < exposeOutModes.size(); i++)
    {
        printf("    Name Value Pair %zu (%d - %s)\n",
                i, exposeOutModes[i].value, exposeOutModes[i].name.c_str());
    }

    printf("\n");

    // Install generic ctr+c handler
    if (!InstallGenericCliTerminationHandler(contexts))
    {
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }

    // Register callback to receive a notification when EOF event arrives.
    // The handler function will be called by PVCAM, last parameter
    // might hold a pointer to user-specific content that will be passed with
    // the callback once it is invoked.
    if (PV_OK != pl_cam_register_callback_ex3(ctx->hcam, PL_CALLBACK_EOF,
                (void*)GenericEofHandler, ctx))
    {
        PrintErrorMessage(pl_error_code(), "pl_cam_register_callback() error");
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }
    printf("EOF callback handler registered on camera %d\n", ctx->hcam);

    uns32 exposureBytes;
    const uns32 exposureTime = 10; // milliseconds

    // The below portion of the code is specific to trig & expose-out modes
    // concept. This is now specific to that particular feature instead of what
    // the rest of the code example is for, which is understanding the new
    // extensions to pl_enum_str_length and pl_get_enum_param that allow for
    // retrieval of strings directly from the camera.

    // Sweep through the trigger and expose out modes and get a frame.

    bool errorOccurred = false;
    uns8* frameInMemory = nullptr;
    // Step through all the expose-out modes
    for (size_t expIDX = 0; expIDX < exposeOutModes.size(); expIDX++)
    {
        // Set the trigger mode for each of the modes
        for (size_t trigIDX = 0; trigIDX < triggerModes.size(); trigIDX++)
        {
            // Set the PVCAM exposure mode
            const uns16 expoMode = (uns16)triggerModes[trigIDX].value
                | (uns16)exposeOutModes[expIDX].value;

            printf("\nGoing to capture data with %s, %s <--> (%d, %d): expoMode (0x%04X)\n",
                    triggerModes[trigIDX].name.c_str(),
                    exposeOutModes[expIDX].name.c_str(),
                    triggerModes[trigIDX].value,
                    exposeOutModes[expIDX].value,
                    expoMode);

            printf("Press <Enter> to proceed...");
            WaitForInput();
            // If pressed ctrl+c while waiting to user input, stream sets a fail flag
            if (std::cin.fail())
            {
                errorOccurred = true;
                break;
            }

            // Setup a sequence of 1 frames with a 10ms exposure
            if (PV_OK != pl_exp_setup_seq(ctx->hcam, 1, 1, &ctx->region, expoMode,
                        exposureTime, &exposureBytes))
            {
                PrintErrorMessage(pl_error_code(), "pl_exp_setup_seq() error");
                errorOccurred = true;
                break;
            }
            printf("Acquisition setup successful on camera %d\n", ctx->hcam);
            UpdateCtxImageFormat(ctx);

            // Allocate enough memory to capture all the frames.
            // The frame size is the same for all exposure modes, so we don't
            // need to re-allocate the frame memory again and again. But because
            // we get the size after first call to pl_exp_setup_seq we do it
            // this way to keep example simple.
            frameInMemory = new (std::nothrow) uns8[exposureBytes];
            if (!frameInMemory)
            {
                printf("Unable to allocate buffer for camera %d\n", ctx->hcam);
                errorOccurred = true;
                break;
            }

            // Send the go signal
            if (PV_OK != pl_exp_start_seq(ctx->hcam, frameInMemory))
            {
                PrintErrorMessage(pl_error_code(), "pl_exp_start_seq() error");
                errorOccurred = true;
                break;
            }
            printf("Acquisition started on camera %d\n", ctx->hcam);

            // Here we need to wait for a frame readout notification signaled by
            // event raised in the callback.
            printf("Waiting for EOF event to occur on camera %d\n", ctx->hcam);
            if (!WaitForEofEvent(ctx, 5000, errorOccurred))
                break;

            printf("Frame has been delivered from camera %d\n", ctx->hcam);
            ShowImage(ctx, frameInMemory, exposureBytes);

            if (PV_OK != pl_exp_finish_seq(ctx->hcam, frameInMemory, 0))
            {
                PrintErrorMessage(pl_error_code(), "pl_exp_finish_seq() error");
            }
            else
            {
                printf("Acquisition finished on camera %d\n", ctx->hcam);
            }

            delete [] frameInMemory;
            frameInMemory = nullptr;
        }
        if (errorOccurred || ctx->threadAbortFlag)
            break;
    }

    if (PV_OK != pl_exp_abort(ctx->hcam, CCS_NO_CHANGE))
    {
        PrintErrorMessage(pl_error_code(), "pl_exp_abort() error");
    }

    delete [] frameInMemory;

    CloseAllCamerasAndUninit(contexts);

    if (errorOccurred)
        return APP_EXIT_ERROR;
    return 0;
}

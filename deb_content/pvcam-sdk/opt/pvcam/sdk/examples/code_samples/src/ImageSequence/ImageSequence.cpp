/******************************************************************************/
/* Copyright (C) Teledyne Photometrics. All rights reserved.                  */
/******************************************************************************/

// Local
#include "Common.h"

// This sample application shows how to run an acquisition with known number of
// exposures. This is the same approach as for single image acquisition with the
// only difference being in the pl_exp_setup_seq() parameters.
// Since READOUT_COMPLETE flag is only reported when the whole sequence is
// acquired, in case user wants to know acquisition progress it is necessary to
// periodically read PARAM_BOF_EOF_COUNT as shown in the code.
int main(int argc, char* argv[])
{
    if (!ShowAppInfo(argc, argv))
        return APP_EXIT_ERROR;

    std::vector<CameraContext*> contexts;
    if (!InitAndOpenOneCamera(contexts, cSingleCamIndex))
        return APP_EXIT_ERROR;

    CameraContext* ctx = contexts[cSingleCamIndex];

    // Install generic ctr+c handler
    if (!InstallGenericCliTerminationHandler(contexts))
    {
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }

    // To check the progress of the sequence capture we need to monitor EOF counter
    if (!IsParamAvailable(ctx->hcam, PARAM_BOF_EOF_COUNT, "PARAM_BOF_EOF_COUNT"))
    {
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }

    // Enable BOF/EOF counter
    if (!IsParamAvailable(ctx->hcam, PARAM_BOF_EOF_ENABLE, "PARAM_BOF_EOF_ENABLE"))
    {
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }
    uns32 paramEofEnable = END_FRAME_IRQS;
    if (PV_OK != pl_set_param(ctx->hcam, PARAM_BOF_EOF_ENABLE,
                (void*)&paramEofEnable))
    {
        PrintErrorMessage(pl_error_code(),
                "pl_set_param(PARAM_BOF_EOF_ENABLE) error");
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }

    // Clear BOF/EOF counter
    if (!IsParamAvailable(ctx->hcam, PARAM_BOF_EOF_CLR, "PARAM_BOF_EOF_CLR"))
    {
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }
    rs_bool bofEofClear = TRUE;
    if (PV_OK != pl_set_param(ctx->hcam, PARAM_BOF_EOF_CLR, (void*)&bofEofClear))
    {
        PrintErrorMessage(pl_error_code(),
                "pl_set_param(PARAM_BOF_EOF_CLR) error");
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }
    // Call this after each clear of BOF/EOF counter
    if (PV_OK != pl_exp_abort(ctx->hcam, CCS_NO_CHANGE))
    {
        PrintErrorMessage(pl_error_code(), "pl_exp_abort() error");
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }

    uns32 exposureBytes;
    const uns32 exposureTime = 40; // milliseconds
    const uns16 framesToAcquire = 5;

    // Setup the acquisition
    // FramesToAcquire specifies number of frames to acquire in this sequence.
    // exposureBytes will now hold the size of the whole buffer for all the
    // images in the sequence.
    if (PV_OK != pl_exp_setup_seq(ctx->hcam, framesToAcquire, 1, &ctx->region,
                TIMED_MODE, exposureTime, &exposureBytes))
    {
        PrintErrorMessage(pl_error_code(), "pl_exp_setup_seq() error");
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }
    printf("Acquisition setup successful on camera %d\n", ctx->hcam);
    UpdateCtxImageFormat(ctx);

    // Calculate size of each frame
    const uns32 oneFrameBytes = exposureBytes / framesToAcquire;

    // Allocate buffer, used uns8 to allocate raw bytes
    uns8* frameInMemory = new (std::nothrow) uns8[exposureBytes];
    if (!frameInMemory)
    {
        printf("Unable to allocate buffer for camera %d\n", ctx->hcam);
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }

    // Start the acquisition - it is used as software trigger in TIMED trigger mode.
    // In hardware trigger mode (Strobe or Bulb) after this call camera waits for
    // external trigger signal.
    if (PV_OK != pl_exp_start_seq(ctx->hcam, (void*)frameInMemory))
    {
        PrintErrorMessage(pl_error_code(), "pl_exp_start_seq() error");
        CloseAllCamerasAndUninit(contexts);
        delete [] frameInMemory;
        return APP_EXIT_ERROR;
    }
    printf("Acquisition started on camera %d\n", ctx->hcam);

    int16 status;
    uns32 byte_cnt;

    uns32 eofCount = 0;
    uns32 oldEofCount = 0;

    bool errorOccurred = false;

    // Keep checking the camera readout status.
    // Function returns FALSE if status is READOUT_FAILED
    while (PV_OK == pl_exp_check_status(ctx->hcam, &status, &byte_cnt)
            && status != READOUT_COMPLETE && status != READOUT_NOT_ACTIVE
            && !ctx->threadAbortFlag)
    {
        // In each loop you can optionally make this call to see how many frames
        // have been acquired so far
        if (PV_OK != pl_get_param(ctx->hcam, PARAM_BOF_EOF_COUNT, ATTR_CURRENT,
                    (void*)&eofCount))
        {
            PrintErrorMessage(pl_error_code(),
                    "pl_get_param(PARAM_BOF_EOF_COUNT) error, ignored");
        }
        else if (eofCount > oldEofCount)
        {
            oldEofCount = eofCount;
            printf("Acquired frame #%u out of %u on camera %d\n",
                    eofCount, framesToAcquire, ctx->hcam);
        }

        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }
    if (ctx->threadAbortFlag)
    {
        // This flag is set on ctrl+c, but in this sample we have to handle
        // the acquisition abort explicitly
        if (PV_OK != pl_exp_abort(ctx->hcam, CCS_NO_CHANGE))
        {
            PrintErrorMessage(pl_error_code(), "pl_exp_abort() error");
            errorOccurred = true;
        }
        printf("Processing aborted on camera %d\n", ctx->hcam);
    }
    else if (status == READOUT_FAILED)
    {
        printf("Frame readout failed on camera %d\n", ctx->hcam);
        errorOccurred = true;
    }
    else
    {
        // Same call as in while loop above for the last frame since EOF counter
        // was incremented only after READOUT_COMPLETE status was reported and
        // the loop exited.
        if (PV_OK != pl_get_param(ctx->hcam, PARAM_BOF_EOF_COUNT, ATTR_CURRENT,
                    (void*)&eofCount))
        {
            PrintErrorMessage(pl_error_code(), "pl_get_param(PARAM_BOF_EOF_COUNT) error");
            errorOccurred = true;
        }
        else
        {
            printf("Acquired frame #%u out of %u on camera %d\n",
                    eofCount, framesToAcquire, ctx->hcam);
        }

        // Display values of the first 5 pixels
        for (uns32 i = 0; i < framesToAcquire; i++)
        {
            printf("Frame #%u readout completed on camera %d\n", i + 1, ctx->hcam);
            ShowImage(ctx, frameInMemory + i * oneFrameBytes, exposureBytes);
        }
    }

    // When acquiring single frames with callback notifications call this
    // after each frame before new acquisition is started with pl_exp_start_seq()
    if (PV_OK != pl_exp_finish_seq(ctx->hcam, frameInMemory, 0))
    {
        PrintErrorMessage(pl_error_code(), "pl_exp_finish_seq() error");
    }
    else
    {
        printf("Acquisition finished on camera %d\n", ctx->hcam);
    }

    delete [] frameInMemory;

    CloseAllCamerasAndUninit(contexts);

    if (errorOccurred)
        return APP_EXIT_ERROR;
    return 0;
}

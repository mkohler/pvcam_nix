/******************************************************************************/
/* Copyright (C) Teledyne Photometrics. All rights reserved.                  */
/******************************************************************************/

// Local
#include "Common.h"

// This sample application shows how to acquire a single frame with software
// trigger (started by a host) and polling for the readout complete status.
int main(int argc, char* argv[])
{
    if (!ShowAppInfo(argc, argv))
        return APP_EXIT_ERROR;

    std::vector<CameraContext*> contexts;
    if (!InitAndOpenOneCamera(contexts, cSingleCamIndex))
        return APP_EXIT_ERROR;

    CameraContext* ctx = contexts[cSingleCamIndex];

    // Install generic ctr+c handler
    if (!InstallGenericCliTerminationHandler(contexts))
    {
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }

    uns32 exposureBytes;
    const uns32 exposureTime = 40; // milliseconds

    // Setup the acquisition
    // TIMED_MODE flag means acquisition will start with software trigger,
    // other flags such as STROBED_MODE can be used for hardware trigger.
    if (PV_OK != pl_exp_setup_seq(ctx->hcam, 1, 1, &ctx->region, TIMED_MODE,
                exposureTime, &exposureBytes))
    {
        PrintErrorMessage(pl_error_code(), "pl_exp_setup_seq() error");
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }
    printf("Acquisition setup successful on camera %d\n", ctx->hcam);
    UpdateCtxImageFormat(ctx);

    // Allocate buffer, used uns8 to allocate raw bytes
    uns8* frameInMemory = new (std::nothrow) uns8[exposureBytes];
    if (!frameInMemory)
    {
        printf("Unable to allocate buffer for camera %d\n", ctx->hcam);
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }

    bool errorOccurred = false;
    uns32 imageCounter = 0;
    while (imageCounter < 50)
    {
        // Start the acquisition - it is used as software trigger in TIMED
        // trigger mode. In hardware trigger mode (Strobe or Bulb) after this
        // call camera waits for external trigger signal.
        if (PV_OK != pl_exp_start_seq(ctx->hcam, frameInMemory))
        {
            PrintErrorMessage(pl_error_code(), "pl_exp_start_seq() error");
            errorOccurred = true;
            break;
        }
        printf("Acquisition started on camera %d\n", ctx->hcam);

        int16 status;
        uns32 byte_cnt;

        // Keep checking the camera readout status.
        // Function returns FALSE if status is READOUT_FAILED
        while (PV_OK == pl_exp_check_status(ctx->hcam, &status, &byte_cnt)
                && status != READOUT_COMPLETE && status != READOUT_NOT_ACTIVE
                && !ctx->threadAbortFlag)
        {
            printf("Waiting 20ms for frame #%u exposure and readout\n",
                    imageCounter + 1);
            std::this_thread::sleep_for(std::chrono::milliseconds(20));
        }
        if (ctx->threadAbortFlag)
        {
            // This flag is set on ctrl+c, just break the loop, the acquisition
            // is aborted after that
            printf("Processing aborted on camera %d\n", ctx->hcam);
            break;
        }
        if (status == READOUT_FAILED)
        {
            printf("Frame #%u readout failed on camera %d\n",
                    imageCounter + 1, ctx->hcam);
            errorOccurred = true;
            break;
        }

        printf("Frame #%u readout successfully completed on camera %d\n",
                imageCounter + 1, ctx->hcam);
        ShowImage(ctx, frameInMemory, exposureBytes);

        // When acquiring single frames with callback notifications call this
        // after each frame before new acquisition is started with pl_exp_start_seq()
        if (PV_OK != pl_exp_finish_seq(ctx->hcam, frameInMemory, 0))
        {
            PrintErrorMessage(pl_error_code(), "pl_exp_finish_seq() error");
        }
        else
        {
            printf("Acquisition finished on camera %d\n", ctx->hcam);
        }

        imageCounter++;
    }

    if (PV_OK != pl_exp_abort(ctx->hcam, CCS_NO_CHANGE))
    {
        PrintErrorMessage(pl_error_code(), "pl_exp_abort() error");
    }

    delete [] frameInMemory;

    CloseAllCamerasAndUninit(contexts);

    if (errorOccurred)
        return APP_EXIT_ERROR;
    return 0;
}

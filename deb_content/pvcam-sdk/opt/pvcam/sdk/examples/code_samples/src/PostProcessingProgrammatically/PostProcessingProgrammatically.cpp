/******************************************************************************/
/* Copyright (C) Teledyne Photometrics. All rights reserved.                  */
/******************************************************************************/

// Local
#include "Common.h"

// This sample application demonstrates how to correctly control
// post processing features programmatically.
//
// Every camera may support different set of post processing 'features'.
// Features are only groups, they group one or more 'parameters'.
// These 'parameters' are similar to regular PVCAM parameters but they
// need to be discovered by the application at run-time. The 'parameters'
// are currently limited to UNS32 data type.
// The application will usually build a table similar to the following one:
// - Feature: "B.E.R.T" (PARAM_PP_INDEX=0)
//   - Parameter: "ENABLED" (PARAM_PP_PARAM_INDEX=0)
//   - Parameter: "Threshold" (PARAM_PP_PARAM_INDEX=1)
// - Feature: "DENOISING" (PARAM_PP_INDEX=1)
//   - Parameter: "ENABLED" (PARAM_PP_PARAM_INDEX=0)
//   - Parameter: "Number of iterations" (PARAM_PP_PARAM_INDEX=1)
//   - Parameter: "Sigma" (PARAM_PP_PARAM_INDEX=2)
//
// Because the indexes and their order may be different for every camera
// the PVCAM also provides a PARAM_PP_FEAT_ID and PARAM_PP_PARAM_ID parameters.
// These IDs are uniquely defined in pvcam.h and they are assigned to a particular
// feature/parameter.
// If the application wants to programmatically control a particular PP parameter
// then it should use the ID to find to corresponding indexes first, then set the
// indexes to PVCAM and control the parameter state via PARAM_PP_PARAM parameter.
// The above process is shown in the following code example.
int main(int argc, char* argv[])
{
    if (!ShowAppInfo(argc, argv))
        return APP_EXIT_ERROR;

    std::vector<CameraContext*> contexts;
    if (!InitAndOpenOneCamera(contexts, cSingleCamIndex))
        return APP_EXIT_ERROR;

    CameraContext* ctx = contexts[cSingleCamIndex];

    // Check and determine if there are any post processing features available
    if (!IsParamAvailable(ctx->hcam, PARAM_PP_INDEX, "PARAM_PP_INDEX"))
    {
        printf("\n\nNo post processing features available in this camera.\n");
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }

    // Build the post processing table first
    const std::vector<PvcamPpFeature> ppFeatures = DiscoverCameraPostProcessing(ctx);
    if (ppFeatures.empty())
    {
        // Some error must have occurred in the above function.
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }

    // Print the table to console, just for information
    printf("Camera Post Processing table:\n");
    for (const PvcamPpFeature& ppFeature : ppFeatures)
    {
        // Print IDs as signed to display invalid value as -1 instead of huge number.
        // Valid ID is usually small number based on current pattern in pvcam.h.
        printf("- FEATURE at index %d: id=%d, name='%s'\n",
                ppFeature.index, (int32)ppFeature.id, ppFeature.name.c_str());
        for (const PvcamPpParameter& ppParam : ppFeature.parameterList)
        {
            printf("  - PARAMETER at index %d: id=%d, name='%s'\n",
                    ppParam.index, (int32)ppParam.id, ppParam.name.c_str());
        }
    }
    printf("\n");

    // In this example we will try to enable "DENOISING" parameter, if available
    int16 denoFeatureIndex;
    int16 denoParamIndex;
    const bool denoFound = FindPpParamIndexes(ppFeatures,
            PP_FEATURE_DENOISING_ENABLED, denoFeatureIndex, denoParamIndex);
    if (!denoFound)
    {
        printf("Camera %d doesn't support DENOISING post-processing feature\n",
                ctx->hcam);
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }

    uns32 curValue = 0;
    if (!GetPpParamValue(ctx, denoFeatureIndex, denoParamIndex, curValue))
    {
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }
    uns32 newValue = (curValue != 0) ? 0 : 1;
    printf("Current PP_FEATURE_DENOISING_ENABLED value is: %u, setting to %u...\n",
            curValue, newValue);
    if (!SetPpParamValue(ctx, denoFeatureIndex, denoParamIndex, newValue))
    {
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }
    if (!GetPpParamValue(ctx, denoFeatureIndex, denoParamIndex, curValue))
    {
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }
    printf("New PP_FEATURE_DENOISING_ENABLED value is: %u\n", curValue);

    CloseAllCamerasAndUninit(contexts);

    return 0;
}

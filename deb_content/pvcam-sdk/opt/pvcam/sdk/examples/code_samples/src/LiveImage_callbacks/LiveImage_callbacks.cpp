/******************************************************************************/
/* Copyright (C) Teledyne Photometrics. All rights reserved.                  */
/******************************************************************************/

// Local
#include "Common.h"

typedef struct SampleContext
{
   int myData1;
   int myData2;
}
SampleContext;

// Function that gets called from PVCAM when EOF event arrives
void PV_DECL CustomEofHandler(FRAME_INFO* pFrameInfo, void* pContext)
{
    if (!pFrameInfo || !pContext)
        return;
    auto ctx = static_cast<CameraContext*>(pContext);

    SampleContext* eofCtx = static_cast<SampleContext*>(ctx->eofContext);
    if (eofCtx)
    {
        printf("SampleContext::myData1 = %d\n", eofCtx->myData1);
        printf("SampleContext::myData2 = %d\n", eofCtx->myData2);
    }

    // Store frame information for later use on main thread
    ctx->eofFrameInfo = *pFrameInfo;

    // Obtain a pointer to the just acquired frame
    if (PV_OK != pl_exp_get_latest_frame(ctx->hcam, &ctx->eofFrame))
    {
        PrintErrorMessage(pl_error_code(), "pl_exp_get_latest_frame() error");
        ctx->eofFrame = nullptr;
    }

    // Unblock acquisition thread
    {
        std::lock_guard<std::mutex> lock(ctx->eofEvent.mutex);
        ctx->eofEvent.flag = true;
    }
    ctx->eofEvent.cond.notify_all();
}

// This sample application shows how to setup and run camera in continuous mode
// with circular buffer with callback frame readout notification, additionally
// extended callback is used that delivers extended frame information (e.g.
// frame number, timestamp etc). This circular buffer mode is typically used to
// run the camera until user stops the acquisition, or until an external
// condition is met, or system gets in focus etc.
// This sample code collects 50 frames, but this condition can be replaced by
// a UI event such as user pressing a stop button etc.
int main(int argc, char* argv[])
{
    if (!ShowAppInfo(argc, argv))
        return APP_EXIT_ERROR;

    std::vector<CameraContext*> contexts;
    if (!InitAndOpenOneCamera(contexts, cSingleCamIndex))
        return APP_EXIT_ERROR;

    CameraContext* ctx = contexts[cSingleCamIndex];

    // Install generic ctr+c handler
    if (!InstallGenericCliTerminationHandler(contexts))
    {
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }

    // Setup some custom data needed in callback handler
    SampleContext dataContext;
    dataContext.myData1 = 0;
    dataContext.myData2 = 50;
    ctx->eofContext = &dataContext;

    // Register callback to receive a notification when EOF event arrives.
    // The handler function will be called by PVCAM, last parameter
    // might hold a pointer to user-specific content that will be passed with
    // the callback once it is invoked.
    if (PV_OK != pl_cam_register_callback_ex3(ctx->hcam, PL_CALLBACK_EOF,
                (void*)CustomEofHandler, ctx))
    {
        PrintErrorMessage(pl_error_code(), "pl_cam_register_callback() error");
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }
    printf("EOF callback handler registered on camera %d\n", ctx->hcam);

    uns32 exposureBytes;
    const uns32 exposureTime = 5; // milliseconds

    const uns16 circBufferFrames = 20;
    const int16 bufferMode = CIRC_OVERWRITE;

    // Setup continuous acquisition with circular buffer mode. TIMED_MODE
    // indicates this is software trigger mode and each acquisition is after
    // initial pl_exp_start_cont() call started internally from the camera.
    // To run in hardware trigger mode use either STROBED_MODE, BULB_MODE or
    // TRIGGER_FIRST_MODE.
    if (PV_OK != pl_exp_setup_cont(ctx->hcam, 1, &ctx->region, TIMED_MODE,
                exposureTime, &exposureBytes, bufferMode))
    {
        PrintErrorMessage(pl_error_code(), "pl_exp_setup_cont() error");
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }
    printf("Acquisition setup successful on camera %d\n", ctx->hcam);
    UpdateCtxImageFormat(ctx);

    const uns32 circBufferBytes = circBufferFrames * exposureBytes;

    // Allocate memory for circular buffer of frames, size of one frame is
    // returned in exposureBytes by pl_exp_setup_cont()
    uns8* circBufferInMemory = new (std::nothrow) uns8[circBufferBytes];
    if (!circBufferInMemory)
    {
        printf("Unable to allocate buffer for camera %d\n", ctx->hcam);
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }

    // Start the continuous acquisition, again tell this function size of buffer
    // it has for the frames. Camera starts the first acquisition based on this
    // host command and subsequent acquisitions are triggered internally by the
    // camera itself. In hardware trigger mode this sets the camera to waiting
    // state awaiting external trigger signals.
    if (PV_OK != pl_exp_start_cont(ctx->hcam, circBufferInMemory, circBufferBytes))
    {
        PrintErrorMessage(pl_error_code(), "pl_exp_start_cont() error");
        CloseAllCamerasAndUninit(contexts);
        delete [] circBufferInMemory;
        return APP_EXIT_ERROR;
    }
    printf("Acquisition started on camera %d\n", ctx->hcam);

    bool errorOccurred = false;
    uns32 framesAcquired = 0;
    while (framesAcquired < 50)
    {
        dataContext.myData1++;
        dataContext.myData2--;

        // Here we need to wait for a frame readout notification signaled by
        // event raised in the callback.
        printf("Waiting for EOF event to occur on camera %d\n", ctx->hcam);
        if (!WaitForEofEvent(ctx, 5000, errorOccurred))
            break;

        // Timestamp is in hundreds of microseconds
        printf("Frame #%d acquired, timestamp = %lldus\n",
                ctx->eofFrameInfo.FrameNr, 100 * ctx->eofFrameInfo.TimeStamp);
        ShowImage(ctx, ctx->eofFrame, exposureBytes);

        framesAcquired++;
    }

    // Once we have acquired the number of frames needed the acquisition can be
    // stopped, no other call is needed to stop the acquisition.
    if (PV_OK != pl_exp_abort(ctx->hcam, CCS_NO_CHANGE))
    {
        PrintErrorMessage(pl_error_code(), "pl_exp_abort() error");
    }
    else
    {
        printf("Acquisition stopped on camera %d\n", ctx->hcam);
    }

    delete [] circBufferInMemory;

    CloseAllCamerasAndUninit(contexts);

    if (errorOccurred)
        return APP_EXIT_ERROR;
    return 0;
}

/******************************************************************************/
/* Copyright (C) Teledyne Photometrics. All rights reserved.                  */
/******************************************************************************/

// Local
#include "Common.h"

// Mutex for synchronized printing to standard output
std::mutex g_printMutex;

void ThreadFunc(CameraContext* ctx)
{
    // Register callback to receive a notification when EOF event arrives.
    // The handler function will be called by PVCAM, last parameter
    // might hold a pointer to user-specific content that will be passed with
    // the callback once it is invoked.
    if (PV_OK != pl_cam_register_callback_ex3(ctx->hcam, PL_CALLBACK_EOF,
                (void*)GenericEofHandler, ctx))
    {
        PrintErrorMessage(pl_error_code(), "pl_cam_register_callback() error");
        return;
    }
    printf("EOF callback handler registered on camera %d\n", ctx->hcam);

    uns32 exposureBytes;
    const uns32 exposureTime = 5;

    // Setup the acquisition
    // TIMED_MODE flag means acquisition will start with software trigger,
    // other flags such as STROBED_MODE can be used for hardware trigger.
    if (PV_OK != pl_exp_setup_seq(ctx->hcam, 1, 1, &ctx->region, TIMED_MODE,
                exposureTime, &exposureBytes))
    {
        PrintErrorMessage(pl_error_code(), "pl_exp_setup_seq() error");
        return;
    }
    printf("Acquisition setup successful on camera %d\n", ctx->hcam);
    UpdateCtxImageFormat(ctx);

    // Allocate buffer, used uns8 to allocate raw bytes
    uns8* frameInMemory = new (std::nothrow) uns8[exposureBytes];
    if (!frameInMemory)
    {
        printf("Unable to allocate buffer for camera %d\n", ctx->hcam);
        return;
    }

    uns32 imageCounter = 0;
    while (imageCounter < 5)
    {
        // Start the acquisition - it is used as software trigger in TIMED
        // trigger mode. In hardware trigger mode (Strobe or Bulb) after this
        // call camera waits for external trigger signal.
        if (PV_OK != pl_exp_start_seq(ctx->hcam, frameInMemory))
        {
            PrintErrorMessage(pl_error_code(), "pl_exp_start_seq() error");
            break;
        }
        printf("Acquisition started on camera %d\n", ctx->hcam);

        // Here we need to wait for a frame readout notification signaled by
        // event raised in the callback.
        printf("Waiting for EOF event to occur on camera %d\n", ctx->hcam);
        bool errorOccurred;
        if (!WaitForEofEvent(ctx, 5000, errorOccurred))
            break;

        g_printMutex.lock();

        printf("Frame #%u has been delivered from camera %d\n",
                imageCounter + 1, ctx->hcam);
        ShowImage(ctx, frameInMemory, exposureBytes);

        g_printMutex.unlock();

        // When acquiring single frames with callback notifications call this
        // after each frame before new acquisition is started with pl_exp_start_seq()
        if (PV_OK != pl_exp_finish_seq(ctx->hcam, frameInMemory, 0))
        {
            PrintErrorMessage(pl_error_code(), "pl_exp_finish_seq() error");
        }
        else
        {
            printf("Acquisition finished on camera %d\n", ctx->hcam);
        }

        imageCounter++;
    }

    if (PV_OK != pl_exp_abort(ctx->hcam, CCS_NO_CHANGE))
    {
        PrintErrorMessage(pl_error_code(), "pl_exp_abort() error");
    }

    delete [] frameInMemory;
}

// This sample application demonstrates acquisition of single frames with
// callback notification in a loop which collects 5 frames.
// Each acquisition is started by the host with a software trigger
// (pl_exp_start_seq()).
// Please note pl_exp_finish_seq() needs to be called after each frame is
// acquired before new software trigger pl_exp_start_seq() is sent.
int main(int argc, char* argv[])
{
    if (!ShowAppInfo(argc, argv))
        return APP_EXIT_ERROR;

    std::vector<CameraContext*> contexts;
    uns16 nrOfCameras = cMultiCamCount;
    if (!InitAndOpenMultipleCameras(contexts, nrOfCameras))
        return APP_EXIT_ERROR;

    // Install generic ctr+c handler
    if (!InstallGenericCliTerminationHandler(contexts))
    {
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }

    bool errorOccurred = false;
    // Start acquisition threads for all cameras
    for (int16 i = 0; i < nrOfCameras; i++)
    {
        CameraContext* ctx = contexts[i];

        ctx->threadAbortFlag = false;
        ctx->thread = new(std::nothrow) std::thread(ThreadFunc, ctx);
        if (!ctx->thread)
        {
            errorOccurred = true;

            g_printMutex.lock();
            printf("Failed to start acquisition thread for camera %d\n", ctx->hcam);
            g_printMutex.unlock();

            // Don't start remaining threads and abort those already running
            for (int16 j = 0; j < i; j++)
            {
                CameraContext* ctx2 = contexts[j];
                {
                    std::lock_guard<std::mutex> lock(ctx2->eofEvent.mutex);
                    // Set abort flag, eof flag doesn't matter
                    ctx2->threadAbortFlag = true;
                }
                ctx2->eofEvent.cond.notify_all();
            }
            break;
        }
    }

    // Wait for all acq. threads to finish
    for (int i = 0; i < nrOfCameras; i++)
    {
        CameraContext* ctx = contexts[i];

        if (ctx->thread && ctx->thread->joinable())
        {
            ctx->thread->join();
        }
        delete ctx->thread;
        ctx->thread = nullptr;
    }

    CloseAllCamerasAndUninit(contexts);

    if (errorOccurred)
        return APP_EXIT_ERROR;
    return 0;
}

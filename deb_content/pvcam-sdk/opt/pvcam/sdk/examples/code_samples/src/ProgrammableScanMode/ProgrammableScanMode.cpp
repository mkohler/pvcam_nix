/******************************************************************************/
/* Copyright (C) Teledyne Photometrics. All rights reserved.                  */
/******************************************************************************/

// Local
#include "Common.h"

// This sample application demonstrates how to use the Programmable Scan Mode,
// including the use of PARAM_SCAN_MODE, PARAM_SCAN_DIRECTION, PARAM_SCAN_DIRECTION_RESET,
// PARAM_SCAN_LINE_DELAY, PARAM_SCAN_LINE_TIME and PARAM_SCAN_WIDTH.
// Each acquisition is started by the host with a software trigger
// (pl_exp_start_seq()).
// Please note pl_exp_finish_seq() needs to be called after each frame is
// acquired before new software trigger pl_exp_start_seq() is sent.
int main(int argc, char* argv[])
{
    if (!ShowAppInfo(argc, argv))
        return APP_EXIT_ERROR;

    std::vector<CameraContext*> contexts;
    if (!InitAndOpenOneCamera(contexts, cSingleCamIndex))
        return APP_EXIT_ERROR;

    CameraContext* ctx = contexts[cSingleCamIndex];

    // Configure the scan mode

    // The default scan mode is "Auto", this is the regular mode in which
    // the camera controls the readout. The camera reads each row in succession
    // without inserting additional delays between rows.
    // We are interested in the other two modes where we can control the readout
    // by either inserting a delay after each line or by specifying scan width.
    // This example will configure the readout using the line delay.
    NVPC scanModes;
    if (!ReadEnumeration(ctx->hcam, &scanModes, PARAM_SCAN_MODE, "PARAM_SCAN_MODE"))
    {
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }
    printf("Camera %d supports following Scan Modes:\n", ctx->hcam);
    for (size_t i = 0; i < scanModes.size(); ++i)
    {
        printf("  %d: %s\n", scanModes[i].value, scanModes[i].name.c_str());
    }

    // Read the available 'Expose Out Modes'. If the camera supports Programmable
    // Scan Mode it should also support the 'Line Trigger' output which will send
    // a pulse to the Expose Out signal for every line read.
    // We will set the desired Expose Out mode in the pl_exp_setup() call below.
    NVPC exposeOutModes;
    if (!ReadEnumeration(ctx->hcam, &exposeOutModes, PARAM_EXPOSE_OUT_MODE,
                "PARAM_EXPOSE_OUT_MODE"))
    {
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }
    printf("Camera %d supports following Expose Out modes:\n", ctx->hcam);
    for (size_t i = 0; i < exposeOutModes.size(); ++i)
    {
        printf("  %d: %s\n", exposeOutModes[i].value, exposeOutModes[i].name.c_str());
    }
    printf("\n");

    // Just for information, we will read current line time. This is the time
    // it takes to read one sensor line. This parameter is camera-specific
    // so the call will fail for cameras not supporting it.
    long64 currentScanLineTimeNs = 0;
    if (PV_OK != pl_get_param(ctx->hcam, PARAM_SCAN_LINE_TIME, ATTR_CURRENT,
                (void*)&currentScanLineTimeNs))
    {
        PrintErrorMessage(pl_error_code(), "pl_get_param(PARAM_SCAN_LINE_TIME) error");
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }
    printf("Current line time: %lldns\n", currentScanLineTimeNs);

    // Set the desired scan mode, this call may fail if, for some reason,
    // the mode is not present in the above list.
    // In this mode the PARAM_SCAN_LINE_DELAY parameter will become available.
    // The PARAM_SCAN_WIDTH will become read-only and its value will
    // be auto-calculated and reported by the camera
    const int32 newScanMode = PL_SCAN_MODE_PROGRAMMABLE_LINE_DELAY;
    if (PV_OK != pl_set_param(ctx->hcam, PARAM_SCAN_MODE, (void*)&newScanMode))
    {
        PrintErrorMessage(pl_error_code(), "pl_set_param(PARAM_SCAN_MODE) error");
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }

    // The delay is represented in number of lines. We want to delay every line
    // readout by twenty line times.
    const uns16 newScanLineDelay = 20;
    if (PV_OK != pl_set_param(ctx->hcam, PARAM_SCAN_LINE_DELAY,
                (void*)&newScanLineDelay))
    {
        PrintErrorMessage(pl_error_code(), "pl_set_param(PARAM_SCAN_LINE_DELAY) error");
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }

    // We can also configure the camera to flip the scan direction after every frame.
    NVPC scanDirections;
    if (!ReadEnumeration(ctx->hcam, &scanDirections, PARAM_SCAN_DIRECTION,
                "PARAM_SCAN_DIRECTION"))
    {
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }
    printf("Camera %d supports following Scan Directions:\n", ctx->hcam);
    for (size_t i = 0; i < scanDirections.size(); ++i)
    {
        printf(" %d: %s\n", scanDirections[i].value, scanDirections[i].name.c_str());
    }
    printf("\n");

    int32 newScanDirection = PL_SCAN_DIRECTION_DOWN_UP; // Alternate the readout direction
    if (PV_OK != pl_set_param(ctx->hcam, PARAM_SCAN_DIRECTION,
                (void*)&newScanDirection))
    {
        PrintErrorMessage(pl_error_code(), "pl_set_param(PARAM_SCAN_DIRECTION) error");
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }

    // The PARAM_SCAN_DIRECTION_RESET is only useful for the alternate DOWN_UP mode
    // to reset the scan direction for every acquisition. We keep the reset disabled.
    rs_bool newScanDirectionRst = FALSE;
    if (PV_OK != pl_set_param(ctx->hcam, PARAM_SCAN_DIRECTION_RESET,
                (void*)&newScanDirectionRst))
    {
        PrintErrorMessage(pl_error_code(), "pl_set_param(PARAM_SCAN_DIRECTION_RESET) error");
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }

    // Configure the acquisition

    // Install generic ctr+c handler
    if (!InstallGenericCliTerminationHandler(contexts))
    {
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }

    // Register callback to receive a notification when EOF event arrives.
    // The handler function will be called by PVCAM, last parameter
    // might hold a pointer to user-specific content that will be passed with
    // the callback once it is invoked.
    if (PV_OK != pl_cam_register_callback_ex3(ctx->hcam, PL_CALLBACK_EOF,
                (void*)GenericEofHandler, ctx))
    {
        PrintErrorMessage(pl_error_code(), "pl_cam_register_callback() error");
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }
    printf("EOF callback handler registered on camera %d\n", ctx->hcam);

    uns32 exposureBytes;
    const uns32 exposureTime = 5; // milliseconds

    // Setup the acquisition
    // TIMED_MODE flag means acquisition will start with software trigger,
    // other flags such as STROBED_MODE can be used for hardware trigger.
    const int16 expMode = EXT_TRIG_INTERNAL | EXPOSE_OUT_LINE_TRIGGER;
    if (PV_OK != pl_exp_setup_seq(ctx->hcam, 1, 1, &ctx->region, expMode,
                exposureTime, &exposureBytes))
    {
        PrintErrorMessage(pl_error_code(), "pl_exp_setup_seq() error");
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }
    printf("Acquisition setup successful on camera %d\n", ctx->hcam);
    UpdateCtxImageFormat(ctx);

    // The PARAM_SCAN_LINE_TIME may depend on ROI, exposure and other
    // parameters set via pl_exp_setup() routines. We can update it only
    // after that.

    long64 newScanLineTimeNs = 0;
    if (PV_OK != pl_get_param(ctx->hcam, PARAM_SCAN_LINE_TIME, ATTR_CURRENT,
                (void*)&newScanLineTimeNs))
    {
        PrintErrorMessage(pl_error_code(), "pl_get_param(PARAM_SCAN_LINE_TIME) error");
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }
    printf("Line time after setup: %lldns\n", newScanLineTimeNs);

    // We can also update the scan width which is calculated based on the configured
    // line delay and exposure time.

    uns16 currentScanWidth = 0;
    if (PV_OK != pl_get_param(ctx->hcam, PARAM_SCAN_WIDTH, ATTR_CURRENT,
                (void*)&currentScanWidth))
    {
        PrintErrorMessage(pl_error_code(), "pl_get_param(PARAM_SCAN_WIDTH) error");
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }
    printf("Current scan width: %u\n", currentScanWidth);

    printf("\n");

    // Allocate buffer, used uns8 to allocate raw bytes
    uns8* frameInMemory = new (std::nothrow) uns8[exposureBytes];
    if (!frameInMemory)
    {
        printf("Unable to allocate buffer for camera %d\n", ctx->hcam);
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }

    bool errorOccurred = false;
    uns32 imageCounter = 0;
    while (imageCounter < 5)
    {
        // Start the acquisition - it is used as software trigger in TIMED
        // trigger mode. In hardware trigger mode (Strobe or Bulb) after this
        // call camera waits for external trigger signal.
        if (PV_OK != pl_exp_start_seq(ctx->hcam, frameInMemory))
        {
            PrintErrorMessage(pl_error_code(), "pl_exp_start_seq() error");
            errorOccurred = true;
            break;
        }
        printf("Acquisition started on camera %d\n", ctx->hcam);

        // Here we need to wait for a frame readout notification signaled by
        // event raised in the callback.
        printf("Waiting for EOF event to occur on camera %d\n", ctx->hcam);
        if (!WaitForEofEvent(ctx, 5000, errorOccurred))
            break;

        printf("Frame #%u has been delivered from camera %d\n",
                imageCounter + 1, ctx->hcam);
        ShowImage(ctx, frameInMemory, exposureBytes);

        // Try to save the image, this will fail if the executable is launched
        // from Program Files or other restricted location.
        // Save the image as IMG_1920x1080_1.raw
        const std::string path = "IMG_" + std::to_string(ctx->sensorResX)
            + "x" + std::to_string(ctx->sensorResY)
            + "_" + std::to_string(imageCounter + 1) + ".raw";
        SaveImage(frameInMemory, exposureBytes, path.c_str());

        // When acquiring single frames with callback notifications call this
        // after each frame before new acquisition is started with pl_exp_start_seq()
        if (PV_OK != pl_exp_finish_seq(ctx->hcam, frameInMemory, 0))
        {
            PrintErrorMessage(pl_error_code(), "pl_exp_finish_seq() error");
        }
        else
        {
            printf("Acquisition finished on camera %d\n", ctx->hcam);
        }

        imageCounter++;
    }

    if (PV_OK != pl_exp_abort(ctx->hcam, CCS_NO_CHANGE))
    {
        PrintErrorMessage(pl_error_code(), "pl_exp_abort() error");
    }

    delete [] frameInMemory;

    CloseAllCamerasAndUninit(contexts);

    if (errorOccurred)
        return APP_EXIT_ERROR;
    return 0;
}

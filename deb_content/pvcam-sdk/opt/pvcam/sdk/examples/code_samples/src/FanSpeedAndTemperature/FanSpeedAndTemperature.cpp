/******************************************************************************/
/* Copyright (C) Teledyne Photometrics. All rights reserved.                  */
/******************************************************************************/

// System
#include <cmath>
#include <cstdio> // fflush

// Local
#include "Common.h"

// The thread body monitoring sensor temperature
void TemperatureThread(CameraContext* ctx)
{
    const std::vector<char> progress{ '|', '/', '-', '\\' };
    size_t progressIndex = 0;

    for (;;)
    {
        // eofEvent is used for thread synchronization in this code sample
        std::unique_lock<std::mutex> lock(ctx->eofEvent.mutex);
        // Update the temperature 4x per second if not blocked by main thread
        ctx->eofEvent.cond.wait_for(lock, std::chrono::milliseconds(250),
                [ctx]() { return ctx->threadAbortFlag; });
        if (ctx->threadAbortFlag)
            break;

        int16 temperature;
        if (PV_OK != pl_get_param(ctx->hcam, PARAM_TEMP, ATTR_CURRENT,
                    (void*)&temperature))
        {
            printf("\n");
            PrintErrorMessage(pl_error_code(), "pl_get_param(PARAM_TEMP) error");
            break;
        }

        // Stay on the same line while printing
        printf("\r%c Current sensor temperature on camera %d is %+7.2f C",
                progress[progressIndex], ctx->hcam, temperature / 100.0);
        fflush(stdout); // No new line at the end, flush output manually

        progressIndex = (progressIndex + 1) % progress.size();
    }
}

// This sample application demonstrates sensor temperature scanning and changing
// of fan speed and temperature set point
int main(int argc, char* argv[])
{
    if (!ShowAppInfo(argc, argv))
        return APP_EXIT_ERROR;

    std::vector<CameraContext*> contexts;
    if (!InitAndOpenOneCamera(contexts, cSingleCamIndex))
        return APP_EXIT_ERROR;

    CameraContext* ctx = contexts[cSingleCamIndex];

    NVPC mainMenuItems;

    // Check that we can read the sensor temperature
    if (!IsParamAvailable(ctx->hcam, PARAM_TEMP, "PARAM_TEMP"))
    {
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }

    // Check that we can read/set the sensor temperature setpoint
    // Do not exit if not available
    int16 setpointMin = 0;
    int16 setpointMax = 0;
    if (IsParamAvailable(ctx->hcam, PARAM_TEMP_SETPOINT, "PARAM_TEMP_SETPOINT"))
    {
        NVP nvp;
        nvp.value = 1; // Temperature setpoint
        nvp.name = "Change temperature setpoint";
        mainMenuItems.push_back(nvp);

        // Update minimum setpoint value
        if (PV_OK != pl_get_param(ctx->hcam, PARAM_TEMP_SETPOINT, ATTR_MIN,
                    (void*)&setpointMin))
        {
            PrintErrorMessage(pl_error_code(),
                    "pl_get_param(PARAM_TEMP_SETPOINT, ATTR_MIN) error");
            CloseAllCamerasAndUninit(contexts);
            return APP_EXIT_ERROR;
        }

        // Update maximum setpoint value
        if (PV_OK != pl_get_param(ctx->hcam, PARAM_TEMP_SETPOINT, ATTR_MAX,
                    (void*)&setpointMax))
        {
            PrintErrorMessage(pl_error_code(),
                    "pl_get_param(PARAM_TEMP_SETPOINT, ATTR_MAX) error");
            CloseAllCamerasAndUninit(contexts);
            return APP_EXIT_ERROR;
        }
    }

    // Check that we can read/set fan speed
    // Do not exit if not available
    NVPC fanSpeeds;
    if (ReadEnumeration(ctx->hcam, &fanSpeeds, PARAM_FAN_SPEED_SETPOINT,
                "PARAM_FAN_SPEED_SETPOINT"))
    {
        NVP nvp;
        nvp.value = 2; // Fan speed
        nvp.name = "Change fan speed";
        mainMenuItems.push_back(nvp);
    }

    // Let's use the mutex from eofEvent here even it has nothing to do with callbacks
    ctx->eofEvent.mutex.lock();

    // Start the thread that shows current temperature
    ctx->threadAbortFlag = false;
    ctx->thread = new(std::nothrow) std::thread(TemperatureThread, ctx);
    if (!ctx->thread)
    {
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }

    bool errorOccurred = false;
    // Allow user to interact with menus as long as valid input is entered
    for (;;)
    {
        printf("\nPress <Enter> to show main menu\n\n");
        ctx->eofEvent.mutex.unlock();
        // Here we are waiting for user input and the temperature thread is
        // updating last line with current sensor temperature
        WaitForInput();
        ctx->eofEvent.mutex.lock();

        int32 selection;
        if (!GetMenuSelection("Main menu", mainMenuItems, selection))
            break;

        // Temperature setpoint
        if (selection == 1)
        {
            // Update maximum setpoint value
            int16 setpoint;
            if (PV_OK != pl_get_param(ctx->hcam, PARAM_TEMP_SETPOINT, ATTR_CURRENT,
                        (void*)&setpoint))
            {
                PrintErrorMessage(pl_error_code(),
                        "pl_get_param(PARAM_TEMP_SETPOINT, ATTR_CURRENT) error");
                errorOccurred = true;
                break;
            }
            printf("Current temperature setpoint on camera %d is %+7.2f C\n",
                    ctx->hcam, setpoint / 100.0);

            printf("Type new temperature setpoint and press <Enter>: ");
            const std::string input = WaitForInput();
            double setpointF;
            if (!StrToDouble(input, setpointF))
            {
                printf("Invalid value entered, exiting...\n");
                errorOccurred = true;
                break;
            }
            setpoint = static_cast<int16>(std::round(setpointF * 100.0));
            if (setpoint < setpointMin || setpoint > setpointMax)
            {
                printf("Value out of allowed range [%+7.2f, %+7.2f], exiting...\n",
                        setpointMin / 100.0, setpointMax / 100.0);
                errorOccurred = true;
                break;
            }

            // Set new temperature setpoint
            if (PV_OK != pl_set_param(ctx->hcam, PARAM_TEMP_SETPOINT,
                        (void*)&setpoint))
            {
                PrintErrorMessage(pl_error_code(),
                        "pl_set_param(PARAM_TEMP_SETPOINT) error");
                errorOccurred = true;
                break;
            }
            printf("Temperature setpoint on camera %d set to %+7.2f C\n",
                    ctx->hcam, setpoint / 100.0);
        }
        // Fan speed
        else if (selection == 2)
        {
            if (!GetMenuSelection("Fan speeds", fanSpeeds, selection))
                break;

            // Set new fan speed
            if (PV_OK != pl_set_param(ctx->hcam, PARAM_FAN_SPEED_SETPOINT,
                        (void*)&selection))
            {
                PrintErrorMessage(pl_error_code(),
                        "pl_set_param(PARAM_FAN_SPEED_SETPOINT) error");
                errorOccurred = true;
                break;
            }
            for (size_t n = 0; n < fanSpeeds.size(); n++)
            {
                if (fanSpeeds[n].value == selection)
                {
                    printf("Fan speed on camera %d set to %s\n",
                            ctx->hcam, fanSpeeds[n].name.c_str());
                    break;
                }
            }
        }
    }

    // Request thread to abort
    ctx->threadAbortFlag = true;
    ctx->eofEvent.mutex.unlock();
    ctx->eofEvent.cond.notify_all();

    // Wait for thread to finish
    if (ctx->thread->joinable())
    {
        ctx->thread->join();
    }
    delete ctx->thread;
    ctx->thread = nullptr;

    CloseAllCamerasAndUninit(contexts);

    if (errorOccurred)
        return APP_EXIT_ERROR;
    return 0;
}

/******************************************************************************/
/* Copyright (C) Teledyne Photometrics. All rights reserved.                  */
/******************************************************************************/

// Local
#include "Common.h"

// This sample application demonstrates continuous acquisition in circular
// buffer mode with 8 exposures to show how camera loops through the list of the
// loaded values.
int main(int argc, char* argv[])
{
    if (!ShowAppInfo(argc, argv))
        return APP_EXIT_ERROR;

    std::vector<CameraContext*> contexts;
    if (!InitAndOpenOneCamera(contexts, cSingleCamIndex))
        return APP_EXIT_ERROR;

    CameraContext* ctx = contexts[cSingleCamIndex];

    if (!ctx->isSmartStreaming)
    {
        printf("Camera %d does not support Smart Streaming\n", ctx->hcam);
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }

    // Install generic ctr+c handler
    if (!InstallGenericCliTerminationHandler(contexts))
    {
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }

    // Register callback to receive a notification when EOF event arrives.
    // The handler function will be called by PVCAM, last parameter
    // might hold a pointer to user-specific content that will be passed with
    // the callback once it is invoked.
    if (PV_OK != pl_cam_register_callback_ex3(ctx->hcam, PL_CALLBACK_EOF,
                (void*)GenericEofHandler, ctx))
    {
        PrintErrorMessage(pl_error_code(), "pl_cam_register_callback() error");
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }
    printf("EOF callback handler registered on camera %d\n", ctx->hcam);

    // Here we tell the camera we want to load following exposure values.
    // If the camera supports less values, only first N values are uploaded.
    const uns32 smartStreamingValues[] = {10, 20, 30, 40, 50, 60, 70, 80};
    const uns32 smartStreamingValueCount =
        sizeof(smartStreamingValues) / sizeof(uns32);
    if (!UploadSmartStreamingExposures(ctx, smartStreamingValues,
                smartStreamingValueCount))
    {
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }

    uns32 exposureBytes;
    uns32 exposureTime = 40; // milliseconds

    const uns16 circBufferFrames = 20;
    const int16 bufferMode = CIRC_OVERWRITE;

    // Setup the acquisition
    //
    // WARNING:
    // If Smart Streaming mode is enabled the exposure time entered here is
    // ignored and the values from Smart Array are used, however, the value
    // entered here must not be 0.
    //
    // TIMED_MODE indicates this is software trigger mode and each acquisition
    // is after initial pl_exp_start_cont() call started internally from the
    // camera.
    // To run in hardware trigger mode use either STROBED_MODE, BULB_MODE or
    // TRIGGER_FIRST_MODE.
    if (PV_OK != pl_exp_setup_cont(ctx->hcam, 1, &ctx->region, TIMED_MODE,
                exposureTime, &exposureBytes, bufferMode))
    {
        PrintErrorMessage(pl_error_code(), "pl_exp_setup_cont() error");
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }
    printf("Acquisition setup successful on camera %d\n", ctx->hcam);
    UpdateCtxImageFormat(ctx);

    const uns32 circBufferBytes = circBufferFrames * exposureBytes;

    // Allocate memory for circular buffer of frames, size of one frame is
    // returned in exposureBytes by pl_exp_setup_cont()
    uns8* circBufferInMemory = new (std::nothrow) uns8[circBufferBytes];
    if (!circBufferInMemory)
    {
        printf("Unable to allocate buffer for camera %d\n", ctx->hcam);
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }

    // Start the continuous acquisition, again tell this function size of buffer
    // it has for the frames. Camera starts the first acquisition based on this
    // host command and subsequent acquisitions are triggered internally by the
    // camera itself.
    // In hardware trigger mode this sets the camera to waiting state awaiting
    // external trigger signals.
    if (PV_OK != pl_exp_start_cont(ctx->hcam, circBufferInMemory, circBufferBytes))
    {
        PrintErrorMessage(pl_error_code(), "pl_exp_start_cont() error");
        delete [] circBufferInMemory;
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }
    printf("Acquisition started on camera %d\n", ctx->hcam);

    // Loop acquiring 16 frames
    // Since Smart Array holds 8 different exposure times these will be applied
    // in circular manner, so after first 8 exposures are completed the next 8
    // follow starting again with 10ms exposure time.
    bool errorOccurred = false;
    uns32 framesAcquired = 0;
    while (framesAcquired < 16)
    {
        // Here we need to wait for a frame readout notification signaled by
        // event raised in the callback.
        printf("Waiting for EOF event to occur on camera %d\n", ctx->hcam);
        if (!WaitForEofEvent(ctx, 5000, errorOccurred))
            break;

        printf("Frame #%u has been delivered from camera %d, exposure time = %ums\n",
                framesAcquired + 1, ctx->hcam,
                smartStreamingValues[framesAcquired % smartStreamingValueCount]);
        ShowImage(ctx, ctx->eofFrame, exposureBytes);

        framesAcquired++;
    }

    // Once we have acquired the number of frames needed the acquisition can be
    // stopped, no other call is needed to stop the acquisition.
    if (PV_OK != pl_exp_abort(ctx->hcam, CCS_NO_CHANGE))
    {
        PrintErrorMessage(pl_error_code(), "pl_exp_abort() error");
    }
    else
    {
        printf("Acquisition stopped on camera %d\n", ctx->hcam);
    }

    delete [] circBufferInMemory;

    CloseAllCamerasAndUninit(contexts);

    if (errorOccurred)
        return APP_EXIT_ERROR;
    return 0;
}

/******************************************************************************/
/* Copyright (C) Teledyne Photometrics. All rights reserved.                  */
/******************************************************************************/
#pragma once
#ifndef COMMON_H_
#define COMMON_H_

// System
#include <chrono>
#include <condition_variable>
#include <functional>
#include <mutex>
#include <string>
#include <thread>
#include <vector>

// PVCAM
#include <master.h>
#include <pvcam.h>

// The code returned when application exits on error
#define APP_EXIT_ERROR 1

// Camera index used in single-camera code samples, zero-based
constexpr uns16 cSingleCamIndex = 0;
// Number of cameras used in multi-camera code samples
constexpr uns16 cMultiCamCount = 2;

// Custom value to recognize an invalid ID of post-processing parameter
constexpr uns32 cInvalidPpId = (uns32)-1;

/*
 * Common data types
 */

// Name-Value Pair type - an item in enumeration type
struct NVP
{
    int32 value{ 0 };
    std::string name{};
};
// Name-Value Pair Container type - an enumeration type
typedef std::vector<NVP> NVPC;

// The speed table is basically a tree. Each camera has one or more ports,
// this structure holds information with port descriptions. Each camera port has
// one or more speeds (readout frequencies). On most EM cameras there are two
// ports, one EM and one non-EM port with one or two speeds per port.
// On non-EM camera there is usually one port only with multiple speeds.
// Each speed has one or more gains.
struct SpdtabGain
{
    // In PVCAM gain indexes start from 1 for historical reasons
    int32 index{ 1 };
    // The gain name might not be reported by some camera.
    // In that case the name stays empty here.
    std::string name{};
    // Due to behavior changes in Prime BSI and its CMS gain, the bit depth
    // can be different for each gain.
    int16 bitDepth{ 0 };
};
struct SpdtabSpeed
{
    int32 index{ 0 };
    uns16 pixTimeNs{ 1 };
    // The bit depth was long time here, common for all gains.
    // But on Prime BSI camera the CMS gain has different bit depth than the
    // other HDR gain. So if your application is not generic and doesn't need
    // to support Prime BSI and its CMS gain, you can keep bitDepth here.
    // Otherwise use the one in SpdtabGain structure.
    //int16 bitDepth{ 0 };
    std::vector<SpdtabGain> gains;
};
struct SpdtabPort
{
    // Notice here the PARAM_READOUT_PORT is an ENUM_TYPE parameter so the
    // port in speed table doesn't have an index. Instead, it uses a value
    // associated with every enumeration item, which could be any number.
    // But so far the first item always starts on zero, second has value one,
    // and so on.
    int32 value{ 0 };
    std::string name{};
    std::vector<SpdtabSpeed> speeds;
};

// Platform-independent replacement for Windows event
struct Event
{
    // Mutex that guards all other members
    std::mutex mutex{};
    // Condition that any thread could wait on
    std::condition_variable cond{};
    // A flag that helps with spurious wakeups
    bool flag{ false };
};

// PVCAM Post Processing parameter descriptor
struct PvcamPpParameter
{
    int16 index{ -1 };
    uns32 id{ cInvalidPpId };
    std::string name{};
    uns32 minValue{ 0 };
    uns32 maxValue{ 0 };
    uns32 defValue{ 0 };
};

// PVCAM Post Processing feature descriptor
struct PvcamPpFeature
{
    int16 index{ -1 };
    uns32 id{ cInvalidPpId };
    std::string name{};
    std::vector<PvcamPpParameter> parameterList{};
};

// Structure holding camera-related properties, one instance per camera
struct CameraContext
{
    // Camera name, the only member valid before opening camera
    char camName[CAM_NAME_LEN]{ '\0' };

    // Set to true when PVCAM opens camera
    bool isCamOpen{ false };

    // All members below are initialized once the camera is successfully open

    // Camera handle
    int16 hcam{ -1 };

    // Camera serial resolution
    uns16 sensorResX{ 0 };
    // Camera parallel resolution
    uns16 sensorResY{ 0 };
    // Sensor region and binning factors to be used for the acquisition,
    // initialized to full sensor size and no binning after camera open
    rgn_type region{ 0, 0, 0, 0, 0, 0 };

    // Vector of camera readout options
    std::vector<SpdtabPort> speedTable{};

    // Image format reported after acq. setup, value from PL_IMAGE_FORMATS
    int32 imageFormat{ PL_IMAGE_FORMAT_MONO16 };
    // Sensor type (if not Frame Transfer then camera is most likely Interline)
    bool isFrameTransfer{ false };
    // Flag marking camera Smart Streaming capable
    bool isSmartStreaming{ false };

    // Event used for communication between acq. loop and EOF callback routine
    Event eofEvent{};
    // Storage for a code sample specific context data needed in callback
    void* eofContext{ nullptr };
    // Frame info structure used to store data e.g. in EOF callback handlers
    FRAME_INFO eofFrameInfo{};
    // The address of latest frame stored e.g. in EOF callback handlers
    void* eofFrame{ nullptr };

    // Either acquisition thread or used for other independent tasks
    std::thread* thread{ nullptr };
    // Flag to be set to abort thread (used e.g. in multi-camera code samples)
    bool threadAbortFlag{ false };
};

/*
 * Common function prototypes
 */

// Converts string to double precision float number
bool StrToDouble(const std::string& str, double& number);

// Converts string to unsigned integer number
bool StrToInt(const std::string& str, int& number);

// Returns true and a value of selected item assigned to NVP item.
// When user enters nothing or anything but not valid value, this function
// returns false.
bool GetMenuSelection(const std::string& title, const NVPC& menu, int32& selection);

// Reads a string from standard input until Enter/Return key is pressed.
// If pressed ctrl+c while waiting to user input, stream sets a fail flag,
// do not forget to check when used InstallCliTerminationHandler function.
std::string WaitForInput();

// Platform-agnostic version of Windows getch function
int GetCh();
// Platform-agnostic version of Windows getche function
int GetChE();

// Install custom termination handler that catches e.g. ctrl+c in terminal and
// does correct acquisition abort before the app is terminated.
// In most cases GenericCliTerminationHandler should be sufficient for us.
bool InstallCliTerminationHandler(std::function<void()> customHandler);

// Generic handler that sets threadAbortFlag in CameraContext for each open
// camera and uses eofEvent to wake up all possible waiters.
void GenericCliTerminationHandler(const std::vector<CameraContext*>& contexts);

// A helper function installing GenericCliTerminationHandler as a handler.
bool InstallGenericCliTerminationHandler(const std::vector<CameraContext*>& contexts);

// Displays application name and version
bool ShowAppInfo(int argc, char* argv[]);

// Retrieves last PVCAM error code and displays an error message
void PrintErrorMessage(int16 errorCode, const char* message);

// Releases allocated camera contexts and uninitializes PVCAM library
void UninitPVCAM(std::vector<CameraContext*>& contexts);

// Initializes PVCAM library and allocates camera context for all detected cameras
bool InitPVCAM(std::vector<CameraContext*>& contexts);

// Closes given camera if not closed yet
void CloseCamera(CameraContext* ctx);

// Opens given camera if not open yet
bool OpenCamera(CameraContext* ctx);

// Initializes PVCAM library, gets basic camera availability information,
// opens one camera and retrieves basic camera parameters and characteristics.
bool InitAndOpenOneCamera(std::vector<CameraContext*>& contexts, uns16 camIndex);

// Initializes PVCAM library, gets basic camera availability information,
// opens multiple cameras and retrieves basic camera parameters and characteristics.
// If there is less than camCount cameras available the camCount value is
// updated to number of cameras actually open.
bool InitAndOpenMultipleCameras(std::vector<CameraContext*>& contexts, uns16& camCount);

// Closes the camera and uninitializes PVCAM
void CloseAllCamerasAndUninit(std::vector<CameraContext*>& contexts);

// Generic EOF callback handler used in most code samples
// This is the function registered as callback function and PVCAM will call it
// when new frame arrives
void PV_DECL GenericEofHandler(FRAME_INFO* pFrameInfo, void* pContext);

// Checks parameter availability
bool IsParamAvailable(int16 hcam, uns32 paramID, const char* paramName);

// Reads name-value pairs for given PVCAM enumeration
bool ReadEnumeration(int16 hcam, NVPC* pNvpc, uns32 paramID, const char* paramName);

// Discovers all camera readout options
bool GetSpeedTable(const CameraContext* ctx, std::vector<SpdtabPort>& speedTable);

// After pl_exp_setup_* function call or after changing value of some
// post-processing parameters, the image format reported by camera could change.
// So far each pixel was transferred in 2 bytes, i.e. 16 bits per pixel.
// Recently added features could generate pixels of different size, for instance
// 32 bits per pixel or 8 bits per pixel.
// The actual bit depth, i.e. the number of bits holding real data, is still
// independent and reported by PARAM_BIT_DEPTH parameter.
void UpdateCtxImageFormat(CameraContext* ctx);

// Waits for a notification usually sent by EOF callback handler.
// Returns false if the event didn't occur before timeout or when user aborted it.
bool WaitForEofEvent(CameraContext* ctx, uns32 timeoutMs, bool& errorOccurred);

// "Shows" the image (prints pixel values, opens external image viewer, ...)
// The title is optional.
void ShowImage(const CameraContext* ctx, const void* pBuffer,
        uns32 bufferBytes, const char* title = nullptr);

// Saves the image pixels to a file.
// The image is saved as raw data, however it can be imported into ImageJ
// or other application that allows importing raw data.
// For ImageJ use drag & drop or File->Import->Raw and then specify usually
// 16-bit unsigned type, width & height and Little-endian byte order.
bool SaveImage(const void* pBuffer, uns32 bufferBytes, const char* path);

// Prints the Extended metadata to console output, this function is
// called from PrintMetaFrame() and PrintMetaRoi() as well
void PrintMetaExtMd(void* pMetaData, uns32 metaDataSize);

// Prints the ROI descriptor to console output, this function is
// called from PrintMetaFrame() as well
void PrintMetaRoi(const CameraContext* ctx, const md_frame_roi* pRoiDesc);

// Prints the frame descriptor to console output including ROIs
// and Extended metadata structures.
// If printAllRois is false only first ROI is printed out.
void PrintMetaFrame(const CameraContext* ctx, const md_frame* pFrameDesc,
        bool printAllRois);

// Reads number from console window
int ConsoleReadNumber(int min, int max, int def);

// If Smart Streaming is supported enable it and load given exposures to camera.
bool UploadSmartStreamingExposures(const CameraContext* ctx,
        const uns32* pExposures, uns16 exposuresCount);

// This function discovers all camera post processing features and stores them
// into a list of locally cached descriptors.
std::vector<PvcamPpFeature> DiscoverCameraPostProcessing(CameraContext* ctx);

// This function iterates over the cached PP feature list and returns
// feature index for given feature ID.
bool FindPpFeatureIndex(const std::vector<PvcamPpFeature>& ppFeatureList,
        uns32 ppFeatureId, int16& featIdx);

// This function iterates over the cached PP parameter list and returns
// parameter index for given parameter ID.
bool FindPpParamIndex(const std::vector<PvcamPpParameter>& ppParameterList,
        uns32 ppParamId, int16& paramIdx);

// This function iterates over the cached PP feature list and returns
// feature and parameter indexes for given parameter ID.
bool FindPpParamIndexes(const std::vector<PvcamPpFeature>& ppFeatureList,
        uns32 ppParamId, int16& featIdx, int16& paramIdx);

// Gets the current value of PP parameter under given feature
bool GetPpParamValue(CameraContext* ctx, int16 featIdx, int16 paramIdx, uns32& value);

// Sets the new value of PP parameter under given feature
bool SetPpParamValue(CameraContext* ctx, int16 featIdx, int16 paramIdx, uns32 value);

#endif // COMMON_H_

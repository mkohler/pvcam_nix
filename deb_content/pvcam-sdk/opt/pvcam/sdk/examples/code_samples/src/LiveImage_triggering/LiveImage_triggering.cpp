/******************************************************************************/
/* Copyright (C) Teledyne Photometrics. All rights reserved.                  */
/******************************************************************************/

// Local
#include "Common.h"

// This sample application demonstrates how to run the camera in continuous
// circular buffer mode. The camera is triggered by external hardware signal
// (Strobed mode) and extended frame information is retrieved with
// pl_exp_get_latest_frame_ex() function.
int main(int argc, char* argv[])
{
    if (!ShowAppInfo(argc, argv))
        return APP_EXIT_ERROR;

    std::vector<CameraContext*> contexts;
    if (!InitAndOpenOneCamera(contexts, cSingleCamIndex))
        return APP_EXIT_ERROR;

    CameraContext* ctx = contexts[cSingleCamIndex];

    // Install generic ctr+c handler
    if (!InstallGenericCliTerminationHandler(contexts))
    {
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }

    // Register callback to receive a notification when EOF event arrives.
    // The handler function will be called by PVCAM, last parameter
    // might hold a pointer to user-specific content that will be passed with
    // the callback once it is invoked.
    if (PV_OK != pl_cam_register_callback_ex3(ctx->hcam, PL_CALLBACK_EOF,
                (void*)GenericEofHandler, ctx))
    {
        PrintErrorMessage(pl_error_code(), "pl_cam_register_callback() error");
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }
    printf("EOF callback handler registered on camera %d\n", ctx->hcam);

    uns32 exposureBytes;
    const uns32 exposureTime = 5; // milliseconds

    const uns16 circBufferFrames = 20;
    const int16 bufferMode = CIRC_OVERWRITE;

    // Setup the acquisition.
    // STROBED_MODE in the function parameters indicates camera will be exposing
    // every frame with triggers on external signal. If you wish to run the
    // camera in software triggered mode (internal camera triggering in case of
    // circular buffer) replace the STROBED_MODE below by TIMED_MODE.
    if (PV_OK != pl_exp_setup_cont(ctx->hcam, 1, &ctx->region, STROBED_MODE,
                exposureTime, &exposureBytes, bufferMode))
    {
        PrintErrorMessage(pl_error_code(), "pl_exp_setup_cont() error");
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }
    printf("Acquisition setup successful on camera %d\n", ctx->hcam);
    UpdateCtxImageFormat(ctx);

    const uns32 circBufferBytes = circBufferFrames * exposureBytes;

    // Allocate memory for circular buffer of frames, size of one frame is
    // returned in exposureBytes by pl_exp_setup_cont()
    uns8* circBufferInMemory = new (std::nothrow) uns8[circBufferBytes];
    if (!circBufferInMemory)
    {
        printf("Unable to allocate buffer for camera %d\n", ctx->hcam);
        CloseAllCamerasAndUninit(contexts);
        return APP_EXIT_ERROR;
    }

    // Start the continuous acquisition, again tell this function size of buffer
    // it has for the frames. In hardware trigger mode this sets the camera to
    // waiting state awaiting external trigger signals.
    if (PV_OK != pl_exp_start_cont(ctx->hcam, circBufferInMemory, circBufferBytes))
    {
        PrintErrorMessage(pl_error_code(), "pl_exp_start_cont() error");
        CloseAllCamerasAndUninit(contexts);
        delete [] circBufferInMemory;
        return APP_EXIT_ERROR;
    }
    printf("Acquisition started on camera %d\n", ctx->hcam);

    bool errorOccurred = false;
    uns32 framesAcquired = 0;
    while (framesAcquired < 50)
    {
        // Here we need to wait for a frame readout notification signaled by
        // event raised in the callback.
        printf("Waiting for EOF event to occur on camera %d\n", ctx->hcam);
        if (!WaitForEofEvent(ctx, 5000, errorOccurred))
            break;

        // Get the address of the latest frame in the circular buffer
        void* frameAddress;
        FRAME_INFO latestFrameInfo;
        if (PV_OK != pl_exp_get_latest_frame_ex(ctx->hcam, &frameAddress,
                    &latestFrameInfo))
        {
            PrintErrorMessage(pl_error_code(),
                    "pl_exp_get_latest_frame_ex() error");
            errorOccurred = true;
            break;
        }

        // Timestamp is in hundreds of microseconds
        printf("Frame #%u acquired (# from internal counter)\n",
                framesAcquired + 1);
        printf("Frame #%d acquired (# from frame info), timestamp = %lldus\n",
                latestFrameInfo.FrameNr, 100 * latestFrameInfo.TimeStamp);
        if (latestFrameInfo.FrameNr != ctx->eofFrameInfo.FrameNr)
        {
            // Frame rate is too high or our processing is too slow
            printf("  Frame number from callback handler (%d) differs from "
                    "number returned for latest frame (%d)!\n",
                    ctx->eofFrameInfo.FrameNr, latestFrameInfo.FrameNr);
        }
        if (frameAddress != ctx->eofFrame)
        {
            // Frame rate is too high or our processing is too slow
            printf("  Frame address from callback handler differs from "
                    "address returned for latest frame!\n");
        }
        ShowImage(ctx, frameAddress, exposureBytes);

        framesAcquired++;
    }

    // Once we have acquired the number of frames needed the acquisition can be
    // stopped, no other call is needed to stop the acquisition.
    if (PV_OK != pl_exp_abort(ctx->hcam, CCS_NO_CHANGE))
    {
        PrintErrorMessage(pl_error_code(), "pl_exp_abort() error");
    }
    else
    {
        printf("Acquisition stopped on camera %d\n", ctx->hcam);
    }

    delete [] circBufferInMemory;

    CloseAllCamerasAndUninit(contexts);

    if (errorOccurred)
        return APP_EXIT_ERROR;
    return 0;
}

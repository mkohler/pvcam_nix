/******************************************************************************/
/* Copyright (C) Teledyne Photometrics. All rights reserved.                  */
/******************************************************************************/

/* Local */
#include "common.h"

/* Helper */
#include <pvcam_helper_color.h>

/* System */
#include <stdio.h>
#include <sstream>
#include <string>

/*
This sample application captures a frame and then debayers the frame using the
various debayer algorithms.
*/
int main(int argc, char* argv[])
{
    int retVal = EXIT_FAILURE;
    uns16 verMajor;
    uns16 verMinor;
    uns16 verBuild;

    int32 err;
    char errMsg[PH_COLOR_MAX_ERROR_LEN];
    uns32 errMsgSize;

    if (!ShowAppInfo(argc, argv))
    {
        return EXIT_FAILURE;
    }

    if (!InitAndOpenFirstCamera())
    {
        return EXIT_FAILURE;
    }

    err = ph_color_get_lib_version(&verMajor, &verMinor, &verBuild);
    if (err != PH_COLOR_ERROR_NONE)
    {
        errMsgSize = PH_COLOR_MAX_ERROR_LEN;
        ph_color_get_last_error_message(errMsg, &errMsgSize);
        printf("* Could not get color helper library version (%s)\n", errMsg);
        CloseCameraAndUninit();
        return EXIT_FAILURE;
    }
    printf("Using pvcam_helper_color version %u.%u.%u\n", verMajor, verMinor,
            verBuild);

    ph_color_context* colorCtx = nullptr;

    err = ph_color_context_create(&colorCtx);
    if (err != PH_COLOR_ERROR_NONE)
    {
        errMsgSize = PH_COLOR_MAX_ERROR_LEN;
        ph_color_get_last_error_message(errMsg, &errMsgSize);
        printf("* Could not create context for color library (%s)\n", errMsg);
        CloseCameraAndUninit();
        return EXIT_FAILURE;
    }

    colorCtx->algorithm = PH_COLOR_DEBAYER_ALG_NEAREST;
    colorCtx->pattern   = g_SensorColorMode;
    colorCtx->bitDepth  = g_SensorBitDepth;
    colorCtx->rgbFormat = PH_COLOR_RGB_FORMAT_RGB48;
    colorCtx->autoExpAlgorithm = PH_COLOR_AUTOEXP_ALG_AVERAGE;
    colorCtx->sensorWidth = g_SensorResX;
    colorCtx->sensorHeight = g_SensorResY;
    colorCtx->forceCpu = FALSE;

    err = ph_color_context_apply_changes(colorCtx);
    if (err != PH_COLOR_ERROR_NONE)
    {
        errMsgSize = PH_COLOR_MAX_ERROR_LEN;
        ph_color_get_last_error_message(errMsg, &errMsgSize);
        printf("* Could not apply new settings to color library (%s)\n", errMsg);
        CloseCameraAndUninit();
        return EXIT_FAILURE;
    }

    // Camera settings
    uns32 exposureTime = 50; // milliseconds

    // Currently saving file name
    std::ostringstream fname;

    printf("\nTest white balance of full frame size image\n"
            "--------------------------------------------\n");

    // Set the CCD g_Region to Full Frame, serial and parallel binning is set to 1
    g_Region.s1 = 0;
    g_Region.s2 = g_SensorResX - 1;
    g_Region.sbin = 1;
    g_Region.p1 = 0;
    g_Region.p2 = g_SensorResY - 1;
    g_Region.pbin = 1;

    const uns16 w = g_Region.s2 - g_Region.s1 + 1; // width of ROI
    const uns16 h = g_Region.p2 - g_Region.p1 + 1; // height of ROI

    const uns16 xeo = g_Region.s1 % 2; // x coord of ROI starts on even/odd pixel
    const uns16 yeo = g_Region.p1 % 2; // y coord of ROI starts on even/odd pixel

    // Setup acquisition once to get the full frame size (exp. time 0)
    uns32 fullFrameBytes;
    if (PV_OK != pl_exp_setup_seq(g_hCam, 1, 1, &g_Region, TIMED_MODE, 0,
                &fullFrameBytes))
    {
        PrintErrorMessage(pl_error_code(), "pl_exp_setup_seq() error");
        CloseCameraAndUninit();
        return EXIT_FAILURE;
    }

    // Raw image from sensor size in bytes
    // Note: Frame size can be greater than w * h * sizeof(uns16) !!!
    //       Always use the size returned by pl_exp_setup_* call
    const uns32 rawFrameBytes = fullFrameBytes;
    // RGB image size in bytes
    const uns32 rgbFrameBytes = 3 * rawFrameBytes;

    void* rawFrame = nullptr;
    void* rgbFrame = nullptr;
    void* rgbFrame16b = nullptr;

    auto SaveRgbImage = [&](void* buffer, const char* name) -> rs_bool
    {
        // Save the non-balanced image
        if (!SaveBinaryFile(buffer, rgbFrameBytes, name))
        {
            printf("* Error saving binary file\n");
            return PV_FAIL;
        }
        // The buffer is expected with big endian encoding for PPM and PAM
        if (!SwapEndianness(buffer, rgbFrameBytes))
        {
            printf("* Error swapping endianness\n");
            return PV_FAIL;
        }
        if (!SavePpmFile(buffer, w, h, 48, name))
        {
            printf("* Error saving PPM file\n");
            return PV_FAIL;
        }
        if (!SavePamFile(buffer, w, h, 48, name))
        {
            printf("* Error saving PAM file\n");
            return PV_FAIL;
        }
        // Swap endianness back, the same buffer data is used later on
        if (!SwapEndianness(buffer, rgbFrameBytes))
        {
            printf("* Error swapping endianness\n");
            return PV_FAIL;
        }
        return PV_OK;
    };

    auto ConvertTo16BitsAndSave = [&](const char* name) -> rs_bool
    {
        // Up-scale
        err = ph_color_convert_format(colorCtx, g_Region,
                rgbFrame, colorCtx->rgbFormat, colorCtx->bitDepth,
                rgbFrame16b, colorCtx->rgbFormat, 16);
        if (err != PH_COLOR_ERROR_NONE)
        {
            errMsgSize = PH_COLOR_MAX_ERROR_LEN;
            ph_color_get_last_error_message(errMsg, &errMsgSize);
            printf("* Could not white balance image (%s)\n", errMsg);
            return PV_FAIL;
        }

        // Save the up-scaled RGB image
        if (!SaveRgbImage(rgbFrame16b, name))
        {
            return PV_FAIL;
        }
        return PV_OK;
    };

    // Loop for one iteration only - simplifies error handling a lot
    for (;;)
    {
        // Allocate frame memory
        err = ph_color_buffer_alloc(&rawFrame, rawFrameBytes);
        if (err != PH_COLOR_ERROR_NONE || !rawFrame)
        {
            errMsgSize = PH_COLOR_MAX_ERROR_LEN;
            ph_color_get_last_error_message(errMsg, &errMsgSize);
            printf("* Unable to allocate memory for a frame (%s)\n", errMsg);
            break;
        }

        // Allocate corresponding RGB frame 3 times as large
        err = ph_color_buffer_alloc(&rgbFrame, rgbFrameBytes);
        if (err != PH_COLOR_ERROR_NONE || !rgbFrame)
        {
            errMsgSize = PH_COLOR_MAX_ERROR_LEN;
            ph_color_get_last_error_message(errMsg, &errMsgSize);
            printf("* Unable to allocate memory for a RGB frame (%s)\n", errMsg);
            break;
        }

        // Allocate another RGB frame for up-scaled image
        err = ph_color_buffer_alloc(&rgbFrame16b, rgbFrameBytes);
        if (err != PH_COLOR_ERROR_NONE || !rgbFrame16b)
        {
            errMsgSize = PH_COLOR_MAX_ERROR_LEN;
            ph_color_get_last_error_message(errMsg, &errMsgSize);
            printf("* Unable to allocate memory for second RGB frame (%s)\n",
                    errMsg);
            break;
        }

        printf("Test %ux%u full frame size conversion...\n", w, h);
        printf("\n");
        printf("Frame coord: [%u,%u] - [%u,%u] = %u x %u = %u pixels\n",
                g_Region.s1, g_Region.p1, g_Region.s2, g_Region.p2, w, h, w * h);
        printf("Raw frame size: %u bytes\n", rawFrameBytes);
        printf("RGB frame size: %u bytes\n", rgbFrameBytes);


        // Grab image (1), a raw mono frame, and save it

        // Acquire a raw frame
        printf("\n");
        if (!GrabFrame(exposureTime, g_Region, rawFrame))
        {
            printf("* Could not grab frame\n");
            break;
        }
        // Save the raw image buffer
        fname.str(std::string());
        fname << "img1_" << w << "x" << h << "_16bpp_RAW_" << xeo << yeo;
        if (!SaveBinaryFile(rawFrame, rawFrameBytes, fname.str().c_str()))
        {
            printf("* Error saving binary file\n");
            break;
        }
        // Debayer the color image
        err = ph_color_debayer(colorCtx, rawFrame, g_Region, rgbFrame);
        if (err != PH_COLOR_ERROR_NONE)
        {
            errMsgSize = PH_COLOR_MAX_ERROR_LEN;
            ph_color_get_last_error_message(errMsg, &errMsgSize);
            printf("* Could not debayer image (%s)\n", errMsg);
            break;
        }
        // Save the non-balanced RGB image
        fname.str(std::string());
        fname << "img1_" << w << "x" << h << "_48bpp_RGB_" << xeo << yeo;
        if (!SaveRgbImage(rgbFrame, fname.str().c_str()))
        {
            break;
        }
        // Up-scale and save
        fname.str(std::string());
        fname << "img1_" << w << "x" << h << "_48bpp_RGB-16b_" << xeo << yeo;
        if (!ConvertTo16BitsAndSave(fname.str().c_str()))
        {
            break;
        }


        // Here's where the auto exposure and auto white balance is done

        // Choose an ROI where it is white/gray for balancing
        // Used centered quarter of the image
        rgn_type roi;
        roi.s1 = w / 4;
        roi.s2 = w - roi.s1 - 1;
        roi.sbin = 1;
        roi.p1 = h / 4;
        roi.p2 = h - roi.p1 - 1;
        roi.pbin = 1;

        const uns16 roiW = roi.s2 - roi.s1 + 1; // width of ROI
        const uns16 roiH = roi.p2 - roi.p1 + 1; // height of ROI

        uns32 autoExpTime;

        // Automatically find the exposure and scale factors for white balance
        printf("\n");
        printf("Determining exposure for white balance...\n");
        printf("ROI coord: [%u,%u] - [%u,%u] = %u x %u = %u pixels\n",
                roi.s1, roi.p1, roi.s2, roi.p2, roiW, roiH, roiW * roiH);

        err = ph_color_auto_exposure_and_white_balance(colorCtx, g_hCam, roi,
                &autoExpTime, &colorCtx->redScale, &colorCtx->greenScale,
                &colorCtx->blueScale);
        if (err != PH_COLOR_ERROR_NONE)
        {
            errMsgSize = PH_COLOR_MAX_ERROR_LEN;
            ph_color_get_last_error_message(errMsg, &errMsgSize);
            printf("* Error from ph_color_auto_exposure_and_white_balance (%s)\n",
                    errMsg);
            break;
        }
        printf("Optimal exposure time = %u\n", autoExpTime);
        printf("Red scale factor   = %f\n", colorCtx->redScale);
        printf("Green scale factor = %f\n", colorCtx->greenScale);
        printf("Blue scale factor  = %f\n", colorCtx->blueScale);
        printf("... exposure for white balance done.\n");

        // Scale factors are set in context, let all color functions use them
        err = ph_color_context_apply_changes(colorCtx);
        if (err != PH_COLOR_ERROR_NONE)
        {
            errMsgSize = PH_COLOR_MAX_ERROR_LEN;
            ph_color_get_last_error_message(errMsg, &errMsgSize);
            printf("* Could not apply new settings to color library (%s)\n",
                    errMsg);
            CloseCameraAndUninit();
            return EXIT_FAILURE;
        }


        // White balance previously captured image (1) with new scale factors
        err = ph_color_white_balance(colorCtx, rgbFrame, g_Region);
        if (err != PH_COLOR_ERROR_NONE)
        {
            errMsgSize = PH_COLOR_MAX_ERROR_LEN;
            ph_color_get_last_error_message(errMsg, &errMsgSize);
            printf("* Could not white balance image (%s)\n", errMsg);
            break;
        }
        // Save the White balanced RGB image
        fname.str(std::string());
        fname << "img1_" << w << "x" << h << "_48bpp_RGB-WB_" << xeo << yeo;
        if (!SaveRgbImage(rgbFrame, fname.str().c_str()))
        {
            break;
        }
        // Up-scale and save
        fname.str(std::string());
        fname << "img1_" << w << "x" << h << "_48bpp_RGB-WB-16b_" << xeo << yeo;
        if (!ConvertTo16BitsAndSave(fname.str().c_str()))
        {
            break;
        }


        // Get another image (2), debayer and use the same white balance scaling

        // Acquire a raw frame with optimal exposure time
        printf("\n");
        if (!GrabFrame(autoExpTime, g_Region, rawFrame))
        {
            printf("* Could not grab frame\n");
            break;
        }
        // Save the raw image buffer
        fname.str(std::string());
        fname << "img2_" << w << "x" << h << "_16bpp_RAW_" << xeo << yeo;
        if (!SaveBinaryFile(rawFrame, rawFrameBytes, fname.str().c_str()))
        {
            printf("* Error saving binary file\n");
            break;
        }
        // Convert frame to RGB
        err = ph_color_debayer(colorCtx, rawFrame, g_Region, rgbFrame);
        if (err != PH_COLOR_ERROR_NONE)
        {
            errMsgSize = PH_COLOR_MAX_ERROR_LEN;
            ph_color_get_last_error_message(errMsg, &errMsgSize);
            printf("* Could not debayer image (%s)\n", errMsg);
            break;
        }
        // Save the non-balanced RGB image
        fname.str(std::string());
        fname << "img2_" << w << "x" << h << "_48bpp_RGB_" << xeo << yeo;
        if (!SaveRgbImage(rgbFrame, fname.str().c_str()))
        {
            break;
        }
        // Up-scale and save
        fname.str(std::string());
        fname << "img2_" << w << "x" << h << "_48bpp_RGB-16b_" << xeo << yeo;
        if (!ConvertTo16BitsAndSave(fname.str().c_str()))
        {
            break;
        }
        // White balance it
        err = ph_color_white_balance(colorCtx, rgbFrame, g_Region);
        if (err != PH_COLOR_ERROR_NONE)
        {
            errMsgSize = PH_COLOR_MAX_ERROR_LEN;
            ph_color_get_last_error_message(errMsg, &errMsgSize);
            printf("* Could not white balance image (%s)\n", errMsg);
            break;
        }
        // Save the white balanced RGB image
        fname.str(std::string());
        fname << "img2_" << w << "x" << h << "_48bpp_RGB-WB_" << xeo << yeo;
        if (!SaveRgbImage(rgbFrame, fname.str().c_str()))
        {
            break;
        }
        // Up-scale and save
        fname.str(std::string());
        fname << "img2_" << w << "x" << h << "_48bpp_RGB-WB-16b_" << xeo << yeo;
        if (!ConvertTo16BitsAndSave(fname.str().c_str()))
        {
            break;
        }

        retVal = EXIT_SUCCESS;
        break;
    }

    ph_color_buffer_free(&rawFrame);
    ph_color_buffer_free(&rgbFrame);
    ph_color_buffer_free(&rgbFrame16b);

    ph_color_context_release(&colorCtx);
    CloseCameraAndUninit();

    return retVal;
}

/******************************************************************************/
/* Copyright (C) Teledyne Photometrics. All rights reserved.                  */
/******************************************************************************/
#pragma once
#ifndef COMMON_H_
#define COMMON_H_

/* PVCAM */
#include <master.h>
#include <pvcam.h>

/* System */
#include <string>
#include <vector>

/*
 * Common data types
 */

/* Name-Value Pair type - an item in enumeration type */
typedef struct NVP
{
    int32 value;
    std::string name;
}
NVP;
/* Name-Value Pair Container type - an enumeration type */
typedef std::vector<NVP> NVPC;

/* Each camera has one or more ports, this structure holds information with port
descriptions. Each camera port has one or more speeds (readout frequencies).
On most EM cameras there are two ports - one EM and one non-EM port with one or
two speeds per port.
On non-EM camera there is usually one port only with multiple speeds. */
typedef struct READOUT_OPTION
{
    NVP port;
    int16 speedIndex;
    float readoutFrequency;
    int16 bitDepth;
    std::vector<int16> gains;
}
READOUT_OPTION;

/*
 * Declaration of common global variables
 */

/* Vector of camera readout options */
extern std::vector<READOUT_OPTION> g_SpeedTable;

/* Camera handle */
extern int16 g_hCam;

/* Number of cameras */
extern int16 g_NrOfCameras;

/* Camera serial and parallel resolution */
extern uns16 g_SensorResX;
extern uns16 g_SensorResY;

/* Camera current sensor bit depth after setting speed */
extern int16 g_SensorBitDepth;

/* Camera color mode */
extern uns32 g_SensorColorMode;

/* Camera name */
extern char g_Camera0_Name[CAM_NAME_LEN];

/* Sensor type (if not Frame Transfer then camera is most likely Interline) */
extern rs_bool g_IsFrameTransfer;

/* Sensor region to be used for the acquisition */
extern rgn_type g_Region;

/* Frame info structure used to store data in EOF callbacks */
extern FRAME_INFO* g_pFrameInfo;

/*
 * Common function prototypes
 */

/* Reads a string from standard input and return first character or 0 */
char WaitForKey();

/* Displays application name and version */
rs_bool ShowAppInfo(int argc, char* argv[]);

/* Retrieves last PVCAM error code and displays an error message */
void PrintErrorMessage(int16 errorCode, const char* message);

/* Initializes PVCAM library, gets basic camera availability information,
opens the camera and retrieves basic camera parameters and characteristics */
rs_bool InitAndOpenFirstCamera();

/* Closes the camera and uninitializes PVCAM */
void CloseCameraAndUninit();

/* Checks parameter availability */
rs_bool IsParamAvailable(uns32 paramID, const char* paramName);

/* Reads name-value pairs for given PVCAM enumeration */
rs_bool ReadEnumeration(NVPC* nvpc, uns32 paramID, const char* paramName);

/* Grab a single full frame from the currently open camera */
rs_bool GrabFrame(uns32 expTime, rgn_type rgn, void* pFrame);

/* Save the binary image data in a buffer as sequence of bytes, no header */
rs_bool SaveBinaryFile(const void* pFrame, uns32 len, const char* fileName);

/* Save color binary data as Portable Pixel Map (PPM) file format.
Header format is in ASCII then immediately follows binary data. This function
only handles 8 and 16 bits/pixel/channel (color): 24bppRGB, 48bppRGB.
See http://netpbm.sourceforge.net/doc/ppm.html for details.
This format can easily be opend by image viewers like IrfanView that use the
.ppm file extension.
The pFrame buffer is expected with big endian encoding, PVCAM uses little
endian! */
rs_bool SavePpmFile(const void* pFrame, uns32 width, uns32 height, uns16 bpp,
        const char* fileName);

/* Save color binary data as Portable Arbitrary Map (PAM) file format.
Header format is in ASCII then immediately follows binary data. This function
only handles 8 and 16 bits/pixel/channel (color): 24bppRGB, 48bppRGB
See http://netpbm.sourceforge.net/doc/pam.html for details.
This format can be openen by a image viewer like ImageMagick Diplay:
IMDisplay.exe.
The pFrame buffer is expected with big endian encoding, PVCAM uses little
endian! */
rs_bool SavePamFile(const void* pFrame, uns32 width, uns32 height, uns16 bpp,
        const char* fileName);

/* Swap higher and lower bytes for each pixel in place for 16 bit images. */
rs_bool SwapEndianness(void* pFrame, uns32 len);

#endif // COMMON_H_

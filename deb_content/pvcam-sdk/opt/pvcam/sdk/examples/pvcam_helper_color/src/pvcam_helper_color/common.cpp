/******************************************************************************/
/* Copyright (C) Teledyne Photometrics. All rights reserved.                  */
/******************************************************************************/
#include "common.h"

/* Local */
#include "version.h"

/* System */
#include <chrono>
#include <fstream>
#include <iostream>
#include <thread>

/* Public Global Variable Definitions */
std::vector<READOUT_OPTION> g_SpeedTable;
int16 g_hCam = -1;
int16 g_NrOfCameras = 0;
uns16 g_SensorResX = 0;
uns16 g_SensorResY = 0;
int16 g_SensorBitDepth = 0;
uns32 g_SensorColorMode = COLOR_NONE;
char g_Camera0_Name[CAM_NAME_LEN] = "";
rs_bool g_IsFrameTransfer = FALSE;
rgn_type g_Region = { 0, 0, 0, 0, 0, 0 };
FRAME_INFO* g_pFrameInfo = NULL;

/* Private Module local variables */
static rs_bool s_IsPvcamInitialized = FALSE;
static rs_bool s_IsCameraOpen = FALSE;

/* Common function implementations */

char WaitForKey()
{
    std::string str;
    std::cin.clear();
    getline(std::cin, str);
    return (str.empty()) ? '\0' : str[0];
}

rs_bool ShowAppInfo(int argc, char* argv[])
{
    const char* appName = "<unable to get name>";
    if (argc > 0 && argv != NULL && argv[0] != NULL)
    {
        appName = argv[0];
    }

    // Read PVCAM library version
    uns16 pvcamVersion;
    if (PV_OK != pl_pvcam_get_ver(&pvcamVersion))
    {
        PrintErrorMessage(pl_error_code(), "pl_pvcam_get_ver() error");
        return PV_FAIL;
    }

    printf("************************************************************\n");
    printf("Application  : %s\n", appName);
    printf("App. version : %d.%d.%d\n",
            VERSION_MAJOR,
            VERSION_MINOR,
            VERSION_BUILD);
    printf("PVCAM version: %d.%d.%d\n",
            (pvcamVersion >> 8) & 0xFF,
            (pvcamVersion >> 4) & 0x0F,
            (pvcamVersion >> 0) & 0x0F);
    printf("************************************************************\n\n");

    return PV_OK;
}

void PrintErrorMessage(int16 errorCode, const char* message)
{
    char pvcamErrMsg[ERROR_MSG_LEN];
    pl_error_message(errorCode, pvcamErrMsg);
    printf("%s\nError code: %d\nError message: %s\n", message, errorCode,
            pvcamErrMsg);
}

rs_bool InitPVCAM()
{
    printf("Initializing PVCAM and checking for cameras...\n");

    // Initialize PVCAM library
    if (PV_OK != pl_pvcam_init())
    {
        PrintErrorMessage(pl_error_code(), "pl_pvcam_init() error");
        return PV_FAIL;
    }
    s_IsPvcamInitialized = TRUE;
    printf("PVCAM initialized\n");

    // Read number of cameras in the system.
    // This will return total number of PVCAM cameras regardless of interface.
    if (PV_OK != pl_cam_get_total(&g_NrOfCameras))
    {
        PrintErrorMessage(pl_error_code(), "pl_cam_get_total() error");
        return PV_FAIL;
    }

    // Exit if no cameras have been found
    if (g_NrOfCameras == 0)
    {
        printf("No cameras found in the system\n");
        return PV_FAIL;
    }
    printf("Number of cameras found: %d\n", g_NrOfCameras);

    // Get PVCAM-name of camera 0 (can be modified via RSConfig)
    if (PV_OK != pl_cam_get_name(0, g_Camera0_Name))
    {
        PrintErrorMessage(pl_error_code(), "pl_cam_get_name() error");
        return PV_FAIL;
    }
    printf("Camera 0 name: %s\n", g_Camera0_Name);

    return PV_OK;
}

rs_bool OpenCamera()
{
    printf("Opening the camera and reading parameters...\n");

    // Open camera with the specified camera name obtained in InitPVCAM() function
    if (PV_OK != pl_cam_open(g_Camera0_Name, &g_hCam, OPEN_EXCLUSIVE))
    {
        PrintErrorMessage(pl_error_code(), "pl_cam_open() error");
        return PV_FAIL;
    }
    s_IsCameraOpen = TRUE;
    printf("Camera %s opened\n", g_Camera0_Name);

    // Read the version of Device Driver
    if (!IsParamAvailable(PARAM_DD_VERSION, "PARAM_DD_VERSION"))
    {
        return PV_FAIL;
    }
    uns16 ddVersion;
    if (PV_OK != pl_get_param(g_hCam, PARAM_DD_VERSION, ATTR_CURRENT,
                (void*)&ddVersion))
    {
        PrintErrorMessage(pl_error_code(),
                "pl_get_param(PARAM_DD_VERSION) error");
        return PV_FAIL;
    }
    printf("Device driver version: %d.%d.%d\n",
            (ddVersion >> 8) & 0xFF,
            (ddVersion >> 4) & 0x0F,
            (ddVersion >> 0) & 0x0F);

    // Get camera chip name string. Typically holds both chip and camera model
    // name, therefore is the best camera identifier for most models
    if (!IsParamAvailable(PARAM_CHIP_NAME, "PARAM_CHIP_NAME"))
    {
        return PV_FAIL;
    }
    char chipName[CCD_NAME_LEN];
    if (PV_OK != pl_get_param(g_hCam, PARAM_CHIP_NAME, ATTR_CURRENT,
                (void*)chipName))
    {
        PrintErrorMessage(pl_error_code(),
                "pl_get_param(PARAM_CHIP_NAME) error");
        return PV_FAIL;
    }
    printf("Sensor chip name: %s\n", chipName);

    // Get camera firmware version
    if (!IsParamAvailable(PARAM_CAM_FW_VERSION, "PARAM_CAM_FW_VERSION"))
    {
        return PV_FAIL;
    }
    uns16 fwVersion;
    if (PV_OK != pl_get_param(g_hCam, PARAM_CAM_FW_VERSION, ATTR_CURRENT,
                (void*)&fwVersion))
    {
        PrintErrorMessage(pl_error_code(),
                "pl_get_param(PARAM_CAM_FW_VERSION) error");
        return PV_FAIL;
    }
    printf("Camera firmware version: %d.%d\n",
            (fwVersion >> 8) & 0xFF,
            (fwVersion >> 0) & 0xFF);

    // Find out if the sensor is a frame transfer or other (typically interline)
    // type. This is a two-step process.
    // Please, follow the procedure below in your applications.
    if (PV_OK != pl_get_param(g_hCam, PARAM_FRAME_CAPABLE, ATTR_AVAIL,
                (void*)&g_IsFrameTransfer))
    {
        g_IsFrameTransfer = 0;
        PrintErrorMessage(pl_error_code(),
                "pl_get_param(PARAM_FRAME_CAPABLE) error");
        return PV_FAIL;
    }

    if (g_IsFrameTransfer == TRUE)
    {
        if (PV_OK != pl_get_param(g_hCam, PARAM_FRAME_CAPABLE, ATTR_CURRENT,
                    (void*)&g_IsFrameTransfer))
        {
            g_IsFrameTransfer = 0;
            PrintErrorMessage(pl_error_code(),
                    "pl_get_param(PARAM_FRAME_CAPABLE) error");
            return PV_FAIL;
        }
        if (g_IsFrameTransfer == TRUE)
        {
            printf("Camera with Frame Transfer capability sensor\n");
        }
    }
    if (g_IsFrameTransfer == FALSE)
    {
        g_IsFrameTransfer = 0;
        printf("Camera without Frame Transfer capability sensor\n");
    }

    // If this is a Frame Transfer sensor set PARAM_PMODE to PMODE_FT.
    // The other common mode for these sensors is PMODE_ALT_FT.
    if (!IsParamAvailable(PARAM_PMODE, "PARAM_PMODE"))
    {
        return PV_FAIL;
    }
    if (g_IsFrameTransfer == TRUE)
    {
        int32 PMode = PMODE_FT;
        if (PV_OK != pl_set_param(g_hCam, PARAM_PMODE, (void*)&PMode))
        {
            PrintErrorMessage(pl_error_code(),
                    "pl_set_param(PARAM_PMODE) error");
            return PV_FAIL;
        }
    }
    // If not a Frame Transfer sensor (i.e. Interline), set PARAM_PMODE to
    // PMODE_NORMAL, or PMODE_ALT_NORMAL.
    else
    {
        int32 PMode = PMODE_NORMAL;
        if (PV_OK != pl_set_param(g_hCam, PARAM_PMODE, (void*)&PMode))
        {
            PrintErrorMessage(pl_error_code(),
                    "pl_set_param(PARAM_PMODE) error");
            return PV_FAIL;
        }
    }

    // If this is a color sensor, get and show the bayer pattern
    if (PV_OK != pl_get_param(g_hCam, PARAM_COLOR_MODE, ATTR_CURRENT,
                (void*)&g_SensorColorMode))
    {
        PrintErrorMessage(pl_error_code(),
                "pl_get_param(PARAM_COLOR_MODE) error");
        return PV_FAIL;
    }

    printf("Camera sensor is ");
    switch(g_SensorColorMode){
    case COLOR_NONE:
        printf("Mono\n");
        break;
    case COLOR_RGGB:
        printf("Color with bayer RGGB\n");
        break;
    case COLOR_GRBG:
        printf("Color with bayer GRBG\n");
        break;
    case COLOR_GBRG:
        printf("Color with bayer GBRG\n");
        break;
    case COLOR_BGGR:
        printf("Color with bayer BGGR\n");
        break;

    // Consider any others as Mono = no color
    default:
        printf("unrecognized type\n");
        break;
    }
    printf("\n");

    // This code iterates through all available camera ports and their readout
    // speeds and creates a Speed Table which holds indexes of ports and speeds,
    // readout frequencies and bit depths.

    NVPC ports;
    if (!ReadEnumeration(&ports, PARAM_READOUT_PORT, "PARAM_READOUT_PORT"))
    {
        return PV_FAIL;
    }

    if (!IsParamAvailable(PARAM_SPDTAB_INDEX, "PARAM_SPDTAB_INDEX"))
    {
        return PV_FAIL;
    }
    if (!IsParamAvailable(PARAM_PIX_TIME, "PARAM_PIX_TIME"))
    {
        return PV_FAIL;
    }
    if (!IsParamAvailable(PARAM_BIT_DEPTH, "PARAM_BIT_DEPTH"))
    {
        return PV_FAIL;
    }

    // Iterate through available ports and their speeds
    for (size_t pi = 0; pi < ports.size(); pi++)
    {
        // Set readout port
        if (PV_OK != pl_set_param(g_hCam, PARAM_READOUT_PORT,
                    (void*)&ports[pi].value))
        {
            PrintErrorMessage(pl_error_code(),
                    "pl_set_param(PARAM_READOUT_PORT) error");
            return PV_FAIL;
        }

        // Get number of available speeds for this port
        uns32 speedCount;
        if (PV_OK != pl_get_param(g_hCam, PARAM_SPDTAB_INDEX, ATTR_COUNT,
                    (void*)&speedCount))
        {
            PrintErrorMessage(pl_error_code(),
                    "pl_get_param(PARAM_SPDTAB_INDEX) error");
            return PV_FAIL;
        }

        // Iterate through all the speeds
        for (int16 si = 0; si < (int16)speedCount; si++)
        {
            // Set camera to new speed index
            if (PV_OK != pl_set_param(g_hCam, PARAM_SPDTAB_INDEX, (void*)&si))
            {
                PrintErrorMessage(pl_error_code(),
                        "pl_set_param(g_hCam, PARAM_SPDTAB_INDEX) error");
                return PV_FAIL;
            }

            // Get pixel time (readout time of one pixel in nanoseconds) for the
            // current port/speed pair. This can be used to calculate readout
            // frequency of the port/speed pair.
            uns16 pixTime;
            if (PV_OK != pl_get_param(g_hCam, PARAM_PIX_TIME, ATTR_CURRENT,
                        (void*)&pixTime))
            {
                PrintErrorMessage(pl_error_code(),
                        "pl_get_param(g_hCam, PARAM_PIX_TIME) error");
                return PV_FAIL;
            }

            // Get bit depth of the current readout port/speed pair
            int16 bitDepth;
            if (PV_OK != pl_get_param(g_hCam, PARAM_BIT_DEPTH, ATTR_CURRENT,
                        (void*)&bitDepth))
            {
                PrintErrorMessage(pl_error_code(),
                        "pl_get_param(PARAM_BIT_DEPTH) error");
                return PV_FAIL;
            }

            int16 gainMin;
            if (PV_OK != pl_get_param(g_hCam, PARAM_GAIN_INDEX, ATTR_MIN,
                    (void*)&gainMin))
            {
                PrintErrorMessage(pl_error_code(),
                        "pl_get_param(PARAM_GAIN_INDEX) error");
                return PV_FAIL;
            }

            int16 gainMax;
            if (PV_OK != pl_get_param(g_hCam, PARAM_GAIN_INDEX, ATTR_MAX,
                    (void*)&gainMax))
            {
                PrintErrorMessage(pl_error_code(),
                        "pl_get_param(PARAM_GAIN_INDEX) error");
                return PV_FAIL;
            }

            int16 gainIncrement;
            if (PV_OK != pl_get_param(g_hCam, PARAM_GAIN_INDEX, ATTR_INCREMENT,
                    (void*)&gainIncrement))
            {
                PrintErrorMessage(pl_error_code(),
                        "pl_get_param(PARAM_GAIN_INDEX) error");
                return PV_FAIL;
            }

            // Save the port/speed information to our Speed Table
            READOUT_OPTION ro;
            ro.port = ports[pi];
            ro.speedIndex = si;
            ro.readoutFrequency = 1000 / (float)pixTime;
            ro.bitDepth = bitDepth;
            ro.gains.clear();

            int16 gainValue = gainMin;

            while (gainValue <= gainMax)
            {
                ro.gains.push_back(gainValue);
                gainValue += gainIncrement;
            }

            g_SpeedTable.push_back(ro);

            printf("g_SpeedTable[%lu].Port = %lu (%d - %s)\n",
                    (unsigned long)(g_SpeedTable.size() - 1),
                    (unsigned long)pi,
                    g_SpeedTable[g_SpeedTable.size() - 1].port.value,
                    g_SpeedTable[g_SpeedTable.size() - 1].port.name.c_str());
            printf("g_SpeedTable[%lu].SpeedIndex = %d\n",
                    (unsigned long)(g_SpeedTable.size() - 1),
                    g_SpeedTable[g_SpeedTable.size() - 1].speedIndex);
            printf("g_SpeedTable[%lu].PortReadoutFrequency = %.3f MHz\n",
                    (unsigned long)(g_SpeedTable.size() - 1),
                    g_SpeedTable[g_SpeedTable.size() - 1].readoutFrequency);
            printf("g_SpeedTable[%lu].bitDepth = %d bit\n",
                    (unsigned long)(g_SpeedTable.size() - 1),
                    g_SpeedTable[g_SpeedTable.size() - 1].bitDepth);
            for (int16 gi = 0; gi < (int16)ro.gains.size(); gi++)
            {
                printf("g_SpeedTable[%lu].gains[%d] = %d \n",
                        (unsigned long)(g_SpeedTable.size() - 1),
                        (int)gi,
                        ro.gains[gi]);
            }
            printf("\n");
        }
    }

    // Speed Table has been created

    // Set camera to first port
    if (PV_OK != pl_set_param(g_hCam, PARAM_READOUT_PORT,
                (void*)&g_SpeedTable[0].port.value))
    {
        PrintErrorMessage(pl_error_code(), "Readout port could not be set");
        return PV_FAIL;
    }
    printf("Setting readout port to %s\n", g_SpeedTable[0].port.name.c_str());

    // Set camera to speed 0
    if (PV_OK != pl_set_param(g_hCam, PARAM_SPDTAB_INDEX,
                (void*)&g_SpeedTable[0].speedIndex))
    {
        PrintErrorMessage(pl_error_code(), "Readout port could not be set");
        return PV_FAIL;
    }
    printf("Setting readout speed index to %d\n", g_SpeedTable[0].speedIndex);

    // Set gain index to one (the first one)
    if (PV_OK != pl_set_param(g_hCam, PARAM_GAIN_INDEX,
                (void*)&g_SpeedTable[0].gains[0]))
    {
        PrintErrorMessage(pl_error_code(), "Gain index could not be set");
        return PV_FAIL;
    }
    printf("Setting gain index to %d\n", g_SpeedTable[0].gains[0]);

    // Get the sensor bit depth
    g_SensorBitDepth = g_SpeedTable[0].bitDepth;
    printf("Sensor bit depth is %d bits\n", g_SensorBitDepth);
    printf("\n");

    // Get number of sensor columns
    if (!IsParamAvailable(PARAM_SER_SIZE, "PARAM_SER_SIZE"))
    {
        return PV_FAIL;
    }
    if (PV_OK != pl_get_param(g_hCam, PARAM_SER_SIZE, ATTR_CURRENT,
                (void*)&g_SensorResX))
    {
        PrintErrorMessage(pl_error_code(), "Couldn't read CCD X-resolution");
        return PV_FAIL;
    }
    // Get number of sensor lines
    if (!IsParamAvailable(PARAM_PAR_SIZE, "PARAM_PAR_SIZE"))
    {
        return PV_FAIL;
    }
    if (PV_OK != pl_get_param(g_hCam, PARAM_PAR_SIZE, ATTR_CURRENT,
                (void*)&g_SensorResY))
    {
        PrintErrorMessage(pl_error_code(), "Couldn't read CCD Y-resolution");
        return PV_FAIL;
    }
    printf("Sensor size: %dx%d\n", g_SensorResX, g_SensorResY);

    // Set number of sensor clear cycles to 2 (default)
    if (!IsParamAvailable(PARAM_CLEAR_CYCLES, "PARAM_CLEAR_CYCLES"))
    {
        return PV_FAIL;
    }
    uns16 ClearCycles = 2;
    if (PV_OK != pl_set_param(g_hCam, PARAM_CLEAR_CYCLES, (void*)&ClearCycles))
    {
        PrintErrorMessage(pl_error_code(),
                "pl_set_param(PARAM_CLEAR_CYCLES) error");
        return PV_FAIL;
    }

    printf("\n");

    return PV_OK;
}

rs_bool InitAndOpenFirstCamera()
{
    if (!InitPVCAM())
    {
        CloseCameraAndUninit();
        return PV_FAIL;
    }

    if (!OpenCamera())
    {
        CloseCameraAndUninit();
        return PV_FAIL;
    }

    // Create this structure that will be used to received extended information
    // about the frame.
    // Support on interfaces may vary, full support on Firewire at the moment,
    // partial support on PCIe LVDS and USB interfaces, no support on legacy
    // LVDS.
    // FRAME_INFO can be allocated on stack bu we demonstrate here how to do the
    // same on heap.
    if (PV_OK != pl_create_frame_info_struct(&g_pFrameInfo))
    {
        PrintErrorMessage(pl_error_code(),
                "pl_create_frame_info_struct() error");
        CloseCameraAndUninit();
        return PV_FAIL;
    }

    return PV_OK;
}

void CloseCameraAndUninit()
{
    printf("\nClosing the camera and uninitializing PVCAM...\n");

    // Do not close camera if none has been detected and open
    if (s_IsCameraOpen == TRUE)
    {
        if (PV_OK != pl_cam_close(g_hCam))
        {
            PrintErrorMessage(pl_error_code(), "pl_cam_close() error");
        }
        else
        {
            printf("Camera closed\n");
        }
    }

    // Uninitialize PVCAM library
    if (s_IsPvcamInitialized == TRUE)
    {
        if (PV_OK != pl_pvcam_uninit())
        {
            PrintErrorMessage(pl_error_code(), "pl_pvcam_uninit() error");
        }
        else
        {
            printf("PVCAM uninitialized\n");
        }
    }

    // Release frame info
    if (g_pFrameInfo != NULL)
    {
        if (PV_OK != pl_release_frame_info_struct(g_pFrameInfo))
        {
            PrintErrorMessage(pl_error_code(),
                    "pl_release_frame_info_struct() error");
        }
    }

    printf("\nPress Enter to exit");
    WaitForKey();
}

rs_bool IsParamAvailable(uns32 paramID, const char* paramName)
{
    if (paramName == NULL)
    {
        return PV_FAIL;
    }

    rs_bool isAvailable;
    if (PV_OK != pl_get_param(g_hCam, paramID, ATTR_AVAIL, (void*)&isAvailable))
    {
        printf("Error reading ATTR_AVAIL of %s\n", paramName);
        return PV_FAIL;
    }
    if (isAvailable == FALSE)
    {
        printf("Parameter %s is not available\n", paramName);
        return PV_FAIL;
    }

    return PV_OK;
}

rs_bool ReadEnumeration(NVPC* nvpc, uns32 paramID, const char* paramName)
{
    if (nvpc == NULL && paramName == NULL)
    {
        return PV_FAIL;
    }

    if (!IsParamAvailable(paramID, paramName))
    {
        return PV_FAIL;
    }

    uns32 count;
    if (PV_OK != pl_get_param(g_hCam, paramID, ATTR_COUNT, (void*)&count))
    {
        const std::string msg =
            "pl_get_param(" + std::string(paramName) + ") error";
        PrintErrorMessage(pl_error_code(), msg.c_str());
        return PV_FAIL;
    }

    // Actually get the enumeration names
    for (uns32 i = 0; i < count; ++i)
    {
        // Ask how long the string is
        uns32 strLength;
        if (PV_OK != pl_enum_str_length(g_hCam, paramID, i, &strLength))
        {
            const std::string msg =
                "pl_enum_str_length(" + std::string(paramName) + ") error";
            PrintErrorMessage(pl_error_code(), msg.c_str());
            return PV_FAIL;
        }

        // Allocate the destination string
        // Let app crash on exception if not enough memory
        char* name = new char[strLength];

        // Actually get the string and value
        int32 value;
        if (PV_OK != pl_get_enum_param(g_hCam, paramID, i, &value, name,
                    strLength))
        {
            const std::string msg =
                "pl_get_enum_param(" + std::string(paramName) + ") error";
            PrintErrorMessage(pl_error_code(), msg.c_str());
            delete [] name;
            return PV_FAIL;
        }

        NVP nvp;
        nvp.value = value;
        nvp.name = name;
        nvpc->push_back(nvp);

        delete [] name;
    }

    if (nvpc->empty())
    {
        return PV_FAIL;
    }
    
    return PV_OK;
}

rs_bool GrabFrame(uns32 expTime, rgn_type rgn, void* pFrame)
{
    if (pFrame == NULL)
    {
        return PV_FAIL;
    }

    uns32 expBytes;
    rs_bool retStatus = PV_OK;

    // Setup the acquisition
    // TIMED_MODE flag means acquisition will start with software trigger.
    if (PV_OK != pl_exp_setup_seq(g_hCam, 1, 1, &rgn, TIMED_MODE, expTime,
                &expBytes))
    {
        PrintErrorMessage(pl_error_code(), "pl_exp_setup_seq() error");
        return PV_FAIL;
    }

    // Start the acquisition
    if (PV_OK != pl_exp_start_seq(g_hCam, pFrame))
    {
        PrintErrorMessage(pl_error_code(), "pl_exp_start_seq() error");
        return PV_FAIL;
    }
    printf("GrabFrame>");

    int16 status;
    uns32 byte_cnt;
    // Keep checking the camera readout status
    // Function returns PV_FAIL if status is READOUT_FAILED
    // Waiting 20ms for frame exposure and readout
    while (pl_exp_check_status(g_hCam, &status, &byte_cnt)
            && status != READOUT_COMPLETE)
    {
        printf(".");
        std::this_thread::sleep_for(std::chrono::milliseconds(20));
    }

    // Display message on readout status
    if (status == READOUT_FAILED)
    {
        printf("Frame readout failed\n");
        retStatus = PV_FAIL;
    }
    else if (status == READOUT_COMPLETE)
    {
        printf("done\n");
    }
    else
    {
        printf("Frame readout unsuccessful with status=%d.\n", status);
        retStatus = PV_FAIL;
    }
    // When acquiring single frames with callback notifications call this
    // after each frame before new acquisition is started with pl_exp_start_seq
    if (PV_OK != pl_exp_finish_seq(g_hCam, pFrame, 0))
    {
        PrintErrorMessage(pl_error_code(), "pl_exp_finish_seq() error");
        return PV_FAIL;
    }

    return retStatus;
}

rs_bool SaveBinaryFile(const void* pFrame, uns32 len, const char* fileName)
{
    if (pFrame == NULL || fileName == NULL)
    {
        return PV_FAIL;
    }

    std::ofstream fh;
    const std::string name = std::string(fileName) + ".bin";

    fh.open(name, std::ios::out | std::ios::binary);
    if (!fh.is_open())
    {
        printf("Cannot open file '%s'\n", name.c_str());
        return PV_FAIL;
    }
    fh.write((const char*)pFrame, len);
    fh.close();

    printf("Saved bin file '%s'\n", name.c_str());

    return PV_OK;
}

rs_bool SavePpmFile(const void* pFrame, uns32 width, uns32 height, uns16 bpp,
        const char* fileName)
{
    if (pFrame == NULL || fileName == NULL)
    {
        return PV_FAIL;
    }

    uns16 maxVal;
    uns16 bytesPerPixel;

    switch (bpp)
    {
    case 24:
        maxVal = 255;
        bytesPerPixel = 3;
        break;
    case 48:
        maxVal = 65535;
        bytesPerPixel = 6;
        break;
    default:
        printf("Not a valid RGB bits per pixel value for PPM file: %u", bpp);
        return PV_FAIL;
        break;
    }

    std::ofstream fh;
    const std::string name = std::string(fileName) + ".ppm";

    fh.open(name, std::ios::out | std::ios::binary);
    if (!fh.is_open())
    {
        printf("Cannot open file '%s'\n", name.c_str());
        return PV_FAIL;
    }

    // Output PPM file header block, magic number "P6"
    fh << "P6" << std::endl;
    fh << "#PPM format, linear RGB data" << std::endl;
    fh << width << " " << height << std::endl;
    fh << maxVal << std::endl;

    // Output binary image data
    fh.write((const char*)pFrame, width * height * bytesPerPixel);
    fh.close();

    printf("Saved PPM file '%s'\n", name.c_str());
    return PV_OK;
}

rs_bool SavePamFile(const void* pFrame, uns32 width, uns32 height, uns16 bpp,
        const char* fileName)
{
    if (pFrame == NULL || fileName == NULL)
    {
        return PV_FAIL;
    }

    uns16 maxVal;
    uns16 bytesPerPixel;

    switch(bpp)
    {
    case 24:
        maxVal = 255;
        bytesPerPixel = 3;
        break;
    case 48:
        maxVal = 65535;
        bytesPerPixel = 6;
        break;
    default:
        printf("Not a valid RGB bits per pixel value for PAM file: %u", bpp);
        return PV_OK;
        break;
    }

    std::ofstream fh;
    const std::string name = std::string(fileName) + ".pam";

    fh.open(name, std::ios::out | std::ios::binary);
    if (!fh.is_open())
    {
        printf("Cannot open file '%s'\n", name.c_str());
        return PV_FAIL;
    }

    // Output PPM file header block, magic number "P7"
    fh << "P7" << std::endl;
    // ImageMagick does not like the comment
    //fh << "# PAM format, linear RGB data" << std::endl;
    fh << "WIDTH " << width << std::endl;
    fh << "HEIGHT " << height << std::endl;
    fh << "DEPTH " << "3" << std::endl;
    fh << "MAXVAL " << maxVal << std::endl;
    fh << "TUPLTYPE " << "RGB" << std::endl;
    fh << "ENDHDR" << std::endl;

    // Output binary image data
    fh.write((const char*)pFrame, width * height * bytesPerPixel);
    fh.close();

    printf("Saved PAM file '%s'\n", name.c_str());

    return PV_OK;
}

rs_bool SwapEndianness(void* pFrame, uns32 len)
{
    if (pFrame == NULL || len % 2 != 0)
    {
        return PV_FAIL;
    }

    const uns32 pixelCount = len / 2;
    uns16* pixels = (uns16*)pFrame;

    for (uns32 n = 0; n < pixelCount; n++)
    {
        pixels[n] = (pixels[n] >> 8) | ((pixels[n] & 0xFF) << 8);
    }

    return PV_OK;
}

var NAVTREE =
[
  [ "PVCAM", "index.xhtml", [
    [ "About SDK", "index.xhtml", [
      [ "Contact information", "index.xhtml#secContact", null ]
    ] ],
    [ "Introduction", "_introduction.xhtml", [
      [ "System Overview", "_introduction.xhtml#secSystemOverview", null ],
      [ "Hardware Support", "_introduction.xhtml#secHardwareSupport", null ],
      [ "Include Files", "_introduction.xhtml#secIncFiles", null ]
    ] ],
    [ "PVCAM API", "_pvcam_api.xhtml", "_pvcam_api" ],
    [ "Code Example Guides", "_example_guides.xhtml", "_example_guides" ],
    [ "Modules", "modules.xhtml", "modules" ],
    [ "Data Structures", "annotated.xhtml", [
      [ "Data Structures", "annotated.xhtml", "annotated_dup" ],
      [ "Data Structure Index", "classes.xhtml", null ],
      [ "Data Fields", "functions.xhtml", [
        [ "All", "functions.xhtml", null ],
        [ "Variables", "functions_vars.xhtml", null ]
      ] ]
    ] ],
    [ "Files", null, [
      [ "File List", "files.xhtml", "files" ],
      [ "Globals", "globals.xhtml", [
        [ "All", "globals.xhtml", "globals_dup" ],
        [ "Functions", "globals_func.xhtml", null ],
        [ "Typedefs", "globals_type.xhtml", null ],
        [ "Enumerations", "globals_enum.xhtml", null ],
        [ "Enumerator", "globals_eval.xhtml", "globals_eval" ],
        [ "Macros", "globals_defs.xhtml", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"_api_acquisition.xhtml",
"group__grp__pm__deprecated__functions.xhtml#ga6260285944d19b38624573280d410577",
"group__grp__pm__functions.xhtml#ga424b470985bb7821ba817fb9bd98af86",
"group__grp__pm__parameters.xhtml#gadfdc632fc186cbda380dff97bbd3a20f",
"pvcam_8h.xhtml#a659a53e2c86aa65782a44cf74b1b54fba73f8ebaf777b185eb7f163d6ed723e0c",
"structio__list.xhtml#abecf838bb4792b79c384a9ee76796a13"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';
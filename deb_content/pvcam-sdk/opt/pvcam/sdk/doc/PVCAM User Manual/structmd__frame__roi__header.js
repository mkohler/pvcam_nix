var structmd__frame__roi__header =
[
    [ "_reserved", "structmd__frame__roi__header.xhtml#af8ef98dbf4deca49a10636593609b3f8", null ],
    [ "extendedMdSize", "structmd__frame__roi__header.xhtml#a19d5b9e61a0f39ad2149967a0f5ec6d9", null ],
    [ "flags", "structmd__frame__roi__header.xhtml#a57cef87bb42da1252861ef685a78fbf8", null ],
    [ "roi", "structmd__frame__roi__header.xhtml#a9a3339d5f7ad7e328c574f7630990c21", null ],
    [ "roiDataSize", "structmd__frame__roi__header.xhtml#a7e4fc32752d1679699eaf4c7847ee06c", null ],
    [ "roiNr", "structmd__frame__roi__header.xhtml#abc6c000f4b433a77b938c5f3d7dd959a", null ],
    [ "timestampBOR", "structmd__frame__roi__header.xhtml#aa80464dcc18c4856f018c779ff227f7b", null ],
    [ "timestampEOR", "structmd__frame__roi__header.xhtml#a7df363e50c8ce1f705919cdcd3b256a1", null ]
];
var _api_misc =
[
    [ "sCMOS Readout Modes", "_s_c_m_o_s__readout_modes.xhtml", null ],
    [ "CCD Frame Transfer", "_c_c_d__frame_transfer.xhtml", null ],
    [ "CCD Image Smear", "_c_c_d__image_smear.xhtml", null ],
    [ "CCD Shutter Open & Close Delay", "_c_c_d__shtr_open_close_delay.xhtml", null ]
];
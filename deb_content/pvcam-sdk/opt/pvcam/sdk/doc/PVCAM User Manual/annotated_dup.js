var annotated_dup =
[
    [ "_TAG_FRAME_INFO", "struct___t_a_g___f_r_a_m_e___i_n_f_o.xhtml", "struct___t_a_g___f_r_a_m_e___i_n_f_o" ],
    [ "_TAG_PVCAM_FRAME_INFO_GUID", "struct___t_a_g___p_v_c_a_m___f_r_a_m_e___i_n_f_o___g_u_i_d.xhtml", "struct___t_a_g___p_v_c_a_m___f_r_a_m_e___i_n_f_o___g_u_i_d" ],
    [ "active_camera_type", "structactive__camera__type.xhtml", "structactive__camera__type" ],
    [ "io_list", "structio__list.xhtml", "structio__list" ],
    [ "io_struct", "structio__struct.xhtml", "structio__struct" ],
    [ "md_ext_item", "structmd__ext__item.xhtml", "structmd__ext__item" ],
    [ "md_ext_item_collection", "structmd__ext__item__collection.xhtml", "structmd__ext__item__collection" ],
    [ "md_ext_item_info", "structmd__ext__item__info.xhtml", "structmd__ext__item__info" ],
    [ "md_frame", "structmd__frame.xhtml", "structmd__frame" ],
    [ "md_frame_header", "structmd__frame__header.xhtml", "structmd__frame__header" ],
    [ "md_frame_roi", "structmd__frame__roi.xhtml", "structmd__frame__roi" ],
    [ "md_frame_roi_header", "structmd__frame__roi__header.xhtml", "structmd__frame__roi__header" ],
    [ "rgn_type", "structrgn__type.xhtml", "structrgn__type" ],
    [ "smart_stream_type", "structsmart__stream__type.xhtml", "structsmart__stream__type" ]
];
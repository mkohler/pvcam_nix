var searchData=
[
  ['readouttime',['ReadoutTime',['../struct___t_a_g___f_r_a_m_e___i_n_f_o.xhtml#ab0053b93cbe6693b723adbb9e5e9178c',1,'_TAG_FRAME_INFO']]],
  ['roi',['roi',['../structmd__frame__roi__header.xhtml#a9a3339d5f7ad7e328c574f7630990c21',1,'md_frame_roi_header']]],
  ['roiarray',['roiArray',['../structmd__frame.xhtml#a132a631a714105e8feb00b632e3f393d',1,'md_frame']]],
  ['roicapacity',['roiCapacity',['../structmd__frame.xhtml#a695286e05e929a7c9e0526c170066ecf',1,'md_frame']]],
  ['roicount',['roiCount',['../structmd__frame__header.xhtml#a49c843485fdb60496082bc68af1ec4a7',1,'md_frame_header::roiCount()'],['../structmd__frame.xhtml#a9deeed7193502e6be0a85c733c880da5',1,'md_frame::roiCount()']]],
  ['roidatasize',['roiDataSize',['../structmd__frame__roi__header.xhtml#a7e4fc32752d1679699eaf4c7847ee06c',1,'md_frame_roi_header']]],
  ['roinr',['roiNr',['../structmd__frame__roi__header.xhtml#abc6c000f4b433a77b938c5f3d7dd959a',1,'md_frame_roi_header']]],
  ['roitimestampresns',['roiTimestampResNs',['../structmd__frame__header.xhtml#aff3160d01d7cd7663de4a16b165ab29e',1,'md_frame_header']]],
  ['rows',['rows',['../structactive__camera__type.xhtml#aa0d3167eb508020f0957dfc559347781',1,'active_camera_type']]]
];

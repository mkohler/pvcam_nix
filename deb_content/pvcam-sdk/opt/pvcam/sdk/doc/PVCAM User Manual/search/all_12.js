var searchData=
[
  ['readout_5fcomplete',['READOUT_COMPLETE',['../pvcam_8h.xhtml#af730d1ebe1da3e281a2bdf9cafc793a9aa3f272a6472281aa5cf1fcc82a3fba24',1,'pvcam.h']]],
  ['readout_5ffailed',['READOUT_FAILED',['../pvcam_8h.xhtml#af730d1ebe1da3e281a2bdf9cafc793a9ab0fa7e7b978de71285affda7872af1f0',1,'pvcam.h']]],
  ['readout_5fin_5fprogress',['READOUT_IN_PROGRESS',['../pvcam_8h.xhtml#af730d1ebe1da3e281a2bdf9cafc793a9ab00c17c38928c5d516c4ef1bb30ad46e',1,'pvcam.h']]],
  ['readout_5fnot_5factive',['READOUT_NOT_ACTIVE',['../pvcam_8h.xhtml#af730d1ebe1da3e281a2bdf9cafc793a9abbebd491ad8772b0e030f658b01436c5',1,'pvcam.h']]],
  ['readout_5fport_5f0',['READOUT_PORT_0',['../pvcam_8h.xhtml#a784c27265eea03b9ff4cb322786e2002a78487260d95b02aea82fcd908f8f977f',1,'pvcam.h']]],
  ['readout_5fport_5f1',['READOUT_PORT_1',['../pvcam_8h.xhtml#a784c27265eea03b9ff4cb322786e2002add9e90de7db5c57c0ce67d1d1d7ed226',1,'pvcam.h']]],
  ['readouttime',['ReadoutTime',['../struct___t_a_g___f_r_a_m_e___i_n_f_o.xhtml#ab0053b93cbe6693b723adbb9e5e9178c',1,'_TAG_FRAME_INFO']]],
  ['rgn_5fconst_5fptr',['rgn_const_ptr',['../group__grp__pm__deprecated__typedefs.xhtml#ga96c9b63f9061f4b62f9c7e1699b58781',1,'pvcam.h']]],
  ['rgn_5fptr',['rgn_ptr',['../group__grp__pm__deprecated__typedefs.xhtml#gaea5e112993dbc55150685769100dd03b',1,'pvcam.h']]],
  ['rgn_5ftype',['rgn_type',['../structrgn__type.xhtml',1,'rgn_type'],['../pvcam_8h.xhtml#adbce612eec591a6652c76acbbb790f58',1,'rgn_type():&#160;pvcam.h']]],
  ['roi',['roi',['../structmd__frame__roi__header.xhtml#a9a3339d5f7ad7e328c574f7630990c21',1,'md_frame_roi_header']]],
  ['roiarray',['roiArray',['../structmd__frame.xhtml#a132a631a714105e8feb00b632e3f393d',1,'md_frame']]],
  ['roicapacity',['roiCapacity',['../structmd__frame.xhtml#a695286e05e929a7c9e0526c170066ecf',1,'md_frame']]],
  ['roicount',['roiCount',['../structmd__frame__header.xhtml#a49c843485fdb60496082bc68af1ec4a7',1,'md_frame_header::roiCount()'],['../structmd__frame.xhtml#a9deeed7193502e6be0a85c733c880da5',1,'md_frame::roiCount()']]],
  ['roidatasize',['roiDataSize',['../structmd__frame__roi__header.xhtml#a7e4fc32752d1679699eaf4c7847ee06c',1,'md_frame_roi_header']]],
  ['roinr',['roiNr',['../structmd__frame__roi__header.xhtml#abc6c000f4b433a77b938c5f3d7dd959a',1,'md_frame_roi_header']]],
  ['roitimestampresns',['roiTimestampResNs',['../structmd__frame__header.xhtml#aff3160d01d7cd7663de4a16b165ab29e',1,'md_frame_header']]],
  ['rows',['rows',['../structactive__camera__type.xhtml#aa0d3167eb508020f0957dfc559347781',1,'active_camera_type']]],
  ['rs_5fbool',['rs_bool',['../master_8h.xhtml#a2662757d0c37032b0bf48fc086824e88',1,'master.h']]],
  ['rs_5fbool_5fconst_5fptr',['rs_bool_const_ptr',['../group__grp__pm__deprecated__typedefs.xhtml#ga1ada5e5a7aeb82038f628131322ef27e',1,'master.h']]],
  ['rs_5fbool_5fptr',['rs_bool_ptr',['../group__grp__pm__deprecated__typedefs.xhtml#ga835f4ff0851aaf074abf7891d3082895',1,'master.h']]]
];

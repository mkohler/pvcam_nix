var searchData=
[
  ['data',['data',['../structmd__frame__roi.xhtml#a2b49c9f39f0c87e1cd2d7bd4c1fa848d',1,'md_frame_roi']]],
  ['datasize',['dataSize',['../structmd__frame__roi.xhtml#aa08f31346b41cac292e6a07f30b2fdb1',1,'md_frame_roi']]],
  ['do_5fclear',['do_clear',['../structactive__camera__type.xhtml#a7e7e531b10faaf65c149b30c1bf3935b',1,'active_camera_type']]],
  ['deprecated_20pvcam_20symbols',['Deprecated PVCAM symbols',['../group__grp__pm__deprecated.xhtml',1,'']]],
  ['deprecated_20pvcam_20functions',['Deprecated PVCAM functions',['../group__grp__pm__deprecated__functions.xhtml',1,'']]],
  ['deprecated_20pvcam_20types',['Deprecated PVCAM types',['../group__grp__pm__deprecated__typedefs.xhtml',1,'']]]
];

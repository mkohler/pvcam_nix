var indexSectionsWithContent =
{
  0: "0123_abcdefhilmnoprstuv",
  1: "_aimrs",
  2: "0123abcempst",
  3: "p",
  4: "_abcdefhilmnoprstv",
  5: "acfilmprsuv",
  6: "p",
  7: "abcefimnoprstv",
  8: "cefmpt",
  9: "cdps",
  10: "abcefilmpst"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "defines",
  9: "groups",
  10: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Macros",
  9: "Modules",
  10: "Pages"
};


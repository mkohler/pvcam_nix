var structmd__frame =
[
    [ "extMdData", "structmd__frame.xhtml#a93d088b5dcf00c262e1ddd626304bfbf", null ],
    [ "extMdDataSize", "structmd__frame.xhtml#a84a0b3a2b19cdf030e26c8c87aa5a690", null ],
    [ "header", "structmd__frame.xhtml#a9640a6ecdd8ae5ba866fa645ec046c25", null ],
    [ "impliedRoi", "structmd__frame.xhtml#a19a0eff712a4ab718bc9044d2dc0983c", null ],
    [ "roiArray", "structmd__frame.xhtml#a132a631a714105e8feb00b632e3f393d", null ],
    [ "roiCapacity", "structmd__frame.xhtml#a695286e05e929a7c9e0526c170066ecf", null ],
    [ "roiCount", "structmd__frame.xhtml#a9deeed7193502e6be0a85c733c880da5", null ]
];
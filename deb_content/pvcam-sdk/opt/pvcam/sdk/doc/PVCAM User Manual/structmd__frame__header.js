var structmd__frame__header =
[
    [ "_reserved", "structmd__frame__header.xhtml#acc4629b6fd614607fbfe42a9718a5a22", null ],
    [ "bitDepth", "structmd__frame__header.xhtml#a5ed73673289db0ebb51852ef59edf435", null ],
    [ "colorMask", "structmd__frame__header.xhtml#a02caddb06617c202b5a897451d2140af", null ],
    [ "exposureTime", "structmd__frame__header.xhtml#ad91ce398ed75e5c6cd1ef85cc4b7f675", null ],
    [ "exposureTimeResNs", "structmd__frame__header.xhtml#aef15647e9bd575ec89a2606e77fea7fc", null ],
    [ "extendedMdSize", "structmd__frame__header.xhtml#a0299252362853560ae914a4d0cd03bde", null ],
    [ "flags", "structmd__frame__header.xhtml#af455bde72bb4e399a26094ebdac43f93", null ],
    [ "frameNr", "structmd__frame__header.xhtml#a3abb6f589d6db87bd5129a27afff9420", null ],
    [ "imageCompression", "structmd__frame__header.xhtml#a17e8d2ebab2fb11bb081e27123011e39", null ],
    [ "imageFormat", "structmd__frame__header.xhtml#acf43f0d7734cf7f26026ca1eb261aea8", null ],
    [ "roiCount", "structmd__frame__header.xhtml#a49c843485fdb60496082bc68af1ec4a7", null ],
    [ "roiTimestampResNs", "structmd__frame__header.xhtml#aff3160d01d7cd7663de4a16b165ab29e", null ],
    [ "signature", "structmd__frame__header.xhtml#affa929b1581ee6efaa3b60596bf19f29", null ],
    [ "timestampBOF", "structmd__frame__header.xhtml#ad0857f6803354889efde950d2195a73a", null ],
    [ "timestampEOF", "structmd__frame__header.xhtml#aaa30d452c9203d389da558aadf823e9d", null ],
    [ "timestampResNs", "structmd__frame__header.xhtml#a18ea44c8405283c3906dad07a8595758", null ],
    [ "version", "structmd__frame__header.xhtml#a008e223267a2578ba126bfd0bfc8b9f3", null ]
];
var _api_advanced =
[
    [ "Port and Speed Choices", "_speed_table.xhtml", null ],
    [ "SMART Streaming", "_smart_streaming.xhtml", [
      [ "Data Types", "_smart_streaming.xhtml#secSmartDataTypes", null ],
      [ "Possible Scenarios", "_smart_streaming.xhtml#secSmartScenarios", null ]
    ] ],
    [ "Embedded Frame Metadata", "_embedded_frame_metadata.xhtml", null ],
    [ "Centroids", "_centroids.xhtml", null ],
    [ "Binning Factors Discovery", "_binning_discovery.xhtml", null ],
    [ "Triggering Table", "_triggering_table.xhtml", null ],
    [ "Post-Processing", "_post_processing.xhtml", [
      [ "Involved Parameters", "_post_processing.xhtml#secPpParams", null ],
      [ "Initial Discovery", "_post_processing.xhtml#secPpDiscovery", null ],
      [ "Using Unique IDs", "_post_processing.xhtml#secPpDirectAccess", null ]
    ] ],
    [ "Scan Mode", "_scan_mode.xhtml", null ]
];
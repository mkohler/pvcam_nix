var _api_acquisition =
[
    [ "Involved Functions", "_api_acquisition.xhtml#secApiAcqFns", null ],
    [ "Acquisition Configuration", "_api_acquisition.xhtml#secApiAcqConfiguration", null ],
    [ "Frame Count Limit", "_api_acquisition.xhtml#secApiAcqFrameCount", null ],
    [ "Sequence Acquisition", "_api_acquisition.xhtml#secApiAcqSequence", null ],
    [ "Continuous Acquisition", "_api_acquisition.xhtml#secApiAcqContinuous", null ],
    [ "Multiple ROI Acquisition", "_multi_roi_acq.xhtml", null ],
    [ "Exposure Modes", "_exposure_modes.xhtml", [
      [ "Legacy Exposure Modes", "_exposure_modes.xhtml#secTriggerModes", [
        [ "Timed Mode", "_exposure_modes.xhtml#ssecTrigTimed", null ],
        [ "Trigger-First Mode", "_exposure_modes.xhtml#ssecTrigTrigFirst", null ],
        [ "Strobed Mode", "_exposure_modes.xhtml#ssecTrigStrobed", null ],
        [ "Bulb Mode", "_exposure_modes.xhtml#ssecTrigBulb", null ],
        [ "Variable-Timed Mode", "_exposure_modes.xhtml#ssecTrigVTM", null ]
      ] ],
      [ "Extended Exposure Modes", "_exposure_modes.xhtml#secExtTriggerModes", [
        [ "Internal Trigger", "_exposure_modes.xhtml#ssecExtTrigInternal", null ],
        [ "Trigger-First", "_exposure_modes.xhtml#ssecExtTrigTrigFirst", null ],
        [ "Edge Trigger", "_exposure_modes.xhtml#ssecExtTrigEdge", null ],
        [ "Level Trigger", "_exposure_modes.xhtml#ssecExtTrigLevel", null ]
      ] ],
      [ "Expose Out Modes", "_exposure_modes.xhtml#secExpOutModes", [
        [ "First Row", "_exposure_modes.xhtml#ssecExpOutFirstRow", null ],
        [ "Any Row", "_exposure_modes.xhtml#ssecExpOutAnyRow", null ],
        [ "All Rows", "_exposure_modes.xhtml#ssecExpOutAllRows", null ],
        [ "Rolling Shutter", "_exposure_modes.xhtml#ssecExpOutRollingShutter", null ],
        [ "Line Trigger", "_exposure_modes.xhtml#ssecExpOutLineTrigger", null ]
      ] ],
      [ "Best Practice Example", "_exposure_modes.xhtml#secTrigExample", null ]
    ] ],
    [ "Polling versus Callbacks", "_polling_vs_callbacks.xhtml", null ],
    [ "Exposure Loops", "_exposure_loops.xhtml", null ],
    [ "Clear Modes", "_clear_modes.xhtml", [
      [ "Sensor Clearing", "_clear_modes.xhtml#secClrSensorClearing", null ],
      [ "Clearing Mode Selection", "_clear_modes.xhtml#secClrModeSelection", null ]
    ] ]
];
var group__grp__single__byte__macros =
[
    [ "LS16_BYTE", "group__grp__single__byte__macros.xhtml#gaf669765bc309e5835a80336b84684e71", null ],
    [ "LS32_BYTE", "group__grp__single__byte__macros.xhtml#ga1ba1d71f9308f5cdc46d75dfe29f3089", null ],
    [ "MH32_BYTE", "group__grp__single__byte__macros.xhtml#ga10b01809a257fd97e10a673b06e0f953", null ],
    [ "ML32_BYTE", "group__grp__single__byte__macros.xhtml#gacb56418ed0bea25d279b8eb123503a33", null ],
    [ "MS16_BYTE", "group__grp__single__byte__macros.xhtml#ga6d56969ca07a65d2a6dd1455f79beaac", null ],
    [ "MS32_BYTE", "group__grp__single__byte__macros.xhtml#gaeb454327cef1981eeb46e6997dbab081", null ],
    [ "VAL_INT64", "group__grp__single__byte__macros.xhtml#ga3cbcfb0653b39381064d719addecc34b", null ],
    [ "VAL_UNS16", "group__grp__single__byte__macros.xhtml#ga6541690a9a09cf8c57ec8dfe411953fb", null ],
    [ "VAL_UNS32", "group__grp__single__byte__macros.xhtml#ga3d72f444ea113e9951db08bf5258d159", null ]
];
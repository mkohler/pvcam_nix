var modules =
[
    [ "Deprecated PVCAM symbols", "group__grp__pm__deprecated.xhtml", "group__grp__pm__deprecated" ],
    [ "Single-byte macros", "group__grp__single__byte__macros.xhtml", "group__grp__single__byte__macros" ],
    [ "PVCAM parameters", "group__grp__pm__parameters.xhtml", "group__grp__pm__parameters" ],
    [ "Callbacks", "group__grp__callbacks.xhtml", "group__grp__callbacks" ],
    [ "Public API functions", "group__grp__pm__functions.xhtml", "group__grp__pm__functions" ]
];
var _pvcam_api =
[
    [ "Basics and Terminology", "_api_basics.xhtml", [
      [ "Defined Data Types", "_api_basics.xhtml#secApiDataTypes", null ],
      [ "Function Arguments Passing", "_api_basics.xhtml#secApiFuncArgPassing", null ],
      [ "Image Buffers", "_api_basics.xhtml#secApiImageBuffers", null ],
      [ "Coordinates Model", "_api_basics.xhtml#secApiCoordinatesModel", [
        [ "Regions and Images", "_api_basics.xhtml#ssecApiRegionsAndImages", null ],
        [ "Binning Factors", "_api_basics.xhtml#ssecApiBinning", null ],
        [ "Data Array", "_api_basics.xhtml#ssecApiDataArray", null ],
        [ "Display Orientation", "_api_basics.xhtml#ssecApiDisplayOrientation", null ]
      ] ]
    ] ],
    [ "Error Handling", "_api_errors.xhtml", [
      [ "Involved Functions", "_api_errors.xhtml#secApiErrFns", null ],
      [ "Multiple threads", "_api_errors.xhtml#secApiErrThreads", null ]
    ] ],
    [ "Init and Open Functions", "_api_camera_enumeration.xhtml", [
      [ "Involved Functions", "_api_camera_enumeration.xhtml#secApiListFns", null ],
      [ "Initialization", "_api_camera_enumeration.xhtml#secApiListInit", null ],
      [ "Camera Enumeration", "_api_camera_enumeration.xhtml#secApiListEnum", null ],
      [ "Open & Close", "_api_camera_enumeration.xhtml#secApiListOpen", null ]
    ] ],
    [ "Parameter Access Functions", "_api_parameters.xhtml", [
      [ "Involved Functions", "_api_parameters.xhtml#secApiParamFns", null ],
      [ "Parameter Attributes", "_api_parameters.xhtml#secApiParamAttrs", null ],
      [ "Reading Parameters", "_api_parameters.xhtml#secApiParamGet", null ],
      [ "Setting Parameters", "_api_parameters.xhtml#secApiParamSet", null ],
      [ "Enumeration Parameters", "_api_parameters.xhtml#secApiParamEnum", null ]
    ] ],
    [ "Image Acquisition Functions", "_api_acquisition.xhtml", "_api_acquisition" ],
    [ "Advanced Features", "_api_advanced.xhtml", "_api_advanced" ],
    [ "Miscellaneous", "_api_misc.xhtml", "_api_misc" ]
];
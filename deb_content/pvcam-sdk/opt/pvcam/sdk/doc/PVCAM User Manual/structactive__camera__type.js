var structactive__camera__type =
[
    [ "alt_mode", "structactive__camera__type.xhtml#a0a1417b475023c4af52b172441355d1e", null ],
    [ "clear_count", "structactive__camera__type.xhtml#a53fd45a83acd470a4345e440d3e0c588", null ],
    [ "cols", "structactive__camera__type.xhtml#a0143c64d72e82ef6f468d2883768584f", null ],
    [ "do_clear", "structactive__camera__type.xhtml#a7e7e531b10faaf65c149b30c1bf3935b", null ],
    [ "exp_res", "structactive__camera__type.xhtml#a37363c14ab576a7c73587d37664a243c", null ],
    [ "frame_selectable", "structactive__camera__type.xhtml#a3e48935b1c6c2cc2bc8c8eccd5792dc5", null ],
    [ "frame_transfer", "structactive__camera__type.xhtml#ab81cf73e740963cd0ddbcd3a690827bb", null ],
    [ "io_hdr", "structactive__camera__type.xhtml#adb4f22a08b49715c4651c64537bd01e4", null ],
    [ "mpp_mode", "structactive__camera__type.xhtml#aac816536c675e305679ea93989b8142b", null ],
    [ "mpp_selectable", "structactive__camera__type.xhtml#aad80720a2282bbcde09b396353220080", null ],
    [ "open_shutter", "structactive__camera__type.xhtml#a63bf132629816e81cbebe75b384a8341", null ],
    [ "postmask", "structactive__camera__type.xhtml#a8134cca7d81f30bca8bbdb4bdff16187", null ],
    [ "postscan", "structactive__camera__type.xhtml#ae02ea7bf5f5c5ceb5d5082ea6b10743d", null ],
    [ "preamp_delay", "structactive__camera__type.xhtml#a1d9b1de3efceda277eb3ac3feeac35fa", null ],
    [ "preflash", "structactive__camera__type.xhtml#a5e950e062184c441808d11b215c1bd54", null ],
    [ "premask", "structactive__camera__type.xhtml#a6b9fba312dbd4160a8a6bdd314d1a0fc", null ],
    [ "prescan", "structactive__camera__type.xhtml#a960026b86faa0dd4a3c579ac3a6b40cf", null ],
    [ "rows", "structactive__camera__type.xhtml#aa0d3167eb508020f0957dfc559347781", null ],
    [ "shutter_close_delay", "structactive__camera__type.xhtml#a47a8bd70802fb1cfc935eecdc37c92c6", null ],
    [ "shutter_open_delay", "structactive__camera__type.xhtml#aedb846445beef21253294c7c6ce1e405", null ]
];
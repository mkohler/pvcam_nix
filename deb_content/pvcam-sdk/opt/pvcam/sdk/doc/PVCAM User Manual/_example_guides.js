var _example_guides =
[
    [ "Common Data Types", "_ex__common_types.xhtml", [
      [ "CameraContext Structure", "_ex__common_types.xhtml#Ex_CameraContext_Struct", null ],
      [ "Speed Table Structures", "_ex__common_types.xhtml#Ex_SpeedTable_Structs", null ],
      [ "NVPC Structure", "_ex__common_types.xhtml#Ex_NVPC_Struct", null ],
      [ "Post-Processing Structures", "_ex__common_types.xhtml#Ex_PpStructs", null ]
    ] ],
    [ "Common Functions", "_ex__common_functions.xhtml", [
      [ "Getting Error Message", "_ex__common_functions.xhtml#secExErrors", null ],
      [ "Initializing PVCAM and Opening Camera", "_ex__common_functions.xhtml#secExInit", [
        [ "Initialize PVCAM Library", "_ex__common_functions.xhtml#ssecExInit", null ],
        [ "Scan for Cameras", "_ex__common_functions.xhtml#ssecExCamNum", null ],
        [ "Create CameraContext for each camera", "_ex__common_functions.xhtml#ssecExCamNames", null ],
        [ "Open the Camera", "_ex__common_functions.xhtml#ssecExCamOpen", null ]
      ] ],
      [ "Closing Camera and Uninitializing PVCAM", "_ex__common_functions.xhtml#secExUninit", null ],
      [ "Terminating the Application", "_ex__common_functions.xhtml#secExAbortCliApp", null ],
      [ "Working With Parameters", "_ex__common_functions.xhtml#secExParameters", null ],
      [ "Frame Callback Handler", "_ex__common_functions.xhtml#secExCallbackHandler", null ],
      [ "Interaction with User", "_ex__common_functions.xhtml#secExUserInput", null ],
      [ "Data Presentation", "_ex__common_functions.xhtml#secExUserOutput", null ],
      [ "Post-Processing Helpers", "_ex__common_functions.xhtml#secExPostProcessing", null ],
      [ "Other Helpers", "_ex__common_functions.xhtml#secExOthers", null ]
    ] ],
    [ "Single Image Callbacks", "_ex__single_image__callbacks.xhtml", null ],
    [ "Single Image Polling", "_ex__single_image__polling.xhtml", null ],
    [ "Image Sequence", "_ex__image_sequence.xhtml", null ],
    [ "Live Image Callbacks", "_ex__live_image__callbacks.xhtml", null ],
    [ "Live Image Polling", "_ex__live_image__polling.xhtml", null ],
    [ "Live Image Triggering", "_ex__live_image__triggering.xhtml", null ],
    [ "Live Image SMART Streaming", "_ex__live_image__smart_streaming.xhtml", null ],
    [ "Live Image Change Exposure", "_ex__live_image__change_exposure.xhtml", null ],
    [ "Extended Enumerations", "_ex__extended_enumerations.xhtml", null ],
    [ "Extended Binning Factors", "_ex__extended_binning_factors.xhtml", null ],
    [ "Fan Speed and Temperature", "_ex__fan_speed_and_temperature.xhtml", null ],
    [ "Post-Processing Parameters", "_ex__post_processing_parameters.xhtml", null ],
    [ "Post-Processing Programmatically", "_ex__post_processing_programmatically.xhtml", null ],
    [ "Multiple Regions", "_ex__multiple_regions.xhtml", null ],
    [ "Centroids", "_ex__centroids.xhtml", null ],
    [ "Programmable Scan Mode", "_ex__programmable_scan_mode.xhtml", null ],
    [ "Single Image on Multiple Cameras", "_ex__single_image__multi_cam.xhtml", null ],
    [ "Live Image on Multiple Cameras", "_ex__live_image__multi_cam.xhtml", null ]
];